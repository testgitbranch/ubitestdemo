//
//  SecondViewController.m
//  UBITestDemo
//
//  Created by 金鑫 on 2021/12/15.
//

#import "FirstViewController.h"
#import "SecondViewController.h"
#import "UBIDemoConst.h"

@interface SecondViewController ()
@property (nonatomic, strong) UILabel *locationLab;
@property (nonatomic, strong) UIButton *authGuideBtn;
@property (nonatomic, strong) UIButton *authSettingBtn;
@property (nonatomic, strong) UIButton *openAutoBtn;
@property (nonatomic, strong) UIButton *closeAutoBtn;
@property (nonatomic, strong) UIButton *startBtn;
@property (nonatomic, strong) UIButton *stopBtn;
@property (nonatomic, strong) UIButton *showBtn;
@property (nonatomic, strong) UIButton *logoutBtn;

@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
}

- (void)setup {
    [self setupUI];
    [self setupLayout];
    [self setupAppearce];
    [self setupNotifacations];
}

- (void)setupUI {
    [self.view addSubview:self.locationLab];
    [self.view addSubview:self.authGuideBtn];
    [self.view addSubview:self.authSettingBtn];
    [self.view addSubview:self.openAutoBtn];
    [self.view addSubview:self.closeAutoBtn];
    [self.view addSubview:self.startBtn];
    [self.view addSubview:self.stopBtn];
    [self.view addSubview:self.showBtn];
    [self.view addSubview:self.logoutBtn];
}

- (void)setupLayout {
    [self.showBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.showBtn.superview).offset(kBtnMargin);
        make.bottom.equalTo(self.showBtn.superview).offset(-150);
        make.width.mas_equalTo(kBtnWidth);
        make.height.mas_equalTo(kBtnHeight);
    }];
    
    [self.logoutBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.logoutBtn.superview).offset(-kBtnMargin);
        make.centerY.equalTo(self.showBtn);
        make.width.mas_equalTo(kBtnWidth);
        make.height.mas_equalTo(kBtnHeight);
    }];
    
    [self.startBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.showBtn);
        make.bottom.equalTo(self.showBtn.mas_top).offset(-kBtnLineSpace);
        make.width.mas_equalTo(kBtnWidth);
        make.height.mas_equalTo(kBtnHeight);
    }];
    
    [self.stopBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.logoutBtn);
        make.centerY.equalTo(self.startBtn);
        make.width.mas_equalTo(kBtnWidth);
        make.height.mas_equalTo(kBtnHeight);
    }];
    
    [self.openAutoBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.startBtn);
        make.bottom.equalTo(self.startBtn.mas_top).offset(-kBtnLineSpace);
        make.width.mas_equalTo(kBtnWidth);
        make.height.mas_equalTo(kBtnHeight);
    }];
    
    [self.closeAutoBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.stopBtn);
        make.centerY.equalTo(self.openAutoBtn);
        make.width.mas_equalTo(kBtnWidth);
        make.height.mas_equalTo(kBtnHeight);
    }];
    
    [self.authGuideBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.openAutoBtn);
        make.bottom.equalTo(self.openAutoBtn.mas_top).offset(-kBtnLineSpace);
        make.width.mas_equalTo(kBtnWidth);
        make.height.mas_equalTo(kBtnHeight);
    }];
    
    [self.authSettingBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.closeAutoBtn);
        make.centerY.equalTo(self.authGuideBtn);
        make.width.mas_equalTo(kBtnWidth);
        make.height.mas_equalTo(kBtnHeight);
    }];
    
    [self.locationLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.locationLab.superview);
        make.bottom.equalTo(self.authGuideBtn.mas_top).offset(-30);
        make.width.mas_equalTo(kBtnWidth);
        make.height.mas_equalTo(kBtnHeight);
    }];
}

- (void)setupAppearce {
    self.view.backgroundColor = UIColor.whiteColor;
    self.authGuideBtn.backgroundColor = UIColor.orangeColor;
    self.authSettingBtn.backgroundColor = UIColor.orangeColor;
    self.openAutoBtn.backgroundColor = UIColor.orangeColor;
    self.closeAutoBtn.backgroundColor = UIColor.orangeColor;
    self.startBtn.backgroundColor = UIColor.orangeColor;
    self.stopBtn.backgroundColor = UIColor.orangeColor;
    self.showBtn.backgroundColor = UIColor.orangeColor;
    self.logoutBtn.backgroundColor = UIColor.orangeColor;
    
    [self.locationLab setHidden:YES];
}

- (void)setupNotifacations {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startRecord) name:UBMStartNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopRecord) name:UBMEndNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeSpeed:) name:UBMUIdataUpdateNotification object:nil];
    
}

- (void)startRecord {
    [self.locationLab setHidden:NO];
}

- (void)stopRecord {
    [self.locationLab setHidden:YES];
}

- (void)changeSpeed:(NSNotification *)notify {
    dispatch_async(dispatch_get_main_queue(), ^{
        UBMTipUIModel* uimodel = notify.object;
        if (uimodel) {
            self.locationLab.text = [NSString stringWithFormat:@"%@km/h", uimodel.speed];
        }
    });
}

- (UILabel *)locationLab {
    if (!_locationLab) {
        _locationLab = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        _locationLab.text = @"0.0km/h";
        _locationLab.textAlignment = NSTextAlignmentCenter;
    }
    return _locationLab;
}

- (UIButton *)authGuideBtn {
    if (!_authGuideBtn) {
        _authGuideBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_authGuideBtn setTitle:@"权限引导" forState:UIControlStateNormal];
        [_authGuideBtn setTitleColor:UIColor.blackColor forState:UIControlStateNormal];
        [_authGuideBtn setTitleColor:UIColor.grayColor forState:UIControlStateHighlighted];
        _authGuideBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [_authGuideBtn addTarget:self action:@selector(authGuide) forControlEvents:UIControlEventTouchUpInside];
    }
    return _authGuideBtn;
}

- (void)authGuide {
    NSLog(@"authGuide");
    [UBMAuthGuideController jumpToAuthGuideWebViewByNavigation:self.navigationController];
}

- (UIButton *)authSettingBtn {
    if (!_authSettingBtn) {
        _authSettingBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_authSettingBtn setTitle:@"权限设置" forState:UIControlStateNormal];
        [_authSettingBtn setTitleColor:UIColor.blackColor forState:UIControlStateNormal];
        [_authSettingBtn setTitleColor:UIColor.grayColor forState:UIControlStateHighlighted];
        _authSettingBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [_authSettingBtn addTarget:self action:@selector(authSetting) forControlEvents:UIControlEventTouchUpInside];
    }
    return _authSettingBtn;
}

- (void)authSetting {
    NSLog(@"authSetting");
    UBMAuthSettingController *vc = [[UBMAuthSettingController alloc] init];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    [self.navigationController pushViewController:nav animated:YES];
}

- (UIButton *)openAutoBtn {
    if (!_openAutoBtn) {
        _openAutoBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_openAutoBtn setTitle:@"打开自动记录" forState:UIControlStateNormal];
        [_openAutoBtn setTitleColor:UIColor.blackColor forState:UIControlStateNormal];
        [_openAutoBtn setTitleColor:UIColor.grayColor forState:UIControlStateHighlighted];
        _openAutoBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [_openAutoBtn addTarget:self action:@selector(openAuto) forControlEvents:UIControlEventTouchUpInside];
    }
    return _openAutoBtn;
}

- (void)openAuto {
    NSLog(@"openAuto");
    [[UBITracking shared] startAutoRecord];
    NSLog(@"isAutoOn:%d", [UBITracking shared].isAutoOn);
}

- (UIButton *)closeAutoBtn {
    if (!_closeAutoBtn) {
        _closeAutoBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_closeAutoBtn setTitle:@"关闭自动记录" forState:UIControlStateNormal];
        [_closeAutoBtn setTitleColor:UIColor.blackColor forState:UIControlStateNormal];
        [_closeAutoBtn setTitleColor:UIColor.grayColor forState:UIControlStateHighlighted];
        _closeAutoBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [_closeAutoBtn addTarget:self action:@selector(closeAuto) forControlEvents:UIControlEventTouchUpInside];
    }
    return _closeAutoBtn;
}

- (void)closeAuto {
    NSLog(@"closeAuto");
    [[UBITracking shared] stopAutoRecord];
    NSLog(@"isAutoOn:%d", [UBITracking shared].isAutoOn);
}

- (UIButton *)startBtn {
    if (!_startBtn) {
        _startBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_startBtn setTitle:@"开始手动行程" forState:UIControlStateNormal];
        [_startBtn setTitleColor:UIColor.blackColor forState:UIControlStateNormal];
        [_startBtn setTitleColor:UIColor.grayColor forState:UIControlStateHighlighted];
        _startBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [_startBtn addTarget:self action:@selector(start) forControlEvents:UIControlEventTouchUpInside];
    }
    return _startBtn;
}

- (void)start {
    NSLog(@"start");
    [self.locationLab setHidden:NO];
    [UBITracking.shared startTrip];
    NSLog(@"isTripOnGoing:%d", [UBITracking.shared isTripOnGoing]);
}

- (UIButton *)stopBtn {
    if (!_stopBtn) {
        _stopBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_stopBtn setTitle:@"结束手动行程" forState:UIControlStateNormal];
        [_stopBtn setTitleColor:UIColor.blackColor forState:UIControlStateNormal];
        [_stopBtn setTitleColor:UIColor.grayColor forState:UIControlStateHighlighted];
        _stopBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [_stopBtn addTarget:self action:@selector(stop) forControlEvents:UIControlEventTouchUpInside];
    }
    return _stopBtn;
}

- (void)stop {
    NSLog(@"stop");
    [UBITracking.shared stopTrip:^(UBMResultModel * _Nonnull result) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.locationLab setHidden:YES];
            if (result.resultCode == UBMResultSuccess) {
                NSLog(@"手动关闭行程成功");
                NSLog(@"isTripOnGoing:%d", [UBITracking.shared isTripOnGoing]);
            } else {
                NSLog(@"手动关闭行程失败");
            }
        });
    }];
}

- (UIButton *)showBtn {
    if (!_showBtn) {
        _showBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_showBtn setTitle:@"展示行程" forState:UIControlStateNormal];
        [_showBtn setTitleColor:UIColor.blackColor forState:UIControlStateNormal];
        [_showBtn setTitleColor:UIColor.grayColor forState:UIControlStateHighlighted];
        _showBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [_showBtn addTarget:self action:@selector(show) forControlEvents:UIControlEventTouchUpInside];
    }
    return _showBtn;
}

- (void)show {
    [[UBITracking shared] removeAllRoutes];
    NSLog(@"show");
    UBMRouteHistoryController *historyVc = [[UBMRouteHistoryController alloc] init];
    historyVc.hidesBottomBarWhenPushed = YES;
    UINavigationController *nav = self.navigationController;
    NSLog(@"viewControllers:%@", nav.viewControllers);
    [nav pushViewController:historyVc animated:YES];
//    [nav presentViewController:historyVc animated:YES completion:^{
//
//    }];
}

- (UIButton *)logoutBtn {
    if (!_logoutBtn) {
        _logoutBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_logoutBtn setTitle:@"退出登录" forState:UIControlStateNormal];
        [_logoutBtn setTitleColor:UIColor.blackColor forState:UIControlStateNormal];
        [_logoutBtn setTitleColor:UIColor.grayColor forState:UIControlStateHighlighted];
        _logoutBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [_logoutBtn addTarget:self action:@selector(logout) forControlEvents:UIControlEventTouchUpInside];
    }
    return _logoutBtn;
}

- (void)logout {
    NSLog(@"logout");
    // 清除用户Uid和Token缓存
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UBMUserUidUDKey];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UBIPassportTokenUDKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [UBITracking.shared disableSDK:^(UBMResultModel * _Nonnull result) {
        [self.locationLab setHidden:YES];
        if (result.resultCode == UBMResultSuccess) {
            NSLog(@"关闭行程成功");
        } else {
            NSLog(@"关闭行程失败");
        }
        [self.navigationController popToRootViewControllerAnimated:YES];
    }];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UBMUIdataUpdateNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UBMStartNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UBMEndNotification object:nil];
}

@end
