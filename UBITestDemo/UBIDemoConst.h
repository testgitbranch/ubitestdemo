//
//  UBIDemoConst.h
//  UBITestDemo
//
//  Created by 金鑫 on 2021/12/14.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

UIKIT_EXTERN const CGFloat kBtnHeight;
UIKIT_EXTERN const CGFloat kBtnWidth;
UIKIT_EXTERN const CGFloat kBtnMargin;
UIKIT_EXTERN const CGFloat kBtnLineSpace;

#pragma mark - nslog
#ifndef __OPTIMIZE__
#define NSLog(...) NSLog(__VA_ARGS__)
#else
#define NSLog(...) {}
#endif

#ifndef strongify
#if DEBUG
#if __has_feature(objc_arc)
#define bx_strongify(object) autoreleasepool{} __typeof__(object) object = weak##_##object;
#else
#define bx_strongify(object) autoreleasepool{} __typeof__(object) object = block##_##object;
#endif
#else
#if __has_feature(objc_arc)
#define bx_strongify(object) try{} @finally{} __typeof__(object) object = weak##_##object;
#else
#define bx_strongify(object) try{} @finally{} __typeof__(object) object = block##_##object;
#endif
#endif
#endif

#ifndef weakify
#if DEBUG
#if __has_feature(objc_arc)
#define bx_weakify(object) autoreleasepool{} __weak __typeof__(object) weak##_##object = object;
#else
#define bx_weakify(object) autoreleasepool{} __block __typeof__(object) block##_##object = object;
#endif
#else
#if __has_feature(objc_arc)
#define bx_weakify(object) try{} @finally{} {} __weak __typeof__(object) weak##_##object = object;
#else
#define bx_weakify(object) try{} @finally{} {} __block __typeof__(object) block##_##object = object;
#endif
#endif
#endif

NS_ASSUME_NONNULL_END
