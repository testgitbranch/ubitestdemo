//
//  UBMManager.m
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/12/9.
//  Copyright © 2020 魏振伟. All rights reserved.
//

/*
 tripid逻辑：首先创建用户本地唯一loctripid, 接着获取一次接口的tripID。
 上传时需要用到tripID，而存储时需要loctripid。而存储标识需要更改，需要根据loctripid来判断和存储状态。
 */

#import "UBMManager.h"
#import "UBMCoreMotionCenter.h"
#import "UBMPbFileManger.h"
#import "UBMNetPbFileManager.h"
#import "Sensor.pbobjc.h"
#import "UBMTipUIModel.h"
#import "UBMStartOrEndModel.h"
#import "UBMSqliteHelper.h"
#import "UBMAutoHelper.h"
#import "UBMBaseUserModel.h"
#import "UBMCacheManager.h"
#import "UBMMTools.h"
#import "UBMPrefixHeader.h"
#import "UBMVendorTool.h"
#import "UBMNotificationNameConst.h"

#define uploadTime 5 //一个pb文件的时间跨度。默认5min
#define keepWrongTripRestartTime 15 //自动开启异常结束行程的最大时间间隔

static const uint64_t kCycleUploadTripInterval = 60 * 60 * NSEC_PER_SEC;

@interface UBMManager()

// UBM自动记录开关

@property(nonatomic, assign)BOOL hadPrepareCoreMotionCenter;

@property(nonatomic, assign)UBMOrigintype origintype; //开启类型，手动还是自动

@property(atomic, assign)BOOL isNavigatting; //导航中。导航中不接收自动结束通知事件。

@property(atomic, assign)BOOL inForeground;        //是否在前台
@property(nonatomic, assign)long rowCount;            //当前pb累计数据条数。用来判断5分钟一次的上传pb时机。
@property(nonatomic, assign)int nowTripPbIndex;       //pb文件的index，用来确定名字。从0开始 0.pb
@property(nonatomic, copy)NSString* nowFileName;      //当前文件名

@property(nonatomic, copy)NSString* locTripID;        //此tripid为本地创建的tripid，在上传一个行程文件之前要先请求tripid，然后使用。
@property(nonatomic, strong)dispatch_semaphore_t netTripIDRequestSignal; //保证TripID的请求的同一时刻唯一性

@property(nonatomic, strong)UBMUiDataModel* lastLocModel;       //上一秒的数据
@property(nonatomic, strong)UBMUiDataModel* lastLocModelForLoc;  //上一次有效的定位数据：专为距离计算使用。speed>0有效。

@property(nonatomic, strong)UBMTipUIModel* tipUIModel;       //传出需要展示的UI数据

@property(nonatomic, assign)double mileage;   //当前行程里程
@property(nonatomic, assign)double totalTime; //当前行程总时间

@property(nonatomic, assign)BOOL isEnding; //是否是结束中。结束中什么都不做

@property(nonatomic, assign)int saveCount; //10s存一次行程信息数据到数据库。用于异常行程的判断。
@property(atomic, assign)int loctionUpdateCount; //15s发送一次位置更新

@property(nonatomic, assign)int restartTime;

@property (nonatomic, strong) dispatch_source_t uploadTripTimer; //定期检查本地行程的定时器

@end

@implementation UBMManager

+(instancetype)shared
{
    static id instance;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    
    return instance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.restartTime = keepWrongTripRestartTime;
        
        self.saveCount = 0;
        self.loctionUpdateCount = 0;
        
        self.hadPrepareCoreMotionCenter = NO;
        self.inForeground = YES;
        self.isCollecting = NO;
        self.netTripIDRequestSignal = dispatch_semaphore_create(1);
        
        //创建数据库和表
        //[[UBMSqliteHelper shareUBMSqliteHelper] creatUBMTripTable];
        
        //检查并上传所有本地已停止且未上传结束的trip。
        //[self checkAndUploadLocTrip];
        
        //初始化收集中心并请求定位权限(后移，在需要时再初始化数据收集中心)
        //[self prepareCoreMotionCenter];
        
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goBackGround) name:UIApplicationDidEnterBackgroundNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goForeground) name:UIApplicationWillEnterForegroundNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startCollect:) name:UBMStartNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(endCollect:) name:UBMEndNotification object:nil];
        
        // 开启循环检测未上传行程，并上传
        [self startCycleCheckAndUploadLocTrip];
        
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UBMStartNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UBMEndNotification object:nil];
}

-(void)setUserId:(NSString *)userId{
    
    if ([UBMMTools IsEmptyStr:userId])
    {
        UBMBaseUserModel.shared.userId = @"";
    }
    else
    {
        UBMBaseUserModel.shared.userId = userId;
        
        //创建或打开数据库
        [[UBMSqliteHelper sharedHelper] changeUserSqlite];
        
        //创建表
        [[UBMSqliteHelper sharedHelper] creatUBMTripTable];
    
        //缓存管理，用到时再放开
        //[BXUBMCacheManager removeCacheBeforeOfDays:7];
    }
    
    // 处理因Uid传入不同，而是否需要结束之前的行程
    NSString* prevUserId = [[NSUserDefaults standardUserDefaults] stringForKey:UBMUserIdForResettingUDKey];
    if (![prevUserId isEqualToString:userId]) {
        if(UBMManager.shared.isCollecting == YES ){
            UBMStartOrEndModel* endModel = [[UBMStartOrEndModel alloc] init];
            endModel.origintype = ubmUserMovementType;
            [[NSNotificationCenter defaultCenter] postNotificationName:UBMEndNotification object:endModel];
        }
        [[NSUserDefaults standardUserDefaults] setObject:userId forKey:UBMUserIdForResettingUDKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

-(void)setVid:(NSString *)vid{
    
    if ([UBMMTools IsEmptyStr:vid]) {
        UBMBaseUserModel.shared.vid = @"";
    }else{
        UBMBaseUserModel.shared.vid = vid;
    }
}

-(void)setIsNaving:(BOOL)isNav{
    self.isNavigatting = isNav;
    if (isNav == YES) {
        [UBMCoreMotionCenter.shareCoreMotionCenter setDataCollectType:CMCollectTypeManual];
    }else{
        [UBMCoreMotionCenter.shareCoreMotionCenter setDataCollectType:CMCollectTypeAuto];
    }
}

-(void)setIsStoping:(BOOL)isStoping{
    [UBMCoreMotionCenter.shareCoreMotionCenter setAutoStoping:isStoping];
}

#pragma mark -判断是否继续异常结束的行程
-(void)keepLastTripWithInterval:(int)interval keepLastTripResult: (keepLastTripResultBlock)lastTripResultBlock {
    self.restartTime = interval;
    
    NSArray* notStopTripList = [UBMManager.shared getAllUnStopTrip];
    if(notStopTripList && notStopTripList.count>0){
        
        UBMTripTableModel* tripModel = notStopTripList[0];
        
        double time1 = [[NSDate date] timeIntervalSince1970];
        double time2 = [tripModel.lastUpdateTime floatValue];
        
        //自动继续。
        if ((time1-time2) < self.restartTime*60 ) {
            
//            [MobClick event:@"trip_recover" attributes:[[NSDictionary alloc] initWithObjectsAndKeys:UBMUserStatus.shared.uid, @"uid", nil]];
            
            UBMStartOrEndModel* startModel = [[UBMStartOrEndModel alloc] init];
            startModel.origintype = ubmAbnormalType;
            startModel.incorrectLocTripdId = tripModel.locTripID;
            [UBMManager.shared startRecord:startModel andResult:^(UBMResultModel * _Nonnull result) {
                if (result.resultCode == NO) {
                    
                    //上传并结束掉这个行程。
                    [[UBMManager shared] stopAndUploadIncorrectTrip: tripModel.locTripID];
                    
                }else{
                    [UBMSLSLog addUBMLog:@"trip_recover" andTripID:tripModel.tripID];
                    
                    if (lastTripResultBlock) {
                        lastTripResultBlock(YES);
                    }
                }
            }];
            
        }else{
            //上传并结束掉这个行程。
            [UBMSLSLog addUBMLog:@"trip_stopIncorrectTrip" andTripID:tripModel.tripID];
            [[UBMManager shared] stopAndUploadIncorrectTrip: tripModel.locTripID];
        }
    }
}

//自动记录触发
-(void)startAutoRecord{
    [UBMAutoHelper.shareAutoHelper startAutoRecordManager];
}

//自动记录关闭
-(void)stopAutoRecord{
    [UBMAutoHelper.shareAutoHelper stopAutoRecordManager];
}

#pragma mark - 初次请求权限并准备接收数据。
-(void)prepareCoreMotionCenter{
    
    if (self.hadPrepareCoreMotionCenter == YES) {
        return;
    }else{
        self.hadPrepareCoreMotionCenter = YES;
    }
    __weak typeof(self) weakself = self;
    UBMCoreMotionCenter* coreMotioncenter = [UBMCoreMotionCenter shareCoreMotionCenter];
    coreMotioncenter.dataBlcok = ^(NSArray * _Nonnull dataArr) {
        
        if (weakself.isCollecting == YES) {
            //接收并处理pb数据。存储上传等。
//            NSArray* arr1 = [[NSArray alloc] initWithArray:dataArr];
            NSArray* arr1 = dataArr;
            NSLog(@" %lu",(unsigned long)arr1.count);
            [weakself saveAndUploadData:arr1];
        }
        
    };
    coreMotioncenter.actualBlock = ^(UBMUiDataModel * _Nonnull dataDic) {
        
        if (weakself.isCollecting == YES) {
            //ui数据
            [weakself handleUIData:dataDic];
        }
        
    };
}

#pragma mark - APP进入前台的通知
-(void)goForeground{
    
    @synchronized (self) {
        
        self.inForeground = YES;
        [UBMSLSLog addLog:@"goForeground"];
        
        if (self.isCollecting == YES) {
            [[UBMCoreMotionCenter shareCoreMotionCenter] restartCoreMotin];
        }
        
    }
}

#pragma mark -APP进入后台的通知
-(void)goBackGround{
    
    @synchronized (self) {
        
        self.inForeground = NO;
        [UBMSLSLog addLog:@"goBackGround"];
        
        if (self.isCollecting == YES) {
            [[UBMCoreMotionCenter shareCoreMotionCenter] restartCoreMotin];
        }
        
    }
}


#pragma mark - 开始收集
-(void)startCollect:(NSNotification*)notifa{
    
    UBMStartOrEndModel* startModel = notifa.object;
    [self startRecord:startModel andResult:^(UBMResultModel * _Nonnull result) {
        
    }];
    
}

-(void)startRecord:(UBMStartOrEndModel *)startModel andResult:(void (^)(UBMResultModel * _Nonnull))startBack{
    
    //如果已经是在开启状态了。
    if (self.isCollecting == YES) {
        
        UBMResultModel* startResultModel = [[UBMResultModel alloc] init];
        startResultModel.resultCode = NO;
        if (startBack) {
            startBack(startResultModel);
        }
        
        return;
        
    }else{
        //reset
        self.nowTripPbIndex = 0;
        self.rowCount = 0;
        self.tipUIModel = [[UBMTipUIModel alloc] init];
        self.lastLocModel = nil;
        self.lastLocModelForLoc = nil;
        self.nowFileName = @"";
        self.locTripID = @"";
        self.mileage = 0.0;
        self.totalTime = 0.0;
        
        //用户ID为空，则不能开启！
        if ([UBMMTools IsEmptyStr:UBMBaseUserModel.shared.userId]) {
            NSLog(@"***userid为空,ubm不能开启！！！***");
            UBMResultModel* startResultModel = [[UBMResultModel alloc] init];
            startResultModel.resultCode = NO;
            [[NSNotificationCenter defaultCenter] postNotificationName:UBMStartResultNotification object:startResultModel];
            if (startBack) {
                startBack(startResultModel);
            }
            return;
        }
    }
    
    //判断收集条件是否满足。如果满足发出开启结果的通知。
    //需要判断是自动还是手动，自动如果失败，则不提示失败原因。手动则给出提示。
    
    //准备接收数据，初始化收集中心模块。
    [self prepareCoreMotionCenter];
    
    UBMResultModel* startResultModel = [[UBMResultModel alloc] init];
    
    //先判断权限
    @bx_weakify(self);
    [self startLimitsJuge:startModel andReslut:^(BOOL result) {
        
        if (result == YES || startModel.origintype == ubmAbnormalType)
        {
            weak_self.isCollecting = YES;
            
            [weak_self prepareLocTripIdAndNetTripId:startModel andReult:^(BOOL result) {
                
                if (result == YES)
                {
                    //开启传感器
                    weak_self.origintype = startModel.origintype;
                    
                    CMCollectType cmtype = CMCollectTypeManual;
                    if (startModel.origintype == ubmAutoType) {
                        cmtype = CMCollectTypeAuto;
                    }
                    [[UBMCoreMotionCenter shareCoreMotionCenter] startGetDataWithType:cmtype];
                    
                    startResultModel.resultCode = YES;
                    [[NSNotificationCenter defaultCenter] postNotificationName:UBMStartResultNotification object:startResultModel];
                    
                    if (startBack) {
                        startBack(startResultModel);
                    }
                    
                    [UBMSLSLog addUBMLog:@"startTripSucess"];
                    
                }else{
                    
                    weak_self.isCollecting = NO;
                    startResultModel.resultCode = NO;
                    [[NSNotificationCenter defaultCenter] postNotificationName:UBMStartResultNotification object:startResultModel];
                    
                    if (startBack) {
                        startBack(startResultModel);
                    }
                    
                    [UBMSLSLog addUBMLog:@"startTripFail"];
                }
            }];
        }
        else
        {
            weak_self.isCollecting = NO;
            startResultModel.resultCode = NO;
            [[NSNotificationCenter defaultCenter] postNotificationName:UBMStartResultNotification object:startResultModel];
            
            if (startBack) {
                startBack(startResultModel);
            }
        }
        
    }];
}

#pragma mark -权限判断
-(void)startLimitsJuge:(UBMStartOrEndModel*)startModel andReslut:(void(^)(BOOL result))limitsBack{
    
    NSString* alertTitle = @"开启失败";
    if(startModel.origintype == ubmAbnormalType){
        alertTitle = @"提示";
    }
    
    [[UBMCoreMotionCenter shareCoreMotionCenter] limitsJuge:^(CMStartErrorType response) {
        
        if (response == CMStartErrorNone) {
            
            if (limitsBack) {
                limitsBack(YES);
            }
            
        }else if (response == CMStartErrorCLAuthorizationStatus){
            
            if (startModel.origintype == ubmUserMovementType || startModel.origintype == ubmAbnormalType)
            {
                // 关闭了定位 或者被拒绝
                UBMWCustomAlert* wc = [[UBMWCustomAlert alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight) andTitle:alertTitle andContent:@"请检查定位权限是否开启" andleftBton:@"取消" andRightBton:@"去设置"];
                wc.rightActionBlock = ^{
                    NSURL *url2 = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                    if ([[UIApplication sharedApplication] canOpenURL:url2]){
                        if (@available(iOS 10.0, *)) {
                            [[UIApplication sharedApplication] openURL:url2 options:@{} completionHandler:nil];
                        } else {
                            [[UIApplication sharedApplication] openURL:url2];
                        }
                    }
                };
                [wc show];
            }
            
            if (limitsBack) {
                limitsBack(NO);
            }

        }
        else if (response == CMStartErrorCLNotAvailable){
            
            //定位因为某些原因不可用
            if (startModel.origintype == ubmUserMovementType || startModel.origintype == ubmAbnormalType)
            {
                UBMWCustomAlert* wc = [[UBMWCustomAlert alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight) andTitle:alertTitle andContent:@"请检查 \"设置\" - \"隐私\" - \"定位\" 是否开启" andleftBton:@"我知道了" andRightBton:@""];
                [wc show];
            }
            
            if (limitsBack) {
                limitsBack(NO);
            }
        }
        else if (response == CMStartErrorAMauthorizationStatus){
            
            //运动与健身数据被拒绝
            if (startModel.origintype == ubmUserMovementType || startModel.origintype == ubmAbnormalType)
            {
                UBMWCustomAlert* wc = [[UBMWCustomAlert alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight) andTitle:alertTitle andContent:@"您已拒绝我们访问您的\"运动与健身\"数据，请先去设置中打开权限" andleftBton:@"取消" andRightBton:@"去设置"];
                wc.rightActionBlock = ^{
                    NSURL *url2 = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                    if ([[UIApplication sharedApplication] canOpenURL:url2]){
                        if (@available(iOS 10.0, *)) {
                            [[UIApplication sharedApplication] openURL:url2 options:@{} completionHandler:nil];
                        } else {
                            [[UIApplication sharedApplication] openURL:url2];
                        }
                    }
                };
                [wc show];
            }
            
            if (limitsBack) {
                limitsBack(NO);
            }
            
        }else if (response == CMStartErrorAMIsNotAvailable){
            
            //健康数据不可用
            if (startModel.origintype == ubmUserMovementType || startModel.origintype == ubmAbnormalType)
            {
                UBMWCustomAlert* wc = [[UBMWCustomAlert alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight) andTitle:alertTitle andContent:@"请检查 \"设置\" - \"隐私\" - \"运动与健身\" 是否开启" andleftBton:@"我知道了" andRightBton:@""];
                [wc show];
            }
                    
            if (limitsBack) {
                limitsBack(NO);
            }
        }
            
    }];
}

#pragma mark - 准备TripId和NetTripId。
-(void)prepareLocTripIdAndNetTripId:(UBMStartOrEndModel*)startModel andReult:(void(^)(BOOL result))preBack{
    
    //无网也可开启 直接放开。2.1.1
    BOOL creatResult = [self creatLocTripID:startModel];
    if (creatResult == NO) {
        if (preBack) {
            preBack(NO);
        }
        [UBMSLSLog addUBMLogOfW:@"creatLocTripFail"];
        return;
    }
     
    
    //如果传入的有tripid，则直接写入。写入失败，则开启失败。
    if (startModel.tripId && ![UBMMTools IsEmptyStr:startModel.tripId]) {
        
        BOOL result = [[UBMSqliteHelper sharedHelper] updateTripID:startModel.tripId OfLocTripID:self.locTripID];
        if (result == YES) {
            NSLog(@"写入传入的tripId成功！%@",startModel.tripId);
            if (preBack) {
                preBack(YES);
            }
        }else{
            
            //删除已经写入的LocTripID那一条
            [[UBMSqliteHelper sharedHelper] deleteTheTripOflocTripId:self.locTripID];
            self.locTripID = @"";
            
            if (preBack) {
                preBack(NO);
            }
        }
        
    }else{
        
        if (preBack) {
            preBack(YES);
        }
        
        //继续去请求tripid，但是不阻塞开启。
        __weak typeof(self) weakself = self;
        [self getTripIdWithLocTripId:self.locTripID and:^(NSString *tripId) {
            
            if (![UBMMTools IsEmptyStr:tripId]){
                weakself.nowTripid = tripId;
            }else{
                [UBMSLSLog addUBMLogOfW:@"getNetTripIdFail"];
            }
            
        }];
        
    }
    
    
    
    //2.1.0本版本先以网络tripid为条件。已改成无网也能开启。
    /*
    __weak typeof(self) weakself = self;
    [self getTripIdWithLocTripId:self.locTripID and:^(NSString *tripId) {
        
        if (MTools.IsEmptyStr(tripId)) {
            //删除LocTripID那一条
            [[UBMSqliteHelper shareUBMSqliteHelper] deleteTheTripOflocTripId:self.locTripID];
            weakself.locTripID = @"";
            
            if (preBack) {
                preBack(NO);
            }
            
        }else{
            weakself.nowTripid = tripId;
            
            if (preBack) {
                preBack(YES);
            }
        }
    }];
     */
    
}

#pragma mark -创建locTripid。
-(BOOL)creatLocTripID:(UBMStartOrEndModel*)startModel{
    
    //如果没有LocTripdId，则新建。
    if ([UBMMTools IsEmptyStr:startModel.incorrectLocTripdId]) {
        
        long nowTime = (long)[[NSDate date] timeIntervalSince1970];
        self.locTripID = [NSString stringWithFormat:@"%@_%ld", UBMBaseUserModel.shared.userId, nowTime];
        int startType = 0;
        if (startModel.origintype == ubmAutoType) {
            startType = 1;
        }
        
        NSLog(@"开始类型： %d",startType);
        
        BOOL result = [[UBMSqliteHelper sharedHelper] addNewTripItemWith:self.locTripID andVid:UBMBaseUserModel.shared.vid andStartType:startType];
        if (result == YES) {
            NSLog(@"插入一条新trip成功");
            return YES;
        }else{
            NSLog(@"插入一条新trip失败");
            return NO;
        }
        
    }else{
        
        UBMTripTableModel* tripModel = [[UBMSqliteHelper sharedHelper] getUBMTripTableModelOfLocTripID:startModel.incorrectLocTripdId OrTripID:@""];
        if (tripModel == nil) {
            return NO;
        }
        //计算nowTripPbIndex。看看之前的文件夹下面有没有这个tripID的文件，有几个，就是几；（这个是异常恢复的情况）
        NSString* path = [UBMPbFileManger pbFolderPathWithLocTripId:tripModel.locTripID];
        NSArray* arr = [UBMPbFileManger getAllFileAtPath:path];
        if (arr.count>0) {
            self.nowTripPbIndex = (int)arr.count;
        }
        
        self.mileage = [tripModel.mileage doubleValue];
        self.totalTime = [tripModel.totalTime doubleValue];
        self.locTripID = startModel.incorrectLocTripdId;
        
        return YES;
    }
    
}

#pragma mark -请求网络tripId
-(void)getTripIdWithLocTripId:(NSString*)locTripId and:(void(^)(NSString* tripId))dataBack{
    
    //接口回调是在主线程，为了避免信号量在主线程带来的死锁，故选择任务执行在子线程。
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        //加锁，保证一个在请求中，另一个请求不能走，否则可能出现一个本地loctripid同时请求到两个tripID的问题。此问题发生在无网-有网上传、同时结束的场景中
        dispatch_semaphore_wait(self.netTripIDRequestSignal, DISPATCH_TIME_FOREVER);
        
        UBMTripTableModel* tripModel = [[UBMSqliteHelper sharedHelper] getUBMTripTableModelOfLocTripID:locTripId OrTripID:@""];
        
        if (tripModel == nil) {
            dispatch_semaphore_signal(self.netTripIDRequestSignal);
            dataBack(@"");
            return;
        }
        
        if (IsEmptyStr(tripModel.tripID))
        {
            NSString* carId = tripModel.vid;
            if (IsEmptyStr(carId)) {
                carId = @"";
            }
            
            [[UBMNetWorkTool shareNetWorkTool] postRequestWithBaseUrl:[UBMDomainName.shareDomain ubmURL] lastUrl:UBMGetTripId parameters:@{@"userId":[UBITracking shared].userID, @"deviceId":[UBMVendorTool getIDFV]} needShowHUD:NO needShowError:NO needBackInMainQueue:NO success:^(id response) {
                
                if (response && [response isKindOfClass:[NSDictionary class]] && [response[@"code"]intValue]==1 && response[@"data"][@"tripId"]) {
                    
                    //更新服务器tripID
                    NSString* tripId = response[@"data"][@"tripId"];
                    NSLog(@"写入tripID:%@",tripId);
                    [UBMSLSLog addUBMLog:@"获取tripid成功！" andTripID:tripId];
                    
                    BOOL result = [[UBMSqliteHelper sharedHelper] updateTripID:tripId OfLocTripID:locTripId];
                    if (result == YES) {
                        
                        //NSLog(@"写入tripID完事");
                        //UBMTripTableModel* tripModel = [[UBMSqliteHelper shareUBMSqliteHelper] getUBMTripTableModelOfLocTripID:locTripId OrTripID:@""];
                        //NSLog(@"检查写入的tripID： %@",tripModel.tripID);
                         
                        dataBack(tripId);
                    }else{
                        NSLog(@"程序出了问题，更改数据库tripid出错！！");
                        dataBack(@"");
                    }
            
                }else{
                    dataBack(@"");
                }
                
                dispatch_semaphore_signal(self.netTripIDRequestSignal);
            } error:^(id response) {
                dataBack(@"");
                dispatch_semaphore_signal(self.netTripIDRequestSignal);
            }];
            
        }
        else
        {
            NSLog(@"www: 已经有网络tripID啦");
            dataBack(tripModel.tripID);
            dispatch_semaphore_signal(self.netTripIDRequestSignal);
            
        }
        
        
    });
    
    
}

#pragma mark -处理ui数据
-(void)handleUIData:(UBMUiDataModel*)dataModel{
    
    if (self.lastLocModel == nil) {
        self.lastLocModel = dataModel;
        self.lastLocModelForLoc = dataModel;
    }else{
        
        if (dataModel.speed > 0) {
            double dis = [UBMMTools distanceBetweenOrderBy:self.lastLocModelForLoc.lat :dataModel.lat :self.lastLocModelForLoc.lon :dataModel.lon];
            self.mileage = self.mileage + dis;
            self.lastLocModelForLoc = dataModel;
        }
        
        double tim = [UBMMTools timeInterval:[NSString stringWithFormat:@"%f",self.lastLocModel.timeStemp] sinceDate:[NSString stringWithFormat:@"%f",dataModel.timeStemp ]];
        self.totalTime = self.totalTime+tim;
        self.lastLocModel = dataModel;

    }
    
    //前台时才发出更新UI的通知
    if (self.inForeground == YES) {
        
        //组装通知数据
        self.tipUIModel.mileage = [NSString stringWithFormat:@"%.1f", self.mileage/1000.0];
        self.tipUIModel.totalTime = [self getMMSSFromSS:[NSString stringWithFormat:@"%f",self.totalTime]];
        //NSLog(@"self.tipUIModel.totalTime == %@",self.tipUIModel.totalTime);
        NSString* speedx = [NSString stringWithFormat:@"%.1f", dataModel.speed*3600.0/1000>0 ? dataModel.speed*3600.0/1000:0];
        //大于29km/h时，主动加3。
        if ([speedx floatValue] >= 29.0) {
            CGFloat newspeed = [speedx floatValue]+3.0;
            speedx = [NSString stringWithFormat:@"%.1f", newspeed];
        }
        self.tipUIModel.speed = speedx;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:UBMUIdataUpdateNotification object:self.tipUIModel];
    }
    
    //发送速度的通知
    if (UBMAutoHelper.shareAutoHelper.isAutoRecord == YES) {
        self.loctionUpdateCount++;
        if (self.loctionUpdateCount >= 15) {
            self.loctionUpdateCount = 0;
            [[NSNotificationCenter defaultCenter] postNotificationName:UBMSpeedAutoNotification object:dataModel];
        }
        
    }
}

#pragma mark -保存5秒一次的数据
-(void)saveAndUploadData:(NSArray*)dataArr{
    if ( dataArr==nil || dataArr.count==0) {
        return;
    }
    
    self.rowCount = self.rowCount + dataArr.count;
    int rowNum = 10*60*uploadTime; //五分钟的条数。频率10。
    if (self.rowCount > rowNum) {
        
        [UBMSLSLog addUBMLog:@"saveOf5min" andTripID:self.nowTripid];
        
        //异步上传上一段pb
        NSString* lastFileName = [NSString stringWithFormat:@"%@", self.nowFileName];
        [self upLoadFileWithName:lastFileName];
        
        //需要创建一段新pb
        self.nowTripPbIndex = self.nowTripPbIndex + 1;
        self.rowCount = 0;
        self.nowFileName = @"";
        
    }
    
    //是否需要新建pb文件
    if (IsEmptyStr(self.nowFileName)){
        
        NSString* fileName = [self fileNameWithIndex:self.nowTripPbIndex];
        BOOL res = [UBMPbFileManger careatFileAtUBMFolderWithFileName:fileName andFolderName:self.locTripID];
        if (res == YES) {
            self.nowFileName = fileName;
            [self addPbToSqlite];
            //NSLog(@"创建pb文件成功： %@ ", self.nowFileName);
        }else{
            NSLog(@"创建pb文件失败");
            return;
        }
    }
    
    //往文件中追加pb数据
    NSString* nowPath = [NSString stringWithFormat:@"%@/%@",[UBMPbFileManger pbFolderPathWithLocTripId:self.locTripID], self.nowFileName];
    [UBMPbFileManger addDataToPbFilePath:nowPath andData:dataArr];
    
    //10s存一次瞬时数据。用于断掉了15分内的判断。
    self.saveCount++;
    if (self.saveCount >= 2) {
        self.saveCount = 0;
        
        dispatch_queue_t wq = dispatch_queue_create("ws_Queue", DISPATCH_QUEUE_SERIAL);
        dispatch_async(wq, ^{
            
            [[UBMSqliteHelper sharedHelper] updateLastUpdateTime:[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]] OfLocTripID:self.locTripID OrTripID:@""];
            [[UBMSqliteHelper sharedHelper] updateMileage:[NSString stringWithFormat:@"%f", self.mileage] OfLocTripID:self.locTripID OrTripID:@""];
            [[UBMSqliteHelper sharedHelper] updateTotalTime:[NSString stringWithFormat:@"%f", self.totalTime] OfLocTripID:self.locTripID OrTripID:@""];
            
        });
    }
    
}

#pragma mark -新增pb文件记录
-(void)addPbToSqlite{
    
    if (!(IsEmptyStr(self.nowFileName))) {
        //记录一个小pb文件的状态。
        UBMTripPBModel* pbModel = [[UBMTripPBModel alloc] init];
        pbModel.hadUpload = NO;
        pbModel.pbName = self.nowFileName;
        pbModel.pbMd5 = @"";
        [[UBMSqliteHelper sharedHelper] updateItemOfTripPBList:pbModel OfLocTripID:self.locTripID OrTripID:@""];
        NSLog(@"www: 插入新的pb到数据库成功！%@",self.nowFileName);
        //看看是否插入成功！
        //UBMTripTableModel* tripModel = [[UBMSqliteHelper shareUBMSqliteHelper] getUBMTripTableModelOfLocTripID:self.locTripID OrTripID:@""];
        //NSLog(@"www-vvv: pb总数：%d", (int)tripModel.tripPBList.count);
    }
    
}

#pragma mark -上传一个pb数据。注意上传的前提是获取到接口取到的tripid。
-(void)upLoadFileWithName:(NSString*)fileName{
    
    NSString* lastFilePath = [NSString stringWithFormat:@"%@/%@",[UBMPbFileManger pbFolderPathWithLocTripId:self.locTripID], fileName];
    
    //没有服务器的tripId，则请求。有则上传。
    @bx_weakify(self);
    [self getTripIdWithLocTripId:self.locTripID and:^(NSString *tripId) {
        if (IsEmptyStr(tripId)) {
            return;
        }else{
            
            weak_self.nowTripid = tripId;
            
            [[UBMNetPbFileManager shareNetPbFileManger] uploadFileWithPath:lastFilePath andTripId:tripId andFileName:fileName and:^(BOOL result) {
                if (result == YES) {
                    //NSLog(@"pb文件： %@ 已上传", fileName);
                }
            }];
            
        }
    }];
    
}


#pragma mark - 结束收集
-(void)endCollect:(NSNotification*)notifa{
    
    UBMStartOrEndModel* endModel = notifa.object;
    [self stopRecord:endModel andResult:^(UBMResultModel * _Nonnull result) {
        
    }];
    
}

-(void)stopRecord:(UBMStartOrEndModel *)startModel andResult:(void (^)(UBMResultModel * _Nonnull))endBack{
    
    [UBMSLSLog addUBMLog:@"stopRecord_receive" andTripID:self.nowTripid];
    
    //判断需要不需要结束：导航中不可结束。（或者在结束过程中的、不在收集中的，不可结束）
    if (self.isEnding == YES || self.isCollecting == NO || self.isNavigatting == YES) {
        
        [UBMSLSLog addUBMLog:[NSString stringWithFormat:@"stopRecord_return isEnding=%d, isCollecting=%d, isNavigatting=%d", self.isEnding, self.isCollecting, self.isNavigatting] andTripID:self.nowTripid];
        UBMResultModel* endModel = [[UBMResultModel alloc] init];
        endModel.tripId = @"";
        endModel.resultCode = NO;
        if (endBack) {
            endBack(endModel);
        }
        return;
    }
    
    self.isEnding = YES;
    
    //更新本trip结束类型
    if (startModel.origintype == ubmAutoType) {
        BOOL result = [[UBMSqliteHelper sharedHelper] updateIsAutoStop:YES OfLocTripID:self.locTripID OrTripID:@""];
        if (result == YES) {
            NSLog(@"结束类型更新成功");
        }else{
            NSLog(@"结束类型更新失败");
        }
    }
    
    @bx_weakify(self);
    [self endTheTrip:^(BOOL result, NSString *tripId) {
        
        weak_self.isEnding = NO;
        
        UBMResultModel* rmodel = [[UBMResultModel alloc] init];

        if (result == NO) {
            rmodel.tripId = @"";
            rmodel.resultCode = NO;
        }else{
            rmodel.tripId = tripId;
            rmodel.resultCode = YES;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:UBMEndResultNotification object:rmodel];
        });
        
        if (endBack) {
            endBack(rmodel);
        }
        
        [UBMSLSLog addUBMLog:@"endTrip" andTripID:weak_self.nowTripid];
        
    }];
    
}

#pragma mark -结束行程
-(void)endTheTrip:(void(^)(BOOL result, NSString *tripId))dataBack{
    
    if (IsEmptyStr(self.locTripID)) {
        if (dataBack) {
            dataBack(NO, @"");
        }
        return;
    }
    
    //不足5s的，需要等到5s后，生成文件后再停止。与后端协商的结果。没有文件调用合并接口，会失败。
    if (self.totalTime <5 ) {
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            self.isCollecting = NO;
            self.nowTripid = @"";
            
            //NSLog(@"行程结束, 上传文件 走loading ；没网则本地记着，（下次在ubm主页检索结束），同时回到上页");
            [[UBMCoreMotionCenter shareCoreMotionCenter] stopGetData];
            
            //上传当前行程的文件。检测并上传所有未上传的文件。
            [self stopAndUploadTrip:self.locTripID andreslut:^(BOOL result, NSString *tripId) {
                
                if (dataBack) {
                    dataBack(result, tripId);
                }
                
            }];
            
        });
        
    }else{
        
        self.isCollecting = NO;
        self.nowTripid = @"";
        
        //NSLog(@"行程结束, 上传文件 走loading ；没网则本地记着，下次在ubm主页检索结束，同时回到上页");
        [[UBMCoreMotionCenter shareCoreMotionCenter] stopGetData];
        
        //上传当前行程的文件。检测并上传所有未上传的文件。
        [self stopAndUploadTrip:self.locTripID andreslut:^(BOOL result, NSString *tripId) {
            
            if (dataBack) {
                dataBack(result, tripId);
            }
            
        }];
        
    }
    
}

#pragma mark -结束时确定网络tripid是否已就绪
-(void)stopAndUploadTrip:(NSString *)locTripID andreslut:(void(^)(BOOL result, NSString* tripId))dataBack{

    BOOL result = [[UBMSqliteHelper sharedHelper] updateHadStop:YES OfLocTripID:locTripID OrTripID:@""];
    if (result == YES) {
        NSLog(@"结束状态更新成功");
    }else{
        NSLog(@"结束状态更新失败");
    }
    
    __weak typeof(self) weakself = self;
    [self getTripIdWithLocTripId:locTripID and:^(NSString *tripId) {
        if (IsEmptyStr(tripId)) {
            
            if(dataBack){
                dataBack(NO,@"");
            }
            
        }else{
            [weakself netPbFileMangerCheckAndUpload:tripId andreslut:^(BOOL result) {
                if(dataBack){
                    dataBack(result, tripId);
                }
            }];
        }
    }];
    
}

#pragma mark -最后的校验（检查全部未上传pb，上传+校验+结束接口）
-(void)netPbFileMangerCheckAndUpload:(NSString*)tripID andreslut:(void(^)(BOOL result))dataBack{
    
    [[UBMNetPbFileManager shareNetPbFileManger] checkAndUploadTripIDS:@[tripID] andResPonse:^(BOOL result) {
        
        if (dataBack) {
            dataBack(result);
        }
        
    }];
    
}

#pragma mark - 检查并上传所有的本地未上传行程
-(void)checkAndUploadLocTrip:(void (^)(BOOL))callBack{
    
    NSArray* notStopTripList = [[UBMSqliteHelper sharedHelper] getUBMTripTableModelOfUploadOver:NO];
    if (notStopTripList && notStopTripList.count>0) {
        
        __block NSMutableArray* shouldUploadArr = [[NSMutableArray alloc] init];
        NSMutableArray* needGetTripIdArr = [[NSMutableArray alloc] init];
        for (UBMTripTableModel* model in notStopTripList) {
            if (model.hadStop == YES && model.uploadOver == NO) {
                
                if (IsEmptyStr(model.tripID)) {
                    [needGetTripIdArr addObject:model.locTripID];
                }else{
                    [shouldUploadArr addObject:model.tripID];
                }
            }
        }
        
        //请求trip的网络tripid
        if (needGetTripIdArr.count>0) {
            
            dispatch_queue_t quene = dispatch_queue_create("quene.group", DISPATCH_QUEUE_CONCURRENT);
            dispatch_group_t group1 = dispatch_group_create();
            
            for (NSString* locTipeId in needGetTripIdArr) {
                dispatch_group_enter(group1);
                [self getTripIdWithLocTripId:locTipeId and:^(NSString *tripId) {
                    if(![UBMMTools IsEmptyStr:tripId]){
                        [shouldUploadArr addObject:tripId];
                    }
                    dispatch_group_leave(group1);
                }];
            }
            
            dispatch_group_notify(group1, quene, ^{
    
                [[UBMNetPbFileManager shareNetPbFileManger] checkAndUploadTripIDS:shouldUploadArr andResPonse:^(BOOL result) {
                    if (callBack) {
                        callBack(result);
                    }
                }];
                
            });
            
        }else if(shouldUploadArr.count>0){
            
            [[UBMNetPbFileManager shareNetPbFileManger] checkAndUploadTripIDS:shouldUploadArr andResPonse:^(BOOL result) {
                if (callBack) {
                    callBack(result);
                }
            }];
            
        }

    }else{
        if (callBack) {
            callBack(YES);
        }
    }
    
}

#pragma mark -查找本地未结束的行程
-(NSArray *)getAllUnStopTrip{
    NSArray* notStopTripList = [[UBMSqliteHelper sharedHelper] getUBMTripTableModelOfHadStop:NO];
    return notStopTripList;
}

#pragma mark - 手动处理异常停止的trip并上传、校验、合并。
-(void)stopAndUploadIncorrectTrip:(NSString *)locTripID{
    [self stopAndUploadTrip:locTripID andreslut:nil];
}

#pragma mark -查找本地UploadOver==NO的行程。
-(NSArray *)getAllUnUploadOverTrip{
    NSArray* notStopTripList = [[UBMSqliteHelper sharedHelper] getUBMTripTableModelOfUploadOver:NO];
    return notStopTripList;
}

#pragma mark - 上传指定tripId的行程
-(void)uploadTheTripsOfTripids:(NSArray*)tripids andCallBack:(nonnull void (^)(BOOL))upresult{
    
    if (tripids && [tripids isKindOfClass:[NSArray class]] && tripids.count>0) {
        
        [[UBMNetPbFileManager shareNetPbFileManger] checkAndUploadTripIDS:tripids andResPonse:^(BOOL result) {
            
            if (upresult) {
                upresult(result);
            }
            
        }];
    }else{
        if (upresult) {
            upresult(YES);
        }
    }
    
}

#pragma mark - 更改记录模式（手动、自动）
-(void)changeOrigintype:(UBMOrigintype)origintype{
    self.origintype = origintype;
}

#pragma mark - 文件名规则
-(NSString*)fileNameWithIndex:(int)Index{
    NSString* fileName = [NSString stringWithFormat:@"%d.pb",Index];
    return fileName;
}

#pragma mark -s -> hh:mm:ss
-(NSString *)getMMSSFromSS:(NSString *)totalTime{
    
    long seconds = roundf([totalTime floatValue]);
    
    NSString *str_hour = [NSString stringWithFormat:@"%d", (int)seconds/3600];
    if (str_hour.length==1) {
        str_hour = [NSString stringWithFormat:@"0%@",str_hour];
    }
    
    NSString *str_minute = [NSString stringWithFormat:@"%d", (int)(seconds%3600)/60];
    if (str_minute.length==1) {
        str_minute = [NSString stringWithFormat:@"0%@",str_minute];
    }
    
    NSString *str_second = [NSString stringWithFormat:@"%d", (int)seconds%60];
    if (str_second.length==1) {
        str_second = [NSString stringWithFormat:@"0%@",str_second];
    }
    
    NSString *format_time = [NSString stringWithFormat:@"%@:%@:%@",str_hour,str_minute,str_second];
    return format_time;
}

- (void)closeAutoRecordSwitch {
    NSNumber *sw = [[NSUserDefaults standardUserDefaults] objectForKey:UBMAutoRecordSwitchUDKey];
    if ([sw integerValue] == 1) {
        [[NSUserDefaults standardUserDefaults] setObject:@0 forKey:UBMAutoRecordSwitchUDKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (void)openAutoRecordSwitch {
    NSNumber *sw = [[NSUserDefaults standardUserDefaults] objectForKey:UBMAutoRecordSwitchUDKey];
    if ([sw integerValue] == 0) {
        [[NSUserDefaults standardUserDefaults] setObject:@1 forKey:UBMAutoRecordSwitchUDKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (void)startCycleCheckAndUploadLocTrip {
    if (self.uploadTripTimer){
        [self stopCycleCheckAndUploadLocTrip];
    }
    
    dispatch_queue_t queue = dispatch_get_global_queue(0, 0);
    self.uploadTripTimer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    dispatch_source_set_timer(self.uploadTripTimer, DISPATCH_TIME_NOW, kCycleUploadTripInterval, NSEC_PER_SEC);
    
    dispatch_source_set_event_handler(self.uploadTripTimer, ^{
        [[UBMManager shared] checkAndUploadLocTrip:^(BOOL result) {
            if (result == NO) {
                NSLog(@"行程上传失败，请稍后重新上传~");

            } else {
                NSLog(@"行程上传成功~");
            }
        }];
        NSLog(@"checkAndUploadLocTrip() +1");
    });
    
    dispatch_resume(self.uploadTripTimer);
}

- (void)stopCycleCheckAndUploadLocTrip {
    if (self.uploadTripTimer) {
        dispatch_source_cancel(self.uploadTripTimer);
        self.uploadTripTimer = nil;
    }
}

@end
