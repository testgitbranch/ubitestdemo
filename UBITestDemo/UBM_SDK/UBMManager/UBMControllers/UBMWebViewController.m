//
//  UBMWebViewController.m
//  LoveCarChannel_iOS
//
//  Created by 金鑫 on 2021/11/25.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

#import "UBMWebViewController.h"
#import "UBMPrefixHeader.h"
#import "UBMGetDeviceSomeFrame.h"
#import <WebKit/WebKit.h>

static NSString* const SptMessHnameGetSystemInfo = @"getSystemInfo";
static NSString* const SptMessHnameGetAutoStatus = @"getAutoStatus";
static NSString* const SptMessHnameSwitchAuto = @"switchAuto";
static NSString* const SptMessHnameOpenSetting = @"openSetting";

static CGFloat const kRequestTimeOutTime = 15.0;  //请求超时时间

typedef void(^sholudUpdateBlock)(void); //需要返回后刷新的回调

@interface UBMWebViewController () <WKScriptMessageHandler, WKUIDelegate, WKNavigationDelegate>
@property(nonatomic, strong) WKWebView *webView;
@property(nonatomic, copy)sholudUpdateBlock webBackBlock; //需要监听网页关闭的block。
@end

@implementation UBMWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
    self.zx_navStatusBarStyle = ZXNavStatusBarStyleDefault;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.zx_navItemMargin = 5;
    self.zx_navTitle = @"设置";
    
    __weak typeof(self) weakself = self;
    //默认在第二层级的web才显示返回按钮
    [self zx_setLeftBtnWithImg:[UIImage imageFromUbiBundleWithName:@"arrow_left"] clickedBlock:^(ZXNavItemBtn * _Nonnull btn) {
        [weakself webBack];
    }];
    
    self.webView.UIDelegate = self;
    self.webView.navigationDelegate = self;
    
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.url] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:kRequestTimeOutTime]];
}

#pragma mark - 返回方法
- (void)webBack {
    
    if(self.webView.canGoBack == YES){
        [self.webView goBack];
    }else{
        [self closeSelf];
    }
}

#pragma mark -关闭当前页面
- (void)closeSelf {
    if (self.presentingViewController) {
        __weak typeof(self) weakSelf = self;
        [self dismissViewControllerAnimated:YES completion:^{
            [weakSelf closedhadle];
        }];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark -页面关闭时的处理，回调和取消注册监听方法
- (void)closedhadle {
    NSLog(@"关闭web页面，取消注册监听");
    [self cancelMonitor]; //取消注册监听方法
}

#pragma mark -判断pop，处理回调。(pod和右滑返回都会调用此方法，而dismiss不会调用此方法，所以注意closedhadle)
- (void)didMoveToParentViewController:(UIViewController *)parent {
    [super didMoveToParentViewController:parent];
    if (!parent) {
        //parent==nil时 是 页面pop时的方法。在此回调是为了防止右划返回时的回调被忽略。
        [self closedhadle];
    }
}

//MARK:神策埋点标题
- (nonnull NSDictionary *)getTrackProperties {
    return @{@"$title":@"ios_h5_container"};
}

- (void)dealloc {
    [self cancelMonitor]; //取消注册监听方法
    NSLog(@"网页控制器被释放");
}

- (void)setup {
    [self setupUI];
    [self setupMonitor];
}

- (void)setupUI {
    [self.view addSubview:self.webView];
}

- (void)setupMonitor {
    [self.webView.configuration.userContentController addScriptMessageHandler:self name:SptMessHnameGetSystemInfo];
    [self.webView.configuration.userContentController addScriptMessageHandler:self name:SptMessHnameGetAutoStatus];
    [self.webView.configuration.userContentController addScriptMessageHandler:self name:SptMessHnameSwitchAuto];
    [self.webView.configuration.userContentController addScriptMessageHandler:self name:SptMessHnameOpenSetting];
}

- (void)cancelMonitor {
    [[self.webView configuration].userContentController removeScriptMessageHandlerForName:SptMessHnameGetSystemInfo];
    [[self.webView configuration].userContentController removeScriptMessageHandlerForName:SptMessHnameGetAutoStatus];
    [self.webView.configuration.userContentController removeScriptMessageHandlerForName:SptMessHnameSwitchAuto];
    [self.webView.configuration.userContentController removeScriptMessageHandlerForName:SptMessHnameOpenSetting];
}

#pragma mark - 懒加载UI
- (WKWebView *)webView {
    if (_webView == nil) {
        //进行配置控制器
        WKWebViewConfiguration *configuration = [[WKWebViewConfiguration alloc] init];
        //实例化对象
        configuration.userContentController = [[WKUserContentController alloc] init];
        configuration.allowsInlineMediaPlayback = YES; //允许页面播放视频
        
        WKPreferences *preferences = [[WKPreferences alloc] init];
        preferences.javaScriptCanOpenWindowsAutomatically = YES;
        configuration.preferences = preferences;
        
        _webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, HeightNavBar, ScreenWidth, ScreenHeight - HeightNavBar) configuration:configuration];
        [_webView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        [_webView setMultipleTouchEnabled:YES];
        [_webView setAutoresizesSubviews:YES];
        [_webView.scrollView setAlwaysBounceVertical:YES];
        [_webView setAllowsBackForwardNavigationGestures:true];
    }
    return _webView;
}

+ (void)jumpToWebViewWithUrl:(NSString *)url navigation:(UINavigationController *)nav {
    UBMWebViewController *vc = [[UBMWebViewController alloc] init];
    vc.url = url;
    vc.hidesBottomBarWhenPushed = YES;
    nav.navigationBar.hidden = NO;
    [nav pushViewController:vc animated:YES];
}

- (void)userContentController:(nonnull WKUserContentController *)userContentController didReceiveScriptMessage:(nonnull WKScriptMessage *)message {
    if ([message.name isEqualToString:SptMessHnameGetSystemInfo]) {
        [self getSystemInfo:[self handleJsDate:message.body][@"sn"]];
        
    } else if([message.name isEqualToString:SptMessHnameGetAutoStatus]){
        [self getAutoRecordSwitchStatus:[self handleJsDate:message.body][@"sn"]];
        
    } else if ([message.name isEqualToString:SptMessHnameSwitchAuto]) {
        [self turnAutoRecordSwitch:[self handleJsDate:message.body][@"sn"]];
        
    } else if([message.name isEqualToString:SptMessHnameOpenSetting]){
        [self openSetting:[self handleJsDate:message.body][@"sn"]];
    }
}

#pragma mark - 获得系统信息
- (void)getSystemInfo:(NSString *)jsMothed {
    NSString *brand = UIDevice.currentDevice.model;
    NSString *model = UIDevice.currentDevice.name;
    NSString *system = UIDevice.currentDevice.systemVersion;
    NSString *version = NSBundle.mainBundle.infoDictionary[@"CFBundleShortVersionString"];
    
    NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:brand, @"brand", model, @"model", version, @"version", system, @"system", nil];
    NSString* jsonStr = [UBMMTools JsonWithDictionary:dict];
    [self.webView evaluateJavaScript:[NSString stringWithFormat:@"%@('%@')", jsMothed, jsonStr] completionHandler:^(id xx,  NSError * _Nullable error) {
        if(error){
            NSLog(@"%@",error);
        }
    }];
}

#pragma mark - 自动记录开关控制
- (void)getAutoRecordSwitchStatus:(NSString *)jsMothed {
    NSNumber *sw = [[NSUserDefaults standardUserDefaults] objectForKey:UBMAutoRecordSwitchUDKey];
    if (!sw) {
        [[NSUserDefaults standardUserDefaults] setObject:@0 forKey:UBMAutoRecordSwitchUDKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        sw = @0;
    }
    NSDictionary* dict = [[NSDictionary alloc] initWithObjectsAndKeys:sw, @"code", nil];
    NSString* jsonStr = [UBMMTools JsonWithDictionary:dict];
    [self.webView evaluateJavaScript:[NSString stringWithFormat:@"%@('%@')", jsMothed, jsonStr] completionHandler:^(id xx,  NSError * _Nullable error) {
        if(error){
            NSLog(@"%@",error);
        }
    }];
}

- (NSNumber *)turnAutoRecordSwitch:(NSString *)jsMothed {
    NSNumber *sw = [[NSUserDefaults standardUserDefaults] objectForKey:UBMAutoRecordSwitchUDKey];
    sw = [sw integerValue] == 0 ? @1 : @0;
    [[NSUserDefaults standardUserDefaults] setObject:sw forKey:UBMAutoRecordSwitchUDKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if ([sw intValue] == 1) {
        [[UBITracking shared] startAutoRecord];
        [UBMSLSLog addUBMLog:@"ubmmian_startAutoRecord"];
    }else{
        [[UBITracking shared] stopAutoRecord];
        [UBMSLSLog addUBMLog:@"ubmmian_stopAutoRecord"];
    }
    
    NSDictionary* dict = [[NSDictionary alloc] initWithObjectsAndKeys:sw, @"code", nil];
    NSString* jsonStr = [UBMMTools JsonWithDictionary:dict];
    [self.webView evaluateJavaScript:[NSString stringWithFormat:@"%@('%@')", jsMothed, jsonStr] completionHandler:^(id xx,  NSError * _Nullable error) {
        if(error){
            NSLog(@"%@",error);
        }
    }];
    return sw;
}

- (void)openSetting:(NSString *)jsMothed {
    NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
    }
    NSDictionary* dict = [[NSDictionary alloc] initWithObjectsAndKeys:@"1", @"code", nil];
    NSString* jsonStr = [UBMMTools JsonWithDictionary:dict];
    [self.webView evaluateJavaScript:[NSString stringWithFormat:@"%@('%@')", jsMothed, jsonStr] completionHandler:^(id xx,  NSError * _Nullable error) {
        if(error){
            NSLog(@"%@",error);
        }
    }];
}

#pragma mark - 处理js携带的参数
- (NSDictionary*)handleJsDate:(id)data
{
    NSDictionary* dicx = [[NSDictionary alloc] init];
    if (data == nil) {
        return dicx;
    }
    
    NSString* strx;
    if([[NSString stringWithFormat:@"%@",[data class]] isEqualToString:@"__NSCFString"])
    {
        strx = [NSString stringWithFormat:@"%@", data];
    }
    
    if (IsEmptyStr(strx)) {
        return dicx;
    }
    
    NSError* error;
    NSData *jsonData = [strx dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
    
    return dic;
}

@end
