//
//  UBMBaseViewController.h
//  EngineeProject
//
//  Created by SuperMan on 2017/7/31.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ZXNavigationBar/ZXNavigationBarController.h>

@interface UBMBaseViewController : ZXNavigationBarController

/**
 通过NIB实例化(xib名=类名)
 
 @return self
 */
+ (instancetype)classInitWithNib;

/**
 *  是否支持右滑返回
 *
 *  @return YES=支持; NO=不支持(默认是yes)
 */
- (BOOL)gesturePopShouldBegin;

/**
 *  是否需要隐藏导航栏底部的线
 *
 *   YES=隐藏; NO=不隐藏(默认是NO)。注意：如果用到了viewWillAppear，则一定实现一下[super viewWillAppear]
 */
//@property(nonatomic, assign)BOOL HidenNaviGBsaeline;



@end
