//
//  UBMRouteInfoViewController.m
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/10/15.
//  Copyright © 2020 魏振伟. All rights reserved.
//

#import "UBMRouteInfoViewController.h"
//#import "RoutingViewController.h"

#import "UBMRouteInforView.h"
#import "UBMRouteShareView.h"
#import "UBMShareAlert.h"
#import "UBMPbFileManger.h"
#import "Route.pbobjc.h"
#import <MJExtension/MJExtensionConst.h>
#import "UBMRouterHistoryData.h"
#import "UBMEventExplainView.h"
#import "UBMPrefixHeader.h"
#import "UBMGetDeviceSomeFrame.h"
#import "UBMConfigure.h"
#import "UBITracking.h"

@interface UBMRouteInfoViewController ()<UBMRouteInforViewDelegate>

@property(nonatomic, strong)UBMRouteInforView* rv;  //view
@property(nonatomic, strong)UBMRouteShareView* rsv; //分享view
@property(nonatomic, strong)UIImage* mapImg;
@property(nonatomic, strong)UIImage* shaerImg;

@property(nonatomic, strong)NSDictionary* allData;

@property(nonatomic, assign)int pollingCount; //轮询次数


@end

@implementation UBMRouteInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.zx_navStatusBarStyle = ZXNavStatusBarStyleLight;
    self.zx_navBarBackgroundColor = ColorWithRGB(@"#222D55");
    self.zx_navBarBackgroundColorAlpha = 0;
    self.view.backgroundColor = ColorWithRGB(@"#222D55");
    __weak typeof(self) weakself = self;
    [self zx_setLeftBtnWithImg:[UIImage imageNamed:@"ubm_vcBackImg"] clickedBlock:^(ZXNavItemBtn * _Nonnull btn) {
        [weakself gotoBack];
    }];
    [self zx_setRightBtnWithImg:[UIImage imageNamed:@"ubm_shareImg"] clickedBlock:^(ZXNavItemBtn * _Nonnull btn) {
        [weakself shareAction];
    }];
    [self zx_setSubRightBtnWithImg:[UIImage imageNamed:@"ubm_question_answer"] clickedBlock:^(ZXNavItemBtn * _Nonnull btn) {
        [weakself questionAnswer];
    }];
    
    self.zx_navRightBtn.imageView.contentMode = UIViewContentModeCenter;
    self.zx_navRightBtn.zx_fixWidth = 44;
    self.zx_navRightBtn.zx_fixHeight = 44;
    self.zx_navSubRightBtn.imageView.contentMode = UIViewContentModeCenter;
    self.zx_navSubRightBtn.zx_fixWidth = 44;
    self.zx_navSubRightBtn.zx_fixHeight = 44;
    self.zx_navItemMargin = 10;
    
    [self setSomeViews];
    
    if (self.routingEnd == YES) {
        //走轮训逻辑
        [UBMWJProgressHUD showActivityIndicatorWithText:@"行程结束\n正在生成数据"];
        [self polling];

    }else{
        [self getTripInfor];
    }
    
    [UBMSLSLog addLog:@"InRoutInforVC"];
    
    NSLog(@"bbbbbb");
    
    //测试图表
//    NSString *path = [NSString stringWithFormat:@"%@/RouteTest.pb", [NSBundle mainBundle].bundlePath];
//    NSData *data = [NSData dataWithContentsOfFile:path];
//    Route* r1 = [Route parseFromData:data error:nil];
//    NSLog(@"r1 %lu",(unsigned long)r1.routePointsArray.count);
//    NSArray* pointarr = r1.routePointsArray;
//    //解析成能展示的点。
//    self.rv.sv.speedPointArray = pointarr;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

-(void)gotoBack{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)setSomeViews{
    UBMRouteInforView* rv = [[UBMRouteInforView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    rv.routeInforViewDelegate = self;
    rv.tripId = self.tripId;
    [self.view addSubview:rv];
    self.rv = rv;
}

#pragma mark - 问题反馈 RouteInforViewDelegate
-(void)fankui{
    NSString* parm = [NSString stringWithFormat:@"scene=trip&tripId=%@&phone=%@", self.tripId, UBMUserModel.shared.phone];
//    [UBMWebPageUrlManeger gotoFeedback:self.navigationController andParmString:parm];
}

#pragma mark - 分享
-(void)shareAction{
    
//    __weak typeof(self) weakself = self;
//    [self.rv.gv.mapView takeSnapshotInRect:CGRectMake(0, 0, self.rv.gv.mapView.width, self.rv.gv.mapView.height) withCompletionBlock:^(UIImage *resultImage, NSInteger state) {
//
//        if (resultImage) {
//            weakself.mapImg = resultImage;
//        }else{
//            NSLog(@"截图失败");
//        }
//    }];

//    [MobClick event:@"trip_share_click" attributes:[[NSDictionary alloc] initWithObjectsAndKeys:UserModel.shareUserModel.uid, @"uid", nil]];
}

#pragma mark - 安全隐患问答弹窗
-(void)questionAnswer {
    UBMEventExplainView* ev = [[UBMEventExplainView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    [ev show];
}

-(void)setMapImg:(UIImage *)mapImg{
    _mapImg = mapImg;
    
    self.rsv = nil;
    self.rsv = [[UBMRouteShareView alloc] initWithFrame:CGRectMake(0, 0, 375, 616+196.0) andMapview:mapImg andOtherData:self.rv.routeInforData];
    self.rsv.backgroundColor = UIColor.whiteColor;
    self.rsv.image = self.rv.scoreStampImageView.image;
    self.shaerImg = [UBMViewTools getmakeImageWithView:self.rsv andWithSize:CGSizeMake(self.rsv.width, self.rsv.height)];
    
    if (self.shaerImg == nil) {
        NSLog(@"图片为空");
        return;
    }
    
    __block UIView* shareBackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    shareBackView.backgroundColor = ColorSet(0, 0, 0, 0.5);
    UIImageView* img = [[UIImageView alloc] initWithFrame:CGRectMake(0, HeightStatusBar+10, ScreenWidth, ScreenHeight-254-20-(HeightStatusBar+10)-HeightSafeBottom)];
    img.contentMode = UIViewContentModeScaleAspectFit;
    img.image = self.shaerImg;
    [shareBackView addSubview:img];
    [[[UIApplication sharedApplication] delegate].window addSubview:shareBackView];
    
    //调试页面使用
//    UIScrollView* src = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
//    src.contentSize = CGSizeMake(ScreenWidth, self.rsv.height+20);
//    [src addSubview:self.rsv];
//    [self.view addSubview:src];
    
    //然后弹出分享提示框、截图
    __weak typeof(self) weakself = self;
    UBMShareAlert* sa = [[UBMShareAlert alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    sa.backgroundColor = UIColor.clearColor;
    sa.selectBlock = ^(int index) {
        
        //分享代码
//        if (index == 0) {
//
//            [ShareClass shareImg:weakself.shaerImg andType:0 andRes:^(BOOL success) {
//                if (success == YES) {
//                    [MobClick event:@"trip_share_to_wechat_succed" attributes:[[NSDictionary alloc] initWithObjectsAndKeys:UserModel.shareUserModel.uid, @"uid", nil]];
//                }
//            }];
//            [MobClick event:@"trip_share_to_wechat_click" attributes:[[NSDictionary alloc] initWithObjectsAndKeys:UserModel.shareUserModel.uid, @"uid", nil]];
//
//        }else{
//
//            [ShareClass shareImg:weakself.shaerImg andType:1 andRes:^(BOOL success) {
//                if (success == YES) {
//                    [MobClick event:@"trip_share_to_timeline_succed" attributes:[[NSDictionary alloc] initWithObjectsAndKeys:UserModel.shareUserModel.uid, @"uid", nil]];
//                }
//            }];
//            [MobClick event:@"trip_share_to_timeline_click" attributes:[[NSDictionary alloc] initWithObjectsAndKeys:UserModel.shareUserModel.uid, @"uid", nil]];
//        }
    };
    sa.dismissBlock = ^{
        [shareBackView removeFromSuperview];
    };
    [sa show];
    
//    [MobClick event:@"trip_share_show" attributes:[[NSDictionary alloc] initWithObjectsAndKeys:UserModel.shareUserModel.uid, @"uid", nil]];
}

#pragma mark - 数据请求
-(void)getTripInfor{
    
    if (self.routingEnd != YES) {
        [UBMWJProgressHUD show];
    }
    
    NSDictionary* testP = @{@"tripId":self.tripId, @"userId":[UBITracking shared].userID};
    [[UBMNetWorkTool shareNetWorkTool] requestWithBaseUrl:[UBMDomainName.shareDomain ubmURL] UrlString:UBMTripInofr parameters:testP method:@"POST" success:^(id response) {
    
        if (response && [response[@"code"] intValue]==1) {
            
            self.allData = response[@"data"];
            
            [self jugeRoute_points_oss_key:self.allData[@"route_oss_key"]];
            
            //存
            [[UBMRouterHistoryData shareRouterHistoryData].tripInfor setObject:response[@"data"] forKey:self.tripId];
            
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [UBMWJProgressHUD dismiss];
                NSString* toast = [NSString stringWithFormat:@"%@", response[@"msg"]];
                Toast(toast);
                [self.navigationController popViewControllerAnimated:YES];
            });
        }
        
        
    } error:^(id response) {
        [UBMWJProgressHUD dismiss];
        
        //没网时取
        if ([[UBMRouterHistoryData shareRouterHistoryData].tripInfor objectForKey:self.tripId]) {
            
            self.allData = [[UBMRouterHistoryData shareRouterHistoryData].tripInfor objectForKey:self.tripId];
            [self jugeRoute_points_oss_key:self.allData[@"route_oss_key"]];
            
        }else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        }
        
    } isShowHUD:NO];
}

//MARK:代理 去设置
- (void)goAutoRecordSettings{
//    [UBMWebPageUrlManeger gotoAutoRecordSettings:self.navigationController resultBlock:^{
//        
//    }];
}

//MARK:代理 去收集
- (void)goCollect{
//    NSString * jump_url = [NSString stringWithFormat:@"%@%@",BaseUrl,Carbon];
//    [PageJumpManager jumpPageWithUrl:jump_url];
}

//MARK:代理 行程打标
- (void)requstMark{
    NSMutableDictionary * params = @{}.mutableCopy;
    params[@"userId"] = [UBITracking shared].userID;
    params[@"tripId"] = self.tripId;
    params[@"type"] = @"1";//非自驾行程
    
    __weak typeof(self) weakSelf = self;
    [[UBMNetWorkTool shareNetWorkTool] requestWithBaseUrl:[UBMDomainName.shareDomain ubmURL] UrlString:UBMTripMark parameters:params method:@"POST" success:^(id response) {
        [weakSelf getTripInfor];
        if (weakSelf.markSuccess) {
            weakSelf.markSuccess(weakSelf.tripId);
        }
        Toast(@"已将该行程标记为非驾驶行程");
    } error:^(id response) {
        
    } isShowHUD:YES];
}

-(void)setAllData:(NSDictionary *)allData{
    _allData = allData;
    //设置view
    self.rv.routeInforData = allData;
    
    NSString* coordinatesType = [NSString stringWithFormat:@"%@",allData[@"coordinates"]];
    if ([coordinatesType isEqualToString:@"GCJ02"]) {
        self.rv.gv.coordinatesType = @"GCJ02";
    }else{
        self.rv.gv.coordinatesType = @"WGS84";
    }
    if (allData[@"events"] && [allData[@"events"] isKindOfClass:[NSArray class]] && [(NSArray*)allData[@"events"] count]>0) {
        self.rv.gv.eventArray = allData[@"events"];
    }
    self.rv.gv.endPoint = allData[@"end_point"];
    self.rv.gv.statrtPoint = allData[@"start_point"];

}

#pragma mark - 地图 曲线 数据解析和存储
-(void)jugeRoute_points_oss_key:(NSString*)key{
    
    //先判断本地有没有，没有则在请求下来后存入文件。有则直接取。
    
    if (IsEmptyStr(key)) {
        [UBMWJProgressHUD dismiss];
        self.rv.hiddenSpeedChartView = YES;
        NSLog(@"路径点 oss地址为空，返回");
        return;
    }
    
    NSString* fileName = [NSString stringWithFormat:@"Route_%@.pb", self.tripId];
    NSString* path2 = [NSString stringWithFormat:@"%@/%@/%@",[UBMPbFileManger cachesBasePath], @"UBM_Route",fileName];
    
    if ([[UBMPbFileManger sizeOfFileAtPath:path2] isEqualToString:@"0"]) {
        //没有文件，需要下载。下载后存储。
        
        __weak typeof(self) weakself = self;
        
        [[UBMNetWorkTool shareNetWorkTool] downLoadFileOfPath:key success:^(id response) {
            
            //存储
            NSData * data = response;
            [UBMPbFileManger commenCareatFilewithName:fileName andContend:data andFolderName:@"UBM_Route"];
            if (weakself) {
                [weakself openLocFile:data];
            }
            
            [UBMWJProgressHUD dismiss];
            
        } error:^(id response) {
            [UBMWJProgressHUD dismiss];
            NSLog(@"路线文件下载失败，请稍后再试哦");
            self.rv.hiddenSpeedChartView = YES;
        }];
        
    } else{
        
        //测试文件
//        NSString *path = [NSString stringWithFormat:@"%@/RouteTest2.pb", [NSBundle mainBundle].bundlePath];
//        NSData *data = [NSData dataWithContentsOfFile:path];
        
        NSData* data = [NSData dataWithContentsOfFile:path2];
        
        [self openLocFile:data];
        
        [UBMWJProgressHUD dismiss];
    }
    
}

-(void)openLocFile:(NSData*)data{
    
    Route* r1 = [Route parseFromData:data error:nil];
    
    //转换经纬度、拼接经纬度点、划线。
    NSArray* pointarr = r1.routePointsArray;
    
    if (pointarr && pointarr.count>0) {
        if (r1.coordinates && r1.coordinates==Coordinates_Gcj02) {
            self.rv.gv.linePointCoordinates = 1;
        }else{
            self.rv.gv.linePointCoordinates = 0;
        }
        self.rv.gv.linePointArray = pointarr;
        self.rv.speedChartView.maxSpeed = [self.allData[@"max_speed_in_kilometers_per_hour"] floatValue];
        self.rv.speedChartView.speedPointArray = pointarr;
    }else{
        
        self.rv.hiddenSpeedChartView = YES;
        
    }
    
}

#pragma mark - 刚结束时的轮训逻辑。与正常逻辑区分开了。
-(void)polling{
    
    [[UBMNetWorkTool shareNetWorkTool] requestWithBaseUrl:[UBMDomainName.shareDomain ubmURL] UrlString:UBMTripInofr parameters:@{@"tripId":self.tripId, @"userId":[UBITracking shared].userID, @"polling":@"1"} method:@"POST" success:^(id response) {
    
        //合并结束，有了结果。
        if (response && [response[@"code"] intValue]==1) {
            
            [self getTripInfor];
            [[NSNotificationCenter defaultCenter] postNotificationName:UBMNewTripOverNotification object:nil];
            
        } else if (response && [response[@"code"] intValue]== 100001){
            
            //100001 继续轮循
            self.pollingCount = self.pollingCount+1;
            
            if (self.pollingCount >= 10) {
                NSLog(@"详情 请求次数太多，接口太慢了");
                Toast(@"网络请求稍微有点慢哦，请稍后重试");
                
                [UBMWJProgressHUD dismiss];
                [self.navigationController popViewControllerAnimated:YES];
                
            }else{
                
                //前五次一秒轮训。后五次两秒轮训。
                int num = 1;
                if (self.pollingCount>5) {
                    num = 2;
                }
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(num * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self polling];
                });
            }
            
        }else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [UBMWJProgressHUD dismiss];
                NSString* msg = ChangeToString(response[@"msg"]);
                Toast(msg);
                [self.navigationController popViewControllerAnimated:YES];
            });
        }
        
    } error:^(id response) {
        
        [UBMWJProgressHUD dismiss];
        [self.navigationController popViewControllerAnimated:YES];
        
    } isShowHUD:NO];
    
}

//MARK:神策埋点标题
- (NSDictionary *)getTrackProperties{
    return @{@"$title":@"ubm_details_page"};
}

-(void)dealloc{
    NSLog(@"详情被释放啦");
}

@end
