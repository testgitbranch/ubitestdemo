//
//  UBMBaseViewController.m
//  EngineeProject
//
//  Created by SuperMan on 2017/7/31.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "UBMBaseViewController.h"

@interface UBMBaseViewController ()
@property(nonatomic, strong)UIImageView *navBarHairlineImageView;//导航条底部线
@end

@implementation UBMBaseViewController

#pragma mark - init
+ (instancetype)classInitWithNib
{
    NSString *nibName = NSStringFromClass(self);
    return [[self alloc] initWithNibName:nibName bundle:nil];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


#pragma mark - 使用自定义导航栏时的一些默认设置。现项目中基本都使用自定义导航栏。每个vc都有自己的导航栏view，互不影响。避免大部分导航栏问题。
-(void)viewDidLoad{
    [super viewDidLoad];
    
    self.view.backgroundColor = UIColor.whiteColor;
    
    self.zx_navTitleColor = UIColor.blackColor;
    self.zx_navTitleFont = [UIFont systemFontOfSize:17 weight:UIFontWeightRegular];
    self.zx_navLineView.hidden = YES;
    
    if(self.navigationController.viewControllers.count>1){
        UIImage* lefImg = [UIImage imageNamed:@"arrow_left"];
        
        self.zx_navLeftBtn.zx_fixWidth = 44;
        self.zx_navLeftBtn.zx_fixHeight = 44;
        self.zx_navLeftBtn.imageView.contentMode = UIViewContentModeCenter;
        //self.zx_navLeftBtn.zx_fixImageSize = CGSizeMake(lefImg.size.width, lefImg.size.height);
//        self.zx_navLeftBtn.backgroundColor = UIColor.orangeColor;
        
        //默认左按钮是黑色
        __weak typeof(self) weakself = self;
        [self zx_setLeftBtnWithImg:lefImg clickedBlock:^(ZXNavItemBtn * _Nonnull btn) {
            [weakself.navigationController popViewControllerAnimated:YES];
        }];
        //self.zx_backBtnImageName = @"arrow_left";
    }
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
//    [MobClick beginLogPageView:[NSString stringWithFormat:@"%@", self.class]];
    
    //判断是否当前为导航控制器的最后一层，如果是，则判断是否需要进行tabbar重新排列
//    [UBMPreparClass.sharePreparClass handleTabbarResresh];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
//    [MobClick endLogPageView:[NSString stringWithFormat:@"%@", self.class]];
}

#pragma mark - 是否支持右滑返回 YES=支持; NO=不支持(默认是yes)
- (BOOL)gesturePopShouldBegin {
    if (self.navigationController && self.navigationController.viewControllers.count>1) {
        return YES;
    }
    
    return NO;
}

-(void)dealloc{
    NSLog(@"%@ 被释放", [NSString stringWithFormat:@"%@", self.class]);
}

#pragma mark - 使用系统导航栏时的一些基础设置，可以再创建一个使用系统导航栏的basevc。

/*
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if(self.navigationController.viewControllers.count>1){
        [self setLeftBarButtonOfDefault:nil];
    }
    
//    self.tabBarController.tabBar.hidden = YES;
    [self.navigationController setNavigationBarHidden:NO];  //默认显示导航栏
    
    [self.view setExclusiveTouch:YES];
    self.view.backgroundColor = [UIColor whiteColor];

    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    [self setNavigationBarWithColor:[UIColor clearColor]];
    [self setTitleColor:[UIColor blackColor]];
    
    
    self.HidenNaviGBsaeline = YES;
    _navBarHairlineImageView = [self findHairlineImageViewUnder:self.navigationController.navigationBar];

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (@available(iOS 13, *)) {
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDarkContent;
    }else{
       [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    }
    
    
    if(self.HidenNaviGBsaeline == YES)
        _navBarHairlineImageView.hidden = YES;
    else
        _navBarHairlineImageView.hidden = NO;
    
    [MobClick beginLogPageView:[NSString stringWithFormat:@"%@", self.class]];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [MobClick endLogPageView:[NSString stringWithFormat:@"%@", self.class]];
}

#pragma mark - 是否支持右滑返回 YES=支持; NO=不支持(默认是yes)
- (BOOL)gesturePopShouldBegin {
    if (self.navigationController && self.navigationController.viewControllers.count>1) {
        return YES;
    }
    
    return NO;
}

#pragma mark - 找到系统导航栏底部的线
- (UIImageView *)findHairlineImageViewUnder:(UIView *)view
{
    if ([view isKindOfClass:UIImageView.class] && view.bounds.size.height <= 1.0) {
        return (UIImageView *)view;
    }
    for (UIView *subview in view.subviews) {
        UIImageView *imageView = [self findHairlineImageViewUnder:subview];
        if (imageView) {
            return imageView;
        }
    }
    return nil;
}
*/


@end




