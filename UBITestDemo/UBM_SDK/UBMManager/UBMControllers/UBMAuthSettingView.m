//
//  UBMAuthSettingView.m
//  UBITestDemo
//
//  Created by 金鑫 on 2021/12/21.
//

#import "UBMAuthSettingView.h"

static CGFloat const kTitleHeight = 60;
static CGFloat const kDescHeight = 120;
static CGFloat const kBtnWidth = 90;
static CGFloat const kBtnHeight = 60;

@implementation UBMAuthSettingView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup {
    [self setupUI];
    [self setupLayout];
    [self setupAppearce];
    [self setupNotifications];
}

- (void)setupUI {
    [self addSubview: self.titleLab];
    [self addSubview: self.descLab];
    [self addSubview: self.settingBtn];
}

- (void)setupLayout {
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.titleLab.superview);
        make.height.mas_equalTo(kTitleHeight);
    }];
    
    [self.descLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLab.mas_bottom);
        make.left.right.equalTo(self.titleLab);
        make.height.mas_equalTo(kDescHeight);
    }];
    
    [self.settingBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.descLab.mas_bottom).offset(40);
        make.centerX.equalTo(self.settingBtn.superview);
        make.width.mas_equalTo(kBtnWidth);
        make.height.mas_equalTo(kBtnHeight);
        make.bottom.equalTo(self.settingBtn.superview.mas_bottom).offset(-30);
    }];
}

- (void)setupAppearce {
    self.titleLab.backgroundColor = UIColor.clearColor;
    self.descLab.backgroundColor = UIColor.clearColor;
    self.settingBtn.backgroundColor = UIColor.lightGrayColor;
}

- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.textAlignment = NSTextAlignmentCenter;
    }
    return _titleLab;
}

- (UILabel *)descLab {
    if (!_descLab) {
        _descLab = [[UILabel alloc] init];
        _descLab.textAlignment = NSTextAlignmentCenter;
    }
    return _descLab;
}

- (UIButton *)settingBtn {
    if (!_settingBtn) {
        _settingBtn = [UIButton buttonWithType:UIButtonTypeCustom];
         [_settingBtn addTarget:self action:@selector(setting) forControlEvents:UIControlEventTouchUpInside];
    }
    return _settingBtn;
}

- (void)setting {
    NSLog(@"setting");
    if (self.callBack) {
        self.callBack();
    }
}

- (void)setupNotifications {
    
}

@end
