//
//  UBMAuthSettingView.h
//  UBITestDemo
//
//  Created by 金鑫 on 2021/12/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^UBMAuthSettingCallBack)(void);

@interface UBMAuthSettingView : UIView
@property (nonatomic, copy) UBMAuthSettingCallBack callBack; 
@property (nonatomic, strong) UILabel *titleLab;
@property (nonatomic, strong) UILabel *descLab;
@property (nonatomic, strong) UIButton *settingBtn;
@end

NS_ASSUME_NONNULL_END
