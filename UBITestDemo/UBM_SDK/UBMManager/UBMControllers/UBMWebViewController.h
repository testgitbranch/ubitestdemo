//
//  UBMWebViewController.h
//  LoveCarChannel_iOS
//
//  Created by 金鑫 on 2021/11/25.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

#import "UBMBaseViewController.h"
#import <WebKit/WebKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UBMWebViewController : UBMBaseViewController
@property(nonatomic, copy) NSString *url;

+ (void)jumpToWebViewWithUrl:(NSString *)url navigation:(UINavigationController *)nav;
@end

NS_ASSUME_NONNULL_END
