//
//  UBMAuthSettingController.m
//  UBITestDemo
//
//  Created by 金鑫 on 2021/12/21.
//

#import "UBMAuthSettingController.h"
#import "UBMAuthSettingView.h"
#import "UBITestDemo-Swift.h"

@interface UBMAuthSettingController ()
@property (nonatomic, strong) UBMGetStateOfLimit *auth;
@property (nonatomic, strong) UBMAuthSettingView *locationAuthView;
@property (nonatomic, strong) UBMAuthSettingView *activityAuthView;
@end

@implementation UBMAuthSettingController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
}

- (void)setup {
    [self setupUI];
    [self setupLayout];
    [self setupAppearce];
}

- (void)setupUI {
    [self.view addSubview: self.locationAuthView];
    [self.view addSubview: self.activityAuthView];
    [self setupNavigationBar];
}

- (UBMGetStateOfLimit *)auth {
    if (!_auth) {
        _auth = [[UBMGetStateOfLimit alloc] init];
    }
    return _auth;
}

- (UBMAuthSettingView *)locationAuthView {
    if (!_locationAuthView) {
        _locationAuthView = [[UBMAuthSettingView alloc] init];
        _locationAuthView.titleLab.text = @"定位权限";
        _locationAuthView.descLab.text = @"记录行程需要您开启定位权限";
        [_locationAuthView.settingBtn setTitle:@"获取权限" forState:UIControlStateNormal];
        @bx_weakify(self)
        _locationAuthView.callBack = ^{
            @bx_strongify(self)
            if ([UBMGetStateOfLimit needShowLocationPremission] == YES) {
                [self.auth applyLocationPermission];
            } else {
                [self.auth gotoSettings];
            }
        };

    }
    return _locationAuthView;
}

- (UBMAuthSettingView *)activityAuthView {
    if (!_activityAuthView) {
        _activityAuthView = [[UBMAuthSettingView alloc] init];
        _activityAuthView.titleLab.text = @"运动与健康权限";
        _activityAuthView.descLab.text = @"记录行程需要您开启运动与健康权限";
        [_activityAuthView.settingBtn setTitle:@"获取权限" forState:UIControlStateNormal];
        @bx_weakify(self)
        _activityAuthView.callBack = ^{
            @bx_strongify(self)
            if ([UBMGetStateOfLimit needShowHealthServicesPremission] == YES) {
                [self.auth applyHealthPermission];
            } else {
                [self.auth gotoSettings];
            }
        };
    }
    return _activityAuthView;
}

- (void)setupNavigationBar {
    self.zx_navTitle = @"权限设置";
    
    __weak typeof(self) weakself = self;
    //默认在第二层级的web才显示返回按钮
    [self zx_setLeftBtnWithImg:[UIImage imageFromUbiBundleWithName:@"arrow_left"] clickedBlock:^(ZXNavItemBtn * _Nonnull btn) {
        [weakself.navigationController popViewControllerAnimated:YES];
    }];
}

- (void)setupLayout {
    [self.locationAuthView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.locationAuthView.superview).offset(HeightNavBar);
        make.left.right.equalTo(self.locationAuthView.superview);
    }];
    
    [self.activityAuthView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.locationAuthView.mas_bottom).offset(20);
        make.left.right.equalTo(self.activityAuthView.superview);
    }];
}

- (void)setupAppearce {
    self.locationAuthView.backgroundColor = [UIColor ubmColorWithHexString:@"#f5f5f5"];
    self.activityAuthView.backgroundColor = [UIColor ubmColorWithHexString:@"#f5f5f5"];
}

@end
