//
//  UBMRouteInfoViewController.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/10/15.
//  Copyright © 2020 魏振伟. All rights reserved.
//

/*
 行程详情
 */

#import "UBMBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface UBMRouteInfoViewController : UBMBaseViewController

@property(nonatomic, assign)BOOL routingEnd;//是从收集中过来的

@property(nonatomic, strong)NSString* tripId;

///打标成功回调
@property(nonatomic,copy)void(^markSuccess)(NSString * tripId);

@end

NS_ASSUME_NONNULL_END
