//
//  UBMRouteHistoryController.m
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/10/17.
//  Copyright © 2020 魏振伟. All rights reserved.
//

#import "UBMRouteHistoryController.h"
#import "UBMRhTableViewCell.h"
#import "UBMRhHearView.h"
#import "MJRefreshAutoNormalFooter.h"
#import "UBMRouteInfoViewController.h"
#import "UBMRouterHistoryData.h"
#import "UBMPrefixHeader.h"
#import "UBMUrls.h"
#import "UBITracking.h"
#import "UBMRouterHistoryData.h"
#import "UBMGetDeviceSomeFrame.h"
#import "UBMConfigure.h"
#import "UBMVendorTool.h"
#import "UBMPrefixHeader.h"
//#import <UBM_SDK/UBM_SDK-Swift.h>

@interface UBMRouteHistoryController()<UITableViewDelegate, UITableViewDataSource, UBMRhHearViewDelegate>

@property(nonatomic, strong)UITableView* mainTableView;
@property(nonatomic, strong)UBMRhHearView* rv;
@property(nonatomic, strong)UBMNoDataPlaceholderView* noDataView;
@property(nonatomic, strong)UBMLoadFailedPlacehoderView* loadFailedView;

@property(nonatomic, strong)NSMutableArray* tripList;

@property(nonatomic, strong)NSArray* notEndTripList;

@property(nonatomic, strong)NSDictionary* tripStatisticsData;//日数据
@property(nonatomic, strong)NSDictionary* tripStatisticsWeekData;//周数据
@property(nonatomic, strong)NSDictionary* tripStatisticsMonthData;//月数据
@property(nonatomic, assign)int tripStatisticsType; //1日.2周.3月
@property(nonatomic, copy)NSString* startTime; //列表请求条件之一：开始时间

@end

@implementation UBMRouteHistoryController


- (void)viewDidLoad{
    [super viewDidLoad];
    
    self.zx_navTitle = @"历史行程";
    self.zx_navStatusBarStyle = ZXNavStatusBarStyleLight;
    self.zx_navTitleColor = UIColor.whiteColor;
    self.zx_navBarBackgroundColor = ColorWithRGB(@"#222D55");
    self.view.backgroundColor = ColorWithRGB(@"#222D55");
    __weak typeof(self) weakself = self;
    [self zx_setLeftBtnWithImg:[UIImage imageFromUbiBundleWithName:@"arrow_leftWhite"] clickedBlock:^(ZXNavItemBtn * _Nonnull btn) {
        [weakself gotoBack];
    }];
    
//    [self zx_setRightBtnWithText:@"驾驶周报" clickedBlock:^(ZXNavItemBtn * _Nonnull btn) {
////        UBMWeeklyDetailViewController * weeklyVC = [[UBMWeeklyDetailViewController alloc] init];
////        [weakself.navigationController pushViewController:weeklyVC animated:YES];
//    }];
    
    self.startTime = @"";
    self.tripList = [[NSMutableArray alloc] init];
    
    UITableView* mainTablview = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight) style:UITableViewStyleGrouped];
    mainTablview.backgroundColor = ColorWithRGB(@"#f5f5f5");
    mainTablview.delegate = self;
    mainTablview.dataSource = self;
    mainTablview.separatorStyle = UITableViewCellSeparatorStyleNone;
    mainTablview.showsVerticalScrollIndicator = NO;
    [self.view addSubview:mainTablview];
    MJRefreshAutoNormalFooter* mf = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(getMoreData)];
    [mf setTitle:@"加载中" forState:MJRefreshStateRefreshing];
    [mf setTitle:@"" forState:MJRefreshStateIdle];
    [mf setTitle:@"加载中" forState:MJRefreshStatePulling];
    [mf setTitle:@"" forState:MJRefreshStateWillRefresh];
    [mf setTitle:@"—— 安全驾驶 恪守底线 ——" forState:MJRefreshStateNoMoreData];
    mf.stateLabel.textColor = ColorSet(0, 0, 0, 0.45);
    mf.stateLabel.font = [UIFont systemFontOfSize:13];
    mainTablview.mj_footer = mf;
    self.mainTableView = mainTablview;
    
    
    self.tripStatisticsType = 0;//开始给个0，是为了让接口返回默认类型。
    
    //[self getMoreData];
    //在日期筛选数据请求成功之后，会带着默认条件进行刷新列表。所以没必要多一次默认请求了。
    //如果放开，需要考虑接口返回顺序的问题，防止数据重复被添加。
    [self getTripStatistics:[NSString stringWithFormat:@"%d",self.tripStatisticsType] block:^{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.navigationController popViewControllerAnimated:YES];
        });
        
    }];
    [self getNotEndList:NO];
    
    //[MobClick event:@"trip_history_show" attributes:[[NSDictionary alloc] initWithObjectsAndKeys:[UBITracking shared].userID, @"uid", nil]];
    
    [UBMSLSLog addLog:@"InRoutHistoryVC"];
    
//    [BXUMLog addLogWithButtonid:@"trip_history_page" sorcePage:@"trip_history_page" action:UMActionShow];
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

-(void)gotoBack{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)dealloc{
    NSLog(@"列表释放");
}

-(UBMNoDataPlaceholderView *)noDataView{
    if (!_noDataView) {
        _noDataView = [[UBMNoDataPlaceholderView alloc] init];
        _noDataView.frame = CGRectMake(0, 0, ScreenWidth, 370.0);
        //[self.mainTableView addSubview:_noDataView];
    }
    //_noDataView.y = self.rv.height;
    return _noDataView;
}

-(UBMLoadFailedPlacehoderView *)loadFailedView{
    if (!_loadFailedView) {
        _loadFailedView = [[UBMLoadFailedPlacehoderView alloc] init];
        @bx_weakify(self);
        _loadFailedView.loadFailedTapBlock = ^{
            [weak_self getMoreData];
        };
        _loadFailedView.frame = CGRectMake(0, 0, ScreenWidth, 370.0);
        //[self.mainTableView addSubview:_loadFailedView];
    }
    //_loadFailedView.y = self.rv.height;
    return _loadFailedView;
}

#pragma mark - tableviewdelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return HeightNavBar+self.rv.height;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        UIView* headview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, self.rv.height+HeightNavBar)];
        headview.backgroundColor = ColorWithRGB(@"#222D55");
        [headview addSubview:self.rv];
        headview.layer.masksToBounds = YES;
        return headview;
    }
    return nil;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.tripList.count == 0) {
        return 1;
    }else
        return self.tripList.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.tripList.count == 0){
        return 370.0;
    }else{
        CGFloat height = 168;
        NSDictionary * dict = self.tripList[indexPath.row];
        if ([dict[@"is_normal"] integerValue] == 1) {
            //非自己驾驶的打标的行程
            if ([dict[@"not_driver"] integerValue] != 1) {
                height = 190;
            }
        }
        return height;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.tripList.count == 0){
        UITableViewCell* cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"NoDataCell"];
        [cell.contentView addSubview:self.noDataView];
        [cell.contentView addSubview:self.loadFailedView];
        cell.contentView.backgroundColor = ColorWithRGB(@"#f5f5f5");
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
    UBMRhTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"RHCell"];
    if (cell == nil) {
        cell = [[[UBMMTools ubmBundle] loadNibNamed:@"UBMRHTableViewCell" owner:nil options:nil] lastObject];
    }
    if(self.tripList && self.tripList.count>indexPath.row){
        cell.dict = self.tripList[indexPath.row];
    }else{
        return cell;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.tripList.count == 0) {
        return;
    }
    
    NSDictionary* item = self.tripList[indexPath.row];
    
   @bx_weakify(self);
    UBMRouteInfoViewController* rv = [[UBMRouteInfoViewController alloc] init];
    rv.tripId = item[@"trip_id"];
    rv.markSuccess = ^(NSString * _Nonnull tripId) {
        [weak_self reloadCellOfTripID:tripId];
    };
    [self.navigationController pushViewController:rv animated:YES];
    
//    [MobClick event:@"trip_detail_click" attributes:[[NSDictionary alloc] initWithObjectsAndKeys:UserModel.shareUserModel.uid, @"uid", nil]];
//
//    [MobClick event:@"trip_detail_show" attributes:@{@"source":@"history", @"uid":[NSString stringWithFormat:@"%@", UserModel.shareUserModel.uid]}];
}


-(UBMRhHearView *)rv{
    if (_rv == nil) {
        _rv = [[UBMRhHearView alloc] initWithFrame:CGRectMake(0, HeightNavBar, ScreenWidth, 371.0+180+68)];
        _rv.backgroundColor = ColorWithRGB(@"#222D55");
        _rv.delegate = self;
    }
    
    return _rv;
}

-(void)heightChangeNotifay{
    //刷新头视图
    [self.mainTableView reloadData];
}

#pragma mark - 上拉加载更多数据
-(void)getMoreData{
    
    self.noDataView.hidden = YES;
    self.loadFailedView.hidden = YES;
    
    
    int page = 1;
    int size = 10;
    if (self.tripList.count > 0) {
        //计算页码
        page = (int)self.tripList.count/10+1;
    }
    
    NSString* carId = [[NSUserDefaults standardUserDefaults] objectForKey:UBMCarIdUDKey];
    if (carId == nil) {
        carId = @"";
    }
    
    [[UBMNetWorkTool shareNetWorkTool] requestWithBaseUrl:[UBMDomainName.shareDomain ubmURL] UrlString:UBMTripList parameters:@{@"userId":[UBITracking shared].userID, @"pageIndex":[NSString stringWithFormat:@"%d",page], @"pageSize":[NSString stringWithFormat:@"%d",size], @"startTime":self.startTime, @"type":[NSString stringWithFormat:@"%d",self.tripStatisticsType]} method:@"POST" success:^(id response) {
        
        
        if (response && [response[@"code"] intValue]==1) {
            
            NSArray* dataArr = response[@"data"];
            if (dataArr && [dataArr isKindOfClass:[NSArray class]] && dataArr.count>0) {
                
                [self.tripList addObjectsFromArray:dataArr];
                [self.mainTableView reloadData];
                
                if (page == 1) {
                    //内存缓存起来
                    [UBMRouterHistoryData shareRouterHistoryData].tripList = self.tripList;
                }
                
                //没有更多数据了
                if (dataArr.count%10 > 0) {
                    [self.mainTableView.mj_footer endRefreshingWithNoMoreData];
                }else{
                   [self.mainTableView.mj_footer endRefreshing];
                }
                
            }else{
                [self.mainTableView.mj_footer endRefreshingWithNoMoreData];
                
                //显示出无行程数据展位图。
                self.noDataView.hidden = NO;
                [self.mainTableView reloadData];
            }
            
        }else{
            [self.mainTableView.mj_footer endRefreshingWithNoMoreData];
            
            //显示出加载失败占位图
            self.loadFailedView.hidden = NO;
            [self.mainTableView reloadData];
        }
        
        
    } error:^(id response) {
        [self.mainTableView.mj_footer endRefreshing];
        
        //取内存缓存数据
        if (page == 1) {
            [self.tripList removeAllObjects];
            if ([UBMRouterHistoryData shareRouterHistoryData].tripList.count>0) {
                [self.tripList addObjectsFromArray:[UBMRouterHistoryData shareRouterHistoryData].tripList];
                [self.mainTableView reloadData];
            }
            
            if (!self.tripList || self.tripList.count == 0) {
                
                //显示出加载失败占位图
                self.loadFailedView.hidden = NO;
                [self.mainTableView reloadData];
            }
        }
        
        
    } isShowHUD:YES];
    
    
}

-(void)reloadDataWithStartTime:(NSString*)startTime{
    self.startTime = startTime;
    
    //清空数据并请求新的数据
    [UBMRouterHistoryData shareRouterHistoryData].tripList = NSArray.array;
    [self.tripList removeAllObjects];
    [self getMoreData];
}

#pragma mark - 打标后信息同步，刷新对应cell
-(void)reloadCellOfTripID:(NSString*)tripId{
    for (int i=0; i<self.tripList.count; i++) {
        NSDictionary* item = self.tripList[i];
        if ([item[@"trip_id"] isEqualToString:tripId]) {
            NSMutableDictionary* dicx = [[NSMutableDictionary alloc] initWithDictionary:item];
            [dicx setObject:@"1" forKey:@"not_driver"];
            [self.tripList replaceObjectAtIndex:i withObject:dicx];
            [self.mainTableView reloadData];
            break;
        }
    }
}

#pragma mark - 选中了某个时间，根据时间刷新列表数据。
-(void)didSelectTime:(NSString *)startTime{
    [self reloadDataWithStartTime:startTime];
}

#pragma mark - 日月周数据
-(void)tripStatisticsTypeChange:(int)index{
    
     self.tripStatisticsType = index;
    
    //存在就不再请求，直接刷新。
    if (index == 1 && _tripStatisticsData) {
        self.rv.tripStatisticsData = _tripStatisticsData;
        
    }else if((index == 2 && _tripStatisticsWeekData)){
        self.rv.tripStatisticsData = _tripStatisticsWeekData;
//        [MobClick event:@"trip_filter_by_week_click" attributes:[[NSDictionary alloc] initWithObjectsAndKeys:UserModel.shareUserModel.uid, @"uid", nil]];
        
    }else if((index == 3 && _tripStatisticsMonthData)){
        self.rv.tripStatisticsData = _tripStatisticsMonthData;
//        [MobClick event:@"trip_filter_by_month_click" attributes:[[NSDictionary alloc] initWithObjectsAndKeys:UserModel.shareUserModel.uid, @"uid", nil]];
        
    }else{
        
        [self getTripStatistics:[NSString stringWithFormat:@"%d", index] block:nil];
    }
    
}

-(void)getTripStatistics:(NSString*)type block:(void(^)(void))errorResult{
    
    @bx_weakify(self);
    //type 1日 2周 3月
    NSDictionary* pardic = @{@"userId":[UBITracking shared].userID, @"type":type};
    if ([type intValue] == 0) {
        pardic = @{@"userId":[UBITracking shared].userID};
    }
    
    [[UBMNetWorkTool shareNetWorkTool] requestWithBaseUrl:[UBMDomainName.shareDomain ubmURL] UrlString:UBMTripStatistics parameters:pardic method:@"POST" success:^(id response) {
        
        @bx_strongify(self);
        if (response && [response[@"code"] intValue]==1) {
            
            //如果有默认类型，则选择默认数据（日周月）
            if (self.tripStatisticsType == 0 && !IsEmptyStr(ChangeToString(response[@"data"][@"default_type"]))) {
                self.tripStatisticsType = [response[@"data"][@"default_type"] intValue];
            }
            
            if (self.tripStatisticsType == 1) {
                self.tripStatisticsData = response[@"data"];
                [UBMRouterHistoryData shareRouterHistoryData].tripStatisticsData = self.tripStatisticsData;//存
                
            }else if (self.tripStatisticsType == 2){
                self.tripStatisticsWeekData = response[@"data"];
                [UBMRouterHistoryData shareRouterHistoryData].tripStatisticsWeekData = self.tripStatisticsWeekData;//存
                
            }else if (self.tripStatisticsType == 3){
                self.tripStatisticsMonthData = response[@"data"];
                [UBMRouterHistoryData shareRouterHistoryData].tripStatisticsMonthData = self.tripStatisticsMonthData;//存
            }
        
        }else{
            NSString* toast = [NSString stringWithFormat:@"%@", response[@"msg"]];
            Toast(toast);
            
            //从内存缓存取
            [self getStatisticsInforWithType:type FromLocal:^{
                if (errorResult) {
                    errorResult();
                }
            }];
        }
        
    } error:^(id response) {
                
        //从内存缓存取
        [self getStatisticsInforWithType:type FromLocal:^{
            if (errorResult) {
                errorResult();
            }
        }];
        
    } isShowHUD:YES];
    
}

-(void)getStatisticsInforWithType:(NSString*)type FromLocal:(void(^)(void))errorBlock{
    if ([type intValue] == 1) {
        if ([UBMRouterHistoryData shareRouterHistoryData].tripStatisticsData) {
            self.tripStatisticsData = [UBMRouterHistoryData shareRouterHistoryData].tripStatisticsData;
        }else{
            self.tripStatisticsData = nil;
            errorBlock();
        }
        
    }else if ([type intValue] == 2){
        if ([UBMRouterHistoryData shareRouterHistoryData].tripStatisticsWeekData) {
            self.tripStatisticsWeekData = [UBMRouterHistoryData shareRouterHistoryData].tripStatisticsData;
        }else{
            self.tripStatisticsWeekData = nil;
            errorBlock();
        }
        
    }else if ([type intValue] == 3){
        if ([UBMRouterHistoryData shareRouterHistoryData].tripStatisticsMonthData) {
            self.tripStatisticsMonthData = [UBMRouterHistoryData shareRouterHistoryData].tripStatisticsMonthData;
        }else{
            self.tripStatisticsMonthData = nil;
            errorBlock();
        }
    }else{
        errorBlock();
    }
}

-(void)setTripStatisticsData:(NSDictionary *)tripStatisticsData{
    _tripStatisticsData = tripStatisticsData;
    //设置view、刷新图表数据
    self.rv.tripStatisticsData = tripStatisticsData;
}

-(void)setTripStatisticsWeekData:(NSDictionary *)tripStatisticsWeekData{
    _tripStatisticsWeekData = tripStatisticsWeekData;
    //设置view、刷新图表数据
    self.rv.tripStatisticsData = tripStatisticsWeekData;
}

-(void)setTripStatisticsMonthData:(NSDictionary *)tripStatisticsMonthData{
    _tripStatisticsMonthData = tripStatisticsMonthData;
    self.rv.tripStatisticsData = tripStatisticsMonthData;
}


#pragma mark - 未结束行程处理。last统计专用，通过第二次的请求与上次的请求对比，统计成功失败数量。
-(void)getNotEndList:(BOOL)last{
    
    NSString* carId = [[NSUserDefaults standardUserDefaults] objectForKey:UBMCarIdUDKey];
    if (carId == nil) {
        carId = @"";
    }
    
    long lastcount = 0;
    if (last == YES) {
        lastcount = [self.notEndTripList count];
    }
    
    [[UBMNetWorkTool shareNetWorkTool] requestWithBaseUrl:[UBMDomainName.shareDomain ubmURL] UrlString:UBMGetUnFinishTripList parameters:@{@"userId":[UBITracking shared].userID, @"deviceId":[UBMVendorTool getIDFV]} method:@"POST" success:^(id response) {
        
        if (response && [response[@"code"] intValue]==1) {
            
            //剔除当前进行中的行程
            NSMutableArray* unOverTrip = [[NSMutableArray alloc] initWithArray:response[@"data"]];
            if ([UBITracking shared].isCollecting == YES && !IsEmptyStr([UBITracking shared].nowTripid)) {
                if (unOverTrip.count>0) {
                    for (NSDictionary* itemDicx in unOverTrip) {
                        NSString* trip_id = [NSString stringWithFormat:@"%@",itemDicx[@"trip_id"]];
                        if ([trip_id isEqualToString:[UBITracking shared].nowTripid]) {
                            [unOverTrip removeObject:itemDicx];
                            break;
                        }
                    }
                }
            }
            
            if (unOverTrip.count <= 1) {
                
                //拿本地没有tripid的行程
                NSArray* locUnUploadOverTrip = [UBITracking.shared getAllUnUploadOverTrip];
                for (UBMTripTableModel* utmodel in locUnUploadOverTrip) {
                    if (IsEmptyStr(utmodel.tripID)) {
                        //塞入notEndTripList。
                        NSString* ctime = [UBMMTools timeSecondWithTimeIntervalString:utmodel.startTime];
                        NSDictionary* udic = [[NSDictionary alloc] initWithObjectsAndKeys:@"www",@"trip_id", ctime,@"ctime", nil];
                        [unOverTrip addObject:udic];
                    }
                }
                
            }
            
            self.notEndTripList = unOverTrip;
            
        }else{
            self.notEndTripList = nil;
        }
        
        if (last == YES) {
            long newCount = 0;
            if (self.notEndTripList && self.notEndTripList.count>0) {
                newCount = self.notEndTripList.count;
            }
            
//            [MobClick event:@"trip_batch_upload_finish" attributes:@{@"uploadNum":[NSString stringWithFormat:@"%ld",lastcount], @"uploadSucessNum":[NSString stringWithFormat:@"%ld", (lastcount-newCount)], @"uploadFailNum":[NSString stringWithFormat:@"%ld",newCount], @"uid":[NSString stringWithFormat:@"%@",UserModel.shareUserModel.uid]}];
            
        }
            
    } error:^(id response) {
        self.notEndTripList = nil;
    } isShowHUD:YES];
}

-(void)setNotEndTripList:(NSArray *)notEndTripList{
    _notEndTripList = notEndTripList;
    self.rv.notEndTripList = notEndTripList;
    [self.mainTableView reloadData];
}

-(void)checkAndUpload{

    NSMutableArray* tripArr = [[NSMutableArray alloc] init];
    for (int i=0; i<_notEndTripList.count; i++) {
        NSDictionary* item = _notEndTripList[i];
        
        if (item[@"trip_id"] && ![item[@"trip_id"] isEqualToString:@"www"]) {
            [tripArr addObject:ChangeToString(item[@"trip_id"])];
        }
        
    }
    
    //全部上传完成后 。删除所有数据，重新加载所有的数据。
    __weak typeof(self) weakself = self;
    
    [UBITracking.shared uploadTheTripsOfTripids:tripArr andCallBack:^(BOOL result) {
        
        [UBITracking.shared checkAndUploadLocTrip:^(BOOL result) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (result == NO) {
                    Toast(@"行程上传失败，请稍后重新上传~");
                }
                
                weakself.rv.notEndTripUploadEnd = YES;
                
                //刷新所有
                [weakself.tripList removeAllObjects];
                [weakself getMoreData];
                [weakself getTripStatistics:[NSString stringWithFormat:@"%d",self.tripStatisticsType] block:nil];
                [weakself getNotEndList:YES];
            });
            
//            [MobClick event:@"trip_batch_upload_finish" attributes:[[NSDictionary alloc] initWithObjectsAndKeys:UserModel.shareUserModel.uid, @"uid", nil]];
            
        }];
        
    }];
}

//MARK:神策埋点标题
- (NSDictionary *)getTrackProperties{
    return @{@"$title":@"ubm_history_page"};
}

@end
