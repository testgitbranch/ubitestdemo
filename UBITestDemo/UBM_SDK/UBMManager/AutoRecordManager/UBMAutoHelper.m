//
//  UBMAutoHelper.m
//  LoveCarChannel_iOS
//
//  Created by wzw on 2021/1/11.
//  Copyright © 2021 魏振伟. All rights reserved.
//

#import "UBMAutoHelper.h"
#import "UBMAutoRecordManager.h"
#import "UBMPrefixHeader.h"
#import "UBMNotificationNameConst.h"

#pragma mark - 自动记录策略类型

@interface UBMAutoHelper()<UBMAutoRecordDelegate>

@end

@implementation UBMAutoHelper

+ (instancetype)shareAutoHelper {
    static id instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _isAutoRecord = NO;
    }
    return self;
}

- (void)startAutoRecordManager {
    _isAutoRecord = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(manualRecordBegin:) name:UBMStartResultNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(manualRecordEnd) name:UBMEndResultNotification object:nil];
    
    // BXM
    UBMAutoRecordManager.shared.delegate = self;
    [UBMAutoRecordManager.shared startAutoRecordMode];
}

- (void)stopAutoRecordManager {
    _isAutoRecord = NO;
    
    // BXM
    UBMAutoRecordManager.shared.delegate = nil;
    [UBMAutoRecordManager.shared stopAutoRecordMode];
}

- (void)manualRecordBegin:(NSNotification *)notify {
    NSNumber *sw = [[NSUserDefaults standardUserDefaults] objectForKey:UBMAutoRecordSwitchUDKey];
    if (sw && [sw integerValue] == 1) {
        UBMResultModel* resultModle = notify.object;
        UBMAutoRecordManager.shared.isRecording = resultModle.resultCode;
    }
}

- (void)manualRecordEnd {
    NSNumber *sw = [[NSUserDefaults standardUserDefaults] objectForKey:UBMAutoRecordSwitchUDKey];
    if (sw && [sw integerValue] == 1){
        UBMAutoRecordManager.shared.isRecording = NO;
    }
}

- (void)autoRecordBegin{
    NSNumber *sw = [NSUserDefaults.standardUserDefaults objectForKey:UBMAutoRecordSwitchUDKey];
    if (sw && [sw integerValue] == 1){
        if(UBMManager.shared.isCollecting != YES ){
            UBMStartOrEndModel* startModel = [[UBMStartOrEndModel alloc] init];
            startModel.origintype = ubmAutoType;
            [[NSNotificationCenter defaultCenter] postNotificationName:UBMStartNotification object:startModel];
        }
    }
}

- (void)autoRecordEnd{
    NSNumber *sw = [NSUserDefaults.standardUserDefaults objectForKey:UBMAutoRecordSwitchUDKey];
    if (sw && [sw integerValue] == 1){
        if(UBMManager.shared.isCollecting == YES ){
            UBMStartOrEndModel* endModel = [[UBMStartOrEndModel alloc] init];
            endModel.origintype = ubmAutoType;
            [[NSNotificationCenter defaultCenter] postNotificationName:UBMEndNotification object:endModel];
        }
    }
}

- (void)autoRecordReport:(NSDictionary *)info {
    [UBMManager.shared setIsStoping:![info[UBMAutoRecordReportAttributeKeyIsDriving] integerValue]];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
