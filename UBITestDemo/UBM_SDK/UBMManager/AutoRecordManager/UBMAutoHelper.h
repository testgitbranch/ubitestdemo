//
//  UBMAutoHelper.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2021/1/11.
//  Copyright © 2021 魏振伟. All rights reserved.
//

/*
 处理自动收集模块和UBMManager的耦合问题
 */

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UBMAutoHelper : NSObject

@property(atomic, assign, readonly)BOOL isAutoRecord; 

+(instancetype)shareAutoHelper;

/*
 打开自动记录策略
 */
- (void)startAutoRecordManager;

/*
 关闭自动记录策略
 */
- (void)stopAutoRecordManager;

@end

NS_ASSUME_NONNULL_END
