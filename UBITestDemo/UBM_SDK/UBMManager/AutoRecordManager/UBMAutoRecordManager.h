//
//  UBMAutoRecordManager.h
//  LoveCarChannel_iOS
//
//  Created by 金鑫 on 2021/3/20.
//  Copyright © 2021 魏振伟. All rights reserved.
//

// 自动记录策略2: 通过重大位置更新 + 运动状态, 来判断是否在开车状态。

#import <Foundation/Foundation.h>
#import <CoreMotion/CoreMotion.h>
#import <UIKit/UIKit.h>
#import "UBMNotificationNameConst.h"

NS_ASSUME_NONNULL_BEGIN

/// 行程状态汇报属性
typedef NSString *UBMAutoRecordReportAttributeKey;
UIKIT_EXTERN UBMAutoRecordReportAttributeKey const UBMAutoRecordReportAttributeKeyIsDriving;

@protocol UBMAutoRecordDelegate <NSObject>
@optional
/// 开始自动记录
- (void)autoRecordBegin;

/// 结束自动记录
- (void)autoRecordEnd;

/// 行程状态汇报
- (void)autoRecordReport:(NSDictionary *)info;

@end

@interface UBMAutoRecordManager : NSObject
@property (nonatomic, weak) id<UBMAutoRecordDelegate> delegate;
@property (assign) BOOL isRecording;

+ (instancetype)shared;
 
/// 开启自动记录
- (void)startAutoRecordMode;

/// 停止自动记录
- (void)stopAutoRecordMode;

/// 注册自动记录 (为了杀死后唤醒)
- (void)registerAutoRecordMode;

@end

NS_ASSUME_NONNULL_END
