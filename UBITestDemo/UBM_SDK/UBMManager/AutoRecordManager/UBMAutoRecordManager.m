//
//  UBMAutoRecordManager.m
//  LoveCarChannel_iOS
//
//  Created by 金鑫 on 2021/3/20.
//  Copyright © 2021 魏振伟. All rights reserved.
//

#import "UBMAutoRecordManager.h"
#import "UBMUiDataModel.h"
#import "UBMSLSLog.h"
#import "UBMPrefixHeader.h"

const UBMAutoRecordReportAttributeKey UBMAutoRecordReportAttributeKeyIsDriving = @"UBMAutoRecordReportAttributeKeyIsDriving";

typedef NS_ENUM(NSInteger, DrivingStatus) {
    NonDriving,
    Suspense,
    Driving
};

typedef double UnitSpeedKH; //公里/时
typedef double UnitSpeedMS; //米/秒

static const UnitSpeedKH kMinStartSpeedKH = 9;
static const UnitSpeedKH kMinStopSpeedKH = 25;
static const UnitSpeedKH kMaxSpeedKH = 200;
static const UnitSpeedKH kMinMotionSpeedKH = kMinStartSpeedKH - 2; //单位:千米/时
static const UnitSpeedKH kMinMotionSpeedMS = kMinMotionSpeedKH / 3.6; //单位:米/秒
static const UnitSpeedMS kMinStartSpeedMS = kMinStartSpeedKH / 3.6;
static const UnitSpeedMS kMinStopSpeedMS = kMinStopSpeedKH / 3.6;
static const UnitSpeedMS kMaxSpeedMS = kMaxSpeedKH / 3.6;
static const CLLocationSpeedAccuracy kSpeedAccuracyMax = 3.8; //单位:米
static const CLLocationAccuracy kLocationAccuracyMax = 250;
static const NSTimeInterval kMinInterval = 4; // 定位之间的最小时间差，不足就累计到下一次

static const NSInteger kMinuteForEndingRecord = 10; // 10分钟
static const NSInteger kLowAccuracyMaxCount = 5;
static const NSTimeInterval kRequestInterval = 7; // 运动状态回调后请求定位的停顿时间
static const NSTimeInterval kOverdueInterval = 60;

@interface UBMAutoRecordManager()<CLLocationManagerDelegate>
{
@private
    NSInteger _lowAccuracyCount;
}
@property(nonatomic, strong) CMMotionActivityManager *motionManager;
@property(nonatomic, strong) CLLocationManager *locationManager;

@property (nonatomic, strong) CLLocation *prevLocationForBeginRecord;
@property (nonatomic, strong) CLLocation *prevLocationForEndRecord;

@property (nonatomic, strong) dispatch_semaphore_t semForBeginningRecord;
@property (nonatomic, strong) dispatch_queue_t queueForBeginningRecord;
@property (nonatomic, strong) dispatch_semaphore_t semForEndingRecord;
@property (nonatomic, strong) dispatch_queue_t queueForEndingRecord;
@property (nonatomic, strong) NSOperationQueue *opretionQueueForMotion;

@property (nonatomic, assign) UIBackgroundTaskIdentifier backgroundTask;

@property (assign) DrivingStatus drivingStatus;
@property (assign) NSInteger requestLocationCount;
@property (assign) BOOL isWaitingForEndRecord;
@property (assign) BOOL isMotionDriving;

@end

@implementation UBMAutoRecordManager

+ (instancetype)shared {
    static id instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup {
    // 异步线程队列
    self.semForBeginningRecord = dispatch_semaphore_create(0);
    self.semForEndingRecord = dispatch_semaphore_create(0);
    self.queueForBeginningRecord = dispatch_queue_create("com.haochezhuhuzhu.BXAutoRecordManager.beginningRecord", DISPATCH_QUEUE_SERIAL);
    self.queueForEndingRecord = dispatch_queue_create("com.haochezhuhuzhu.BXAutoRecordManager.endingRecord", DISPATCH_QUEUE_SERIAL);
}

- (CMMotionActivityManager *)motionManager {
    if (!_motionManager) {
        _motionManager = [[CMMotionActivityManager alloc] init];
        self.opretionQueueForMotion = [[NSOperationQueue alloc]init];
        self.opretionQueueForMotion.maxConcurrentOperationCount = 1;
    }
    return _motionManager;
}

- (CLLocationManager *)locationManager {
    if (!_locationManager) {
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
        _locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
        _locationManager.allowsBackgroundLocationUpdates = YES;
        _locationManager.pausesLocationUpdatesAutomatically = NO;
        [_locationManager requestAlwaysAuthorization];
    }
    return _locationManager;
}

#pragma mark - 开启和结束自动记录模式
- (void)startAutoRecordMode {
    [UBMSLSLog addUBMLog:[NSString stringWithFormat:@"BXA %s", __FUNCTION__]];
    
    [NSNotificationCenter.defaultCenter addObserver:self selector:@selector(tripLocationUpdate:) name:UBMSpeedAutoNotification object:nil];
    
    @try {
        [self removeObserver:self forKeyPath:@"isRecording"];
    } @catch (NSException *exception) {}
    
    [self addObserver:self forKeyPath:@"isRecording" options:NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew context:nil];
    
    // 开启运动状态 监控
    [self openAutoRecordMonitor];
    
    // 打开重大位置更新
    if ([CLLocationManager significantLocationChangeMonitoringAvailable]) {
        [self.locationManager startMonitoringSignificantLocationChanges];
    }
}

- (void)stopAutoRecordMode {
    [UBMSLSLog addUBMLog:[NSString stringWithFormat:@"BXA %s", __FUNCTION__]];
    
    [NSNotificationCenter.defaultCenter removeObserver:self];
    
    @try {
        [self removeObserver:self forKeyPath:@"isRecording"];
    } @catch (NSException *exception) {}
    
    // 关闭运动状态 监控
    [self closeAutoRecordMonitor];
    
    // 关闭重大位置更新
    if ([CLLocationManager significantLocationChangeMonitoringAvailable]) {
        [self.locationManager stopMonitoringSignificantLocationChanges];
    }
}

#pragma mark - 打开和关闭自动记录监听
- (void)openAutoRecordMonitor {
    // 打开运动状态监听
    [self activityChangeStart];
    
    /*
    // 打开重大位置更新
    if ([CLLocationManager significantLocationChangeMonitoringAvailable]) {
        [self.locationManager startMonitoringSignificantLocationChanges];
    }
     */
}

- (void)closeAutoRecordMonitor {
    // 关闭运动状态监听
    [self activityChangeStop];
    
    // 关闭重大位置更新
    /*
    if ([CLLocationManager significantLocationChangeMonitoringAvailable]) {
        [self.locationManager stopMonitoringSignificantLocationChanges];
    }
     */
}

#pragma mark - 注册自动记录模式 (为了杀死后唤醒)
- (void)registerAutoRecordMode {
    // 开启重大位置更新
    if ([CLLocationManager significantLocationChangeMonitoringAvailable]) {
        [self.locationManager startMonitoringSignificantLocationChanges];
    }
}

#pragma mark - 运动状态的开始与结束
- (void)activityChangeStart {
    if (self.motionManager && [CMMotionActivityManager isActivityAvailable]) {
        @bx_weakify(self);
        [self.motionManager startActivityUpdatesToQueue:self.opretionQueueForMotion withHandler:^(CMMotionActivity * _Nullable activity) {
            @bx_strongify(self);
            if (!activity) { return; }
            
            if (activity.automotive == YES &&
                activity.confidence == CMMotionActivityConfidenceHigh) {
                if (self.drivingStatus != Driving) {
                    self.isMotionDriving = YES;
                    [UBMSLSLog addUBMLog:[NSString stringWithFormat:@"BXA-cm\n+ driveing-high, willRequestLocation, isMotionDriving:%d\n+ drivingStatus:%ld\n# thread:%@, timestamp:%@", self.isMotionDriving, self.drivingStatus, [NSThread currentThread], [[NSDate date] localDate]]];
                    
                    [NSThread sleepForTimeInterval:kRequestInterval];
                    [self.locationManager requestLocation];
                }
            }
        }];
        
        [self.motionManager queryActivityStartingFromDate:[NSDate date] toDate:[NSDate distantFuture] toQueue:self.opretionQueueForMotion withHandler:^(NSArray<CMMotionActivity *> * _Nullable activities, NSError * _Nullable error) {
            if (!activities || activities.count == 0) { return; }
            CMMotionActivity *activity = activities.lastObject;
            [UBMSLSLog addUBMLog:[NSString stringWithFormat:@"BXA-cm\n+ Date driveing-high, willRequestLocation, isMotionDriving:%d\n+ drivingStatus:%ld\n# thread:%@, timestamp:%@\n** count:%ld\n** %@", self.isMotionDriving, self.drivingStatus, [NSThread currentThread], [[NSDate date] localDate], activities.count, activities]];
            
            if (activity.stationary != YES &&
                activity.walking != YES &&
                activity.running != YES &&
                activity.cycling != YES &&
                activity.confidence == CMMotionActivityConfidenceHigh) {
                if (self.drivingStatus != Driving) {
                    self.isMotionDriving = YES;
                    
                    [NSThread sleepForTimeInterval:kRequestInterval];
                    [self.locationManager requestLocation];
                }
            }
        }];
    }
}

- (void)activityChangeStop {
    if(self.motionManager && [CMMotionActivityManager isActivityAvailable]){
        [self.motionManager stopActivityUpdates];
    }
}

#pragma mark - 定位回调
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    
    if (self.drivingStatus == Driving) { return; }
    
    CLLocation *location = locations.lastObject;
    
    // 防止很久以前的定位，误触发
    NSTimeInterval interval = [[NSDate date] timeIntervalSinceDate:location.timestamp];
    if (interval > kOverdueInterval) {
        [UBMSLSLog addUBMLog:[NSString stringWithFormat:@"BXA-didUpdate\n+ Overdue\n- locationTimestamp:%@  nowTimestamp:%@", [location.timestamp localDate], [[NSDate date] localDate]]];
        self.isMotionDriving = NO;
        goto ContinueUpdate;
    }
    
    if (@available(iOS 10.0, *)) {
        if (location.speedAccuracy >= 0 &&
            location.speedAccuracy <= kSpeedAccuracyMax &&
            location.horizontalAccuracy >= 0 &&
            location.horizontalAccuracy <= kLocationAccuracyMax) { // 速度精度，定位精度都达标
            if (self.drivingStatus == NonDriving) { // 初次判断速度
                if (self.isMotionDriving == YES &&
                    location.speed >= kMinMotionSpeedMS &&
                    location.speed <= kMaxSpeedMS) {
                    // 运动状态开启记录
                    self.drivingStatus = Driving;
                    [UBMSLSLog addUBMLog:[NSString stringWithFormat:@"BXA-didUpdate\n- PrevLocation:%@\n- CurrLocation:%@\n+ speedAccuracy:YES, horizontalAccuracy:YES, speed:YES\n+ speedAccuracy:%f, locationSpeed:%f\n+ isMotionDriving:YES\n+ drivingStatus:NonDriving -> Driving\n# thread:%@, timestamp:%@", self.prevLocationForBeginRecord, location, location.speedAccuracy, location.speed, [NSThread currentThread], [[NSDate date] localDate]]];
                    
                    // 开启记录
                    goto StartRecord;
                    
                } else if (location.speed >= kMinStartSpeedMS &&
                           location.speed <= kMaxSpeedMS) { // 速度达标
                    // 定位直接开启记录
                    self.drivingStatus = Driving;
                    [UBMSLSLog addUBMLog:[NSString stringWithFormat:@"BXA-didUpdate\n- PrevLocation:%@\n- CurrLocation:%@\n+ speedAccuracy:YES, horizontalAccuracy:YES, speed:YES\n+ speedAccuracy:%f, locationSpeed:%f\n+ drivingStatus:NonDriving -> Driving\n# thread:%@, timestamp:%@", self.prevLocationForBeginRecord, location, location.speedAccuracy, location.speed, [NSThread currentThread], [[NSDate date] localDate]]];
                    
                    // 开启记录
                    goto StartRecord;
                    
                } else {
                    [UBMSLSLog addUBMLog:[NSString stringWithFormat:@"BXA-didUpdate\n- PrevLocation:%@\n- CurrLocation:%@\n+ speedAccuracy:YES, horizontalAccuracy:YES, speed:NO\n+ speedAccuracy:%f, locationSpeed:%f\n+ drivingStatus:NonDriving -> Suspense\n# thread:%@, timestamp:%@", self.prevLocationForBeginRecord, location, location.speedAccuracy, location.speed, [NSThread currentThread], [[NSDate date] localDate]]];
                    self.drivingStatus = Suspense;
                }
                
            } else if (self.drivingStatus == Suspense) { // 再次判断速度
                if (self.isMotionDriving == YES &&
                    location.speed >= kMinMotionSpeedMS &&
                    location.speed <= kMaxSpeedMS) {
                    // 运动状态开启记录
                    self.drivingStatus = Driving;
                    [UBMSLSLog addUBMLog:[NSString stringWithFormat:@"BXA-didUpdate\n- PrevLocation:%@\n- CurrLocation:%@\n+ speedAccuracy:YES, horizontalAccuracy:YES, speed:YES\n+ speedAccuracy:%f, locationSpeed:%f\n+ isMotionDriving:YES\n+ drivingStatus:Suspense -> Driving\n# thread:%@, timestamp:%@", self.prevLocationForBeginRecord, location, location.speedAccuracy, location.speed, [NSThread currentThread], [[NSDate date] localDate]]];
                    
                    // 开启记录
                    goto StartRecord;
                    
                } else if (location.speed >= kMinStartSpeedMS &&
                           location.speed <= kMaxSpeedMS) { // 速度达标
                    // 定位直接开启记录
                    self.drivingStatus = Driving;
                    [UBMSLSLog addUBMLog:[NSString stringWithFormat:@"BXA-didUpdate\n- PrevLocation:%@\n- CurrLocation:%@\n+ speedAccuracy:YES, horizontalAccuracy:YES, speed:YES\n+ speedAccuracy:%f, locationSpeed:%f\n+ drivingStatus:Suspense -> Driving\n# thread:%@, timestamp:%@", self.prevLocationForBeginRecord, location, location.speedAccuracy, location.speed, [NSThread currentThread], [[NSDate date] localDate]]];
                    
                    // 开启记录
                    goto StartRecord;
                    
                } else { // 定位速度没达标
                    if (self.prevLocationForBeginRecord) {
                        UnitSpeedMS speed = [self avgSpeedFromCurrentLocation:location previousLocation:self.prevLocationForBeginRecord];
                        if (self.isMotionDriving == YES &&
                            speed >= kMinMotionSpeedMS &&
                            speed <= kMaxSpeedMS) {
                            // 运动状态开启记录
                            self.drivingStatus = Driving;
                            [UBMSLSLog addUBMLog:[NSString stringWithFormat:@"BXA-didUpdate\n- PrevLocation:%@\n- CurrLocation:%@\n+ speedAccuracy:YES, horizontalAccuracy:YES, speed:NO, distanceSpeed:%f\n+ speedAccuracy:%f, locationSpeed:%f\n+ isMotionDriving:YES\n+ drivingStatus:Suspense -> Driving\n# thread:%@, timestamp:%@", self.prevLocationForBeginRecord, location, speed, location.speedAccuracy, location.speed, [NSThread currentThread], [[NSDate date] localDate]]];
                            
                            // 开启记录
                            goto StartRecord;
                            
                        } else if (speed >= kMinStartSpeedMS &&
                                   speed <= kMaxSpeedMS) { // 二次定位速度达标
                            self.drivingStatus = Driving;
                            [UBMSLSLog addUBMLog:[NSString stringWithFormat:@"BXA-didUpdate\n- PrevLocation:%@\n- CurrLocation:%@\n+ speedAccuracy:YES, horizontalAccuracy:YES, speed:NO, distanceSpeed:%f\n+ speedAccuracy:%f, locationSpeed:%f\n+ drivingStatus:Suspense -> Driving\n# thread:%@, timestamp:%@", self.prevLocationForBeginRecord, location, speed, location.speedAccuracy, location.speed, [NSThread currentThread], [[NSDate date] localDate]]];
                            
                            // 开启记录
                            goto StartRecord;
                            
                        } else {
                            [UBMSLSLog addUBMLog:[NSString stringWithFormat:@"BXA-didUpdate\n- PrevLocation:%@\n- CurrLocation:%@\n+ speedAccuracy:YES, horizontalAccuracy:YES, speed:NO, distanceSpeed:%f\n+ speedAccuracy:%f, locationSpeed:%f\n+ drivingStatus:Suspense\n# thread:%@, timestamp:%@", self.prevLocationForBeginRecord, location, speed, location.speedAccuracy, location.speed, [NSThread currentThread], [[NSDate date] localDate]]];
                            
                            NSTimeInterval interval = [location.timestamp timeIntervalSinceDate:self.prevLocationForBeginRecord.timestamp];
                            if (interval < kMinInterval) {
                                location = self.prevLocationForBeginRecord;
                            }
                            self.isMotionDriving = NO;
                        }
                    }
                }
            }
            
        } else if (location.horizontalAccuracy >= 0 &&
                   location.horizontalAccuracy <= kLocationAccuracyMax) { // 定位精度达标
            // 定位达标，需要获取第二次定位来判断速度
            if (self.drivingStatus == NonDriving) {
                self.drivingStatus = Suspense;
                
                [UBMSLSLog addUBMLog:[NSString stringWithFormat:@"BXA-didUpdate\n- PrevLocation:%@\n- CurrLocation:%@\n+ speedAccuracy:NO, horizontalAccuracy:YES, speed:--\n+ speedAccuracy:%f, locationSpeed:%f\n+ drivingStatus:NonDriving -> Suspense\n# thread:%@, timestamp:%@", self.prevLocationForBeginRecord, location, location.speedAccuracy, location.speed, [NSThread currentThread], [[NSDate date] localDate]]];
                
            // 二次定位判断速度
            } else if (self.drivingStatus == Suspense) {
                if (self.prevLocationForBeginRecord) {
                    UnitSpeedMS speed = [self avgSpeedFromCurrentLocation:location previousLocation:self.prevLocationForBeginRecord];
                    if (self.isMotionDriving == YES &&
                        speed >= kMinMotionSpeedMS &&
                        speed <= kMaxSpeedMS) {
                        // 运动状态开启记录
                        self.drivingStatus = Driving;
                        [UBMSLSLog addUBMLog:[NSString stringWithFormat:@"BXA-didUpdate\n- PrevLocation:%@\n- CurrLocation:%@\n+ speedAccuracy:NO, horizontalAccuracy:YES, speed:--, distanceSpeed:%f\n+ speedAccuracy:%f, locationSpeed:%f\n+ isMotionDriving:YES\n+ drivingStatus:Suspense -> Driving\n# thread:%@, timestamp:%@", self.prevLocationForBeginRecord, location, speed, location.speedAccuracy, location.speed, [NSThread currentThread], [[NSDate date] localDate]]];
                        
                        // 开启记录
                        goto StartRecord;
                        
                    } else if (speed >= kMinStartSpeedMS &&
                               speed <= kMaxSpeedMS) { // 二次定位速度达标
                        self.drivingStatus = Driving;
                        [UBMSLSLog addUBMLog:[NSString stringWithFormat:@"BXA-didUpdate\n- PrevLocation:%@\n- CurrLocation:%@\n+ speedAccuracy:NO, horizontalAccuracy:YES, speed:--, distanceSpeed:%f\n+ speedAccuracy:%f, locationSpeed:%f\n+ drivingStatus:Suspense -> Driving\n# thread:%@, timestamp:%@", self.prevLocationForBeginRecord, location, speed, location.speedAccuracy, location.speed, [NSThread currentThread], [[NSDate date] localDate]]];
                        
                        // 开启记录
                        goto StartRecord;
                        
                    } else { // 二次定位速度没达标
                        [UBMSLSLog addUBMLog:[NSString stringWithFormat:@"BXA-didUpdate\n- PrevLocation:%@\n- CurrLocation:%@\n+ speedAccuracy:NO, horizontalAccuracy:YES, speed:--, distanceSpeed:%f\n+ speedAccuracy:%f, locationSpeed:%f\n+ drivingStatus:Suspense\n# thread:%@, timestamp:%@", self.prevLocationForBeginRecord, location, speed, location.speedAccuracy, location.speed, [NSThread currentThread], [[NSDate date] localDate]]];
                        NSTimeInterval interval = [location.timestamp timeIntervalSinceDate:self.prevLocationForBeginRecord.timestamp];
                        if (interval < kMinInterval) {
                            location = self.prevLocationForBeginRecord;
                        }
                        self.isMotionDriving = NO;
                    }
                }
            }
            
        } else { // 两个精度都没有达标
            [UBMSLSLog addUBMLog:[NSString stringWithFormat:@"BXA-didUpdate\n- PrevLocation:%@\n- CurrLocation:%@\n+ speedAccuracy:NO, horizontalAccuracy:NO, speed:--\n+ speedAccuracy:%f, locationSpeed:%f\n+ drivingStatus:%ld\n# thread:%@, timestamp:%@", self.prevLocationForBeginRecord, location, location.speedAccuracy, location.speed, self.drivingStatus, [NSThread currentThread], [[NSDate date] localDate]]];
            
            if (++_lowAccuracyCount >= kLowAccuracyMaxCount) {
                self.drivingStatus = NonDriving;
                location = nil;
                self.isMotionDriving = NO;
                
            } else {
                goto BackgroundTask;
            }
        }
        
        goto ContinueUpdate;
        
    } else {
        return;
    }
    
StartRecord:
    [self startAutoRecordTrip];
 
ContinueUpdate:
    [self updatePrevLoaction:location];

BackgroundTask:
    [self applyBackgroundTask];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    if (error.code == kCLErrorLocationUnknown) {
        [UBMSLSLog addUBMLog:[NSString stringWithFormat:@"BXA-error: ErrorCurrently unable to retrieve location."]];
        
    } else if(error.code == kCLErrorNetwork) {
        [UBMSLSLog addUBMLog:[NSString stringWithFormat:@"BXA-error: Network used to retrieve location is unavailable."]];
        
    } else if(error.code == kCLErrorDenied) {
        [UBMSLSLog addUBMLog:[NSString stringWithFormat:@"BXA-error: Permission to retrieve location is denied."]];
    }
}

#pragma mark - 行程的开始与结束
- (void)startAutoRecordTrip {
    if ([self.delegate respondsToSelector:@selector(autoRecordBegin)]) {
        [self.delegate autoRecordBegin];
        [self closeAutoRecordMonitor];
    }
}

- (void)stopAutoRecordTrip {
    if ([self.delegate respondsToSelector:@selector(autoRecordEnd)]) {
        [self.delegate autoRecordEnd];
        [self openAutoRecordMonitor];
    }
}

#pragma mark - 更新定位
- (void)updatePrevLoaction:(CLLocation *)location {
    // 连续低精度次数还原成 0
    if (_lowAccuracyCount != 0) { _lowAccuracyCount = 0; }
    self.prevLocationForBeginRecord = location;
}

#pragma mark - 后台任务
- (void)applyBackgroundTask {
    if (self.drivingStatus != Driving && self.backgroundTask == 0) {
        __weak typeof(self) weakSelf = self;
        self.backgroundTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
            [UBMSLSLog addUBMLog:[NSString stringWithFormat:@"BXA-task: end backgroundTask: %lu", weakSelf.backgroundTask]];
            [[UIApplication sharedApplication] endBackgroundTask:weakSelf.backgroundTask];
            weakSelf.backgroundTask = 0;
        }];
        [UBMSLSLog addUBMLog:[NSString stringWithFormat:@"BXA-task: create backgroundTask: %lu", self.backgroundTask]];
    }
}

#pragma mark -
/// 复位自动记录状态
- (void)resetAutoRecordStatus {
    self.isMotionDriving = NO;
    self.drivingStatus = NonDriving;
    self.prevLocationForEndRecord = nil;
}

/// 计算两个坐标之间距离的平均速度(米/秒)
- (UnitSpeedMS)avgSpeedFromCurrentLocation:(CLLocation *)currLocation previousLocation:(CLLocation *)prevLocation {
    //单位: 米
    CLLocationDistance distance = [currLocation distanceFromLocation:prevLocation];
    //单位: 秒
    NSTimeInterval interval = [currLocation.timestamp timeIntervalSinceDate:prevLocation.timestamp];
    //单位: 米/秒
    UnitSpeedMS speed = distance / interval;
    
    [UBMSLSLog addUBMLog:[NSString stringWithFormat:@"BXA-avgSpeed\n+ distance:%fm interval:%fs avgSpeed:%fkm/h\n# timestamp:%@", distance, interval, speed*3.6, [currLocation.timestamp localDate]]];
    
    return speed;
}

#pragma mark - 行程中数据更新通知
- (void)tripLocationUpdate:(NSNotification*)notify {
    UBMUiDataModel* uimodel = notify.object;
    
    if (self.prevLocationForEndRecord) {
        UnitSpeedMS speed = [self avgSpeedFromCurrentLocation:uimodel.location previousLocation:self.prevLocationForEndRecord];
        self.prevLocationForEndRecord = uimodel.location;
        
        if (speed >= kMinStartSpeedMS && speed <= kMaxSpeedMS) { // 速度达标
            if (self.isWaitingForEndRecord == YES) {
                [UBMSLSLog addUBMLog:[NSString stringWithFormat:@"BXA-tripBack\n+ speed:YES, isWaitingForEndRecord:%ld, semaphoreSignal, drivingStatus:%ld\n# thread:%@, timestamp:%@", (long)self.isWaitingForEndRecord, (long)self.drivingStatus, [NSThread currentThread], [[NSDate date] localDate]]];
                
                dispatch_semaphore_signal(self.semForEndingRecord);
                
            } else {
                [UBMSLSLog addUBMLog:[NSString stringWithFormat:@"BXA-tripBack\n+ speed:YES, isWaitingForEndRecord:%ld, noSemaphoreSignal, drivingStatus:%ld\n# thread:%@, timestamp:%@", (long)self.isWaitingForEndRecord, (long)self.drivingStatus, [NSThread currentThread], [[NSDate date] localDate]]];
            }
            
        } else { // 速度不达标
            if (self.isWaitingForEndRecord == YES) {
                [UBMSLSLog addUBMLog:[NSString stringWithFormat:@"BXA-tripBack\n+ speed:NO, isWaitingForEndRecord:%ld, Semaphore Return, drivingStatus:%ld\n# thread:%@, timestamp:%@", (long)self.isWaitingForEndRecord, (long)self.drivingStatus, [NSThread currentThread], [[NSDate date] localDate]]];
                return;
            }
            // 子线程处理
            dispatch_async(self.queueForEndingRecord, ^{
                if (self.isWaitingForEndRecord == NO) { self.isWaitingForEndRecord = YES; }
                
                [UBMSLSLog addUBMLog:[NSString stringWithFormat:@"BXA-tripBack\n+ speed:NO, semaphoreWait, drivingStatus:%ld\n# thread:%@, timestamp:%@", (long)self.drivingStatus, [NSThread currentThread], [[NSDate date] localDate]]];
                
                // 行车状态报告 isDriving:NO
                if ([self.delegate respondsToSelector:@selector(autoRecordReport:)]) {
                    [self.delegate autoRecordReport:@{UBMAutoRecordReportAttributeKeyIsDriving: @(NO)}];
                }
                
                intptr_t ret = dispatch_semaphore_wait(self.semForEndingRecord, dispatch_time(DISPATCH_TIME_NOW, kMinuteForEndingRecord * 60 * NSEC_PER_SEC));
                if (!ret) { // 没有超时，继续记录
                    [UBMSLSLog addUBMLog:[NSString stringWithFormat:@"BXA-tripBack\n+ ret == NoTimeout, isWaitingForEndRecord:%d, drivingStatus:%ld\n# thread:%@, timestamp:%@", self.isWaitingForEndRecord, (long)self.drivingStatus, [NSThread currentThread], [[NSDate date] localDate]]];
                    
                    // 行车状态报告 isDriving:YES
                    if ([self.delegate respondsToSelector:@selector(autoRecordReport:)]) {
                        [self.delegate autoRecordReport:@{UBMAutoRecordReportAttributeKeyIsDriving: @(YES)}];
                    }
                                        
                } else { // 超时，关闭记录
                    [UBMSLSLog addUBMLog:[NSString stringWithFormat:@"BXA-tripBack\n+ ret == Timeout, drivingStatus:%ld\n# thread:%@, timestamp:%@", (long)self.drivingStatus, [NSThread currentThread], [[NSDate date] localDate]]];
                    
                    // 关闭行程
                    [self stopAutoRecordTrip];
                }
                
                if (self.isWaitingForEndRecord == YES) { self.isWaitingForEndRecord = NO; }
            });
        }
        
    } else {
        self.prevLocationForEndRecord = uimodel.location;
    }
}

#pragma mark - KVObserve
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    BOOL oldValue, newValue;
    if ([keyPath isEqualToString:@"isRecording"]) {
        oldValue = [change[NSKeyValueChangeOldKey] integerValue];
        newValue = [change[NSKeyValueChangeNewKey] integerValue];
        
        // 手动打开行程
        if (newValue == YES) {
            [UBMSLSLog addUBMLog:[NSString stringWithFormat:@"BXA-KVObserve\n+ openTrip, closeMonitor\n+ oldValue:%d, newValue:%d\n+ isWaitingForEndRecord:%d\n# thread:%@, timestamp:%@", oldValue, newValue, self.isWaitingForEndRecord, [NSThread currentThread], [[NSDate date] localDate]]];
            // 关闭自动记录触发监听
            [self closeAutoRecordMonitor];
            
        // 手动关闭行程
        } else if (newValue == NO) {
            [UBMSLSLog addUBMLog:[NSString stringWithFormat:@"BXA-KVObserve\n+ closeTrip, openMonitor\n+ oldValue:%d, newValue:%d\n+ isWaitingForEndRecord:%d\n# thread:%@, timestamp:%@", oldValue, newValue, self.isWaitingForEndRecord, [NSThread currentThread], [[NSDate date] localDate]]];
            // 如在超时等待时，被用户强行关闭行程，需要发送signal
            if (self.isWaitingForEndRecord == YES) {
                dispatch_semaphore_signal(self.semForEndingRecord);
            }
            // 状态复位
            [self resetAutoRecordStatus];
            // 打开自动记录触发监听
            [self openAutoRecordMonitor];
        }
    }
}

@end
