//
//  UBMConfigure.m
//  UBM_SDK
//
//  Created by 金鑫 on 2021/11/24.
//

#import "UBMConfigure.h"

@implementation UBMConfigure

+ (instancetype)shared
{
    static id instance;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    
    return instance;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        self.uid = @"";
        self.nickname = @"";
        self.portrait = @"";
        self.phone = @"";
    }
    return self;
}


@end
