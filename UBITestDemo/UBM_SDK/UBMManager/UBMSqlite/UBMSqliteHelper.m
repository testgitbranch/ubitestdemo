//
//  UBMSqliteHelper.m
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/12/14.
//  Copyright © 2020 魏振伟. All rights reserved.
//

#import "UBMSqliteHelper.h"
#import "UBMFmdbHelper.h"
#import "UBMMTools.h"

//数据库名字
#define ubmSqilteName @"hhkc.sqlite"
//ubm表名字
#define ubmTableName @"tripListTable"

@interface UBMSqliteHelper()
@property(nonatomic, strong)dispatch_semaphore_t pbSignal;
@end

@implementation UBMSqliteHelper

+(instancetype)sharedHelper{
    static id instance;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    
    return instance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.pbSignal = dispatch_semaphore_create(1);
        
        //创建一个名为hczhz的数据库。
        [[UBMFmdbHelper sharedFMDBHelper] createDBWithName:ubmSqilteName];
        
    }
    return self;
}

-(void)changeUserSqlite{
    [[UBMFmdbHelper sharedFMDBHelper] createDBWithName:ubmSqilteName];
}

//新增表
-(BOOL)creatUBMTripTable{
    
    //创建一个表，用来记录每段行程的状态。hadStop 0 1 uploadOver 0 1
    NSString* splName = ubmTableName;
    NSString *sql = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ ('locTripID' TEXT NOT NULL, 'tripID' TEXT, 'startTime' TEXT , 'lastUpdateTime' TEXT, 'mileage' TEXT, 'totalTime' TEXT, 'hadStop' TEXT, 'uploadOver' TEXT, 'tripPBList' TEXT, 'vid' TEXT, 'isAutoStop' TEXT, 'isAutoStart' TEXT)", splName];
    
    BOOL result = [[UBMFmdbHelper sharedFMDBHelper] notResustSetWithSql:sql];
    
    if (result == NO) {
        NSLog(@"创建tripList失败");
    }else{
        NSLog(@"创建tripList成功");
    }
    
    //新增vid字段
    BOOL addresult = [[UBMFmdbHelper sharedFMDBHelper] addAColumn:@"vid" inTableWithName:splName];
    if (addresult == NO) {
        NSLog(@"增加vid失败");
    }else{
        NSLog(@"增加vid成功");
    }
    
    //新增isAutoStop字段
    BOOL isAutoStopaddresult = [[UBMFmdbHelper sharedFMDBHelper] addAColumn:@"isAutoStop" inTableWithName:splName];
    if (isAutoStopaddresult == NO) {
        NSLog(@"增加isAutoStop失败");
    }else{
        NSLog(@"增加isAutoStop成功");
    }
    
    //新增isAutoStart字段
    BOOL isAutoStartaddresult = [[UBMFmdbHelper sharedFMDBHelper] addAColumn:@"isAutoStart" inTableWithName:splName];
    if (isAutoStartaddresult == NO) {
        NSLog(@"增加isAutoStart失败");
    }else{
        NSLog(@"增加isAutoStart成功");
    }
    
    return result;
}

//删除表中所有数据
-(BOOL)deleteUBMTripTable{
    
    NSString *execSql = [NSString stringWithFormat:@"delete from %@", ubmTableName];
    BOOL result = [[UBMFmdbHelper sharedFMDBHelper] notResustSetWithSql:execSql];
    if (result == YES) {
        NSLog(@"删除成功");
    }
    
    return result;
}


/*
 表内操作
 */

#pragma mark - 增:新增一段默认trip。
-(BOOL)addNewTripItemWith:(NSString* )locTripId andVid:(NSString*)vid andStartType:(int)startType{
    
    NSString* execSql = [NSString stringWithFormat:@"INSERT INTO %@ VALUES('%@', '%@', '%f', '%f', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%d')", ubmTableName, locTripId, @"", [[NSDate date] timeIntervalSince1970], [[NSDate date] timeIntervalSince1970], @"0", @"0", @"0", @"0", @"", vid, @"0", startType];
    BOOL result = [[UBMFmdbHelper sharedFMDBHelper] notResustSetWithSql:execSql];
    
    if (result == YES) {
        NSLog(@"插入一条行程成功");
    }else{
        NSLog(@"插入一条行程失败");
    }
    
    return result;;
}


#pragma mark - 查一个行程
-(UBMTripTableModel*)getUBMTripTableModelOfLocTripID:(NSString* )locTripId OrTripID:(NSString*)tripID{
    
    NSString* execSql = @"";
    if ([UBMMTools IsEmptyStr:locTripId]) {
        execSql = [NSString stringWithFormat:@"select * from '%@' where tripID = '%@'", ubmTableName, tripID];
    }else{
        execSql = [NSString stringWithFormat:@"select * from '%@' where locTripID = '%@'", ubmTableName, locTripId];
    }
    NSArray* resultArray = [[UBMFmdbHelper sharedFMDBHelper] qureyWithSql:execSql];
    if (resultArray && resultArray.count>0) {
        NSDictionary* dicx = resultArray[0];
        UBMTripTableModel* model = [self getTripModelWithDicx:dicx];
        return model;
    }
    
    return nil;
}

#pragma mark - 查 根据HadStop查一堆行程
-(NSArray<UBMTripTableModel*>*)getUBMTripTableModelOfHadStop:(BOOL)HadStop{
    
    NSString* hs = @"0";
    if (HadStop == YES) {
        hs = @"1";
    }
    NSString* execSql = [NSString stringWithFormat:@"select * from '%@' where HadStop = '%@'", ubmTableName, hs];
    
    NSMutableArray* modelArr = [[NSMutableArray alloc] init];
    
    NSArray* resultArray = [[UBMFmdbHelper sharedFMDBHelper] qureyWithSql:execSql];
    if (resultArray && resultArray.count>0) {
        
        for (NSDictionary* dicx in resultArray) {
            UBMTripTableModel* model = [self getTripModelWithDicx:dicx];
            [modelArr addObject:model];
        }
    }
    
    return modelArr;
}

#pragma mark - 查 根据UploadOver查一堆行程
-(NSArray<UBMTripTableModel*>*)getUBMTripTableModelOfUploadOver:(BOOL)UploadOver{
    
    NSString* hs = @"0";
    if (UploadOver == YES) {
        hs = @"1";
    }
    NSString* execSql = [NSString stringWithFormat:@"select * from '%@' where uploadOver = '%@'", ubmTableName, hs];
    
    NSMutableArray* modelArr = [[NSMutableArray alloc] init];
    
    NSArray* resultArray = [[UBMFmdbHelper sharedFMDBHelper] qureyWithSql:execSql];
    if (resultArray && resultArray.count>0) {
        
        for (NSDictionary* dicx in resultArray) {
            UBMTripTableModel* model = [self getTripModelWithDicx:dicx];
            [modelArr addObject:model];
        }
    }
    
    return modelArr;
}

#pragma mark - 获取一段五分钟pb数据。
-(UBMTripPBModel *)getUBMTripPBModelOfPbName:(NSString *)pbName OfLocTripID:(NSString* )locTripId OrTripID:(NSString*)tripID{
    if ([UBMMTools IsEmptyStr:pbName]) {
        return nil;
    }
    
    UBMTripPBModel* thePbModel = nil;
    
    UBMTripTableModel* tripMdoel = nil;
    
    if (![UBMMTools IsEmptyStr:tripID]) {
        tripMdoel = [[UBMSqliteHelper sharedHelper] getUBMTripTableModelOfLocTripID:@"" OrTripID:tripID];
    }else{
        tripMdoel = [[UBMSqliteHelper sharedHelper] getUBMTripTableModelOfLocTripID:locTripId OrTripID:@""];
    }
    
    if (tripMdoel == nil) {
        return nil;
    }
 
    for (UBMTripPBModel* pbModel in tripMdoel.tripPBList) {
        
        if ([pbModel.pbName isEqualToString:pbName]) {
            thePbModel = pbModel;
            break;
        }
        
    }
    
    return thePbModel;
}

#pragma mark - 改
-(BOOL)updateaTrip:(UBMTripTableModel *)model OfTripID:(NSString *)tripId OrLocTripID:(NSString *)locTripId{
    NSString *key = @"tripID";
    NSString *keyvalue = tripId;
    if ([UBMMTools IsEmptyStr:tripId]) {
        key = @"locTripID";
        keyvalue = locTripId;
    }
    
    NSString* hadstop = @"0";
    if (model.hadStop == YES) {
        hadstop = @"1";
    }
    NSString* uploadOver = @"0";
    if (model.uploadOver == YES) {
        uploadOver = @"1";
    }
    
    //pblistmodel数组转成json
    NSMutableArray* pblistArr = [[NSMutableArray alloc] init];
    if (model.tripPBList) {
        for (UBMTripPBModel* pbmodel in model.tripPBList) {
            
            NSString* hadUpload = @"0";
            if (pbmodel.hadUpload == YES) {
                hadUpload = @"1";
            }
            
            if ([UBMMTools IsEmptyStr:pbmodel.pbMd5]) {
                pbmodel.pbMd5 = @"";
            }
            
            if ([UBMMTools IsEmptyStr:pbmodel.pbName]) {
                pbmodel.pbName = @"";
            }
            
            NSDictionary* dicx = [[NSDictionary alloc] init];
            dicx = @{@"pbName":pbmodel.pbName, @"pbMd5":pbmodel.pbMd5, @"hadUpload":hadUpload};
            [pblistArr addObject:dicx];
        }
    }
    NSString* pblistJson = @"";
    if (pblistArr.count>0) {
        pblistJson = [UBMMTools JsonWithArrary:pblistArr];
    }
    
    NSString *execSql = [NSString stringWithFormat:@"UPDATE %@ SET startTime='%@', lastUpdateTime='%@', mileage='%@', totalTime='%@', hadStop='%@', uploadOver='%@', tripPBList='%@'  WHERE %@ = '%@'", ubmTableName, model.startTime, model.lastUpdateTime, model.mileage, model.totalTime, hadstop, uploadOver, pblistJson, key, keyvalue];

    
    BOOL result = [[UBMFmdbHelper sharedFMDBHelper] notResustSetWithSql:execSql];
    if (result == YES) {
        //NSLog(@"更改一个tripmodel成功！");
    }else{
        NSLog(@"更改一个tripmodel失败！");
    }
    return result;
}


-(BOOL)updateTripID:(NSString*)tripId OfLocTripID:(NSString* )locTripId{
    
    return [self commomUpdate:@"tripID" andValue:tripId andLocTripID:locTripId OrTripID:@""];
}

-(BOOL)updateMileage:(NSString*)mileage OfLocTripID:(NSString* )locTripId OrTripID:(NSString*)tripID{
    
    return [self commomUpdate:@"mileage" andValue:mileage andLocTripID:locTripId OrTripID:tripID];
}

-(BOOL)updateLastUpdateTime:(NSString *)lastUpdateTime OfLocTripID:(NSString *)locTripId OrTripID:(NSString *)tripID{
    
    return [self commomUpdate:@"lastUpdateTime" andValue:lastUpdateTime andLocTripID:locTripId OrTripID:tripID];
}

-(BOOL)updateTotalTime:(NSString*)totalTime OfLocTripID:(NSString* )locTripId OrTripID:(NSString*)tripID{
    
    return [self commomUpdate:@"totalTime" andValue:totalTime andLocTripID:locTripId OrTripID:tripID];
}

-(BOOL)updateHadStop:(BOOL)hadStop OfLocTripID:(NSString* )locTripId OrTripID:(NSString*)tripID{
    
    NSString* keyvalue = @"0";
    if (hadStop == YES) {
        keyvalue = @"1";
    }
    return [self commomUpdate:@"hadStop" andValue:keyvalue andLocTripID:locTripId OrTripID:tripID];
}

-(BOOL)updateIsAutoStop:(BOOL)isAutoStop OfLocTripID:(NSString *)locTripId OrTripID:(NSString *)tripID{
    NSString* keyvalue = @"0";
    if (isAutoStop == YES) {
        keyvalue = @"1";
    }
    return [self commomUpdate:@"isAutoStop" andValue:keyvalue andLocTripID:locTripId OrTripID:tripID];
}

-(BOOL)updateUploadOver:(BOOL)uploadOver OfLocTripID:(NSString* )locTripId OrTripID:(NSString*)tripID{
    
    NSString* keyvalue = @"0";
    if (uploadOver == YES) {
        keyvalue = @"1";
    }
    return [self commomUpdate:@"uploadOver" andValue:keyvalue andLocTripID:locTripId OrTripID:tripID];
}

//创建一段pb时，和上传完一段pb时。
-(BOOL)updateItemOfTripPBList:(UBMTripPBModel *)pbModel OfLocTripID:(NSString *)locTripId OrTripID:(NSString *)tripID{
    
    //搞个信号量。同一时刻 只能进行一个pbModel的更改（读+写必须保证在一起）。防止多线程同时更改数据的安全性。
    dispatch_semaphore_wait(self.pbSignal, DISPATCH_TIME_FOREVER);
    
    if (pbModel == nil) {
        dispatch_semaphore_signal(self.pbSignal);
        return NO;
    }
    
    //先查出来，如果有就改那个数据；如果没有，则插入。
    UBMTripTableModel* tripModel = [self getUBMTripTableModelOfLocTripID:locTripId OrTripID:tripID];
    if (tripModel == nil) {
        dispatch_semaphore_signal(self.pbSignal);
        return NO;
    }
    
    if (tripModel.tripPBList && tripModel.tripPBList.count>0) {
        
        for (int i=0; i<tripModel.tripPBList.count; i++) {
            
            UBMTripPBModel* itemModel = tripModel.tripPBList[i];
            if ([itemModel.pbName isEqualToString:pbModel.pbName]) {
                [tripModel.tripPBList replaceObjectAtIndex:i withObject:pbModel];
                break;
            }else{
                if (i == tripModel.tripPBList.count-1) {
                    [tripModel.tripPBList addObject:pbModel];
                }
            }
        }
    }else{
        tripModel.tripPBList = [[NSMutableArray alloc] init];
        [tripModel.tripPBList addObject:pbModel];
    }
    
    BOOL reslut = [self updateaTrip:tripModel OfTripID:tripID OrLocTripID:locTripId];
    if (reslut == YES) {
        NSLog(@"更改一段pb成功！");
    }else{
        NSLog(@"更改一段pb失败！");
    }
    
    dispatch_semaphore_signal(self.pbSignal);
    return reslut;
}


-(BOOL)commomUpdate:(NSString*)Name andValue:(NSString*)value andLocTripID:(NSString* )locTripId OrTripID:(NSString*)tripID{
    
    NSString* keyName = [[NSString alloc] init];
    NSString* keyValue = [[NSString alloc] init];
    if ([UBMMTools IsEmptyStr:locTripId]) {
        keyName = @"tripID";
        keyValue = tripID;
    }else{
        keyName = @"locTripID";
        keyValue = locTripId;
    }
    
    NSString *execSql = [NSString stringWithFormat:@"UPDATE %@ SET %@ = '%@' WHERE %@ = '%@'", ubmTableName, Name, value, keyName, keyValue];
    BOOL result = [[UBMFmdbHelper sharedFMDBHelper] notResustSetWithSql:execSql];
    if (result == YES) {
        NSLog(@"数据库表更新成功");
    }else{
        NSLog(@"数据库表更新失败");
    }
    
    return result;
}


-(UBMTripTableModel*)getTripModelWithDicx:(NSDictionary*)dicx{
    UBMTripTableModel* model = [[UBMTripTableModel alloc] init];
    model.locTripID = dicx[@"locTripID"];
    model.tripID = dicx[@"tripID"];
    model.vid = dicx[@"vid"];
    model.startTime = dicx[@"startTime"];
    model.lastUpdateTime = dicx[@"lastUpdateTime"];
    model.mileage = dicx[@"mileage"];
    model.totalTime = dicx[@"totalTime"];
    
    if ([dicx[@"hadStop"] boolValue] == YES) {
        model.hadStop = YES;
    }else{
        model.hadStop = NO;
    }
    
    if([UBMMTools IsEmptyStr:dicx[@"isAutoStop"]] && [dicx[@"isAutoStop"] intValue] == 1){
        model.isAutoStop = YES;
    }else{
        model.isAutoStop = NO;
    }
    
    if(![UBMMTools IsEmptyStr:dicx[@"isAutoStart"]] && [dicx[@"isAutoStart"] intValue] == 1){
        model.isAutoStart = YES;
    }else{
        model.isAutoStart = NO;
    }
    
    if ([dicx[@"uploadOver"] boolValue] == YES) {
        model.uploadOver = YES;
    }else{
        model.uploadOver = NO;
    }
    
    NSArray* pbArr = [UBMMTools jsonToObject:dicx[@"tripPBList"]];
    if (pbArr && [pbArr isKindOfClass:[NSArray class]] && pbArr.count>0) {
        
        NSMutableArray* itemarr = [[NSMutableArray alloc] init];
        for (NSDictionary* dicx in pbArr) {
            UBMTripPBModel* pbmodel = [self getPbModelWith:dicx];
            [itemarr addObject:pbmodel];
        }
        model.tripPBList = itemarr;
    }
    
    return model;
}

-(UBMTripPBModel*)getPbModelWith:(NSDictionary*)dicx{
    
    UBMTripPBModel* pbmodel = [[UBMTripPBModel alloc] init];
    pbmodel.pbName = dicx[@"pbName"];
    pbmodel.pbMd5 = dicx[@"pbMd5"];
    if ([dicx[@"hadUpload"] isEqualToString:@"0"]) {
        pbmodel.hadUpload = NO;
    }else{
        pbmodel.hadUpload = YES;
    }
    return  pbmodel;
}


-(BOOL)deleteTheTripOflocTripId:(NSString *)locTripId{
    
    NSString* execSql = [NSString stringWithFormat:@"delete from '%@' where locTripID = '%@'", ubmTableName, locTripId];
    BOOL result = [[UBMFmdbHelper sharedFMDBHelper] notResustSetWithSql:execSql];
    if (result == YES) {
        NSLog(@"删除一条行程成功: locTripId=%@",locTripId);
    }else{
        NSLog(@"删除一条行程失败: locTripId=%@",locTripId);
    }
    
    return result;
}
@end
