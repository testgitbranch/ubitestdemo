//
//  UBMTripPBModel.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/12/14.
//  Copyright © 2020 魏振伟. All rights reserved.
//

/*
 一个行程中的一段pb的model
 */

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UBMTripPBModel : NSObject

@property(nonatomic, copy)NSString* pbName;
@property(nonatomic, copy)NSString* pbMd5;
@property(nonatomic, assign)BOOL hadUpload;

@end

NS_ASSUME_NONNULL_END
