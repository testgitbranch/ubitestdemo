//
//  UBMTripTableModel.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/12/14.
//  Copyright © 2020 魏振伟. All rights reserved.
//

/*
 一个行程model。
 */

#import <Foundation/Foundation.h>
#import "UBMTripPBModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface UBMTripTableModel : NSObject

@property(nonatomic, copy)NSString* locTripID;  //本地行程ID
@property(nonatomic, copy)NSString* tripID;     //服务端行程ID
@property(nonatomic, copy)NSString* vid;        //车辆id
@property(nonatomic, copy)NSString* startTime;  //行程开始时间
@property(nonatomic, copy)NSString* lastUpdateTime; //行程当前的时间。用来计算是否15分钟内再次打开。
@property(nonatomic, copy)NSString* mileage;    //里程
@property(nonatomic, copy)NSString* totalTime;  //总时间
@property(nonatomic, assign)BOOL hadStop;       //是否已经结束行程
@property(nonatomic, assign)BOOL isAutoStart;   //是否是自动开始 自动1 手动0
@property(nonatomic, assign)BOOL isAutoStop;    //是否是自动结束 自动1 手动0
@property(nonatomic, assign)BOOL uploadOver;    //是否已上传结束
@property(nonatomic, strong)NSMutableArray<UBMTripPBModel*>* tripPBList;  //行程分段pb文件数据（包括名称、状态、MD5值）

@end

NS_ASSUME_NONNULL_END
