//
//  UBMSqliteHelper.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/12/14.
//  Copyright © 2020 魏振伟. All rights reserved.
//

/*
 处理行程数据文件与数据库的交互
 */

#import <Foundation/Foundation.h>
#import "UBMTripTableModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface UBMSqliteHelper : NSObject

#pragma mark - 初始化方法

+(instancetype)sharedHelper;

#pragma mark - 切换数据库。用于切换账号。
-(void)changeUserSqlite;

#pragma mark - 表操作

//新增表
-(BOOL)creatUBMTripTable;

//删除表
-(BOOL)deleteUBMTripTable;


#pragma mark - 新增一条tirp记录 startType:1自动；0手动
-(BOOL)addNewTripItemWith:(NSString* )locTripId andVid:(NSString*)vid andStartType:(int)startType;


#pragma mark - 查询tirp记录

//查一个
-(UBMTripTableModel*)getUBMTripTableModelOfLocTripID:(NSString* )locTripId OrTripID:(NSString*)tripID;
//根据HadStop查一组
-(NSArray<UBMTripTableModel*>*)getUBMTripTableModelOfHadStop:(BOOL)HadStop;
//根据UploadOver查一组
-(NSArray<UBMTripTableModel*>*)getUBMTripTableModelOfUploadOver:(BOOL)UploadOver;
//根据pbName查一个UBMTripPBModel
-(UBMTripPBModel*)getUBMTripPBModelOfPbName:(NSString*)pbName OfLocTripID:(NSString* )locTripId OrTripID:(NSString*)tripID;


#pragma mark - 更新trip数据

//需要注意：更新tripId必须使用本方法。其他的方法都是以tripId或locTripId作为主键进行操作。
-(BOOL)updateTripID:(NSString*)tripId OfLocTripID:(NSString* )locTripId;

//改一整条。
-(BOOL)updateaTrip:(UBMTripTableModel*)model OfTripID:(NSString*)tripId OrLocTripID:(NSString* )locTripId;


//改里程
-(BOOL)updateMileage:(NSString*)mileage OfLocTripID:(NSString* )locTripId OrTripID:(NSString*)tripID;

//改行程当前的时间
-(BOOL)updateLastUpdateTime:(NSString*)lastUpdateTime OfLocTripID:(NSString* )locTripId OrTripID:(NSString*)tripID;

//改总时间
-(BOOL)updateTotalTime:(NSString*)totalTime OfLocTripID:(NSString* )locTripId OrTripID:(NSString*)tripID;
//改是否已停止本行程
-(BOOL)updateHadStop:(BOOL)hadStop OfLocTripID:(NSString* )locTripId OrTripID:(NSString*)tripID;
//改 本行程的结束类型：自动还是手动
-(BOOL)updateIsAutoStop:(BOOL)isAutoStop OfLocTripID:(NSString* )locTripId OrTripID:(NSString*)tripID;
//改本行程pb是否已上传、校验、合并完毕。结束完成。
-(BOOL)updateUploadOver:(BOOL)uploadOver OfLocTripID:(NSString* )locTripId OrTripID:(NSString*)tripID;
//修改本行程一个pb文件的状态。（无则增、有则改）
-(BOOL)updateItemOfTripPBList:(UBMTripPBModel*)pbModel OfLocTripID:(NSString* )locTripId OrTripID:(NSString*)tripID;

#pragma mark - 删除
-(BOOL)deleteTheTripOflocTripId:(NSString*)locTripId;

@end

NS_ASSUME_NONNULL_END
