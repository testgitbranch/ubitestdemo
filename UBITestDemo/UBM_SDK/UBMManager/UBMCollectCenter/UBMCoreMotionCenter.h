//
//  UBMCoreMotionCenter.h
//  CoreMotionOC
//
//  Created by wzw on 2020/8/8.
//  Copyright © 2020 LoveCarHome. All rights reserved.
//

/*
 数据收集中心。
 建议把本类初始化成属性。
 */

#import <Foundation/Foundation.h>
#import "UBMUiDataModel.h"

NS_ASSUME_NONNULL_BEGIN

//还有传感器数据如果不能用，要怎么处理等。可能的错误情况还是很多的。
typedef enum : NSUInteger {
    CMStartErrorNone, //没有错误
    CMStartErrorCLAuthorizationStatus, //定位被拒绝
    CMStartErrorCLNotAvailable,        //定位 不可用
    CMStartErrorAMauthorizationStatus, //运动与健康 权限被拒绝
    CMStartErrorAMIsNotAvailable,      //运动与健康 不可用
} CMStartErrorType;

typedef enum : NSUInteger {
    CMCollectTypeManual,
    CMCollectTypeAuto,
} CMCollectType;

typedef void(^dataBackBlock)(NSArray* dataArr);
typedef void(^actualDataBlock)(UBMUiDataModel* dataDic);


@interface UBMCoreMotionCenter : NSObject

@property(nonatomic, copy)actualDataBlock actualBlock;  //传出更新UI的数据
@property(nonatomic, copy)dataBackBlock dataBlcok;      //传出需要保存的数据


@property(atomic, assign)BOOL ended;//是否已经结束收集，yes:已结束,no:进行中

/*
 初始化
 */
+ (instancetype)shareCoreMotionCenter;

/*
 开始之前先判断权限，要不要开启，由外部决定是开启还是不开。
 */
-(void)limitsJuge:(void(^)(CMStartErrorType response))result;

/*
 开始收集。type暂时没不到。
 */
-(void)startGetDataWithType:(CMCollectType)type;

/*
 停止收集
 */
-(void)stopGetData;

/*
 coremotion Restart。前后台切换时，重新启动coremotion收集。
 为了解决coremotion在iOS12.1.4上的停止回调。
 需要注意iOS11是否也有这个问题。
 其他的api，如
 */
-(void)restartCoreMotin;

/*
 手动设置收集类型。（此收集类型不同于本地。只有在导航中，CMCollectType才是CMCollectTypeManual）
 */
-(void)setDataCollectType:(CMCollectType)type;

/*
 设置是否是结束中:自动行程开始倒计时
 */
-(void)setAutoStoping:(BOOL)stoping;

@end

NS_ASSUME_NONNULL_END
