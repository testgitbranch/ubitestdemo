//
//  UBMAppState.h
//  LoveCarChannel_iOS
//
//  Created by majinglong on 2021/7/1.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Sensor.pbobjc.h"

NS_ASSUME_NONNULL_BEGIN

@interface UBMAppState : NSObject


@property (nonatomic, assign, readonly) AppSwitch  appSwitch;

- (void)resetAppState;

@end

NS_ASSUME_NONNULL_END
