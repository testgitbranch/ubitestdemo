//
//  UBMAppState.m
//  LoveCarChannel_iOS
//
//  Created by majinglong on 2021/7/1.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

#import "UBMAppState.h"
#import <CoreTelephony/CTCallCenter.h>
#import <CoreTelephony/CTCall.h>
#import <UIKit/UIApplication.h>

/** 需求地址：
    https://baixingkefu.yuque.com/docs/share/f7c06893-8ec3-45e1-9d1c-55af7397af8d?#
 */
@interface UBMAppState ()

@property (nonatomic, assign, readwrite) AppSwitch  appSwitch;
@property (nonatomic, strong) CTCallCenter * callCenter;
@property (nonatomic, assign) BOOL isCallComing;//是否打来电话
@end

@implementation UBMAppState

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.appSwitch = AppSwitch_ForegroundPassive;
        
        /**
         AppSwitch_BackgroundPassive = 1,   后台被动
         AppSwitch_ForegroundPassive = 2,   前台被动
         AppSwitch_BackgroundActive = 3,   后台主动
         AppSwitch_ForegroundActive = 4,   前台主动
         */
        /// 电话监听
        [self callCenterStatus];
        
        /**
         UIApplicationProtectedDataDidBecomeAvailable
         当受保护文件可供您的代码访问时发布的通知。
         UIApplicationProtectedDataWillBecomeUnavailable
         在受保护文件前不久发布的通知被锁定并无法访问。
         */
        /// 解锁、锁屏
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(protectedDataDidBecomeAvailable:) name:UIApplicationProtectedDataDidBecomeAvailable object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(protectedDataDidBecomeUnavailable:) name:UIApplicationProtectedDataWillBecomeUnavailable object:nil];
        
        /**
         UIApplicationDidBecomeActiveNotification
         当应用程序处于活动状态时发布的通知。
         UIApplicationDidEnterBackgroundNotification
         当应用程序进入后台时发布的通知。
         UIApplicationWillEnterForegroundNotification
         在应用程序成为活动应用程序之前不久发布的通知，该通知将离开后台状态。
         UIApplicationWillResignActiveNotification
         当应用程序不再活动并失去焦点时发布的通知。
         UIApplicationWillTerminateNotification
         当应用程序即将终止时发布的通知。
         */
        /// app生命周期
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willTerminate:) name:UIApplicationWillTerminateNotification object:nil];
    }
    return self;
}

- (void)resetAppState {
    self.appSwitch = AppSwitch_ForegroundPassive;
}


/// MARK: 锁屏、解锁-监听
- (void)protectedDataDidBecomeUnavailable:(NSNotification *)notification {
    self.appSwitch = AppSwitch_BackgroundPassive;
}

- (void)protectedDataDidBecomeAvailable:(NSNotification *)notification {
    self.appSwitch = AppSwitch_ForegroundActive;
}

/// MARK: app生命周期：
//前台
- (void)willEnterForeground:(NSNotification *)notification {
    
    
    self.appSwitch = AppSwitch_ForegroundActive;
}

- (void)didBecomeActive:(NSNotification *)notification {
    
    if (self.isCallComing == YES) {
        self.isCallComing = NO;
        return;
    }
    
    self.appSwitch = AppSwitch_ForegroundActive;
}

//后台
- (void)willResignActive:(NSNotification *)notification {
    if (self.isCallComing == YES) {
        //此时已经收到来电状态
        return;
    }
    
    self.appSwitch = AppSwitch_BackgroundActive;
}

- (void)didEnterBackground:(NSNotification *)notification {
    if (self.isCallComing == YES) {
        //此时已经收到来电状态
        return;
    }
    self.appSwitch = AppSwitch_BackgroundActive;
}
////结束
//- (void)willTerminate:(NSNotification *)notification {
//
//}

/// MARK: 电话状态监听
- (void)callCenterStatus {
    
    self.callCenter = [[CTCallCenter alloc] init];
    
    __weak typeof(self) weakSelf = self;
    [self.callCenter setCallEventHandler:^(CTCall *call){
        __strong typeof(self) strongSelf = weakSelf;
        
        if (call.callState == CTCallStateDisconnected) {
            strongSelf.appSwitch = AppSwitch_ForegroundPassive;
            NSLog(@"call____挂断");
            
        } else if (call.callState == CTCallStateConnected) {
            NSLog(@"call____已接通");
            
        } else if(call.callState == CTCallStateIncoming) {
            strongSelf.appSwitch = AppSwitch_BackgroundPassive;
            NSLog(@"call____来电");
            if (@available(iOS 14.0, *)) {
                
            } else {
                strongSelf.isCallComing = YES;
            }
        } else if (call.callState == CTCallStateDialing) {
            NSLog(@"call____拨号中");
            
        } else {
            NSLog(@"call____未知");
        }
    }];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
