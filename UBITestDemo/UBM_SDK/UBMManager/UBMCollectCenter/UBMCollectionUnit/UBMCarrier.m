//
//  UBMCarrier.m
//  LoveCarChannel_iOS
//
//  Created by majinglong on 2021/6/29.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

#import "UBMCarrier.h"
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>


@interface UBMCarrier ()

@property (nonatomic, strong) CTTelephonyNetworkInfo * networkInfo;
@property (atomic, strong) NSMutableArray * carrierDataArray;

@end

@implementation UBMCarrier

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.networkInfo = [[CTTelephonyNetworkInfo alloc] init];
        self.carrierDataArray = [[NSMutableArray alloc] init];
        ///添加监听
        [self addRadioUpdateNotification];
        ///初始获取基站信息
        [self updateRadioInfo];
        NSLog(@"ios__carrier___beigin__array___%@",self.carrierDataArray);
    }
    return self;
}

- (void)addRadioUpdateNotification {
    
    if (@available(iOS 12.0, *)) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(carrierDidChange:) name:CTServiceRadioAccessTechnologyDidChangeNotification object:nil];
    } else {
        // Fallback on earlier versions
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(carrierDidChange:) name:CTRadioAccessTechnologyDidChangeNotification object:nil];
    }
    NSLog(@"ios__carrier___noti__install___%@",[NSThread currentThread]);
}

- (void)carrierDidChange:(NSNotification *)noti {
    NSLog(@"ios__carrier___noti_%@__%@____%@",noti,noti.object,[noti.object class]);
    ///输出线程为 子线程
    NSLog(@"ios__carrier___noti__complete___%@",[NSThread currentThread]);
    [self updateRadioInfo];
    
    if (@available(iOS 12.0, *)) {
        NSLog(@"ios__carrier___noti__curr___%@",self.networkInfo.serviceCurrentRadioAccessTechnology);
    } else {
        // Fallback on earlier versions
        NSLog(@"ios__carrier___noti__curr___%@",self.networkInfo.currentRadioAccessTechnology);
    }

    NSLog(@"ios__carrier___noti__array___%@",self.carrierDataArray);
}

- (void)updateRadioInfo {
    @synchronized(self){
        
        NSMutableArray * dataArray = [[NSMutableArray alloc] init];
        if (@available(iOS 12.0, *)) {
            ///ios12返回 空集合； ios10返回nil； 下面判断使用value值的数量
            NSDictionary * radioDict = [self.networkInfo serviceCurrentRadioAccessTechnology];
            for (NSString * value in radioDict.allValues) {
                Cell * cell = [self changeRadioToCellType:value];
                //基站信息类型不为EMPTY
                if (cell && cell.cellType != CellType_Empty) {
                    [dataArray addObject:cell];
                }
            }
            /// 有基站信息 && 没有匹配到类型，添加EMPTY类型
            if (radioDict.allValues.count && dataArray.count == 0) {
                Cell * cell = [[Cell alloc] init];
                cell.dbm = 100;
                cell.cellType = CellType_Empty;
                [dataArray addObject:cell];
            }
        } else {
            // Fallback on earlier versions
            NSString * radio = self.networkInfo.currentRadioAccessTechnology;
            Cell * cell = [self changeRadioToCellType:radio];
            if (cell) {
                [dataArray addObject:cell];
            }
        }
        self.carrierDataArray = dataArray;
    }
}

- (Cell *)changeRadioToCellType:(NSString *)radio {
    
    if (radio.length == 0) {
        return nil;
    }
    
    Cell * cell = [[Cell alloc] init];
    cell.dbm = 100;
    cell.cellType = CellType_Empty;
    //LTE 4G
    if ([radio isEqualToString:CTRadioAccessTechnologyLTE]) {
        cell.cellType = CellType_Lte;
    } else if ([radio isEqualToString:CTRadioAccessTechnologyGPRS]) { //GSM  2G
        cell.cellType = CellType_Gsm;
    } else if ([radio isEqualToString:CTRadioAccessTechnologyWCDMA]) { //WCDMA 3G
        cell.cellType = CellType_Wcdma;
    }
        
    return cell;
}

- (NSArray<Cell *> *)getCurrentRadioInfo {
    return self.carrierDataArray;
}


/// MARK: 暂时用不到
- (Cell *)carrierWithMNC:(NSString *)mnc {
    /// 若为空不返回默认数据
    if (mnc.length == 0) {
        return nil;
    }
    
    Cell * cell = [[Cell alloc] init];
    cell.dbm = 100;
    cell.cellType = CellType_Empty;
    
    ///中国移动MNC为00、02、07，中国联通的MNC为01、06、09，中国电信的MNC为03、05、11
    if ([mnc isEqualToString:@"00"] || [mnc isEqualToString:@"02"] || [mnc isEqualToString:@"07"]) {
        //移动
        cell.cellType =  CellType_Lte;
    } else if ([mnc isEqualToString:@"03"] || [mnc isEqualToString:@"05"] || [mnc isEqualToString:@"11"]) {
        //电信
        cell.cellType =  CellType_Gsm;
    } else if ([mnc isEqualToString:@"01"] || [mnc isEqualToString:@"06"] || [mnc isEqualToString:@"09"]) {
        //联通
        cell.cellType =  CellType_Wcdma;
    }
    
    return cell;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


@end
