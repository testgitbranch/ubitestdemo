//
//  UBMCarrier.h
//  LoveCarChannel_iOS
//
//  Created by majinglong on 2021/6/29.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Sensor.pbobjc.h"

NS_ASSUME_NONNULL_BEGIN

@interface UBMCarrier : NSObject

/// 获取手机卡运营商基站信息
- (NSArray<Cell *> *)getCurrentRadioInfo;

@end

NS_ASSUME_NONNULL_END
