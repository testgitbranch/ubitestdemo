//
//  UBMUiDataModel.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/10/19.
//  Copyright © 2020 魏振伟. All rights reserved.
//

/*
 传出的UI更新的model。专为CoreMotionCenter传出UI数据使用。
 */

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UBMUiDataModel : NSObject

@property(nonatomic, assign) double speed;
@property(nonatomic, assign) double lat;
@property(nonatomic, assign) double lon;
@property(nonatomic, assign) double timeStemp;
@property(nonatomic, assign) double speedAccuracy;

@property(nonatomic, strong) CLLocation *location;

@end

NS_ASSUME_NONNULL_END
