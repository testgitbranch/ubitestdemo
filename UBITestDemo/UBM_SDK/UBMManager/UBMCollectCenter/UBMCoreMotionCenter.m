//
//  UBMCoreMotionCenter.m
//  CoreMotionOC
//
//  Created by wzw on 2020/8/8.
//  Copyright © 2020 LoveCarHome. All rights reserved.
//

#import "UBMCoreMotionCenter.h"
#import <CoreMotion/CoreMotion.h>
#import <CoreLocation/CoreLocation.h>

#import "Sensor.pbobjc.h"
#import "GPBMessage.h"
#import "UBMCarrier.h"
#import "UBMAppState.h"
#import <AMapLocationKit/AMapLocationKit.h>
#import "UBMBaseInforManagerment.h"
#import "UBMPrefixHeader.h"

//static float DistanceFilter = 10.0;    //最小移动距离
static float CoreMotionInterval = 10.0;  //100 占用16%; 50 %9; 10 3%; 收集频率 次/s
static int saveTime = 5;    //x s保存一次数据
static float uiTime = 1.0;  //x s更新一次UI

@interface UBMCoreMotionCenter()<AMapLocationManagerDelegate>

@property(nonatomic, strong) dispatch_semaphore_t signal1; //信号量1
@property(nonatomic, strong) dispatch_queue_t myGCDTimerQueue;
@property(nonatomic, strong) dispatch_source_t myGCDTimer;
@property(nonatomic, assign) float pinlv;
@property(nonatomic, assign) long  maxCount;
@property(nonatomic, assign) long  maxUICount;

@property(atomic, strong)NSMutableArray* ListArr;  //收集数据的数组

@property(atomic, assign)CollectType CollectType; //开启收集的类型
@property(atomic, assign)int willStoping;         //自动结束开始倒计时

@property(nonatomic, strong)CMMotionManager* motionManager;      //设备数据管理类

//@property(nonatomic, strong)CLLocationManager* locationManager;  //位置信息管理类
@property(nonatomic, strong)AMapLocationManager* locationManager; //高德定位管理类
@property(nonatomic, assign)CLAuthorizationStatus locationStatus; //位置权限状态
@property(atomic, strong)CLLocation* location; //当前位置

@property(nonatomic, strong)CMMotionActivityManager* motionActivityManager; //运动状态管理类

@property(atomic, strong)MotionActivity* motionActivity; //记录当前的运动数据

@property(atomic, assign)int proximityState;             //是否靠近了某物体。单独使用通知监听。
@property(atomic, assign)double proximityStateChangeTime; //状态改变时的时间戳

@property(atomic, assign)DeviceOrientationType deviceOrientation; //设备方向。（当关闭屏幕自动旋转后，不能被监听）
@property(atomic, assign)double deviceOrientationChangeTime;      //设备方向改变时间。

@property(atomic, assign)CallState callstate;//电话状态

@property(atomic, assign)AudioInputDevice audioInputType;  //音频输入类型
@property(atomic, assign)AudioOutputDevice audioOutputType; //音频输入类型

@property (nonatomic, strong) UBMCarrier * ubmCarrier;//手机卡运营商信息
@property (atomic, strong) UBMAppState * appState;    //app前后台状态

@end

@implementation UBMCoreMotionCenter

+(instancetype)shareCoreMotionCenter
{
    static id instance;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    
    return instance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.signal1 = dispatch_semaphore_create(1);
        
        self.pinlv = 1.0/CoreMotionInterval;
        self.maxCount = saveTime*CoreMotionInterval;
        self.maxUICount = uiTime*CoreMotionInterval;
        
        dispatch_queue_attr_t attr = dispatch_queue_attr_make_with_qos_class(DISPATCH_QUEUE_SERIAL, QOS_CLASS_USER_INITIATED, 0);
        self.myGCDTimerQueue = dispatch_queue_create("com.icetime.bxcolloctiongcdtimer", attr);
        
        self.ended = YES;
        
        self.ListArr = [[NSMutableArray alloc] init];
        
        self.ubmCarrier = [[UBMCarrier alloc] init];
        
        self.appState = [[UBMAppState alloc] init];
        
        self.motionManager = [[CMMotionManager alloc] init];
        
        self.motionActivityManager = [[CMMotionActivityManager alloc] init];
        
        //init的时候就会请求位置权限。
        self.locationManager = [[AMapLocationManager alloc] init];
        self.locationManager.delegate = self;
        self.locationManager.allowsBackgroundLocationUpdates = YES;
        self.locationManager.pausesLocationUpdatesAutomatically = NO;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        self.locationManager.distanceFilter = kCLDistanceFilterNone;
        
        //初始化的时候开启一下，为了弹出定位权限框。
        [self.locationManager startUpdatingLocation];
        [self.locationManager stopUpdatingLocation];
        
        //在后台如果有任何位置活动，即使APP已经被干掉，也会自启动，太耗电。
        //不会有蓝色提示条。
        //[self.locationManager requestAlwaysAuthorization];
        
        //配合allowsBackgroundLocationUpdates =yes, 和background mode的设置，可以使应该保持后台状态。
        //当应用被干掉，或者位置更新停止后，就不会再做任何动作：比如因位置拉起APP。省电。
        //并会有默认的蓝色提示条。
        //[self.locationManager requestWhenInUseAuthorization];

        [[UBMBaseInforManagerment sharedInstance] addObserver:self forKeyPath:@"callCenterStatus" options:NSKeyValueObservingOptionNew context:nil];
        self.callstate = CallState_Idle;
        
        //请求一次蓝牙状态初始值
        [self BleToothOutput];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(proximityChange) name:UIDeviceProximityStateDidChangeNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationDidChange) name:UIDeviceOrientationDidChangeNotification object:nil];
        //监听输出设备变化
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(outputDeviceChanged:) name:AVAudioSessionRouteChangeNotification object:[AVAudioSession sharedInstance]];
        
    }
    return self;
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceProximityStateDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVAudioSessionRouteChangeNotification object:nil];
    [[UBMBaseInforManagerment sharedInstance] removeObserver:self forKeyPath:@"callCenterStatus"];
}

#pragma mark - 定位回调
-(void)amapLocationManager:(AMapLocationManager *)manager doRequireLocationAuth:(CLLocationManager *)locationManager{
    [locationManager requestAlwaysAuthorization];
}

-(void)amapLocationManager:(AMapLocationManager *)manager didUpdateLocation:(CLLocation *)location{
    NSLog(@"定位更新");
    if (location) {
        self.location = location;
    }
}

-(void)amapLocationManager:(AMapLocationManager *)manager didFailWithError:(NSError *)error{
    NSLog(@"定位失败");
    [UBMSLSLog addUBMLog:@"定位失败"];
    self.location = nil;
}

#pragma mark -定位权限更改、要重新start才能继续获取位置信息
- (void)amapLocationManager:(AMapLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    
    if (self.locationStatus == kCLAuthorizationStatusRestricted || self.locationStatus == kCLAuthorizationStatusDenied)
    {
        //如果是进行中，切从拒绝定位的状态切换回来，则重启定位
        if (self.ended == NO && status != kCLAuthorizationStatusRestricted && status != kCLAuthorizationStatusDenied)
        {
            [self.locationManager stopUpdatingLocation];
            [self.locationManager startUpdatingLocation];
            NSLog(@"已重启定位");
        }
        
    }
    
    self.locationStatus = status;
}

- (void)amapLocationManager:(AMapLocationManager *)manager locationManagerDidChangeAuthorization:(CLLocationManager*)locationManager{
    
    if (@available(iOS 14.0, *))
    {
        if (self.locationStatus == kCLAuthorizationStatusRestricted || self.locationStatus == kCLAuthorizationStatusDenied)
        {
            //如果是进行中，且从拒绝定位的状态切换回来，则重启定位
            if (self.ended == NO && locationManager.authorizationStatus != kCLAuthorizationStatusRestricted && locationManager.authorizationStatus != kCLAuthorizationStatusDenied) {
                [self.locationManager stopUpdatingLocation];
                [self.locationManager startUpdatingLocation];
                NSLog(@"已重启定位");
            }
        }
        
        self.locationStatus = locationManager.authorizationStatus;
    }

}

#pragma mark - 权限判断
-(void)limitsJuge:(void(^)(CMStartErrorType response))result{
    
    //判断位置权限：如果没有位置权限，则给出错误提示。
    if (CLLocationManager.authorizationStatus == kCLAuthorizationStatusAuthorizedWhenInUse || CLLocationManager.authorizationStatus == kCLAuthorizationStatusAuthorizedAlways) {
        //开启定位更新
        //[self.locationManager startUpdatingLocation];
    }else{
        
        if ([CLLocationManager locationServicesEnabled] == NO) {
            result(CMStartErrorCLNotAvailable);
        }else{
            //被拒绝 或者禁用了定位
            result(CMStartErrorCLAuthorizationStatus);
        }
        
        return;
    }
    
    //运动数据如果获取不到，需要暂停吗？看产品需求
    if (@available(iOS 11.0, *)) {
        if (CMMotionActivityManager.authorizationStatus == CMAuthorizationStatusRestricted) {
            //没有开启运动与健身
            result(CMStartErrorAMIsNotAvailable);
            return;
        }else if (CMMotionActivityManager.authorizationStatus == CMAuthorizationStatusDenied) {
            //运动与健身被拒绝
            result(CMStartErrorAMauthorizationStatus);
            return;
        }
    }
    
    //运动与健身不可用，需要暂停吗？看产品需求
    if (![CMMotionActivityManager isActivityAvailable]) {
        result(CMStartErrorAMIsNotAvailable);
        return;
    }
    
     result(CMStartErrorNone);
}

#pragma mark - 开始收集数据
-(void)startGetDataWithType:(CMCollectType)type{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self setDataCollectType:CMCollectTypeAuto];
        
        self.ended = NO;
        
        self.willStoping = EndTag_Run;
        
        [self.appState resetAppState];
        
        [self.ListArr removeAllObjects];
        
        //复位一些基础数据
        [self proximityChange];
        [self orientationDidChange];
        [self BleToothOutput];
        
        //开启定位更新
        [self.locationManager startUpdatingLocation];
        
        //开启物体距离监听
        [UIDevice currentDevice].proximityMonitoringEnabled = YES;
        
        //开启手机方向监听
        [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
        
        //开启运动状态更新。
        [self motionActivityManagerStart];
        
        //开启传感器数据收集
        [self startUseAccelerometerPush];

        [self startTimer];
        
    });
    
}

#pragma mark -开启定时器; 换成定时器方式取数据,iOS12在打电话时传感器回调会暂停。
-(void)startTimer{
    
    /// 创建GCD timer
    self.myGCDTimer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, self.myGCDTimerQueue);
    
    /// 设置timer
    uint64_t interval = self.pinlv * NSEC_PER_SEC;
    dispatch_source_set_timer(self.myGCDTimer, DISPATCH_TIME_NOW, interval, 0);

    /// 设置timer的执行函数
    __weak typeof(self) weakself = self;
    dispatch_source_set_event_handler(self.myGCDTimer, ^{
        [weakself dataSplitting];
    });
    
    /// 启动timer
    dispatch_resume(self.myGCDTimer);
    
    //换成定时器方式取数据。因为iOS12在打电话时传感器回调会暂停。
    /*
    self.myTimer = [NSTimer timerWithTimeInterval:self.pinlv target:self selector:@selector(dataSplitting) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:self.myTimer forMode:NSRunLoopCommonModes];
     */
}

#pragma mark -数据组装
-(void)dataSplitting{
    
    CLLocation* loc = nil;
    if (self.location) {
        loc = [self.location copy];
    }else{
        NSLog(@"location获取结果为空");
        //[BXUbmSLSLog addUBMLog:@"location获取结果为空"];
    }
    
    SensorData* baseRoot = [[SensorData alloc] init];
    baseRoot.os = Os_IOs;
    baseRoot.type = self.CollectType;
    
    Gnss* gnss = [[Gnss alloc] init];
    if (loc && loc.timestamp) {
        gnss.timestamp = [loc.timestamp timeIntervalSince1970];
    }
    gnss.latitude = loc.coordinate.latitude;
    gnss.longitude = loc.coordinate.longitude;
    gnss.altitude = loc.altitude;
    gnss.speed = loc.speed;
    gnss.bearing = loc.course;
    gnss.horizontalAccuracy = loc.horizontalAccuracy;
    gnss.verticalAccuracy = loc.verticalAccuracy;
    gnss.coordinateSystem = CoordinateSystem_Gcj02;
    //todo
    if (@available(iOS 10.0, *)) {
        gnss.speedAccuracy = loc.speedAccuracy;
    } else {
        gnss.speedAccuracy = -1;
    }
    if (@available(iOS 13.4, *)) {
        gnss.bearingAccuracy = loc.courseAccuracy;
    } else {
        gnss.bearingAccuracy = -1;
    }
    baseRoot.gnss = gnss;
    
    Imu* imu = [[Imu alloc] init];
    imu.timestamp = [[NSDate date] timeIntervalSince1970];
    baseRoot.imu = imu;
    
    CMDeviceMotion* motion = self.motionManager.deviceMotion;
    AxisData* gravity = [[AxisData alloc] init];
    gravity.x = motion.gravity.x;
    gravity.y = motion.gravity.y;
    gravity.z = motion.gravity.z;
    imu.gravity = gravity;
    
    AxisData* netAccel = [[AxisData alloc] init];
    netAccel.x = motion.userAcceleration.x;
    netAccel.y = motion.userAcceleration.y;
    netAccel.z = motion.userAcceleration.z;
    imu.netAccel = netAccel;
    
    
    if (self.motionManager.accelerometerData) {
        AxisData* rawAccel = [[AxisData alloc] init];
        rawAccel.x = self.motionManager.accelerometerData.acceleration.x;
        rawAccel.y = self.motionManager.accelerometerData.acceleration.y;
        rawAccel.z = self.motionManager.accelerometerData.acceleration.z;
        imu.rawAccel = rawAccel;
    }
    
    if (self.motionManager.gyroData) {
        AxisData* gyro = [[AxisData alloc] init];
        gyro.x = self.motionManager.gyroData.rotationRate.x;
        gyro.y = self.motionManager.gyroData.rotationRate.y;
        gyro.z = self.motionManager.gyroData.rotationRate.z;
        imu.gyro = gyro;
    }
    
    if (self.motionManager.magnetometerData) {
        AxisData* magneto = [[AxisData alloc] init];
        magneto.x = self.motionManager.magnetometerData.magneticField.x;
        magneto.y = self.motionManager.magnetometerData.magneticField.y;
        magneto.z = self.motionManager.magnetometerData.magneticField.z;
        imu.magneto = magneto;
    }
    
    
    Quaternion * quaternion = [[Quaternion alloc] init];
    quaternion.w = motion.attitude.quaternion.w;
    quaternion.x = motion.attitude.quaternion.x;
    quaternion.y = motion.attitude.quaternion.y;
    quaternion.z = motion.attitude.quaternion.z;
    imu.quaternion = quaternion;
    
    //运动状态数据
    if (self.motionActivity) {
        baseRoot.motions = self.motionActivity;
    }
    
    //是否靠近物体
    Proximity* py = [[Proximity alloc] init];
    py.timestamp = self.proximityStateChangeTime;
    py.proximityState = self.proximityState;
    baseRoot.proximity = py;
    
    //设备方向
    DeviceOrientation* dvor = [[DeviceOrientation alloc] init];
    dvor.timestamp = self.deviceOrientationChangeTime;
    dvor.type = self.deviceOrientation;
    baseRoot.orientation = dvor;
    
    //电话状态
    baseRoot.phoneCall = self.callstate;
    
    //行程倒计时结束状态
    baseRoot.endTag = self.willStoping;
    
    //输入输出状态
    baseRoot.input = self.audioInputType;
    baseRoot.output = self.audioOutputType;
    
    /// 运营商信息
    NSMutableArray<Cell *> * cells = [self.ubmCarrier getCurrentRadioInfo];
    baseRoot.cellListArray = cells;
    
    ///wifi
    Wifi * wifi = [[Wifi alloc] init];
    wifi.freq = -1;
    wifi.rssi = 100;
    baseRoot.wifiListArray = @[wifi].mutableCopy;
    
    /// app state
    baseRoot.appSwitch = self.appState.appSwitch;

    //超时时间给1分钟完全够用
    dispatch_semaphore_wait(_signal1, dispatch_time(DISPATCH_TIME_NOW, 60*NSEC_PER_SEC));
    [self.ListArr addObject:baseRoot];
    
    //在这判断长度，并且根据长度和频率算出时间是否该传出，然后重置数组。
    
    //ui数据
    if (self.ListArr.count%self.maxUICount == 0){
        
        if (self.actualBlock) {
            //5%-6% 100次
            UBMUiDataModel* umodel = [[UBMUiDataModel alloc] init];
            umodel.speed = loc.speed;
            if (@available(iOS 10.0, *)) {
                umodel.speedAccuracy = loc.speedAccuracy;
            } else {
                umodel.speedAccuracy = -1;
            }
            umodel.timeStemp = imu.timestamp;
            
            umodel.lat = loc.coordinate.latitude;
            umodel.lon = loc.coordinate.longitude;
            umodel.location = loc;
            self.actualBlock(umodel);
        }
        
    }
    
    ////传出需要存储的数据
    if (self.ListArr.count == self.maxCount) {
        NSLog(@"数据回调了");
        
        if (self.dataBlcok) {
            self.dataBlcok(self.ListArr);
        }
        //[weakself.ListArr removeAllObjects];
        self.ListArr = [[NSMutableArray alloc] init];
    }
    
    dispatch_semaphore_signal(_signal1);
    
}

#pragma mark - 传感器数据开始收集
-(void)startUseAccelerometerPush{
    
    if (self.motionManager.isDeviceMotionAvailable && !self.motionManager.isDeviceMotionActive) {
        
        self.motionManager.deviceMotionUpdateInterval = self.pinlv;
        self.motionManager.showsDeviceMovementDisplay = true;
        
        // 如果CMMotionManager 支持获取加速度数据
        if (self.motionManager.accelerometerAvailable)
        {
            self.motionManager.accelerometerUpdateInterval = self.pinlv;
            [self.motionManager startAccelerometerUpdates];
            
        }else{
            NSLog(@"该设备不支持获取加速度数据！");
            [UBMSLSLog addUBMLog:@"该设备不支持获取加速度数据！"];
        }
        
        // 如果CMMotionManager 支持获取陀螺仪数据
        if (self.motionManager.gyroAvailable)
        {
            self.motionManager.gyroUpdateInterval = self.pinlv;
            [self.motionManager startGyroUpdates];
            
        }else{
            NSLog(@"该设备不支持获取陀螺仪数据！");
            [UBMSLSLog addUBMLog:@"该设备不支持获取陀螺仪数据！"];
        }
        
        // 如果CMMotionManager 支持获取磁场数据
        if (self.motionManager.magnetometerAvailable)
        {
            self.motionManager.magnetometerUpdateInterval = self.pinlv;
            [self.motionManager startMagnetometerUpdates];
            
        }else{
            NSLog(@"该设备不支持获取磁场数据！");
            [UBMSLSLog addUBMLog:@"该设备不支持获取磁场数据！"];
        }
        
        [self.motionManager startDeviceMotionUpdatesUsingReferenceFrame:CMAttitudeReferenceFrameXMagneticNorthZVertical];
        
        /*
        NSOperationQueue* opretionQun = [[NSOperationQueue alloc]init];
        opretionQun.maxConcurrentOperationCount = 1; //保持串行回调，保障顺序。
        [self.motionManager startDeviceMotionUpdatesUsingReferenceFrame:CMAttitudeReferenceFrameXMagneticNorthZVertical toQueue:opretionQun withHandler:^(CMDeviceMotion * _Nullable motion, NSError * _Nullable error) {
            NSLog(@"传感器回调了");
        }];
         */
        
    }
}

#pragma mark -运动状态开启监听。运动状态改变才会回调。回调时6个状态都可能是0
-(void)motionActivityManagerStart{
    if ([CMMotionActivityManager isActivityAvailable]) {
        
        __weak typeof(self) weakself = self;
        
        NSOperationQueue* opretionQun = [[NSOperationQueue alloc]init];
        opretionQun.maxConcurrentOperationCount = 1; //保持串行回调，保障顺序。
        [self.motionActivityManager startActivityUpdatesToQueue:opretionQun withHandler:^(CMMotionActivity * _Nullable activity) {

            if (!activity) {
                return ;
            }
            
            //组装数据
            CMMotionActivity* lastMotionActivity = activity;
            double lastMotionActivityTime = [[NSDate date] timeIntervalSince1970];
            
            MotionActivity* motionActivity = [[MotionActivity alloc] init];
            motionActivity.timestamp = lastMotionActivityTime;

            //如果同时返回多个运动状态，则全部放到一个数组中
            for (int i=0; i<6; i++) {
                
                Motion* wmotion = [[Motion alloc] init];
                if (lastMotionActivity.confidence == CMMotionActivityConfidenceLow) {
                    wmotion.confidence = MotionConfidence_Low;
                }else if (lastMotionActivity.confidence == CMMotionActivityConfidenceMedium){
                    wmotion.confidence = MotionConfidence_Medium;
                }else if (lastMotionActivity.confidence == CMMotionActivityConfidenceHigh){
                    wmotion.confidence = MotionConfidence_High;
                }else{
                    wmotion.confidence = MotionConfidence_CfUnknown;
                }
                
                wmotion.startTs = lastMotionActivity.startDate.timeIntervalSince1970;
                
                if (i==0 && lastMotionActivity.unknown == YES) {
                    wmotion.type = MotionType_Unknown;
                    [motionActivity.eventArray addObject:wmotion];
                }
                if (i==1 && lastMotionActivity.stationary == YES) {
                    wmotion.type = MotionType_Stationary;
                    [motionActivity.eventArray addObject:wmotion];
                }
                if (i==2 && lastMotionActivity.walking == YES) {
                    wmotion.type = MotionType_Walking;
                    [motionActivity.eventArray addObject:wmotion];
                }
                if (i==3 && lastMotionActivity.running == YES) {
                    wmotion.type = MotionType_Running;
                    [motionActivity.eventArray addObject:wmotion];
                }
                if (i==4 && lastMotionActivity.automotive == YES) {
                    wmotion.type = MotionType_Automotive;
                    [motionActivity.eventArray addObject:wmotion];
                }
                if (i==5) {
                    if (lastMotionActivity.cycling == YES) {
                        wmotion.type = MotionType_Cycling;
                        [motionActivity.eventArray addObject:wmotion];
                    }else{
                        
                        //type给个6个状态之外的状态
                        wmotion.type = MotionType_MtUnknown;
                        if (motionActivity.eventArray == nil || motionActivity.eventArray.count == 0) {
                            wmotion.type = MotionType_MtUnknown;
                            [motionActivity.eventArray addObject:wmotion];
                            break;
                        }
                    }
                }
                
                
            }
            
            weakself.motionActivity = motionActivity;
        }];
    }
}

#pragma mark -物体距离靠近时的通知
-(void)proximityChange{
    if ([UIDevice currentDevice].proximityState == YES) {
        self.proximityState = 1;
    }else{
        self.proximityState = 2;
    }
    
    //NSLog(@"距离靠近： %ld",(long)self.proximityState);
    self.proximityStateChangeTime = [[NSDate date] timeIntervalSince1970];
}

#pragma mark -手机方向通知
-(void)orientationDidChange{
    
    UIDeviceOrientation uor = [UIDevice currentDevice].orientation;
    DeviceOrientationType dvortype = 0;
    
    //NSLog(@"方向： %ld",(long)uor);
    
    if (uor == UIDeviceOrientationUnknown) {
        dvortype = DeviceOrientationType_Dotunknown;
        
    }else if (uor == UIDeviceOrientationPortrait) {
        dvortype = DeviceOrientationType_Portrait;
        
    }else if (uor == UIDeviceOrientationPortraitUpsideDown) {
        dvortype = DeviceOrientationType_PortraitUpsidedown;
        
    }else if (uor == UIDeviceOrientationLandscapeLeft) {
        dvortype = DeviceOrientationType_LandscapeLeft;
        
    }else if (uor == UIDeviceOrientationLandscapeRight) {
        dvortype = DeviceOrientationType_LandscapeRight;
        
    }else if (uor == UIDeviceOrientationFaceUp) {
        dvortype = DeviceOrientationType_Faceup;
        
    }else if (uor == UIDeviceOrientationFaceDown) {
        dvortype = DeviceOrientationType_Facedown;
    }else{
        dvortype = DeviceOrientationType_DotUnknown;
    }
    
    self.deviceOrientation = dvortype;
    self.deviceOrientationChangeTime = [[NSDate date] timeIntervalSince1970];
}

#pragma mark -电话状态监听
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context{
    
    if ([keyPath isEqualToString:@"callCenterStatus"]) {
        UBMHKCallState callState = [change[@"new"] intValue];
        
        if (callState == ubmCallStateDisconnected)
        {
            NSLog(@"挂断");
            self.callstate = CallState_Idle;
        }
        else if (callState == ubmCallStateConnected)
        {
            NSLog(@"电话：接通");
            self.callstate = CallState_Offhook;
        }
        else if(callState == ubmCallStateIncoming)
        {
            NSLog(@"电话：呼入");
            self.callstate = CallState_Ring;
        }
        else if (callState == ubmCallStateDialing)
        {
            NSLog(@"电话：呼出");
            self.callstate = CallState_Offhook;
        }
        else
        {
            NSLog(@"电话：空闲");
            self.callstate = CallState_Idle;
        }
    }
}

#pragma mark -检测是否连接蓝牙
- (void)outputDeviceChanged:(NSNotification *)aNotification{
    
    [self BleToothOutput];
}

-(void)BleToothOutput{

    AVAudioSessionRouteDescription *currentRount = [AVAudioSession sharedInstance].currentRoute;
    
    if (currentRount.outputs && currentRount.outputs.count>0) {
        AVAudioSessionPortDescription *outputPortDesc = currentRount.outputs[0];  //音频输出源类型
        NSLog(@"bt-输出 %@",outputPortDesc.portType);
        
        //现有条件测试：打电话时用到
        if([outputPortDesc.portType isEqualToString:AVAudioSessionPortBuiltInReceiver]){
            NSLog(@"bt-手机听筒输出");
            self.audioOutputType = AudioOutputDevice_PhoneReceiver;
        }else if([outputPortDesc.portType isEqualToString:AVAudioSessionPortBuiltInSpeaker]){
            NSLog(@"bt-手机扬声器输出");
            self.audioOutputType = AudioOutputDevice_PhoneSpeaker;
        }else if([outputPortDesc.portType isEqualToString:AVAudioSessionPortBluetoothHFP]){
            NSLog(@"bt-hfp蓝牙输出");
            self.audioOutputType = AudioOutputDevice_BluetoothHeadset;
        }else if([outputPortDesc.portType isEqualToString:AVAudioSessionPortHeadphones]){
            NSLog(@"bt-有线耳机输出");
            self.audioOutputType = AudioOutputDevice_WiredHeadset;
        }else if([outputPortDesc.portType isEqualToString:AVAudioSessionPortCarAudio]){
            NSLog(@"bt-carplay输出");
            //[BXUbmSLSLog addUBMLog:@"carplay输出"];
            self.audioOutputType = AudioOutputDevice_OutputCarPlay;
        }else{
            self.audioOutputType = AudioOutputDevice_OutputUnknown;
        }
        
    }

    if (currentRount.inputs && currentRount.inputs.count>0) {
        AVAudioSessionPortDescription *inputPortDesc = currentRount.inputs[0];    //音频输入源类型
        NSLog(@"bt-输入 %@",inputPortDesc.portType);
        
        //现有条件测试：打电话时用到了这三个
        if([inputPortDesc.portType isEqualToString:AVAudioSessionPortBuiltInMic]){
            NSLog(@"bt-输入是手机麦克风");
            self.audioInputType = AudioInputDevice_PhoneMic;
        }else if([inputPortDesc.portType isEqualToString:AVAudioSessionPortHeadsetMic]){
            NSLog(@"bt-输入是有线耳机上的麦克风");
            self.audioInputType = AudioInputDevice_WiredHeadsetMic;
        }else if([inputPortDesc.portType isEqualToString:AVAudioSessionPortBluetoothHFP]){
            NSLog(@"bt-hfp蓝牙耳机输入");
            self.audioInputType = AudioInputDevice_BluetoothHeadsetMic;
        }else if([inputPortDesc.portType isEqualToString:AVAudioSessionPortCarAudio]){
            NSLog(@"bt-carplay输入");
            //[BXUbmSLSLog addUBMLog:@"carplay输入"];
            self.audioInputType = AudioInputDevice_InputCarPlay;
        }else{
            self.audioInputType = AudioInputDevice_InputUnknown;
        }
         
        //测试
        /*
        if([inputPortDesc.portType isEqualToString:AVAudioSessionPortUSBAudio]){
            NSLog(@"bt-USB输入");
            [BXUbmSLSLog addUBMLog:@"USB输出"];
        }
         */
        
    }

}

#pragma mark - 停止收集数据
-(void)stopGetData{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        dispatch_source_cancel(self.myGCDTimer);
        self.myGCDTimer = nil;
        
        if (self.ended == YES) {
            return;
        }else{
            self.ended = YES;
        }
        
        //关闭传感器数据收集
        [self stopUseAccelerometerPush];
        
        //关闭定位更新
        [self.locationManager stopUpdatingLocation];
        
        //关闭运动状态更新。
        if ([CMMotionActivityManager isActivityAvailable]){
               [self.motionActivityManager stopActivityUpdates];
           }
        
        //关闭物体距离监听
        [UIDevice currentDevice].proximityMonitoringEnabled = NO;
        
        //开启手机方向监听
        [[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];
        
        
        //处理结束时的数据
        __weak typeof(self) weakself = self;
        if (self.ListArr.count>0 && weakself.dataBlcok) {
            [self.ListArr removeAllObjects];//最后几秒钟的数据丢弃。
        }
        
    });
    
}

#pragma mark -停止传感器数据收集
-(void)stopUseAccelerometerPush{
    if (self.motionManager.isDeviceMotionActive) {
        [self.motionManager stopDeviceMotionUpdates];
        
        [self.motionManager stopAccelerometerUpdates];
        [self.motionManager stopGyroUpdates];
        [self.motionManager stopMagnetometerUpdates];
    }
}


#pragma mark - 收集状态切换
-(void)setDataCollectType:(CMCollectType)type{
    
    if (type == CMCollectTypeManual) {
        self.CollectType = CollectType_Manual;
    }else{
        self.CollectType = CollectType_Auto;
    }
}

#pragma mark - 是否停止中
-(void)setAutoStoping:(BOOL)stoping{
    if (stoping == YES) {
        self.willStoping = EndTag_Stop;
    }else{
        self.willStoping = EndTag_Run;
    }
}


#pragma mark - 切换前后台时重新启动监听。为了iOS12.1.4会在后台时暂停coremotion的回调
-(void)restartCoreMotin{
    if ( self.ended == YES ) {
        return;
    }else{
        
        //先暂停
        [self stopUseAccelerometerPush];
        
        //再开启
        [self startUseAccelerometerPush];
        
    }
}



@end
