//
//  UBMNetPbFileManager.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/10/19.
//  Copyright © 2020 魏振伟. All rights reserved.
//

/*
 处理行程pb文件的网络交互，单个、多个上传、校验、合并等。
 传入的tripID必须是获取从服务器获取的tripid。
*/

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UBMNetPbFileManager : NSObject


+(instancetype)shareNetPbFileManger;

/*
 上传一个文件。
 filePath 文件完整路径
 */
-(void)uploadFileWithPath:(NSString*)filePath andTripId:(NSString*)tripId andFileName:(NSString*)fileName and:(void(^)(BOOL result))dataBack;

/*
 上传多个文件
 检查某个TripID的本地文件，并全部上传。如果一个文件都没了，那直接返回yes。
 */
-(void)checkAllLocalFileOfTheTripID:(NSString*)tripId andResPonse:(void(^)(BOOL result))dataBack;


/*
 上传多个trip的多个文件。可以只传进来没上传的状态的tripid。节省点资源。
 一个大接口 检查所有trip，pb上传+校验+结束接口。
 不管成功失败都会回调。
 */
-(void)checkAndUploadTripIDS:(NSArray*)tripIdArr andResPonse:(void(^)(BOOL result))dataBack;


//校验接口。成功yes，失败NO
-(void)checkAgainWithTripId:(NSString*)triID andResPonse:(void(^)(BOOL result))dataBack;


//结束合并接口
-(void)tripMerge:(NSString*)triID andResPonse:(void(^)(BOOL result))dataBack;



@end

NS_ASSUME_NONNULL_END
