//
//  UBMPbFileManger.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/10/19.
//  Copyright © 2020 魏振伟. All rights reserved.
//

/*
 处理行程pb文件的本地存储交互：创建和保存。
 使用uidMD5之后的数据作为基础文件夹。
 */

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UBMPbFileManger : NSObject

#pragma mark - 文件夹路径获取
//NSLibraryDirectory/uidmd5
+(NSString*)libraryBasePath;

//NSCachesDirectory/uidmd5
+(NSString*)cachesBasePath;

//获取ubm文件夹的路径 NSLibraryDirectory/uidmd5/UBM
+(NSString*)ubmFolderPath;

//获取ubm下某个LocTripId的pb文件夹路径
+(NSString*)pbFolderPathWithLocTripId:(NSString*)locTripId;

//获取ubm下某个TripId的pb文件夹路径
+(NSString *)pbFolderPathWithTripId:(NSString *)tripId;


#pragma mark - 创建收集数据的文件。ubm专用的。(NSLibraryDirectory/"uidmd5"/UBM/"folderName"/"fileName")
+(BOOL)careatFileAtUBMFolderWithFileName:(NSString*)fileName andFolderName:(NSString*)folderName;


#pragma mark - 追加数据到pb文件
+(void)addDataToPbFilePath:(NSString*)pbFilePath andData:(NSArray*)data;


#pragma mark - 通用的创建文件和文件夹(NSCachesDirectory/"uidmd5"/"folderName"/"fileName")
+(BOOL)commenCareatFilewithName:(NSString*)fileName andContend:(NSData*)data andFolderName:(NSString*)folderName;


#pragma mark - 获取目录下的下级文件名称数组。
+(NSArray*)getAllFileAtPath:(NSString*)path;


#pragma mark - 返回文件大小M path：完整路径。
+(NSString*)sizeOfFileAtPath:(NSString*)path;


#pragma mark - 删除文件。 path：完整路径。
+(BOOL)deleteFileAtPath:(NSString*)path;



@end

NS_ASSUME_NONNULL_END
