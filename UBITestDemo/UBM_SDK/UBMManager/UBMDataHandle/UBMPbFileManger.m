//
//  UBMPbFileManger.m
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/10/19.
//  Copyright © 2020 魏振伟. All rights reserved.
//

#import "UBMPbFileManger.h"
#import "Sensor.pbobjc.h"
#import "UBMSqliteHelper.h"
#import "UBMBaseUserModel.h"
#import "UBMMTools.h"

@implementation UBMPbFileManger

+(NSString *)libraryBasePath
{
    NSString* path = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) firstObject];
    NSString* uidMd5 = [UBMMTools MD5HexDigest:UBMBaseUserModel.shared.userId];
    //NSLog(@"uidmd5 == 》》%@",uidMd5);
    if ([UBMMTools IsEmptyStr:uidMd5]) {
        return path;
    }
    NSString* basePath = [NSString stringWithFormat:@"%@/%@", path, uidMd5];
    
    //判断有没有此文件夹，没有则新建文件夹
    NSFileManager* fm = [NSFileManager defaultManager];
    if (![fm fileExistsAtPath:basePath]) {
        [fm createDirectoryAtPath:basePath withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    return basePath;
}

+(NSString *)cachesBasePath
{
    NSString* path = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];
    NSString* uidMd5 = [UBMMTools MD5HexDigest:UBMBaseUserModel.shared.userId];
    if ([UBMMTools IsEmptyStr:uidMd5]) {
        return path;
    }
    NSString* basePath = [NSString stringWithFormat:@"%@/%@", path, uidMd5];
    
    //判断有没有此文件夹，没有则新建文件夹
    NSFileManager* fm = [NSFileManager defaultManager];
    if (![fm fileExistsAtPath:basePath]) {
        [fm createDirectoryAtPath:basePath withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    return basePath;
}

+(NSString *)ubmFolderPath
{
    NSString* ubmPath = [NSString stringWithFormat:@"%@/UBM", [UBMPbFileManger libraryBasePath]];
    //判断有没有此文件夹，没有则新建文件夹
    NSFileManager* fm = [NSFileManager defaultManager];
    if (![fm fileExistsAtPath:ubmPath]) {
        [fm createDirectoryAtPath:ubmPath withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    return ubmPath;
}

+(NSString *)pbFolderPathWithLocTripId:(NSString *)locTripId
{
    NSString* pbPath = [NSString stringWithFormat:@"%@/%@/%@",[UBMPbFileManger libraryBasePath], @"UBM", locTripId];
    return pbPath;
}

+(NSString *)pbFolderPathWithTripId:(NSString *)tripId{
    
    UBMTripTableModel* model = [[UBMSqliteHelper sharedHelper] getUBMTripTableModelOfLocTripID:@"" OrTripID:tripId];
    NSString* locTripId = model.locTripID;
    NSString* pbPath = [NSString stringWithFormat:@"%@/%@/%@",[UBMPbFileManger libraryBasePath], @"UBM", locTripId];
    return pbPath;
}

+(BOOL)careatFileAtUBMFolderWithFileName:(NSString*)fileName andFolderName:(NSString*)folderName
{
    if (fileName == nil) {
        return NO;
    }
    
    NSFileManager* fm = [NSFileManager defaultManager];
    //先创建文件夹，再创建文件
    NSString* fileDirectoryPath = [NSString stringWithFormat:@"%@/%@",[UBMPbFileManger ubmFolderPath], folderName];
    NSLog(@"文件夹路径：%@",fileDirectoryPath);
    if (![fm fileExistsAtPath:fileDirectoryPath]) {
        if ([fm createDirectoryAtPath:fileDirectoryPath withIntermediateDirectories:YES attributes:nil error:nil]) {
            NSLog(@"文件夹创建成功");
        }else{
           NSLog(@"文件夹创建失败");
            return NO;
        }
    }else{
        NSLog(@"已有文件夹");
    }
    
    //创建文件
    NSString* nowPath = [NSString stringWithFormat:@"%@/%@",fileDirectoryPath, fileName];
    NSLog(@"%@", nowPath);
    if ([fm createFileAtPath:nowPath contents:nil attributes:nil]) {
        return YES;
    }else{
        //失败
        NSLog(@"创建文件失败%@", fileName);
        return NO;
    }
    
    return YES;
}

#pragma mark - 追加数据到pb文件
+(void)addDataToPbFilePath:(NSString *)pbFilePath andData:(NSArray *)data
{
    if (data && [data isKindOfClass:[NSArray class]] && [data count]>0) {
        
        dispatch_queue_t myQuen = dispatch_queue_create("SensorQueue", DISPATCH_QUEUE_SERIAL);
        for (int i=0; i<data.count; i++) {

            //异步串行，保证顺序和不占用主线程。
            dispatch_async(myQuen, ^{
                
                NSOutputStream* os = [[NSOutputStream alloc] initToFileAtPath:pbFilePath append:YES];
                //[os scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
                [os open];
                SensorData* baseRoot = data[i];
                [baseRoot writeDelimitedToOutputStream:os];
                //NSLog(@"%f",baseRoot.sensorTs);
                [os close];
                //[os removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
            });
        }
    }else{
        NSLog(@"要写入的pb数据数组为空");
    }
    
}

#pragma mark - 通用的
+(BOOL)commenCareatFilewithName:(NSString*)fileName andContend:(NSData*)data andFolderName:(NSString*)folderName
{
    if (fileName == nil) {
        NSLog(@"文件名为空");
        return NO;
    }
    
    NSFileManager* fm = [[NSFileManager alloc]init];
    
    //先创建文件夹，再创建文件
    NSString* fileDirectoryPath = [NSString stringWithFormat:@"%@/%@", [UBMPbFileManger cachesBasePath], folderName];
    NSLog(@"文件夹路径：%@",fileDirectoryPath);
    if (![fm fileExistsAtPath:fileDirectoryPath]) {
        if ([fm createDirectoryAtPath:fileDirectoryPath withIntermediateDirectories:YES attributes:nil error:nil]) {
            NSLog(@"文件夹创建成功");
        }else{
           NSLog(@"文件夹创建失败");
            return NO;
        }
    }else{
        NSLog(@"已有文件夹");
    }
    
    //创建文件
    NSString* nowPath = [NSString stringWithFormat:@"%@/%@", fileDirectoryPath, fileName];
    NSLog(@"%@", nowPath);
    if ([fm createFileAtPath:nowPath contents:data attributes:nil]) {
        return YES;
    }else{
        //失败
        NSLog(@"创建文件失败%@", fileName);
        return NO;
    }
    
    return YES;
}

+(NSArray *)getAllFileAtPath:(NSString *)path
{
    NSArray* array = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:nil];
    if (array && array.count>0) {
        return array;
    }else{
        return @[];
    }
}

+(NSString*)sizeOfFileAtPath:(NSString*)path
{
    NSData *data1 = [[NSData alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path]];
    
    if(data1 != nil){
        NSString* fileSize = [NSString stringWithFormat:@"%.3f",data1.length/(1024.00*1024.00) ];
        return fileSize;
    }else{
        return @"0";
    }
}

+(BOOL)deleteFileAtPath:(NSString *)path
{
    NSFileManager* fm = [NSFileManager defaultManager];
    BOOL result = [fm removeItemAtPath:path error:nil];
    if (result == YES) {
        return YES;
    }else{
        return NO;
    }
}


@end
