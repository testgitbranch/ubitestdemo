//
//  UBMNetPbFileManager.m
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/10/19.
//  Copyright © 2020 魏振伟. All rights reserved.
//

#import "UBMNetPbFileManager.h"
#import "UBMPbFileManger.h"
#import "UBMSqliteHelper.h"
#import "UBMBaseUserModel.h"
#import <zlib.h>
#include <CommonCrypto/CommonDigest.h>
#import "UBMSLSLog.h"
#import "UBMMTools.h"
#import "UBMNetWorkTool.h"
#import "UBMPrefixHeader.h"

//定义一个字典，保存所有行程的上传状态。

@interface UBMNetPbFileManager()

@property(nonatomic, copy)dispatch_semaphore_t signal1; //信号量1


@end

@implementation UBMNetPbFileManager

+(instancetype)shareNetPbFileManger
{
    static id instance;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    
    return instance;
}

#pragma mark - 上传一个文件。上传成功一个，改一个pb文件的状态。
-(void)uploadFileWithPath:(NSString *)filePath andTripId:(nonnull NSString *)tripId andFileName:(NSString*)fileName and:(nonnull void (^)(BOOL))dataBack{
    
    NSLog(@"文件 %@",filePath);
    NSLog(@"待上传文件大小 %@", [UBMPbFileManger sizeOfFileAtPath:filePath]);
    
    //为空就当成全部上传了。更改pb状态。
    UBMTripPBModel* thePbModel = [[UBMSqliteHelper sharedHelper] getUBMTripPBModelOfPbName:fileName OfLocTripID:@"" OrTripID:tripId];
    if (thePbModel == nil) {
        dataBack(NO);
        return;
    }
    
    NSData* data = [[NSFileManager defaultManager] contentsAtPath:filePath];
    if (data == nil) {
        NSLog(@"文件为空 %@",filePath);
        
        thePbModel.hadUpload = YES;
        [[UBMSqliteHelper sharedHelper] updateItemOfTripPBList:thePbModel OfLocTripID:@"" OrTripID:tripId];
        
        dataBack(YES);
        return;
    }
    
    NSString* newFileName = [NSString string];
    
    //压缩data, 如果压缩成功文件名也要改
    NSData* zipData = [self zlibCompressedData:data];
    if (zipData == nil) {
        zipData = data;
        newFileName = fileName;
    }else{
        newFileName = [NSString stringWithFormat:@"%@.z",fileName];
        
        //看看大小
        NSString* fileSize = [NSString stringWithFormat:@"%.3f",zipData.length/(1024.00*1024.00) ];
        NSLog(@"压缩后-文件大小:%@",fileSize);
    }
    
    //更改状态。md5确定值
    NSString* strOfMd5 = [self MD5HexDigestOfData:data];
    thePbModel.pbMd5 = strOfMd5;
    [[UBMSqliteHelper sharedHelper] updateItemOfTripPBList:thePbModel OfLocTripID:@"" OrTripID:tripId];
    NSLog(@"www: 更新MD5成功！%@",fileName);
    
    
    [self uploadLoadFileAtPath:filePath andMd5:strOfMd5 andData:zipData andFileName:newFileName andTripID:tripId andBlock:^(BOOL reslut) {
        
        if (reslut == YES) {
            //更改状态。
            thePbModel.hadUpload = YES;
            [[UBMSqliteHelper sharedHelper] updateItemOfTripPBList:thePbModel OfLocTripID:@"" OrTripID:tripId];
            
            [UBMSLSLog addUBMLog:@"pbUpLoadSuccess" andTripID:tripId];
            NSLog(@"www: pb文件： %@ 已上传", fileName);
            dataBack(YES);
            
        }else{
            NSLog(@"www: pb文件上传失败！%@", fileName);
            [UBMSLSLog addUBMLog:@"pbUpLoadFail" andTripID:tripId];
            
           dataBack(NO);
        }
        
    }];
    
    /*
     //此直接上传方法已经被废弃。改成了间接上传。
    [[UBMNetWorkTool shareNetWorkTool] uplodImgWithBaseUrl:[UBMDomainName.shareDomain ubmURL] UrlString:UBMUpLoadPB andParameters:@{@"userId":[UBMUserStatus shared].uid, @"tripId":tripId, @"md5":strOfMd5, @"last":@"0" } andFileData:data FileName:fileName method:@"POST" success:^(id response) {
        
        if (response && [response isKindOfClass:[NSDictionary class]]) {
            if ([response[@"code"] intValue] == 1) {
                
                //更改状态。
                thePbModel.hadUpload = YES;
                [[UBMSqliteHelper shareUBMSqliteHelper] updateItemOfTripPBList:thePbModel OfLocTripID:@"" OrTripID:tripId];
                
                NSLog(@"pb文件： %@ 已上传", fileName);
                dataBack(YES);
            }else{
               dataBack(NO);
            }
        }else{
            dataBack(NO);
        }
    } error:^(id response) {
        dataBack(NO);
    } isSync:NO];
     */
    
}


#pragma mark - 检查某个TripID的下的本地文件，并全部上传。如果一个文件都没了，那直接返回yes。如果中间有一个失败，则返回NO。
-(void)checkAllLocalFileOfTheTripID:(NSString *)tripId andResPonse:(void (^)(BOOL))dataBack{
    
    NSLog(@"8进来 开始检查本地pb文件");
    
    NSArray* allArr = [self locPbArrOfTripId:tripId];
    
    /*
    NSMutableArray* sholdUploadArray = [[NSMutableArray alloc] init];
    for (int i=0; i<allArr.count; i++) {
        UBMTripPBModel* pbmodel = allArr[i];
        [sholdUploadArray addObject:pbmodel.pbName];
    }
    NSLog(@"最后应上传文件列表：%@", sholdUploadArray);*/
    
    UBMTripTableModel* tripMdoel = [[UBMSqliteHelper sharedHelper] getUBMTripTableModelOfLocTripID:@"" OrTripID:tripId];
    if (tripMdoel == nil || allArr.count == 0) {
        dataBack(YES);
        return;
    }
    
    NSString* locTripId = tripMdoel.locTripID;
    
    __block NSMutableArray* resultArr = [[NSMutableArray alloc] init];
    
    //搞个线程组，监听全部执行完毕回调。
    if (allArr.count>0) {
        
        //并发执行
        dispatch_queue_t quene = dispatch_queue_create("quene.group", DISPATCH_QUEUE_CONCURRENT);
        dispatch_group_t group1 = dispatch_group_create();
        
        //加入任务
        for (int i=0 ; i<allArr.count; i++) {
            
            dispatch_group_async(group1, quene, ^{
                
                dispatch_group_enter(group1);
                
                NSLog(@"9进来");
                UBMTripPBModel* pbmodel = allArr[i];
                
                NSString* ffilePath = [NSString stringWithFormat:@"%@/%@", [UBMPbFileManger pbFolderPathWithLocTripId:locTripId], pbmodel.pbName];
                [self uploadFileWithPath:ffilePath andTripId:tripId andFileName:pbmodel.pbName and:^(BOOL result) {

                    NSLog(@"9返回");
                    //一个失败，则停止，外面给个提示，就可再次上传。
                    if (result == NO) {
                        [resultArr addObject:@"0"];
                    }else{
                        [resultArr addObject:@"1"];
                    }
                    
                    dispatch_group_leave(group1);
                }];
                
            });
        }
        

        dispatch_group_notify(group1, quene, ^{
            
            for (NSString* str in resultArr) {
                if ([str isEqualToString:@"0"]) {
                    NSLog(@"有上传失败的pb");
                    dataBack(NO);
                    return ;
                }
            }
            
            NSLog(@"全部上传完毕");
            dataBack(YES);
        });
        
    }else{
        dataBack(YES);
    }
}

#pragma mark -检查数据库中记录的pb文件是否和本地一致，同步。
-(NSArray*)locPbArrOfTripId:(NSString*)tripId {
    
    NSMutableArray* allArr = [[NSMutableArray alloc] init];
    
    UBMTripTableModel* tripMdoel = [[UBMSqliteHelper sharedHelper] getUBMTripTableModelOfLocTripID:@"" OrTripID:tripId];
    NSArray* pbfileArr = [UBMPbFileManger getAllFileAtPath:[UBMPbFileManger pbFolderPathWithTripId:tripId]];
    
    if (tripMdoel.tripPBList && tripMdoel.tripPBList.count>0 && tripMdoel.tripPBList.count == pbfileArr.count)
    {
        NSLog(@"tripId: %@ 本地文件数量和数据库记录的一样",tripId);
        
        for (int i=0; i<tripMdoel.tripPBList.count; i++)
        {
            //一小段行程。
            UBMTripPBModel* pbmodel = tripMdoel.tripPBList[i];
            if (pbmodel.hadUpload != YES ) {
                [allArr addObject:pbmodel];
                
                NSLog(@"剩余需要上传的pb：%@", pbmodel.pbName);
            }
        }
        
    }else{
        
        //再检查一次文件夹，以文件夹下的pb文件为准。
        if (pbfileArr == nil || pbfileArr.count == 0)
        {
            NSLog(@"tripId: %@ 本地真的没有pb文件了！",tripId);
            [tripMdoel.tripPBList removeAllObjects];
            [[UBMSqliteHelper sharedHelper] updateaTrip:tripMdoel OfTripID:tripId OrLocTripID:@""];
        }
        else
        {
            NSLog(@"tripId %@ 本地有文件没被记录上。本地文件: %@",tripId, pbfileArr);
            //更新并同步到数据库
            for (int x=0; x<pbfileArr.count; x++) {
                NSString* pbFileName = [NSString stringWithFormat:@"%@", pbfileArr[x]];
                
                //检查遗漏的pb文件，更新到数据库中
                for (int i=0; i<tripMdoel.tripPBList.count; i++)
                {
                    UBMTripPBModel* itemPbmodel = tripMdoel.tripPBList[i];
                    if ([itemPbmodel.pbName isEqualToString:pbFileName])
                    {
                        if (itemPbmodel.hadUpload != YES) {
                            [allArr addObject:itemPbmodel];
                        }
                        break;
                    }
                    else if (i == tripMdoel.tripPBList.count-1)
                    {
                        UBMTripPBModel* pbmodel = [[UBMTripPBModel alloc] init];
                        pbmodel.pbName = pbFileName;
                        [[UBMSqliteHelper sharedHelper] updateItemOfTripPBList:pbmodel OfLocTripID:@"" OrTripID:tripId];
                        [allArr addObject:pbmodel];
                        NSLog(@"需要重新添加到数据库的文件：%@",pbmodel.pbName);
                    }
                }
            }
        }
    }
    
    return allArr;
}


#pragma mark -  上传多个trip的多个文件。检查传入的trip，pb上传+校验+结束接口。要知道成功失败！！！！
-(void)checkAndUploadTripIDS:(NSArray*)tripIdArr andResPonse:(void(^)(BOOL result))dataBack{
    
    dispatch_queue_t quene2 = dispatch_queue_create("quene.group2", NULL);
    dispatch_group_t group2 = dispatch_group_create();
    
    __block BOOL sucess = YES;
    
    for (int i=0; i<tripIdArr.count; i++) {
        
        dispatch_group_enter(group2);
        
        NSString* tripID = tripIdArr[i];
        
        NSLog(@"一层：检查本地文件并上传");
        [self checkAllLocalFileOfTheTripID:tripID andResPonse:^(BOOL result){
            //NSLog(@"一层结束");
            if (result == YES){
                NSLog(@"二层：校验文件");
//                [MobClick event:@"trip_auto_upload" attributes:@{@"state":@"1", @"uid":[NSString stringWithFormat:@"%@",UBMBaseUserModel.shared.userId]}];
                
                //成功，则继续走验证接口
                [self checkAgainWithTripId:tripID andResPonse:^(BOOL result){
                    //NSLog(@"二层结束");
                    if (result == YES){
                        NSLog(@"三层：合并文件");
                        
                        //验证成功，走结束合并接口
                        [self tripMerge:tripID andResPonse:^(BOOL result) {
                            //不管成功失败，下一个任务
                            NSLog(@"三层结束");
                            NSLog(@"levle22");
                            if (result == NO) {
                                sucess = NO;
                            }
                            dispatch_group_leave(group2);
                        }];
                        
                    }else{
                        //继续下个任务。
                        NSLog(@"levle33");
                        sucess = NO;
                        dispatch_group_leave(group2);
                    }
                    
                }];
                
            }else{
                //失败 继续下个任务。
                NSLog(@"levle44");
                sucess = NO;
//                [MobClick event:@"trip_auto_upload" attributes:@{@"state": @"0",@"uid":[NSString stringWithFormat:@"%@",UBMBaseUserModel.shared.userId]}];
                dispatch_group_leave(group2);
            }
            
        }];
        
        
    }
    
    //结束后回调。
    dispatch_group_notify(group2, quene2, ^{
        dataBack(sucess);
        
    });
}

#pragma mark - 校验接口
-(void)checkAgainWithTripId:(NSString*)triID andResPonse:(void(^)(BOOL result))dataBack{
    
    [[UBMNetWorkTool shareNetWorkTool] requestWithBaseUrl:[UBMDomainName.shareDomain ubmURL] UrlString:UBMTheTripFileList parameters:@{@"userId":[UBITracking shared].userID, @"tripId":triID} method:@"POST" success:^(id response) {
        
        if ([response[@"code"] intValue] == 1) {
            
            //开始验证。
            //服务端的pb如果一个都没有，或者某个没有，则应该以本地pb为准。且把本地的这个文件的状态设置为0。重新上传。这种情况发生在：本地记录的状态与服务端记录的状态不同时。
            
            UBMTripTableModel* tripMdoel = [[UBMSqliteHelper sharedHelper] getUBMTripTableModelOfLocTripID:@"" OrTripID:triID];
            if (tripMdoel == nil) {
                dataBack(YES);
                return;
            }
            
            NSArray* netFileList = response[@"data"][@"list"];
            if (netFileList && [netFileList isKindOfClass:[NSArray class]] && netFileList.count>0) {
            
                int a = 0;
                for (NSDictionary* fileDic in netFileList) {
                    
                    NSString* netFileName = fileDic[@"fileName"];
                    NSString* netFileMd5 = fileDic[@"fileMd5"];
                    
                    //查找这小段行程。
                    UBMTripPBModel* thePbmodel = nil;
                    for (UBMTripPBModel* pbmodel in tripMdoel.tripPBList) {
                        if ([pbmodel.pbName isEqualToString:netFileName] ) {
                            thePbmodel = pbmodel;
                            break;;
                        }
                    }
                    
                    if (thePbmodel) {
                        
                        if ([netFileMd5 isEqualToString:thePbmodel.pbMd5]) {
                            //单条通过验证。
                        }else{
                            a = a+1;
                            //更改本地文件状态。
                            thePbmodel.hadUpload = NO;
                            [[UBMSqliteHelper sharedHelper] updateItemOfTripPBList:thePbmodel OfLocTripID:@"" OrTripID:triID];
                            
                        }
                    }
                    
                }
                
                
                if (a>0) {
                    //验证失败。
                    dataBack(NO);
                }else{
                    dataBack(YES);
                }
            }else{
                
                //重置本地对应的所有pb文件的状态。
                if (tripMdoel.tripPBList && tripMdoel.tripPBList.count>0) {

                    for (UBMTripPBModel* pbModel in tripMdoel.tripPBList) {
                        pbModel.hadUpload = NO;
                    }
                    BOOL result = [[UBMSqliteHelper sharedHelper] updateaTrip:tripMdoel OfTripID:triID OrLocTripID:@""];
                    if (result == YES) {
                        NSLog(@"重置所有pb文件状态为0成功");
                    }else{
                        NSLog(@"重置所有pb文件状态为0失败");
                    }

                    dataBack(NO);
                    
                }else{
                    
                    dataBack(YES);
                }
                
                
            }
            
        }else{
            dataBack(NO);
        }
        
    } error:^(id response) {
        dataBack(NO);
    } isShowHUD:NO];

}

#pragma mark - 合并接口
-(void)tripMerge:(NSString*)triID andResPonse:(void(^)(BOOL result))dataBack{
    
    //获取结束类型
    UBMTripTableModel* tripModel = [[UBMSqliteHelper sharedHelper] getUBMTripTableModelOfLocTripID:@"" OrTripID:triID];
    NSString* autoEnd = @"0";
    if (tripModel.isAutoStop == YES) {
        autoEnd = @"1";
    }
    
    NSString* autoStart = @"0";
    if (tripModel.isAutoStart == YES) {
        autoStart = @"1";
    }
    
    
    [[UBMNetWorkTool shareNetWorkTool] requestWithBaseUrl:[UBMDomainName.shareDomain ubmURL] UrlString:UBMTripMerge parameters:@{@"userId":[UBITracking shared].userID, @"tripId":triID, @"autoEnd":autoEnd, @"autoStart":autoStart} method:@"POST" success:^(id response) {
        
        //101003 合并异常。 101005 分析已经完成的，此code代表本行程已经被合并过，直接置成已完成就好。101004：“行程分析中，请稍后”。101009：“行程数据已提交请稍后”。
        if ([response[@"code"] intValue] == 1 || [response[@"code"] intValue] == 101003 || [response[@"code"] intValue] == 101004 ||[response[@"code"] intValue] == 101005 || [response[@"code"] intValue] == 101009) {
            
            BOOL reslut = [[UBMSqliteHelper sharedHelper] updateUploadOver:YES OfLocTripID:@"" OrTripID:triID];
            if (reslut == YES) {
                NSLog(@"更新结束状态成功！");
            }else{
                NSLog(@"更新结束状态失败！");
            }
            
            dataBack(YES);
        }else{
            dataBack(NO);
        }
        
        if ([response[@"code"] intValue] == 1) {
            [UBMSLSLog addUBMLog:@"TripMergeSucess" andTripID:triID];
        }else{
            [UBMSLSLog addUBMLogOfW:[NSString stringWithFormat:@"TripMergeError,code:%@",response[@"code"]] andTripID:triID];
        }
        
    } error:^(id response) {
        dataBack(NO);
    } isShowHUD:NO];
}

#pragma mark - 上传文件分成了三步：1：获取oss凭证 2：上传 3：通知给服务器文件地址。
-(void)uploadLoadFileAtPath:(NSString *)path andMd5:(NSString*)md5 andData:(NSData*)data andFileName:(NSString*)filename andTripID:(NSString *)tripID andBlock:(nonnull void (^)(BOOL))resultBlock{

    [self getOSSpolicyWithTripID:tripID andMd5:md5 andResult:^(NSDictionary * pardic) {
        
        if (pardic != nil) {
            
            NSMutableDictionary* dicx = [[NSMutableDictionary alloc] initWithDictionary:pardic];
            [dicx setObject:pardic[@"accessid"] forKey:@"OSSAccessKeyId"];
            [dicx setObject:[NSString stringWithFormat:@"%@%@",pardic[@"dir"],filename] forKey:@"key"];
            [dicx setObject:md5 forKey:@"x:md5"];
            [dicx removeObjectForKey:@"host"];
            
            NSString* osskey = [NSString stringWithFormat:@"%@%@",pardic[@"dir"],filename];
            
            [[UBMNetWorkTool shareNetWorkTool] uplodImgWithBaseUrl:pardic[@"host"] UrlString:@"" andParameters:dicx andFileData:data FileName:filename method:@"POST" success:^(id reslut) {
                
                NSLog(@"上传oss结果： %@",reslut);
                
                //成功了则上报上传后的地址
                if (reslut && [reslut isKindOfClass:[NSDictionary class]] && [reslut[@"code"] intValue]==1 ) {
                    
                    NSDictionary* pma = [[NSDictionary alloc] initWithObjectsAndKeys:[UBITracking shared].userID,@"userId", tripID,@"tripId", md5,@"md5", osskey,@"oss_key", nil];
                    [[UBMNetWorkTool shareNetWorkTool] requestWithBaseUrl:[UBMDomainName.shareDomain ubmURL] UrlString:UBMUploadNotify parameters:pma method:@"POST" success:^(id response) {
                        
                        if (response && [response isKindOfClass:[NSDictionary class]] && [response[@"code"] intValue]==1) {
                            resultBlock(YES);
                        }else{
                            resultBlock(NO);
                        }
                        
                    } error:^(id response) {
                        resultBlock(NO);
                    } isShowHUD:NO];
                    
                }else{
                    resultBlock(NO);
                }
                
            } error:^(id reslut) {
                resultBlock(NO);
            } isSync:NO];
            
        }else{
            resultBlock(NO);
        }
        
    }];
    
}

-(void)getOSSpolicyWithTripID:(NSString*)tripId andMd5:(NSString*)md5 andResult:(nonnull void (^)(NSDictionary*))resultBlock{
    
    NSDictionary* md5dic = @{@"md5":md5};
    NSString* mdstr = [UBMMTools JsonWithDictionary:md5dic];
    NSDictionary* par = [[NSDictionary alloc] initWithObjectsAndKeys:[UBITracking shared].userID,@"userId", tripId,@"tripId", @"pbUpload",@"type", mdstr,@"customData", nil];
    [[UBMNetWorkTool shareNetWorkTool] requestWithBaseUrl:[UBMDomainName.shareDomain ubmURL] UrlString:UBMOssPolicy parameters:par method:@"POST" success:^(id response) {
        
        if (response && [response isKindOfClass:[NSDictionary class]] && [response[@"code"] intValue] == 1) {
            NSDictionary* par = [[NSDictionary alloc] initWithDictionary:response[@"data"]];
            resultBlock(par);
        }else{
            resultBlock(nil);
        }
        
    } error:^(id response) {
        resultBlock(nil);
    } isShowHUD:NO];
}

- (NSData *)zlibCompressedData:(NSData *)data
{
    if (data.length == 0) return data;
    
    z_stream zStream;
    bzero(&zStream, sizeof(zStream));
    
    zStream.zalloc = Z_NULL;
    zStream.zfree = Z_NULL;
    zStream.opaque = Z_NULL;
    zStream.next_in = (Bytef *) data.bytes;
    zStream.avail_in = (uInt) data.length;
    zStream.total_out = 0;
    
    OSStatus status = deflateInit2(&zStream,
                                   Z_DEFAULT_COMPRESSION,
                                   Z_DEFLATED, MAX_WBITS,
                                   MAX_MEM_LEVEL,
                                   Z_DEFAULT_STRATEGY);
    
    if (status != Z_OK) {
        return nil;
    }
    
    static NSInteger kZlibCompressChunkSize = 2048;
    NSMutableData *compressedData = [NSMutableData dataWithLength:kZlibCompressChunkSize];
    do {
        if ((status == Z_BUF_ERROR) || (zStream.total_out == compressedData.length)) {
            [compressedData increaseLengthBy:kZlibCompressChunkSize];
        }
        zStream.next_out = (Bytef *)compressedData.bytes + zStream.total_out;
        zStream.avail_out = (uInt)(compressedData.length - zStream.total_out);
        status = deflate(&zStream, Z_FINISH);
    } while ((status == Z_BUF_ERROR) || (status == Z_OK));
    
    status = deflateEnd(&zStream);
    
    if ((status != Z_OK) && (status != Z_STREAM_END)) {
        return nil;
    }
    
    compressedData.length = zStream.total_out;
    
    return compressedData;
}

-(NSString*)MD5HexDigestOfString:(NSString*)input{
    NSData* inputData = [input dataUsingEncoding:NSUTF8StringEncoding];
    return [self MD5HexDigestOfData:inputData];
}

-(NSString*)MD5HexDigestOfData:(NSData*)inputData{
    
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(inputData.bytes, (CC_LONG)inputData.length, result);
    NSMutableString *ret = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH*2];
    for (int i = 0; i<CC_MD5_DIGEST_LENGTH; i++)  {
        [ret appendFormat:@"%02x",result[i]];
    }
    return ret;
}
@end
