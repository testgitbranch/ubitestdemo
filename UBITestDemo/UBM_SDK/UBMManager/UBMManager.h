//
//  UBMManager.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/12/9.
//  Copyright © 2020 魏振伟. All rights reserved.
//

/*
 ubm管理类。管理开启、收集、保存、上传等。
 注意：当用户退出登录时或者切换车辆时，要先结束进行中的行程，才能退出登录。
 */

#import <Foundation/Foundation.h>
#import "UBMResultModel.h"
#import "UBMStartOrEndModel.h"
#import "UBMTripTableModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^keepLastTripResultBlock)(BOOL restart);

@interface UBMManager: NSObject
@property(nonatomic, copy) NSString *userToken; //用户Token
@property(atomic, assign)BOOL isCollecting; //是否收集中
@property(atomic, copy)NSString* nowTripid; //收集中的tripId;

+(instancetype)shared;

/*
 shared后必须要调用本方法。用来确定和区分用户。
 用户登录后、或者进入APP后，要调用本方法，更改数据库连接为当前用户的数据库、更新或初始化用户id、vid。
 userId:用户ID，以它来区分用户数据库存储和文件存储位置
 */
-(void)setUserId:(NSString*)userId;

/*
 如果数据要与车绑定，则需要调用本方法。vid只是作为一个行程的车辆参数。
 vid:车id。
 */
-(void)setVid:(NSString*)vid;

//权限请求。可以选择第一次在哪里请求位置权限。当然，当开启之时也会默认先调用一次。
-(void)prepareCoreMotionCenter;


//开启自动检测记录：会自动判断开车时机并开启记录。
-(void)startAutoRecord;

//关闭自动检测记录。比如：退出登录后。
-(void)stopAutoRecord;


//开使收集数据。当isCollecting==yes，会返回失败。
-(void)startRecord:(UBMStartOrEndModel*)startModel andResult:(void(^)(UBMResultModel* result))startBack;

//停止收集数据。返回resultcode失败，代表没有获取到网络tripid。停止是绝对会的。
-(void)stopRecord:(UBMStartOrEndModel*)startModel andResult:(void(^)(UBMResultModel* result))endBack;


//查找未结束的行程：hadStop==NO
-(NSArray*)getAllUnStopTrip;

//处理异常停止的trip并上传、校验、合并：hadStop==NO
-(void)stopAndUploadIncorrectTrip:(NSString*)locTripID;


//查找本地UploadOver==NO的行程。（hadStop==YES && UploadOver==NO）
-(NSArray*)getAllUnUploadOverTrip;

//检查并上传本地所有的未完全结束掉的trip。（hadStop==YES && UploadOver==NO）
-(void)checkAndUploadLocTrip:(void(^)(BOOL result))callBack;;


//上传指定tripIds的行程
-(void)uploadTheTripsOfTripids:(NSArray*)tripids andCallBack:(void(^)(BOOL result))upresult;

//续上异常中断的行程
-(void)keepLastTripWithInterval:(int)interval keepLastTripResult: (keepLastTripResultBlock)lastTripResultBlock;

//设置是否在导航中
-(void)setIsNaving:(BOOL)isNav;

//设置是否在结束倒计时
-(void)setIsStoping:(BOOL)isStoping;

//打开自动记录开关
-(void)openAutoRecordSwitch;

//关闭自动记录开关
-(void)closeAutoRecordSwitch;

//开启定期检测未上传行程并予以上传机制
- (void)startCycleCheckAndUploadLocTrip;

//关闭定期检测未上传行程并予以上传机制
- (void)stopCycleCheckAndUploadLocTrip;

@end

NS_ASSUME_NONNULL_END
