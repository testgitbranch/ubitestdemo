//
//  UBMGetDeviceSomeFrame.h
//  YO
//
//  Created by wzw on 2019/7/25.
//  Copyright © 2019 DeiDaiHuLianCompany. All rights reserved.
//

/*
 获得当前设备的一些大小属性
 是否是iPhone X
 底部非安全区的高度
 头部非安全区的高度
 安全区的高度
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UBMGetDeviceSomeFrame : NSObject

+(instancetype)share;

@property(nonatomic, assign)BOOL wIsiPhoneX; //是否是全面屏系列，根据底部安全区域判断

@property(nonatomic, assign)CGFloat wHeightOfStatusBar;  //状态栏的高度

@property(nonatomic, assign)CGFloat wHeightOfNavBar;  //状态栏+导航栏的高度

@property(nonatomic, assign)CGFloat wHeightOfTabBar;  //tabbar的高度(包括底部非安全区)

@property(nonatomic, assign)CGFloat wHeightOfSafeBottom;  //底部非安全区的高度


@end

NS_ASSUME_NONNULL_END
