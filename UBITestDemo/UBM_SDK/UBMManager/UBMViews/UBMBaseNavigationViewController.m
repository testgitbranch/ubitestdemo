//
//  UBMBaseNavigationViewController.m
//  EngineeProject
//
//  Created by SuperMan on 2017/7/31.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "UBMBaseNavigationViewController.h"
#import "UBMBaseViewController.h"

@interface UBMBaseNavigationViewController ()<UIGestureRecognizerDelegate>
@property (nonatomic, assign) BOOL enableGesturePop; //是否支持退出手势
@end

@implementation UBMBaseNavigationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    [[UINavigationBar appearance] setShadowImage:[self imageWithColor:Main_Color cornerRadius:0]];
    
    self.enableGesturePop = NO;
    self.interactivePopGestureRecognizer.delegate = self;
}


- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    
    [self enablePopGestureWithVC:self.topViewController];
    return self.enableGesturePop;
}

- (void)enablePopGestureWithVC:(UIViewController *)vc {
    
    if (!vc) self.enableGesturePop = NO;
    
    if ([vc isKindOfClass:[UBMBaseViewController class]]) {
        if ([vc respondsToSelector:@selector(gesturePopShouldBegin)]) {
            UBMBaseViewController *baseVC = (UBMBaseViewController *)vc;
            self.enableGesturePop = [baseVC gesturePopShouldBegin];
        }
    }
 
}

- (UIImage *)imageWithColor:(UIColor *)color cornerRadius:(CGFloat)cornerRadius {
    CGFloat minEdgeSize = (cornerRadius * 2 + 1);
    CGRect rect = CGRectMake(0, 0, minEdgeSize, minEdgeSize);
    UIBezierPath *roundedRect = [UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:cornerRadius];
    roundedRect.lineWidth = 0;
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0.0f);
    [color setFill];
    [roundedRect fill];
    [roundedRect stroke];
    [roundedRect addClip];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return [image resizableImageWithCapInsets:UIEdgeInsetsMake(cornerRadius, cornerRadius, cornerRadius, cornerRadius)];
}

@end
