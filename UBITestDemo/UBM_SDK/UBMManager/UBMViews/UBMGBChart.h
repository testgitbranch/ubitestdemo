//
//  UBMGBChart.h
//  GBChartDemo
//
//  Created by midas on 2018/12/26.
//  Copyright © 2018 Midas. All rights reserved.
//

#ifndef GBChart_h
#define GBChart_h

#import "UBMGBLineChartData.h"
#import "UBMWgbLineChart.h"
#import "UBMWPointLab.h"

#endif /* GBChart_h */
