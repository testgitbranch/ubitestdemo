//
//  UBMHistoryItemBottomItemView.h
//  LoveCarChannel_iOS
//
//  Created by 王国松 on 2021/6/3.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UBMHistoryBottomButton.h"

NS_ASSUME_NONNULL_BEGIN

@interface UBMHistoryItemBottomItemView : UIView

@property(nonatomic,strong)UILabel * titleLabel;
@property(nonatomic,strong)UBMHistoryBottomButton * bottomButton;

@end

NS_ASSUME_NONNULL_END
