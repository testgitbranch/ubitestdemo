//
//  WCustomAnnotation.h
//  CoreMotionOC
//
//  Created by wzw on 2020/10/13.
//  Copyright © 2020 LoveCarHome. All rights reserved.
//

/*
 自定义MAPointAnnotation， 增加类型属性
 */
#import "UBMPrefixHeader.h"

NS_ASSUME_NONNULL_BEGIN

typedef enum : NSUInteger {
    ubmStartPoint,             //起始点
    ubmEndPoint,               //结束点
    ubmEventPoint,             //事件类型
    
    //下面的暂时用不到。
    ubmRapidlYacceleratePoint, //急加速
    ubmRapidlSlowdownPoint,    //急减速
    ubmRapidlSwervesPoint,     //急转弯
} UBMWPointAnnotationType;

@interface UBMWCustomAnnotation : MAPointAnnotation

@property(nonatomic, assign)UBMWPointAnnotationType pointType;

@property(nonatomic, copy)NSString* eventIconUrl; //默认图片
@property(nonatomic, copy)NSString* eventTouchIconURL; //选择状态的图片
@property(nonatomic, assign)BOOL is_continuous; //是否是持续事件，1是

@property(nonatomic, strong)NSDictionary* dataDic; //需要气泡显示的数据，比如事件、速度、时间等。

@end

NS_ASSUME_NONNULL_END
