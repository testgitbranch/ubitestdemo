//
//  UBMHuanView.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/10/16.
//  Copyright © 2020 魏振伟. All rights reserved.
//

/*
 类似速度仪表盘view
 */

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UBMHuanView : UIView

@property(nonatomic, strong)UIColor* startColor;
@property(nonatomic, strong)UIColor* middleColor;
@property(nonatomic, strong)UIColor* endcolor;

@property(nonatomic, copy)NSString* speed; //速度

- (void)drawProgress:(CGFloat )progress;

@end

NS_ASSUME_NONNULL_END
