//
//  UBMWBaseAlertView.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/10/26.
//  Copyright © 2020 魏振伟. All rights reserved.
//

/*
各种弹出框可以继承此类，背景色和显示消失方法。
*/

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^dismissBlock)(void);

@interface UBMWBaseAlertView : UIView

@property(nonatomic, copy)dismissBlock dismissBlock;

@property(nonatomic, assign)BOOL touchBackDismiss; //点击背景时弹窗消失。默认yes。

-(void)addSomeViews;

-(void)show;

-(void)dismiss;

@end

NS_ASSUME_NONNULL_END
