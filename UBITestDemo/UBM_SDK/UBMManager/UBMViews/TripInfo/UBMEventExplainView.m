//
//  UBMEventExplainView.m
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/10/26.
//  Copyright © 2020 魏振伟. All rights reserved.
//

#import "UBMEventExplainView.h"
#import "UBMLeftImgLab.h"
#import "UBMPrefixHeader.h"

@interface UBMEventExplainView ()<UITextViewDelegate>
{
    CGFloat cardViewHeight;
}
@property (nonatomic, strong) UIView* cardView;
@property (nonatomic, strong) UILabel* titleLab;
@property (nonatomic, strong) UIButton* bottomBton;
@property (nonatomic, strong) WKWebView * webview;
@property(nonatomic,copy)NSString * title;
@property(nonatomic,copy)NSString * urlString;

@end

@implementation UBMEventExplainView

- (id)initWithFrame:(CGRect)frame title:(NSString *)title urlString:(NSString *)urlString{
    self = [super initWithFrame:frame];
    if (self) {
        _title = title;
        _urlString = urlString;
        cardViewHeight = 500;
        [self setupUI];
        [self setupLayout];
        [self requestUBMEventDescription];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        cardViewHeight = 500;
        [self setupUI];
        [self setupLayout];
        
        [self requestUBMEventDescription];
        
    }
    return self;
}

- (void)setupUI {
    [self addSubview:self.cardView];
    [self.cardView addSubview:self.titleLab];
    [self.cardView addSubview:self.bottomBton];
    [self.cardView addSubview:self.webview];
}

- (void)setupLayout {
    [self.cardView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@34);
        make.right.equalTo(@(-34));
        make.top.equalTo(@130);
        make.bottom.mas_equalTo(-130);
//        make.height.equalTo(@(cardViewHeight));
//        make.centerY.equalTo(self.mas_centerY);
    }];
    
    [self.titleLab mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(@0);
        make.top.equalTo(@26);
    }];
    
    [self.bottomBton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(@(-20));
        make.height.equalTo(@44);
        make.left.equalTo(@66);
        make.right.equalTo(@(-66));
    }];
    
    [self.webview mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(@0);
        make.top.equalTo(self.titleLab.mas_bottom).offset(20);
        make.bottom.equalTo(self.bottomBton.mas_top).offset(-15);
    }];
}

- (void)requestUBMEventDescription {
    if (_urlString) {
        NSURL * url = [NSURL URLWithString:_urlString];
        [self.webview loadRequest:[NSURLRequest requestWithURL:url]];
        return;
    }
    @bx_weakify(self);
    NSDictionary * paramsDict = @{@"userId":[UBITracking shared].userID ?: @""};
    [[UBMNetWorkTool shareNetWorkTool] requestWithBaseUrl:[UBMDomainName.shareDomain ubmURL] UrlString:@"trip/getEventDesc" parameters:paramsDict method:@"POST" success:^(id response) {
        NSLog(@"event_Desc__%@",response);
        @bx_strongify(self);
        if (response && [response isKindOfClass:NSDictionary.class] && [response[@"code"] integerValue] == 1) {
            NSString * urlStr = [[NSString alloc] initWithFormat:@"%@",response[@"data"][@"event_url"]];
            if ([urlStr hasPrefix:@"http"] || [urlStr hasPrefix:@"https"]) {
                NSURL * url = [NSURL URLWithString:urlStr];
                [self.webview loadRequest:[NSURLRequest requestWithURL:url]];
            }
        } else {
            
        }
    } error:^(id response) {
        
    } isShowHUD:NO];
}


#pragma mark- lazy
- (UIView *)cardView {
    if (!_cardView) {
        _cardView = [[UIView alloc] init];
        _cardView.backgroundColor = UIColor.whiteColor;
        _cardView.layer.cornerRadius = 6;
        _cardView.layer.masksToBounds = YES;
    }
    return _cardView;
}

- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.textAlignment = NSTextAlignmentCenter;
        _titleLab.font = [UIFont systemFontOfSize:17 weight:UIFontWeightBold];
        _titleLab.textColor = ColorSet(0, 0, 0, 0.85);
        _titleLab.text = @"什么是安全隐患?";
        if (_title) {
            _titleLab.text = _title;
        }
    }
    return _titleLab;
}

- (UIButton *)bottomBton {
    if (!_bottomBton) {
        _bottomBton = [UIButton buttonWithType:UIButtonTypeCustom];

        _bottomBton.backgroundColor = ColorSet(73, 112, 255, 1);
        [_bottomBton setTitle:@"我知道了" forState:UIControlStateNormal];
        [_bottomBton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
        _bottomBton.titleLabel.font = [UIFont systemFontOfSize:17];
        _bottomBton.layer.cornerRadius = 22;
        _bottomBton.layer.masksToBounds = YES;
        [_bottomBton addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    }
    return _bottomBton;
}

- (WKWebView *)webview {
    if (!_webview) {
        _webview = [[WKWebView alloc] init];
        _webview.scrollView.bounces = NO;
        _webview.scrollView.showsVerticalScrollIndicator = NO;
    }
    return _webview;
}

@end
