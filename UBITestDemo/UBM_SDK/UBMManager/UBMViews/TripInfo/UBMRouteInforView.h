//
//  UBMRouteInforView.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/10/15.
//  Copyright © 2020 魏振伟. All rights reserved.
//

/*
 详情view
 */

#import <UIKit/UIKit.h>
#import "UBMGdMapView.h"
#import "UBMSpeedChartView.h"

NS_ASSUME_NONNULL_BEGIN

@protocol UBMRouteInforViewDelegate <NSObject>

- (void)fankui; // 问题反馈
///非驾驶行为打标
- (void)requstMark;
///去收集
- (void)goCollect;
///去设置自动记录
- (void)goAutoRecordSettings;


@end

@interface UBMRouteInforView : UIScrollView

@property(nonatomic, weak)id<UBMRouteInforViewDelegate> routeInforViewDelegate;

@property(nonatomic, strong)UBMGdMapView* gv;
@property(nonatomic, strong,readonly)UBMSpeedChartView* speedChartView;

@property(nonatomic, assign)BOOL hiddenSpeedChartView;
///评分图片
@property (nonatomic, strong) UIImageView *scoreStampImageView;

@property(nonatomic, strong)NSDictionary* routeInforData;

///行程id
@property(nonatomic,copy)NSString * tripId;


@end

NS_ASSUME_NONNULL_END
