//
//  UBMWVerticalLab.m
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/10/17.
//  Copyright © 2020 魏振伟. All rights reserved.
//

#import "UBMWVerticalLab.h"
#import "UBMPrefixHeader.h"

@interface UBMWVerticalLab ()

@property(nonatomic, strong)UILabel* titleLab;
@property(nonatomic, strong)UILabel* lastLab;

@property(nonatomic, strong)UIImageView* rightLine;

@end

@implementation UBMWVerticalLab

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _lastLab = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, frame.size.width, frame.size.height/2)];
        _lastLab.font = [UIFont systemFontOfSize:13 weight:UIFontWeightRegular];
        _lastLab.textColor = UIColor.blackColor;
        _lastLab.textAlignment = NSTextAlignmentLeft;
        [self addSubview:_lastLab];
        
        _titleLab = [[UILabel alloc] initWithFrame:CGRectMake(15, _lastLab.bottom, frame.size.width, frame.size.height/2)];
        _titleLab.font = [UIFont systemFontOfSize:20 weight:UIFontWeightBold];
        _titleLab.textColor = ColorSet(0, 0, 0, 0.65);
        _titleLab.textAlignment = NSTextAlignmentLeft;
        [self addSubview:_titleLab];
        
        _rightLine = [[UIImageView alloc] initWithFrame:CGRectMake(frame.size.width-1, (frame.size.height-36)/2.0, 1, 36)];
        _rightLine.backgroundColor = ColorSet(0, 0, 0, 0.06);
        [self addSubview:_rightLine];
    }
    return self;
}

-(void)setTitle:(NSString *)title{
    _title = title;
    _titleLab.text = title;
}

-(void)setLastTitle:(NSString *)lastTitle{
    _lastTitle = lastTitle;
    _lastLab.text = lastTitle;
}

-(void)hidenRightLine{
    _rightLine.hidden = YES;
}

-(void)markTitleWithText:(NSString*)markText markColor:(UIColor*)markColor markFont:(UIFont*)markFont{
    
    [UBMViewTools markerLableOverlay:self.titleLab ofString:markText markColor:markColor markFondSize:markFont];

}

@end
