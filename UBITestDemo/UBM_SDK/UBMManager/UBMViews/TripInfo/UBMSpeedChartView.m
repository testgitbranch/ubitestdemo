//
//  UBMSpeedChartView.m
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/10/16.
//  Copyright © 2020 魏振伟. All rights reserved.
//

#import "UBMSpeedChartView.h"
#import "AAChartKit.h"
//#import "AAEasyTool.h"
#import "Route.pbobjc.h"
#import "UBMPrefixHeader.h"

@interface UBMSpeedChartView ()<AAChartViewEventDelegate>

@property (nonatomic, strong) AAChartModel *aaChartModel;
@property (nonatomic, strong) AAChartView  *aaChartView;

@property (nonatomic, assign) NSUInteger selectedIndex; //点击了那个点

@end

@implementation UBMSpeedChartView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self setupAAChartView];
        
        
    }
    return self;
}

- (void)setupAAChartView {

    CGFloat chartViewWidth  = self.frame.size.width;
    CGFloat chartViewHeight = self.frame.size.height;
    _aaChartView = [[AAChartView alloc]init];
    _aaChartView.frame = CGRectMake(0, 0, chartViewWidth-20, chartViewHeight);
    _aaChartView.scrollEnabled = NO;//禁用 AAChartView 滚动效果
//    _aaChartView.isClearBackgroundColor = YES;//设置 AAChartView 的背景色是否为透明
//    _aaChartView.delegate = self;
    [self addSubview:_aaChartView];
    
    //单位lable，单独设置。
    UILabel* yLab = [UBMViewTools lableWithText:@"km/h" font:[UIFont systemFontOfSize:10] textColor:ColorSet(0, 0, 0, 0.45)];
    yLab.frame = CGRectMake(0, _aaChartView.bottom-30, 40, 16);
    yLab.textAlignment = NSTextAlignmentCenter;
    [self addSubview:yLab];
    UILabel* xLab = [UBMViewTools lableWithText:@"分钟" font:[UIFont systemFontOfSize:10] textColor:ColorSet(0, 0, 0, 0.45)];
    xLab.frame = CGRectMake(self.frame.size.width-50, _aaChartView.bottom-8, 40, 16);
    xLab.textAlignment = NSTextAlignmentRight;
    [self addSubview:xLab];
    
    //图标点击事件
//    [self setupChartViewEventHandlers];
    
    //设置图表类型
    [self setupAAChartViewWithChartType:AAChartTypeSpline];
    
    //数据
    [_aaChartView aa_drawChartWithChartModel:_aaChartModel];
    
    //自定义提示框样式
    [self chartConfigurationWithAaChartModel:_aaChartModel];
}

- (void)setupChartViewEventHandlers {
    //获取图表加载完成事件
    [_aaChartView didFinishLoadHandler:^(AAChartView *aaChartView) {
        NSLog(@"🚀🚀🚀🚀 AAChartView content did finish load!!!");
    }];
    
    //获取图表上的手指点击及滑动事件
    [_aaChartView moveOverEventHandler:^(AAChartView *aaChartView,
                                         AAMoveOverEventMessageModel *message) {
        NSDictionary *messageDic = @{
            @"category":message.category,
            @"index":@(message.index),
            @"name":message.name,
            @"offset":message.offset,
            @"x":message.x,
            @"y":message.y
        };
        
        NSString *str1 = [NSString stringWithFormat:@"👌👌👌👌 selected point series element name: %@\n",
                          message.name];
        NSString *str2 = [NSString stringWithFormat:@"user finger moved over!!!,get the move over event message: %@",
                          messageDic];
        NSLog(@"%@%@",str1, str2);
    }];
}

- (void)setupAAChartViewWithChartType:(AAChartType)chartType {
    _aaChartModel = AAChartModel.new
    .chartTypeSet(chartType)//图表类型
    .colorsThemeSet(@[@"#2D75E4",@"#000000",@"#000000",@"#000000"])//设置主题颜色数组
    .tooltipEnabledSet(YES)  //是否允许点击线上的点
    .legendEnabledSet(NO)   //是否显示底部图例
    .tooltipValueSuffixSet(@"")//设置浮动提示框单位后缀
    .markerRadiusSet(@0)  //点的宽度
    .dataLabelsEnabledSet(NO) //是否显示数据
    
    .yAxisVisibleSet(YES)     //显示y轴
    .yAxisLineWidthSet(@0)    //Y轴轴线线宽为0即是隐藏Y轴轴线
    .yAxisGridLineWidthSet(@1)//y轴横向分割线宽度为0(即是隐藏分割线)
    .yAxisTickPositionsSet(@[@20, @40, @60, @80, @100, @120, @140, @160, @180]) //y轴坐标值
    
//    .xAxisTickIntervalSet(@1)  //几个值一个刻度
    .seriesSet(@[
        AASeriesElement.new
        .dataSet(@[@30.0, @10, @77.0, @6.9])
        .allowPointSelectSet(NO),
    ]);
    
    //配置x轴
    _aaChartModel.categories = @[@"1", @"", @"2", @"", @"5",@"6", @"3", ];
    
    
    /*配置 Y 轴标注线,解开注释,即可查看添加标注线之后的图表效果(NOTE:必须设置 Y 轴可见)*/
//    [self configureTheYAxisPlotLineForAAChartView];
    
}


#pragma mark - 表中 横-基线
- (void)configureTheYAxisPlotLineForAAChartView {
    
    _aaChartModel
    .yAxisPlotLinesSet(@[
        AAPlotLinesElement.new
        .colorSet(@"#F5F5F5")//颜色值(16进制)
        .dashStyleSet(AAChartLineDashStyleTypeSolid)//样式：Dash,Dot,Solid等,默认Solid
        .widthSet(@(1)) //标示线粗细
        .valueSet(@(20)) //所在位置
        .zIndexSet(@(0)) //层叠,标示线在图表中显示的层叠级别，值越大，显示越向前
        .labelSet(AALabel.new
                  .textSet(@"")
                  .styleSet((id)ColorWithRGB(@"#F05353")))
        ,
        AAPlotLinesElement.new
        .colorSet(@"#F5F5F5")//颜色值(16进制)
        .dashStyleSet(AAChartLineDashStyleTypeSolid)//样式：Dash,Dot,Solid等,默认Solid
        .widthSet(@(1)) //标示线粗细
        .valueSet(@(40)) //所在位置
        .zIndexSet(@(0)) //层叠,标示线在图表中显示的层叠级别，值越大，显示越向前
        .labelSet(AALabel.new
                  .textSet(@"")
                  .styleSet((id)ColorWithRGB(@"#F05353")))
        ,
        AAPlotLinesElement.new
        .colorSet(@"#F5F5F5")//颜色值(16进制)
        .dashStyleSet(AAChartLineDashStyleTypeSolid)//样式：Dash,Dot,Solid等,默认Solid
        .widthSet(@(1)) //标示线粗细
        .valueSet(@(60)) //所在位置
        .zIndexSet(@(0)) //层叠,标示线在图表中显示的层叠级别，值越大，显示越向前
        .labelSet(AALabel.new
                  .textSet(@"")
                  .styleSet((id)ColorWithRGB(@"#F05353")))
        ,
        AAPlotLinesElement.new
        .colorSet(@"#F5F5F5")//颜色值(16进制)
        .dashStyleSet(AAChartLineDashStyleTypeSolid)//样式：Dash,Dot,Solid等,默认Solid
        .widthSet(@(1)) //标示线粗细
        .valueSet(@(80)) //所在位置
        .zIndexSet(@(0)) //层叠,标示线在图表中显示的层叠级别，值越大，显示越向前
        .labelSet(AALabel.new
                  .textSet(@"")
                  .styleSet((id)ColorWithRGB(@"#F05353")))
        ,
        AAPlotLinesElement.new
        .colorSet(@"#F5F5F5")//颜色值(16进制)
        .dashStyleSet(AAChartLineDashStyleTypeSolid)//样式：Dash,Dot,Solid等,默认Solid
        .widthSet(@(1)) //标示线粗细
        .valueSet(@(100)) //所在位置
        .zIndexSet(@(0)) //层叠,标示线在图表中显示的层叠级别，值越大，显示越向前
        .labelSet(AALabel.new
                  .textSet(@"")
                  .styleSet((id)ColorWithRGB(@"#F05353")))
        ,
        AAPlotLinesElement.new
        .colorSet(@"#F5F5F5")//颜色值(16进制)
        .dashStyleSet(AAChartLineDashStyleTypeSolid)//样式：Dash,Dot,Solid等,默认Solid
        .widthSet(@(1)) //标示线粗细
        .valueSet(@(120)) //所在位置
        .zIndexSet(@(0)) //层叠,标示线在图表中显示的层叠级别，值越大，显示越向前
        .labelSet(AALabel.new
                  .textSet(@"")
                  .styleSet((id)ColorWithRGB(@"#F05353")))
        ,

                       ]);
}

#pragma mark - 配置弹出框样式
-(void)chartConfigurationWithAaChartModel:(AAChartModel*)aaChartModel{
    
    AAOptions *aaOptions = [aaChartModel aa_toAAOptions];
    AATooltip *tooltip = aaOptions.tooltip;
    
    aaOptions.tooltip
    .useHTMLSet(true)
    .formatterSet(@AAJSFunc(function () {
        let colorsArr = [];
        colorsArr.push("mediumspringgreen");
        colorsArr.push("deepskyblue");
        colorsArr.push("red");
        colorsArr.push("sandybrown");
        let yy = this.y.toFixed(1);
        let wholeContentString ='<span style=\"color:#FF7918;font-size:14px;font-weight: bold\">' + yy + 'km/h</span><br/>'+ '<div style=\"color:#FF7918;font-size:10px;text-align: center;color:rgba(0,0,0,.85)\">速度</div>';
        return wholeContentString;
    }))
    .backgroundColorSet(@"#FFFFFF")
    .borderColorSet(@"#FFFFFF")
    .borderRadiusSet(@5)
    ;

    //禁用图例点击事件
     aaOptions.plotOptions.series.events = AAEvents.new
     .legendItemClickSet(@AAJSFunc(function() {
         return false;
     }));
    
//    tooltip
//    .sharedSet(false)
//    .useHTMLSet(true)
//    .formatterSet(@AAJSFunc(function () {
//        return this.y+'km/h <br/> '
//        + '最高速度';
//    }))
//    .valueDecimalsSet(@2)//设置取值精确到小数点后几位
//    .backgroundColorSet(@"#FFFFFF")
//    .borderColorSet(@"#FFFFFF")
//    .styleSet(AAStyleColorSize(@"#000000", 10))
//    ;
    
    [self.aaChartView aa_drawChartWithOptions:aaOptions];
}


#pragma mark - 速度点数组
-(void)setSpeedPointArray:(NSArray *)speedPointArray{
    
    if (speedPointArray && speedPointArray.count>0) {
        
        NSArray* changeArr = speedPointArray;
        for (int i=0; i<20; i++) {
            if (changeArr.count<=200) {
                break;
            }else{
                changeArr = [self chouoxi:changeArr];
            }
        }
        
        speedPointArray = changeArr;
        _speedPointArray = speedPointArray;
        
        //总时间
        RoutePoint* rp1 = speedPointArray[0];
        CGFloat startTime = rp1.timestamp;
        RoutePoint* rp2 = speedPointArray[speedPointArray.count-1];
        CGFloat endTime = rp2.timestamp;
        
        CGFloat totalTime = endTime-startTime;
        if (totalTime == 0) {
            //没有数据
            NSLog(@"没有行程数据");
            return;
        }
        
        //除以10段,取整为刻度
        int x1 = 1; //(totalTime/60)/10+1;

        
        //通过分钟数，计算应该展示的x轴的刻度。和每段的newNum个值。不能简单的除以10取整。
        int newNum = 1;
        int totalMinte = totalTime/60;
        
        if (totalMinte == 0) {
            totalMinte = 1;
        }
        
        //确定刻度值
        for (int i=1; i<500; i++) {
            if (i*10 > totalMinte) {
                x1 = i;
                break;
            }
        }
        
        //新增逻辑，计算刻度
        NSMutableArray* xLabTextArray = [[NSMutableArray alloc] init];
        for (int i=1; i<1000; i++) {
            if (i*10 > totalMinte) {
                
                for (int x=1; x<11; x++) {
                    if (x*i > totalMinte) {
                        break;
                    }else{
                        [xLabTextArray addObject:[NSString stringWithFormat:@"%d", x*i]];
                    }
                }
                
                break;
            }
        }
        CGFloat lableWidth = (self.width-60.0-10.0)/xLabTextArray.count;
        for (int i=0; i<xLabTextArray.count; i++) {
            
            UILabel* xlab = [[UILabel alloc] initWithFrame:CGRectMake(30+i*lableWidth, _aaChartView.bottom-10, lableWidth, 20)];
            xlab.text = xLabTextArray[i];
            xlab.textAlignment = NSTextAlignmentRight;
            xlab.textColor = ColorSet(0, 0, 0, 0.45);
            xlab.font = [UIFont systemFontOfSize:12];
            [self addSubview:xlab];
        }
        
        
        NSMutableArray* xArr = [[NSMutableArray alloc] init];
        NSMutableArray* yArr = [[NSMutableArray alloc] init];
//        for (int i=0; i<11; i++) {
//            NSString* str = [NSString stringWithFormat:@"%d",i*x1];
//            [xArr addObject:str];
//        }
        
        //计算每段应该有几个数
        newNum = (int)speedPointArray.count/totalMinte*x1;
        
        if (newNum == 0) {
            newNum = 1;
        }
        
        
        //循环点。 算出y值数组。
        for (int i=0; i<speedPointArray.count; i++) {
            
            RoutePoint* rp = speedPointArray[i];
            NSString* speed = [NSString stringWithFormat:@"%.1f", rp.speedInKilometersPerHour];
            NSNumber* numy = [[NSNumber alloc] initWithFloat:[speed floatValue]];
            [yArr addObject:numy ];
            
            NSString* xstr = @"";
            if ( i%newNum == 0) {
//                xstr = [NSString stringWithFormat:@"%d", x1*(i/newNum)];
            }
            [xArr addObject:xstr];
        }
        
        //每段是几个值，
//        int num = newNum;
        
        
        //数据准备完毕。开始绘图
        //更新表数据
        AASeriesElement* line1 = [[AASeriesElement alloc] init]
        .dataSet(yArr)
        .allowPointSelectSet(NO)
        .lineWidthSet(@3)
        .statesSet(AAStates.new
            .hoverSet(AAHover.new
                      .enabledSet(true)
                      .lineWidthPlusSet(@0)//手指盘旋或选中图表时,禁止线条变粗
                      ))
        .markerSet(AAMarker.new
//            .lineWidthSet(@4)
            .lineColorSet(@"#2D75E4")
//            .lineWidthSet(@0)
            .fillColorSet(@"#2D75E4")
            .radiusSet(@0)
            .statesSet(AAMarkerStates.new
                       .hoverSet(AAMarkerHover.new
                                 .lineWidthSet(@2)
                                 .lineColorSet(@"#FF7918")
                                 .fillColorSet(AAColor.whiteColor)//设置手指选中点的颜色
                                 .radiusSet(@5))));
        
        _aaChartModel.series = @[line1];
        _aaChartModel.categories = xArr;
//        _aaChartModel.xAxisTickIntervalSet(@(num));  //几个值一个刻度
        
        [_aaChartView aa_refreshChartWithChartModel:_aaChartModel];
//        [_aaChartView aa_drawChartWithChartModel:_aaChartModel];
        
        [self chartConfigurationWithAaChartModel:_aaChartModel];
        
    }
    
}

#pragma mark - 抽稀
-(NSArray*)chouoxi:(NSArray*)pointArr{
    
    RoutePoint* maxSpeedPoint = nil;
    
    NSMutableArray* changeArr = [[NSMutableArray alloc] init];
    for (int i=0; i<pointArr.count; i++) {
        
        if (i%2 == 0) {
            [changeArr addObject:pointArr[i]];
            
            if (i == pointArr.count-1) {
                break;
            }
        }
        
        //加上一个最高速度的点，无论怎么抽，这个点都应该会被留下来。
        if (maxSpeedPoint == nil && self.maxSpeed>1.0) {
            RoutePoint* rp = pointArr[i];
            if (rp.speedInKilometersPerHour-0.5 <= self.maxSpeed && self.maxSpeed <= rp.speedInKilometersPerHour+0.5) {
                maxSpeedPoint = rp;
                [changeArr addObject:rp];
            }
        }
        
        //加上最后一个点。
        if (i == pointArr.count-1) {
            [changeArr addObject:pointArr[i]];
            break;
        }
    }
    
    return changeArr;
}

#pragma mark - 手动填x轴 lab
//-(void)

@end
