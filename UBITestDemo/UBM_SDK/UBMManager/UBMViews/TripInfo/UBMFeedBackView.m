//
//  UBMFeedBackView.m
//  LoveCarChannel_iOS
//
//  Created by 王国松 on 2021/6/28.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

#import "UBMFeedBackView.h"
#import "UBMPrefixHeader.h"

@implementation UBMFeedBackView

- (id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setUp];
        [self setupLayout];
    }
    return self;
}

- (void)setUp{
    [self addSubview:self.feedbackTipLabel];
    [self addSubview:self.feedbackButton];
}

- (void)setupLayout{
    [self.feedbackTipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(16);
        make.centerY.equalTo(self);
    }];
    [self.feedbackButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).offset(-14);
        make.centerY.equalTo(self);
        make.size.mas_equalTo(18);
    }];
}

- (UILabel *)feedbackTipLabel {
    if (!_feedbackTipLabel) {
        _feedbackTipLabel = [[UILabel alloc] init];
        _feedbackTipLabel.text = @"行程记录有问题，我要反馈";
        _feedbackTipLabel.backgroundColor = [UIColor ubmColorWithHexNum:0xFFFFFF];
        _feedbackTipLabel.textColor = [UIColor ubmColorWithHexNum:0x000000 alpha:0.85];
        _feedbackTipLabel.textAlignment = NSTextAlignmentLeft;
        _feedbackTipLabel.font = [UIFont systemFontOfSize:15 weight:UIFontWeightRegular];
    }
    return _feedbackTipLabel;
}

- (UBMExpandButton *)feedbackButton {
    if (!_feedbackButton) {
        _feedbackButton = [UBMExpandButton buttonWithType:UIButtonTypeCustom];
        [_feedbackButton setImage:[UIImage imageNamed:@"toRightImg10"] forState:UIControlStateNormal];
    }
    return _feedbackButton;
}

@end
