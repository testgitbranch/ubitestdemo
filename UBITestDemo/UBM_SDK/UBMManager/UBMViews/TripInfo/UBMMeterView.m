//
//  UBMMeterView.m
//  LoveCarChannel_iOS
//
//  Created by 金鑫 on 2021/3/5.
//  Copyright © 2021 魏振伟. All rights reserved.
//

#import "UBMMeterView.h"

@interface UBMMeterView ()
@property (nonatomic, strong) CALayer *drawLayer;
@end

@implementation UBMMeterView

/// 贝塞尔曲线的外弧
/// @param lineWidth 外弧线宽
/// @param minAngle 初始角度
/// @param maxAngle 终止角度
- (UIBezierPath *)outArcBezierPathWithLineWidth:(CGFloat)lineWidth minAngle:(CGFloat)minAngle maxAngle:(CGFloat)maxAngle {
    return [UIBezierPath bezierPathWithArcCenter:CGPointMake(self.frame.size.width * 0.5, self.frame.size.height * 0.5) radius:self.frame.size.width * 0.5 - lineWidth * 0.5 startAngle:minAngle endAngle:maxAngle clockwise:YES];
}

/// 画仪表盘进度(用于一次性绘画进度值)
/// @param startAngle 开始角
/// @param endAngle 结束角
/// @param lineWidth 边框宽度
/// @param fillColor 填充色
/// @param strokeColor 边框色
/// @param progressValue 进度值
/// @param fullValue 总值
- (void)drawProgressWithLineWidth:(CGFloat)lineWidth minAngle:(CGFloat)minAngle maxAngle:(CGFloat)maxAngle fillColor:(UIColor*)fillColor strokeColor:(UIColor*)strokeColor progressValue:(NSInteger)progressValue fullValue:(CGFloat)fullValue {
    
    if (progressValue < 0 || fullValue <= 0) { return; }
    
    progressValue = progressValue > fullValue ? fullValue : progressValue;
    
    UIBezierPath* outArc = [self outArcBezierPathWithLineWidth:lineWidth minAngle:minAngle maxAngle:maxAngle];
    self.path = outArc;
    
    CAShapeLayer* progressLayer=[CAShapeLayer layer];
    progressLayer.frame = self.bounds;
    progressLayer.lineWidth = lineWidth;
    progressLayer.fillColor = fillColor.CGColor;
    progressLayer.strokeColor = strokeColor.CGColor;
    progressLayer.path = outArc.CGPath;
    progressLayer.strokeStart = 0;
    progressLayer.strokeEnd = 0.01 * progressValue;
    progressLayer.lineCap = kCALineCapButt;
    self.progressLayer = progressLayer;
    [self.layer addSublayer:progressLayer];
}

/// 画仪表盘进度(用于一次性绘画进度值 + 动画)
/// @param lineWidth 边框宽度
/// @param minAngle 开始角
/// @param maxAngle 结束角
/// @param fillColor 填充色
/// @param strokeColor 边框色
/// @param progressValue 进度值
/// @param fullValue 总值
- (void)drawAnimationProgressWithLineWidth:(CGFloat)lineWidth minAngle:(CGFloat)minAngle maxAngle:(CGFloat)maxAngle fillColor:(UIColor*)fillColor strokeColor:(UIColor*)strokeColor progressValue:(CGFloat)progressValue fullValue:(CGFloat)fullValue fullAnimationTime:(CFTimeInterval)time {
    
    if (progressValue < 0 || fullValue <= 0) { return; }
    
    progressValue = progressValue > fullValue ? fullValue : progressValue;
        
    CGFloat progress = progressValue / fullValue;
    CFTimeInterval progressTime = time * progress;
    
    UIBezierPath* outArc = [self outArcBezierPathWithLineWidth:lineWidth minAngle:minAngle maxAngle:maxAngle];
    self.path = outArc;
    
    CABasicAnimation *pathAnim = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    pathAnim.duration = progressTime;
    pathAnim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    pathAnim.fromValue = [NSNumber numberWithFloat: 0.0];
    pathAnim.toValue = [NSNumber numberWithFloat: progress];
    pathAnim.fillMode = kCAFillModeForwards;
    pathAnim.removedOnCompletion = NO;
    
    CAShapeLayer* progressLayer=[CAShapeLayer layer];
    progressLayer.frame = self.bounds;
    progressLayer.lineWidth = lineWidth;
    progressLayer.fillColor = fillColor.CGColor;
    progressLayer.strokeColor = strokeColor.CGColor;
    progressLayer.path = outArc.CGPath;
    progressLayer.lineCap = kCALineCapButt;
    
    [progressLayer addAnimation:pathAnim forKey:@"strokeEndAnimation"];
    
    self.progressLayer = progressLayer;
    [self.layer addSublayer:progressLayer];
}

/// 画仪表盘进度(用于多次绘画进度值)
/// @param lineWidth 边框宽度
/// @param minAngle 开始角
/// @param maxAngle 结束角
/// @param fillColor 填充色
/// @param strokeColor 边框色
- (void)drawProgressWithLineWidth:(CGFloat)lineWidth minAngle:(CGFloat)minAngle maxAngle:(CGFloat)maxAngle fillColor:(UIColor*)fillColor strokeColor:(UIColor*)strokeColor {

    UIBezierPath* outArc = [self outArcBezierPathWithLineWidth:lineWidth minAngle:minAngle maxAngle:maxAngle];
    self.path = outArc;
    
    CAShapeLayer* progressLayer = [CAShapeLayer layer];
    progressLayer.frame = self.bounds;
    progressLayer.lineWidth = lineWidth;
    progressLayer.fillColor = fillColor.CGColor;
    progressLayer.strokeColor = strokeColor.CGColor;
    progressLayer.path = outArc.CGPath;
    progressLayer.strokeStart = 0;
    progressLayer.strokeEnd = 0;
    progressLayer.lineCap = kCALineCapButt;
    self.progressLayer = progressLayer;
    [self.layer addSublayer:progressLayer];
}

/// 为进度添加渐变图层
/// @param colorGradArray 渐变色数组
- (void)setColorGrad:(NSArray*)colorGradArray {
    CALayer *layer = [CALayer layer];
    self.drawLayer = layer;
    CAGradientLayer *gradientLayer =  [CAGradientLayer layer];
    gradientLayer.frame = self.bounds;
    [gradientLayer setColors:colorGradArray];
    [gradientLayer setStartPoint:CGPointMake(0, 1)];
    [gradientLayer setEndPoint:CGPointMake(1, 1)];
    [layer addSublayer:gradientLayer];
    [layer setMask:self.progressLayer];
    self.gradientLayer = gradientLayer;
    [self.layer addSublayer:layer];
}

- (void)setZoreProgress{
    NSArray * array = [NSArray arrayWithObjects:(id)[UIColor clearColor].CGColor, (id)[UIColor clearColor].CGColor, nil];
    [self.gradientLayer setColors:array];
}

- (void)resetProgress {
    [self.progressLayer removeFromSuperlayer];
}

@end
