//
//  UBMScoreView.m
//  LoveCarChannel_iOS
//
//  Created by 王国松 on 2021/6/29.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

#import "UBMScoreView.h"
#import "UBMPrefixHeader.h"

@implementation UBMScoreView

- (id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setUp];
        [self setupLayout];
    }
    return self;
}

- (void)setUp{
    [self addSubview:self.scoreMeterBorderImageView];
    [self addSubview:self.scoreMeterBaseImageView];
    [self addSubview:self.scoreMeterView];
    [self addSubview:self.scoreMeterScaleImageView];
    [self addSubview:self.scoreSafetyContentView];
    [self.scoreSafetyContentView addSubview:self.scoreSafety];
    [self.scoreSafetyContentView addSubview:self.scoreSafetyLabel];
    [self addSubview:self.scoreStampImageView];
}

- (void)setupLayout{
    [self.scoreMeterView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(self.scoreMeterView.width);
        make.height.mas_equalTo(self.scoreMeterView.height);
        make.top.equalTo(self.scoreMeterView.superview).offset(-35);
        make.left.equalTo(self.scoreMeterView.superview).offset(26);
    }];
    [self.scoreMeterBorderImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(134);
        make.height.mas_equalTo(41);
        make.bottom.equalTo(self.scoreMeterBorderImageView.superview.mas_top).offset(1);
        make.centerX.equalTo(self.scoreMeterView);
    }];
    [self.scoreMeterBaseImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(self.scoreMeterView);
        make.height.mas_equalTo(82);
        make.centerX.equalTo(self.scoreMeterView);
        make.top.equalTo(self.scoreMeterView);
    }];
    [self.scoreMeterScaleImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(self.scoreMeterView);
        make.height.mas_equalTo(86);
        make.centerX.equalTo(self.scoreMeterView);
        make.top.equalTo(self.scoreMeterView);
    }];
    [self.scoreSafetyContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(62);
        make.height.mas_equalTo(70);
        make.centerX.equalTo(self.scoreMeterView);
        make.top.equalTo(self.scoreSafetyContentView.superview);
        make.bottom.equalTo(self.scoreSafetyContentView.superview).offset(-7);
    }];
    [self.scoreSafety mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.scoreSafety.superview);
        make.centerX.equalTo(self.scoreSafety.superview);
    }];
    [self.scoreSafetyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.scoreSafetyLabel.superview);
        make.centerX.equalTo(self.scoreSafetyLabel.superview);
    }];
    [self.scoreStampImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.scoreStampImageView.superview);
        make.right.equalTo(self.scoreStampImageView.superview).offset(-16);
        make.width.mas_equalTo(70);
        make.height.mas_equalTo(56);
    }];
    
}

//MARK:赋值
- (void)setRouteInforData:(NSDictionary *)routeInforData{
    _routeInforData = routeInforData;
    if ([routeInforData[@"is_normal"] integerValue] == 1) {
        NSInteger level = [routeInforData[@"level"] integerValue];
        if (level == 1) {
            self.scoreStampImageView.image = [UIImage imageFromUbiBundleWithName:@"ubm_inforType3"];
        } else if (level == 2){
            self.scoreStampImageView.image = [UIImage imageFromUbiBundleWithName:@"ubm_inforType2"];
        } else if (level == 3){
            self.scoreStampImageView.image = [UIImage imageFromUbiBundleWithName:@"ubm_inforType4"];
        } else if (level == 4){
            self.scoreStampImageView.image = [UIImage imageFromUbiBundleWithName:@"ubm_inforType1"];
        } else {
            self.scoreStampImageView.image = [UIImage imageFromUbiBundleWithName:@"ubm_inforType5"];
        }
    } else {
        //异常
        self.scoreStampImageView.image = [UIImage imageFromUbiBundleWithName:@"ubm_inforType5"];
    }
}

- (UIImageView *)scoreMeterBorderImageView {
    if (!_scoreMeterBorderImageView) {
        _scoreMeterBorderImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ubm_score_meter_border_light"]];
    }
    return _scoreMeterBorderImageView;
}

- (UIImageView *)scoreMeterBaseImageView {
    if (!_scoreMeterBaseImageView) {
        _scoreMeterBaseImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ubm_score_meter_base_light"]];
    }
    return _scoreMeterBaseImageView;
}

- (UIImageView *)scoreMeterScaleImageView {
    if (!_scoreMeterScaleImageView) {
        _scoreMeterScaleImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ubm_score_meter_scale_light"]];
    }
    return _scoreMeterScaleImageView;
}

- (UBMMeterView *)scoreMeterView {
    if (!_scoreMeterView) {
        _scoreMeterView = [[UBMMeterView alloc] initWithFrame:CGRectMake(0, 0, 120, 120)];
        _scoreMeterView.backgroundColor = UIColor.clearColor;
    }
    return _scoreMeterView;
}

- (UIView *)scoreSafetyContentView {
    if (!_scoreSafetyContentView) {
        _scoreSafetyContentView = [[UIView alloc] init];
    }
    return _scoreSafetyContentView;
}

- (UILabel *)scoreSafety {
    if (!_scoreSafety) {
        _scoreSafety = [[UILabel alloc] init];
        _scoreSafety.textColor = [UIColor ubmColorWithHexNum:0x008AFF];
        _scoreSafety.textAlignment = NSTextAlignmentCenter;
        _scoreSafety.font = [UIFont systemFontOfSize:36 weight:UIFontWeightBold];
    }
    return _scoreSafety;
}

- (UILabel *)scoreSafetyLabel {
    if (!_scoreSafetyLabel) {
        _scoreSafetyLabel = [[UILabel alloc] init];
        _scoreSafetyLabel.text = @"驾驶安全分";
        _scoreSafetyLabel.textColor = [UIColor ubmColorWithHexNum:0x000000 alpha:0.65];
        _scoreSafetyLabel.textAlignment = NSTextAlignmentCenter;
        _scoreSafetyLabel.font = [UIFont systemFontOfSize:12 weight:UIFontWeightRegular];
    }
    return _scoreSafetyLabel;
}

- (UIImageView *)scoreStampImageView {
    if (!_scoreStampImageView) {
        _scoreStampImageView = [[UIImageView alloc] init];
    }
    return _scoreStampImageView;
}


@end
