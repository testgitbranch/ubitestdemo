//
//  UBMGdMapView.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/10/15.
//  Copyright © 2020 魏振伟. All rights reserved.
//

/*
 为了应对每次进来加载地图和覆盖物时的卡顿，把地图页直接搞成单例。确定就是常驻100+内存，优点是加载迅速不卡顿。
 */

#import <UIKit/UIKit.h>
#import <AMapFoundationKit/AMapFoundationKit.h>
#import <AMapNaviKit/AMapNaviKit.h>
#import "Route.pbobjc.h"

NS_ASSUME_NONNULL_BEGIN

@interface UBMGdMapView : UIView


@property (nonatomic, strong) MAMapView *mapView;


//需要画的线的点
@property (nonatomic, assign)int linePointCoordinates;//线的坐标系类型 0 gps 1:gcj02
@property (nonatomic, strong)NSArray<RoutePoint*> *linePointArray;


//事件和起终点的坐标系类型
@property (nonatomic, copy)NSString* coordinatesType; //(WGS84 | GCJ02）

//需要打的地图上的点。事件
@property (nonatomic, strong)NSArray* eventArray;

//开始点
@property (nonatomic, strong)NSDictionary* statrtPoint;

//结束点
@property (nonatomic, strong)NSDictionary* endPoint;


//移除所有覆盖物和保持的属性。在页面所在控制器被销毁时调用。
-(void)remoAllOverly;

@end

NS_ASSUME_NONNULL_END
