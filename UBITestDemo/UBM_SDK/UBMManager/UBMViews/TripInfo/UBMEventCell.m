//
//  UBMEventCell.m
//  LoveCarChannel_iOS
//
//  Created by 金鑫 on 2021/3/2.
//  Copyright © 2021 魏振伟. All rights reserved.
//

#import "UBMEventCell.h"
#import "UBMEventModel.h"

@interface UBMEventCell ()
@property (weak, nonatomic) IBOutlet UILabel *eventCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *eventUnitLabel;
@property (weak, nonatomic) IBOutlet UILabel *eventTypeLabel;
@end

@implementation UBMEventCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.layer.cornerRadius = 4;
    self.layer.masksToBounds =YES;
}

- (void)setModel:(UBMEventModel *)model {
    _model = model;
    self.eventCountLabel.text = [NSString stringWithFormat:@"%zd", model.eventCount];
    self.eventUnitLabel.text = model.eventUnit;
    self.eventTypeLabel.text = model.eventType;
}

@end
