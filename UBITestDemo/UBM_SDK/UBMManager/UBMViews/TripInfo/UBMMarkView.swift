//
//  UBMMarkView.swift
//  LoveCarChannel_iOS
//
//  Created by 王国松 on 2021/5/20.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

import UIKit

@objcMembers public class UBMMarkView: UIView {
    
    open var markBlock:(()->Void)?
    open var not_driver_url:String?
    
    let titleLabel = UILabel()
    let qustionButton = UIButton()
    let contentLabel = UILabel()
    let markButton = UIButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView()  {
        titleLabel.font = .systemFont(ofSize: 16, weight: .semibold)
        titleLabel.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.85)
        titleLabel.text = "非驾驶行程"
        addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.left.top.equalToSuperview().offset(14)
        }
        qustionButton.setImage(UIImage(named: "ubm_detail_qustion"), for: .normal)
        addSubview(qustionButton)
        qustionButton.snp.makeConstraints { (make) in
            make.left.equalTo(titleLabel.snp.right).offset(4)
            make.centerY.equalTo(titleLabel)
            make.width.height.equalTo(18)
        }
        qustionButton.addTarget(self, action: #selector(showTipAlertView), for: .touchUpInside)
        contentLabel.font = .systemFont(ofSize: 14, weight: .regular)
        contentLabel.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.65)
        contentLabel.text = "这不是我的驾驶行程，可以将行程标记为非驾驶行程"
        contentLabel.numberOfLines = 0
        addSubview(contentLabel)
        contentLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(14)
            make.top.equalToSuperview().offset(44)
            make.right.equalToSuperview().offset(-137)
        }
        markButton.setTitle("标记", for: .normal)
        markButton.backgroundColor = UIColor(red: 32, green: 108, blue: 255, alpha: 1)
        markButton.setTitleColor(.white, for: .normal)
        markButton.layer.masksToBounds = true
        markButton.layer.cornerRadius = 18
        addSubview(markButton)
        markButton.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-14)
            make.centerY.equalToSuperview()
            make.width.equalTo(80)
            make.height.equalTo(36)
        }
        markButton.addTarget(self, action: #selector(markOnclick), for: .touchUpInside)
    }
    
    @objc func showTipAlertView()  {
        let rect = CGRect.init(x: 0, y: 0, width: UIScreen.ubmWidth, height: UIScreen.ubmHeight)
        let explainView = UBMEventExplainView.init(frame: rect, title: "非驾驶行程", urlString: not_driver_url!)
        explainView.show()
    }
    
    @objc func markOnclick() {
        markBlock!()
    }
}
