//
//  UBMGreenEnergyView.swift
//  LoveCarChannel_iOS
//
//  Created by 王国松 on 2021/6/2.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

import UIKit
import SnapKit

@objcMembers open class UBMGreenEnergyView: UIView {

    let titleLabel = UILabel()
    ///减排数量
    let numberLabel = UILabel()
    let contentLabel = UILabel()
    let goLabel = UILabel()
    let imageView = UIImageView()
    open var energy:Int = 0{
        didSet{
            numberLabel.text = String.init(format: "%dg", energy)
            self.isHidden = energy == 0
        }
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView()  {
        self.backgroundColor = .white
        addLables()
        addImageView()
    }
    
    func addLables()  {
        self.addSubview(titleLabel)
        titleLabel.text = "本次行程节能减排"
        titleLabel.font = .systemFont(ofSize: 18, weight: .semibold)
        titleLabel.textColor = .black
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(10)
            make.top.equalTo(20)
        }
        self.addSubview(numberLabel)
        numberLabel.text = "10g"
        numberLabel.font = .systemFont(ofSize: 18, weight: .semibold)
        numberLabel.textColor = UIColor(red: 82, green: 196, blue: 26, alpha: 1)
        numberLabel.snp.makeConstraints { (make) in
            make.left.equalTo(titleLabel.snp.right).offset(2)
            make.centerY.equalTo(titleLabel)
        }
        self.addSubview(contentLabel)
        contentLabel.text = "为你的好好开车行为点赞"
        contentLabel.font = .systemFont(ofSize: 14, weight: .regular)
        contentLabel.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.45)
        contentLabel.snp.makeConstraints { (make) in
            make.left.equalTo(10)
            make.bottom.equalTo(-20)
        }
        let view = UIView()
        self.addSubview(view)
        view.backgroundColor = UIColor(red: 82, green: 196, blue: 26, alpha: 1)
        view.layer.masksToBounds = true
        view.layer.cornerRadius = 10
        view.snp.makeConstraints { (make) in
            make.left.equalTo(contentLabel.snp.right).offset(2)
            make.centerY.equalTo(contentLabel)
            make.height.equalTo(20)
            make.width.equalTo(58)
        }
        view.addSubview(goLabel)
        goLabel.text = "去收集"
        goLabel.font = .systemFont(ofSize: 12, weight: .regular)
        goLabel.textColor = .white
        goLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(6)
            make.centerY.equalToSuperview()
        }
        let iconImageView  = UIImageView()
        iconImageView.image = UIImage(fromUbiBundleWithName: "ubm_detail_green_icon")
        view.addSubview(iconImageView)
        iconImageView.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.width.height.equalTo(12)
            make.right.equalToSuperview().offset(-2);
        }

    }
    
    func addImageView()  {
        self.addSubview(imageView)
        imageView.image = UIImage(fromUbiBundleWithName: "ubm_detail_geen")
        imageView.snp.makeConstraints { (make) in
            make.right.top.equalToSuperview()
            make.width.equalTo(160)
            make.height.equalTo(86)
        }
    }

}
