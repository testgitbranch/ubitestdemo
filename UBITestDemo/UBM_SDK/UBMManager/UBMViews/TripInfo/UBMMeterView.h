//
//  UBMMeterView.h
//  LoveCarChannel_iOS
//
//  Created by 金鑫 on 2021/3/5.
//  Copyright © 2021 魏振伟. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UBMMeterView : UIView

@property(nonatomic,strong) CAGradientLayer *gradientLayer;
@property (nonatomic, strong) CAShapeLayer *progressLayer;
@property (nonatomic, strong) UIBezierPath *path;

/// 画仪表盘进度
/// @param lineWidth 边框宽度
/// @param minAngle 开始角
/// @param maxAngle 结束角
/// @param fillColor 填充色
/// @param strokeColor 边框色
/// @param progressValue 进度值
/// @param fullValue 总值
- (void)drawProgressWithLineWidth:(CGFloat)lineWidth minAngle:(CGFloat)minAngle maxAngle:(CGFloat)maxAngle fillColor:(UIColor*)fillColor strokeColor:(UIColor*)strokeColor progressValue:(NSInteger)progressValue fullValue:(CGFloat)fullValue;

/// 画仪表盘进度(用于一次性绘画进度值 + 动画)
/// @param lineWidth 边框宽度
/// @param minAngle 开始角
/// @param maxAngle 结束角
/// @param fillColor 填充色
/// @param strokeColor 边框色
/// @param progressValue 进度值
/// @param fullValue 总值
- (void)drawAnimationProgressWithLineWidth:(CGFloat)lineWidth minAngle:(CGFloat)minAngle maxAngle:(CGFloat)maxAngle fillColor:(UIColor*)fillColor strokeColor:(UIColor*)strokeColor progressValue:(CGFloat)progressValue fullValue:(CGFloat)fullValue fullAnimationTime:(CFTimeInterval)time;

/// 画仪表盘进度(用于多次绘画进度值)
/// @param lineWidth 边框宽度
/// @param minAngle 开始角
/// @param maxAngle 结束角
/// @param fillColor 填充色
/// @param strokeColor 边框色
- (void)drawProgressWithLineWidth:(CGFloat)lineWidth minAngle:(CGFloat)minAngle maxAngle:(CGFloat)maxAngle fillColor:(UIColor*)fillColor strokeColor:(UIColor*)strokeColor;

/// 为进度添加渐变图层
/// @param colorGradArray 渐变色数组
- (void)setColorGrad:(NSArray*)colorGradArray;

///将进制重置为0
- (void)setZoreProgress;

///进度初始化成0
- (void)resetProgress;

@end

NS_ASSUME_NONNULL_END
