//
//  UBMEventCell.h
//  LoveCarChannel_iOS
//
//  Created by 金鑫 on 2021/3/2.
//  Copyright © 2021 魏振伟. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class UBMEventModel;
@interface UBMEventCell : UICollectionViewCell
@property (nonatomic, strong) UBMEventModel *model;
@end

NS_ASSUME_NONNULL_END
