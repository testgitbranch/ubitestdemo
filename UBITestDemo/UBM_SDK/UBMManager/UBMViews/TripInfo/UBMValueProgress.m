//
//  UBMValueProgress.m
//  LoveCarChannel_iOS
//
//  Created by 金鑫 on 2021/3/11.
//  Copyright © 2021 魏振伟. All rights reserved.
//

#import "UBMValueProgress.h"

@interface UBMValueProgress ()
@property (nonatomic, assign) CGFloat targetValue;
@property (nonatomic, assign) CGFloat totalValue;
@property (nonatomic, assign) CGFloat progressValue;
@property (nonatomic, assign) CGFloat progressPercent;
@property (nonatomic, assign) NSTimeInterval fullDuration;
@property (nonatomic, assign) NSTimeInterval activeDuration;
@property (nonatomic, assign) NSTimeInterval progressDuration;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, assign) NSTimeInterval interval;

@end

@implementation UBMValueProgress
- (void)progressRun {
    if (self.progressDuration >= self.activeDuration) {
        [self.timer invalidate];
        self.timer = nil;
        self.progressDuration = 0;
        return;
    }
    
    if (self.activeDuration - self.progressDuration > self.interval) {
        self.progressDuration += self.interval;
        self.progressPercent = self.progressDuration * (self.targetValue * (1.0 / self.totalValue)) / self.activeDuration;
        self.progressValue = self.targetValue * self.progressPercent;
        
    } else {
        self.progressDuration = self.activeDuration;
        self.progressPercent = self.targetValue * (1.0 / self.totalValue);
        self.progressValue = self.targetValue;
    }

    if ([self.delegate respondsToSelector:@selector(getProgressPercent:)]) {
        [self.delegate getProgressPercent:self.progressPercent];
    }

    if ([self.delegate respondsToSelector:@selector(getProgressValue:)]) {
        [self.delegate getProgressValue:self.progressValue];
    }
    
    if (self.progressDuration == self.activeDuration) {
        if ([self.delegate respondsToSelector:@selector(progressEnd:)]) {
            [self.delegate progressEnd:self.progressPercent];
        }
    }
}

- (void)progressRunWithTargetValue:(CGFloat)targetValue totalValue:(CGFloat)totalValue fullDuration:(NSTimeInterval)duration delegate:(id<UBMValueProgressDelegate>)target {
    self.delegate = target;
    
    if ([self.delegate respondsToSelector:@selector(progressBegin)]) {
        [self.delegate progressBegin];
    }
    
    self.fullDuration = duration;
    self.interval = duration / totalValue;
    self.activeDuration = targetValue * self.interval;
    self.targetValue = targetValue;
    self.totalValue = totalValue;

    [self.timer invalidate];
    self.timer = nil;
    self.timer = [NSTimer timerWithTimeInterval:self.interval target:self selector:@selector(progressRun) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
}

@end
