//
//  UBMAbortView.h
//  LoveCarChannel_iOS
//
//  Created by 王国松 on 2021/6/29.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UBMAbortView : UIView

@property (nonatomic, strong) UIView *abortContainerView;
@property (nonatomic, assign) BOOL isAbnormalTrip;
@property (nonatomic, strong) UILabel *abortTitle;
@property (nonatomic, strong) UILabel *abortSubTitle;
@property (nonatomic, strong) UIButton *abortButton;
@property (nonatomic, strong) UIView *abortButtonBgView;

//数据
@property(nonatomic,strong)NSDictionary * routeInforData;


@end

NS_ASSUME_NONNULL_END
