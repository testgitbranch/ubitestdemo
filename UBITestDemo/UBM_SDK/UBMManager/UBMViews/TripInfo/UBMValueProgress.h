//
//  UBMValueProgress.h
//  LoveCarChannel_iOS
//
//  Created by 金鑫 on 2021/3/11.
//  Copyright © 2021 魏振伟. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol UBMValueProgressDelegate <NSObject>

@optional
- (void)getProgressPercent:(CGFloat)percent;

- (void)getProgressValue:(CGFloat)value;

- (void)progressEnd:(CGFloat)percent;

- (void)progressBegin;

@end

@interface UBMValueProgress : NSObject

@property (nonatomic, weak) id<UBMValueProgressDelegate> delegate;

/// 进度工具
/// @param value 目标值
/// @param totalValue 总值
/// @param duration 总时间
/// @param target 代理
- (void)progressRunWithTargetValue:(CGFloat)targetValue totalValue:(CGFloat)totalValue fullDuration:(NSTimeInterval)duration delegate:(id<UBMValueProgressDelegate>)target;

@end

NS_ASSUME_NONNULL_END
