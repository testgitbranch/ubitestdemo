//
//  UBMEventModel.h
//  LoveCarChannel_iOS
//
//  Created by 金鑫 on 2021/3/2.
//  Copyright © 2021 魏振伟. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UBMEventModel : NSObject
@property (nonatomic, assign) NSInteger eventCount;
@property (nonatomic, strong) NSString *eventUnit;
@property (nonatomic, strong) NSString *eventType;

+ (NSArray *)modelsWithDataArray:(NSArray *)dataArray;

@end

NS_ASSUME_NONNULL_END
