//
//  UBMRouteInforBottomView.m
//  LoveCarChannel_iOS
//
//  Created by 王国松 on 2021/6/28.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

#import "UBMRouteInforBottomView.h"
#import "UBMPrefixHeader.h"

@implementation UBMRouteInforBottomView

- (id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.bottomLineLabel];
        [self setupLayout];
    }
    return self;
}

- (void)setupLayout{
    [self.bottomLineLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self);
    }];
}

- (UILabel *)bottomLineLabel {
    if (!_bottomLineLabel) {
        _bottomLineLabel = [[UILabel alloc] init];
        _bottomLineLabel.text = @"—— 安全驾驶 恪守底线 ——";
        _bottomLineLabel.backgroundColor = UIColor.clearColor;
        _bottomLineLabel.textColor = [UIColor ubmColorWithHexNum:0x000000 alpha:0.45];
        _bottomLineLabel.textAlignment = NSTextAlignmentCenter;
        _bottomLineLabel.font = [UIFont systemFontOfSize:13 weight:UIFontWeightRegular];
    }
    return _bottomLineLabel;
}


@end
