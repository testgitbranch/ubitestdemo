//
//  UBMRouteInforView.m
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/10/15.
//  Copyright © 2020 魏振伟. All rights reserved.
//

#import "UBMRouteInforView.h"
#import "UBMHuanView.h"
#import "UBMEventExplainView.h"
#import "UBMGBRadarChart.h"
#import "UBMGBRadarChartDataItem.h"
#import "UBMEventDisplayView.h"
#import "UBMEventCell.h"
#import "UBMEventModel.h"
#import "UBMMeterView.h"
#import "UBMValueProgress.h"
#import "UBMExpandButton.h"
#import "UBMGradientView.h"
#import "UBMScoreView.h"
#import "UBMTripEventView.h"
#import "UBMTripInforView.h"
#import "UBMSpeedContainerView.h"
#import "UBMFeedBackView.h"
#import "UBMRouteInforBottomView.h"
#import "UBMPrefixHeader.h"
#import "UBMAbortView.h"
#import <MJExtension/MJExtension.h>
#import "UBMMathTool.h"
//#import <UBM_SDK/UBM_SDK-Swift.h>

@interface UBMRouteInforView()< UBMValueProgressDelegate,UIScrollViewDelegate>

@property(nonatomic, strong)UIView* mainView;
//渐变view
@property (nonatomic, strong) UBMGradientView *gradientBackView;
// 评分信息
@property (nonatomic, strong) UBMValueProgress *scoreProgress;

@property (nonatomic, strong) UBMScoreView *scoreView;
// 异常原因
@property (nonatomic, strong) UBMAbortView *abortView;
// 行程事件
@property (nonatomic, strong) UBMTripEventView *tripEvenView;
//自动记录按钮
@property(nonatomic,strong) UIButton * autorecordButton;
// 行程信息
@property (nonatomic, strong) UBMTripInforView *tripSubInforView;
//碳中和
@property(nonatomic,strong)UBMGreenEnergyView * greenEnergyView;
// 行程速度
@property (nonatomic, strong) UBMSpeedContainerView *speedContainerView;
//非驾驶行程标记
@property(nonatomic,strong)UBMMarkView * markView;
// 问题反馈
@property (nonatomic, strong) UBMFeedBackView *feedbackContainerView;
// 底线
@property (nonatomic, strong) UBMRouteInforBottomView *bottomLineContainerView;

@end

@implementation UBMRouteInforView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.showsVerticalScrollIndicator = NO;
        self.backgroundColor = UIColor.blackColor;
        UBMGdMapView* gv = [[UBMGdMapView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, 591.0)];
        [self addSubview:gv];
        self.gv = gv;
        [self setup];
    }
    return self;
}

- (void)setup {
    [self setupUI];
    [self setupLayout];
    [self setupAppearce];
}

- (void)buriedPoints{
    if ([UBMViewTools isDisplayedInSceenWithView:self.autorecordButton]) {//自动记录入口显示埋点
        [self buriedPointShowAutoRecord];
    }
}
#pragma mark - 创建UI
- (void)setupUI {
    //行程MainView
    [self addSubview:self.mainView];
    self.delegate = self;
    //渐变背景
    [self.mainView addSubview:self.gradientBackView];
    // 行程评分
    [self.mainView addSubview:self.scoreView];
    // 异常原因
    [self.mainView addSubview:self.abortView];
    // 行程事件
    [self.mainView addSubview:self.tripEvenView];
    //自动记录按钮
    [self.mainView addSubview:self.autorecordButton];
    // 行程信息
    [self.mainView addSubview:self.tripSubInforView];
    //碳中和
    [self.mainView addSubview:self.greenEnergyView];
    // 行程速度
    [self.mainView addSubview:self.speedContainerView];
    //非驾驶行程标记
    [self.mainView addSubview:self.markView];
    
    __weak typeof(self) weakSelf = self;
    self.markView.markBlock = ^{
        [weakSelf markOnclick];
    };
    // 问题反馈
    [self.mainView addSubview:self.feedbackContainerView];
    // 底线
    [self.mainView addSubview:self.bottomLineContainerView];
}

#pragma mark - 布局UI
- (void)setupLayout {
    // 行程主视图
    [self.mainView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mainView.superview).offset(591 - 30);
        make.left.right.equalTo(self.gv);
    }];
    // 行程评分
    [self.scoreView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.scoreView.superview);
    }];
    //渐变背景颜色view
    [self.gradientBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.scoreView.mas_bottom).offset(-17);
        make.width.equalTo(self.gradientBackView.superview);
        make.height.mas_equalTo(150);
    }];
    //异常行程
    [self.abortView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.scoreView.mas_bottom);
        make.left.right.equalTo(self.abortView.superview);
    }];
    //行程事件
    [self.tripEvenView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.scoreView.mas_bottom);
        make.left.right.equalTo(self.tripEvenView.superview);
    }];
    //自动记录
    [self.autorecordButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.tripEvenView.mas_bottom).offset(10);
        make.left.mas_equalTo(12);
        make.width.mas_equalTo(ScreenWidth - 24);
        make.height.mas_equalTo((ScreenWidth - 24)*89/355);
    }];
    // 行程信息
    [self.tripSubInforView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.autorecordButton.mas_bottom).offset(10);
        make.left.right.equalTo(self.tripSubInforView.superview);
    }];
    //碳中和
    [self.greenEnergyView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.tripSubInforView.mas_bottom).offset(10);
        make.left.right.equalTo(self.speedContainerView);
        make.height.mas_equalTo(86);
    }];
    // 行程速度
    [self.speedContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.greenEnergyView.mas_bottom).offset(10);
        make.left.equalTo(self.speedContainerView.superview).offset(12);
        make.right.equalTo(self.speedContainerView.superview).offset(-12);
        make.height.mas_equalTo(279);
    }];
    //非驾驶行为标记
    [self.markView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.speedContainerView.mas_bottom).offset(10);
        make.left.right.equalTo(self.speedContainerView);
        make.height.mas_equalTo(98);
    }];
    // 问题反馈
    [self.feedbackContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.markView.mas_bottom).offset(10);
        make.left.right.equalTo(self.speedContainerView);
        make.height.mas_equalTo(58);
    }];
    // 底线
    [self.bottomLineContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.feedbackContainerView.mas_bottom).offset(10);
        make.left.right.bottom.equalTo(self.bottomLineContainerView.superview);
        make.height.mas_equalTo(78);
    }];
}

- (void)setupAppearce {
    self.mainView.backgroundColor = [UIColor ubmColorWithHexNum:0xF2F2F2];
    self.scoreView.backgroundColor = [UIColor ubmColorWithHexNum:0xFFFFFF];
    self.speedContainerView.backgroundColor = [UIColor ubmColorWithHexNum:0xFFFFFF];
    self.feedbackContainerView.backgroundColor = [UIColor ubmColorWithHexNum:0xFFFFFF];
    self.bottomLineContainerView.backgroundColor = [UIColor ubmColorWithHexNum:0xF2F2F2];
    self.mainView.layer.cornerRadius = 16;
    self.scoreView.layer.cornerRadius = 16;
    self.greenEnergyView.layer.cornerRadius = 8;
    self.autorecordButton.layer.cornerRadius = 8;
    self.speedContainerView.layer.cornerRadius = 8;
    self.markView.layer.cornerRadius = 8;
    self.feedbackContainerView.layer.cornerRadius = 8;
}

- (void)setRouteInforData:(NSDictionary *)routeInforData{
    _routeInforData = routeInforData;
    //行程评分赋值
    self.scoreView.routeInforData = routeInforData;
    //是否为异常 1为正常
    self.abortView.hidden = [routeInforData[@"is_normal"] integerValue] == 1;
    // 异常原因
    if (!self.abortView.hidden) {
        self.abortView.routeInforData = routeInforData;
    }
    // 行程事件
    self.tripEvenView.routeInforData = routeInforData;
    //自动记录
    BOOL auto_record_should_show = [routeInforData[@"auto_record_should_show"] boolValue];
    self.autorecordButton.hidden = !auto_record_should_show;
    if (auto_record_should_show) {
        NSString * img = routeInforData[@"img"];
        [self.autorecordButton sd_setBackgroundImageWithURL:[NSURL URLWithString:img] forState:UIControlStateNormal];
    }
    // 行程信息
    self.tripSubInforView.routeInforData = routeInforData;
    //碳中和
    int energy = [routeInforData[@"energy"] intValue];
    self.greenEnergyView.energy = energy;
    // 行程速度
    self.speedContainerView.maxSpeedValueLabel.text = [NSString stringWithFormat:@"%@km/h", ChangeToString(routeInforData[@"max_speed_in_kilometers_per_hour"])];
    if (!self.abortView.hidden) {//异常行程
        self.scoreView.scoreSafety.text = @"- -";
        self.scoreView.scoreSafety.textColor = [UIColor ubmColorWithHexNum:0x000000 alpha:0.85];
    } else {
        [self drawScoreMeterProgressWithScore:[routeInforData[@"score"] floatValue] duration:0.8];
    }
    //非驾驶行程 部分
    self.markView.not_driver_url = [NSString stringWithFormat:@"%@",routeInforData[@"not_driver_url"]];
    BOOL isNeedMark = [routeInforData[@"mark_button"] integerValue] == 1;
    self.markView.hidden = !isNeedMark;
   
    BOOL isNeedShowMarkImageState = [routeInforData[@"not_driver_mark"] integerValue] == 1;
    if (isNeedShowMarkImageState) {//非驾驶行程
        self.scoreStampImageView.image = [UIImage imageFromUbiBundleWithName:@"ubm_inforType6"];
        [self drawScoreMeterProgressWithScore:0 duration:0];
        self.scoreView.scoreSafety.text = @"- -";
        self.scoreView.scoreSafety.textColor = [UIColor ubmColorWithHexNum:0x000000 alpha:0.85];
    }
    [self refreshConstraintsAndViews];
    // 埋点
//    if (!self.abortView.hidden) {
//        [UBMUMLog addLogWithButtonid:@"yichang" sorcePage:@"ubm_detail" action:UMActionShow];
//    }
    [self buriedPoints];
}

//MARK:重新布局
- (void)refreshConstraintsAndViews {
    if (self.abortView.hidden == NO) {//异常行程
        [self.tripSubInforView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.abortView.mas_bottom).offset(10);
        }];
    }else if(self.tripEvenView.hidden == NO){//有行程事件
        [self.tripEvenView setupLayout];
    }else{//没有行程事件
        [self.autorecordButton mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.scoreView.mas_bottom);
        }];
    }
    if (self.autorecordButton.hidden == YES) {//没有自动记录按钮
        [self.tripSubInforView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.autorecordButton);
        }];
    }
    if (self.greenEnergyView.hidden == YES) {//没有碳中和
        [self.speedContainerView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.tripSubInforView.mas_bottom).offset(10);
        }];
    }
    if (self.markView.hidden == YES) {//没有打标模块
        [self.feedbackContainerView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.speedContainerView.mas_bottom).offset(10);
        }];
    }
    [self layoutIfNeeded];
    // 异常行程 UI
    if (!self.abortView.hidden) {
        [UBMViewTools addGradualChangeBckColor:[UIColor ubmColorWithHexNum:0xFA9C3E] :[UIColor ubmColorWithHexNum:0xFF5D32] :2 to:self.abortView.abortButtonBgView];
        [UBMViewTools markerLable:self.abortView.abortTitle changeString:@"异常行程" andAllColor:[UIColor ubmColorWithHexNum:0x000000] andMarkColor:[UIColor ubmColorWithHexNum:0xFF4B41] andMarkFondSize:[UIFont systemFontOfSize:16 weight:UIFontWeightSemibold]];
    }
    self.contentSize = CGSizeMake(0, self.mainView.bottom);
}

//MARK: 进度动画效果
- (void)drawScoreMeterProgressWithScore:(CGFloat)score duration:(NSTimeInterval)duration {
    [self.scoreView.scoreMeterView drawProgressWithLineWidth:20 minAngle:[UBMMathTool radianFromDegree:155] maxAngle:[UBMMathTool radianFromDegree:25] fillColor:UIColor.clearColor strokeColor:UIColor.whiteColor];
    [self.scoreView.scoreMeterView setColorGrad:[NSArray arrayWithObjects:(id)[UIColor ubmColorWithHexNum:0x37C6FF].CGColor, (id)[UIColor ubmColorWithHexNum:0x2B58FF].CGColor, nil]];
    // 分数进度
    [self.scoreProgress progressRunWithTargetValue:score totalValue:100 fullDuration:duration delegate:self];
}

#pragma mark - BXScoreProgressDelegate
- (void)getProgressPercent:(CGFloat)percent {
    self.scoreView.scoreMeterView.progressLayer.strokeEnd = percent;
}

- (void)getProgressValue:(CGFloat)score {
    self.scoreView.scoreSafety.text = [NSString stringWithFormat:@"%.f", score];
}

- (void)setHiddenSpeedChartView:(BOOL)hiddenSpeedChartView {
    if (hiddenSpeedChartView == YES) {
        self.speedContainerView.speedChartView.hidden = YES;
        [self layoutIfNeeded];
        self.contentSize = CGSizeMake(self.frame.size.width, self.mainView.bottom);
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if ([UBMViewTools isDisplayedInSceenWithView:self.markView]) {//行程打标显示埋点
        [self buriedPointShowMarkView];
    }
    if ([UBMViewTools isDisplayedInSceenWithView:self.autorecordButton]) {//自动记录入口显示埋点
        [self buriedPointShowAutoRecord];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if (!decelerate) {
        if ([UBMViewTools isDisplayedInSceenWithView:self.markView]) {//行程打标显示埋点
            [self buriedPointShowMarkView];
        }
        if ([UBMViewTools isDisplayedInSceenWithView:self.autorecordButton]) {//自动记录入口显示埋点
            [self buriedPointShowAutoRecord];
        }
    }
}

//MARK:打开自动记录
- (void)openAutoRecord{
    if (_routeInforViewDelegate) {
        [_routeInforViewDelegate goAutoRecordSettings];
        [self buriedPointOnclickAutoRecord];
    }
}

//MARK:自动记录入口显示埋点
- (void)buriedPointShowAutoRecord{
    NSString * buttonId = @"ubm_auto_turn_on_card";
    NSString * sorcePage = @"ubm_trip_detail";
    NSMutableDictionary * customDic = @{}.mutableCopy;
    customDic[@"trip_id"] = self.tripId;
//    [BXUMLog addLogWithButtonid:buttonId
//                      sorcePage:sorcePage
//                         action:UMActionExposure
//                   andCustomDic:customDic];
}

//MARK:自动记录入口点击埋点
- (void)buriedPointOnclickAutoRecord{
    NSString * buttonId = @"ubm_auto_turn_on_card";
    NSString * sorcePage = @"ubm_trip_detail";
    NSMutableDictionary * customDic = @{}.mutableCopy;
    customDic[@"trip_id"] = self.tripId;
//    [BXUMLog addLogWithButtonid:buttonId
//                      sorcePage:sorcePage
//                         action:UMActionClick
//                   andCustomDic:customDic];
}


//MARK:去收集
- (void)greenEnerayOnlcick{
    if (self.routeInforViewDelegate) {
        [self.routeInforViewDelegate goCollect];
    }
}

//MARK:打标
- (void)markOnclick{
    if (_routeInforViewDelegate) {
        [_routeInforViewDelegate requstMark];
    }
    [self buriedPointOnclickMarkView];
}

//MARK:行程打标显示埋点
- (void)buriedPointShowMarkView{
    NSString * buttonId = @"historicaldetails_not_auto_record_card";
    NSString * sorcePage = @"ubm_detail";
    NSMutableDictionary * customDic = @{}.mutableCopy;
    customDic[@"trip_id"] = self.tripId;
//    [BXUMLog addLogWithButtonid:buttonId
//                      sorcePage:sorcePage
//                         action:UMActionExposure
//                   andCustomDic:customDic];
}

//MARK:行程打标点击埋点
- (void)buriedPointOnclickMarkView{
    NSString * buttonId = @"historicaldetails_stamp";
    NSString * sorcePage = @"ubm_detail";
    NSMutableDictionary * customDic = @{}.mutableCopy;
    customDic[@"trip_id"] = self.tripId;
//    [BXUMLog addLogWithButtonid:buttonId
//                      sorcePage:sorcePage
//                         action:UMActionClick
//                   andCustomDic:customDic];
}

#pragma mark - 安全隐患弹窗
- (void)YHAlert {
    UBMEventExplainView* ev = [[UBMEventExplainView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    [ev show];
}

#pragma mark - 反馈
- (void)gotoFanKui {
    if ([self.routeInforViewDelegate respondsToSelector:@selector(fankui)]) {
        [self.routeInforViewDelegate fankui];
    }
}

#pragma mark - 权限设置
- (void)gotoAuthSetting {
    if ([self.routeInforViewDelegate respondsToSelector:@selector(goAutoRecordSettings)]) {
        // 埋点
//        [BXUMLog addLogWithButtonid:@"yichang" sorcePage:@"ubm_detail" action:UMActionClick];
        [self.routeInforViewDelegate goAutoRecordSettings];
    }
}

#pragma mark - 行程主视图
- (UIView *)mainView {
    if (!_mainView) {
        _mainView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 1800)];
        _mainView.backgroundColor = [UIColor ubmColorWithHexNum:0xFFFFFF];
    }
    return _mainView;
}

//MARK:渐变view
- (UBMGradientView *)gradientBackView{
    if (!_gradientBackView) {
        _gradientBackView = [[UBMGradientView alloc] init];
    }
    return _gradientBackView;
}

#pragma mark - 行程评分
- (UBMValueProgress *)scoreProgress {
    if (!_scoreProgress) {
        _scoreProgress = [[UBMValueProgress alloc] init];
    }
    return _scoreProgress;
}

- (UBMScoreView *)scoreView {
    if (!_scoreView) {
        _scoreView = [[UBMScoreView alloc] init];
    }
    return _scoreView;
}

#pragma mark - 异常原因
- (UIView *)abortView {
    if (!_abortView) {
        _abortView = [[UBMAbortView alloc] init];
        [_abortView.abortButton addTarget:self action:@selector(gotoAuthSetting) forControlEvents:UIControlEventTouchUpInside];
    }
    return _abortView;
}

#pragma mark - 行程事件
- (UIView *)tripEvenView {
    if (!_tripEvenView) {
        _tripEvenView = [[UBMTripEventView alloc] init];
        [_tripEvenView.questionButton addTarget:self action:@selector(YHAlert) forControlEvents:UIControlEventTouchUpInside];
    }
    return _tripEvenView;
}

#pragma mark - 自动记录按钮
- (UIButton *)autorecordButton{
    if (!_autorecordButton) {
        _autorecordButton = [[UIButton alloc] init];
        [_autorecordButton addTarget:self action:@selector(openAutoRecord) forControlEvents:UIControlEventTouchUpInside];
    }
    return _autorecordButton;
}

#pragma mark - 行程信息
- (UBMTripInforView *)tripSubInforView {
    if (!_tripSubInforView) {
        _tripSubInforView = [[UBMTripInforView alloc] init];
    }
    return _tripSubInforView;
}

//MARK:碳中和
- (UBMGreenEnergyView *)greenEnergyView{
    if (!_greenEnergyView) {
        _greenEnergyView = [[UBMGreenEnergyView alloc] initWithFrame:CGRectZero];
        _greenEnergyView.userInteractionEnabled = YES;
        UITapGestureRecognizer * tapGestureRecoginzer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(greenEnerayOnlcick)];
        [_greenEnergyView addGestureRecognizer:tapGestureRecoginzer];
    }
    return _greenEnergyView;
}

#pragma mark - 行程速度
- (UIView *)speedContainerView {
    if (!_speedContainerView) {
        _speedContainerView = [[UBMSpeedContainerView alloc] init];
    }
    return _speedContainerView;
}

//MARK:行程速度图标
- (UBMSpeedChartView *)speedChartView{
    return self.speedContainerView.speedChartView;
}

//MARK:行程分图片
- (UIImageView *)scoreStampImageView{
    return self.scoreView.scoreStampImageView;
}

//MARK:非驾驶行程标记
- (UBMMarkView *)markView{
    if (!_markView) {
        _markView = [[UBMMarkView alloc] init];
        _markView.backgroundColor = [UIColor whiteColor];
    }
    return _markView;
}

#pragma mark - 问题反馈
- (UIView *)feedbackContainerView {
    if (!_feedbackContainerView) {
        _feedbackContainerView = [[UBMFeedBackView alloc] init];
        _feedbackContainerView.backgroundColor = [UIColor ubmColorWithHexNum:0x000000];
        [_feedbackContainerView.feedbackButton addTarget:self action:@selector(gotoFanKui) forControlEvents:UIControlEventTouchUpInside];
    }
    return _feedbackContainerView;
}

#pragma mark - 底线
- (UIView *)bottomLineContainerView {
    if (!_bottomLineContainerView) {
        _bottomLineContainerView = [[UBMRouteInforBottomView alloc] init];
    }
    return _bottomLineContainerView;
}


@end
