//
//  UBMLeftImgLab.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/10/17.
//  Copyright © 2020 魏振伟. All rights reserved.
//

/*
 左图 右文 lab, 自适应长度
 */

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UBMLeftImgLab : UIView

@property(nonatomic, strong)UIImageView* leftImgView;
@property(nonatomic, strong)UILabel* contentLab;

-(void)setImg:(UIImage*)leftImg andContentText:(NSString*)contentText;

-(void)setContentText:(NSString*)contentText;

@end

NS_ASSUME_NONNULL_END
