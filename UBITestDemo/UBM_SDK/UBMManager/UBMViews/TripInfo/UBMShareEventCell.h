//
//  UBMShareEventCell.h
//  LoveCarChannel_iOS
//
//  Created by 金鑫 on 2021/3/8.
//  Copyright © 2021 魏振伟. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, ShareEventCellType) {
    ShareEventCellTypeSingle,
    ShareEventCellTypeTotal
};

@interface UBMShareEventCell : UIView
@property (nonatomic, strong) UILabel *eventNum;
@property (nonatomic, strong) UILabel *eventName;

/// 默认Single的初始化
- (instancetype)init;

- (instancetype)initWithType:(ShareEventCellType)type;

@end

NS_ASSUME_NONNULL_END
