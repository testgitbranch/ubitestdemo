//
//  UBMEventExplainView.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/10/26.
//  Copyright © 2020 魏振伟. All rights reserved.
//

/*
 安全隐患说明弹窗
 */
#import "UBMWBaseAlertView.h"

NS_ASSUME_NONNULL_BEGIN

@interface UBMEventExplainView : UBMWBaseAlertView

- (id)initWithFrame:(CGRect)frame title:(NSString *)title urlString:(NSString *)urlString;

@end

NS_ASSUME_NONNULL_END
