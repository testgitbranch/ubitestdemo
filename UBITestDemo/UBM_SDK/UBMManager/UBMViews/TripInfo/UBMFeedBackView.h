//
//  UBMFeedBackView.h
//  LoveCarChannel_iOS
//
//  Created by 王国松 on 2021/6/28.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UBMExpandButton.h"

NS_ASSUME_NONNULL_BEGIN

@interface UBMFeedBackView : UIView

@property (nonatomic, strong) UILabel *feedbackTipLabel;
@property (nonatomic, strong) UBMExpandButton *feedbackButton;

@end

NS_ASSUME_NONNULL_END
