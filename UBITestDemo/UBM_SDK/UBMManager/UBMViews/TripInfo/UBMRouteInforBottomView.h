//
//  UBMRouteInforBottomView.h
//  LoveCarChannel_iOS
//
//  Created by 王国松 on 2021/6/28.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UBMRouteInforBottomView : UIView

@property (nonatomic, strong) UILabel *bottomLineLabel;

@end

NS_ASSUME_NONNULL_END
