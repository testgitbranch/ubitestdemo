//
//  UBMGdMapView.m
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/10/15.
//  Copyright © 2020 魏振伟. All rights reserved.
//

#import "UBMGdMapView.h"
#import "UBMWCustomAnnotation.h"
#import "UBMWCustomMAPinAnnotationView.h"
#import "UBMWCustomMAMultiPolyline.h"
#import "UBMDouglasPeucker.h"
#import "UBMMTools.h"
#import "UBMPrefixHeader.h"

#define V_MAX 140.0
#define V_MIN 10.0


@interface UBMGdMapView()<MAMapViewDelegate>


@property (nonatomic, strong) NSMutableArray* strokeColors;
//@property (nonatomic, strong) NSMutableArray* pointArray;

@property (nonatomic, strong) MAMultiPolyline* grayLine;     //选中时覆盖的半透明灰色路线。
@property (nonatomic, strong) NSMutableArray* grayLineStrokeColors; //半透明颜色

@property (nonatomic, strong) MAMultiPolyline* selectedLine; //选中的事件段的路线。
@property (nonatomic, strong) NSMutableArray* selectedLineStrokeColors; //选中时的路线颜色


@end

@implementation UBMGdMapView


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        //初始化地图
        MAMapView *_mapView = [[MAMapView alloc] initWithFrame:self.bounds];
        _mapView.showsCompass = NO;
        _mapView.delegate = self;
        _mapView.rotateCameraEnabled = NO; //禁用倾斜手势
        _mapView.rotateEnabled = NO; //禁用旋转手势
        self.mapView = _mapView;
        
        //把地图添加至view
        [self addSubview:_mapView];
        
        //设置地图极夜蓝模式。
        NSString *path = [NSString stringWithFormat:@"%@/style.data", [NSBundle mainBundle].bundlePath];
        NSData *data = [NSData dataWithContentsOfFile:path];
        MAMapCustomStyleOptions *options = [[MAMapCustomStyleOptions alloc] init];
        options.styleData = data;
        [self.mapView setCustomMapStyleOptions:options];
        [self.mapView setCustomMapStyleEnabled:YES];
        
        //图例
        UIView* tuliView = [[UIView alloc] initWithFrame:CGRectMake(frame.size.width-100, frame.size.height-70, 100, 30)];
//        tuliView.backgroundColor = UIColor.grayColor;
        [self addSubview:tuliView];
        UILabel* manLab = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 18, tuliView.height)];
        manLab.text = @"慢";
        manLab.font = [UIFont systemFontOfSize:14];
        manLab.textColor = ColorSet(255, 255, 255, 0.85);
        [tuliView addSubview:manLab];
        UILabel* kuaiLab = [[UILabel alloc] initWithFrame:CGRectMake(tuliView.width-23, 0, 18, tuliView.height)];
        kuaiLab.text = @"快";
        kuaiLab.font = [UIFont systemFontOfSize:14];
        kuaiLab.textColor = ColorSet(255, 255, 255, 0.85);
        [tuliView addSubview:kuaiLab];
        // 渐变
        UIView* gv = [[UIView alloc] initWithFrame:CGRectMake(manLab.right+5, (tuliView.height-2)/2.0, tuliView.width-manLab.right-5-20-10, 2)];
        gv.layer.cornerRadius = 1;
        gv.layer.masksToBounds = YES;
        [tuliView addSubview:gv];
        CAGradientLayer *gl = [CAGradientLayer layer];
        gl.frame = CGRectMake(0,0,gv.width,2);
        gl.startPoint = CGPointMake(0, 0.5);
        gl.endPoint = CGPointMake(1, 0.5);
        gl.colors = @[(__bridge id)[UIColor colorWithRed:255/255.0 green:121/255.0 blue:0/255.0 alpha:1.0].CGColor, (__bridge id)[UIColor colorWithRed:137/255.0 green:222/255.0 blue:0/255.0 alpha:1.0].CGColor];
        gl.locations = @[@(0), @(1.0f)];
        [gv.layer addSublayer:gl];
         
    }
    return self;
}

-(void)mapView:(MAMapView *)mapView regionDidChangeAnimated:(BOOL)animated{
//    [MobClick event:@"trip_map_action" attributes:[[NSDictionary alloc] initWithObjectsAndKeys:UserModel.shareUserModel.uid, @"uid", nil]];
}

-(void)mapView:(MAMapView *)mapView didSelectAnnotationView:(MAAnnotationView *)view{
    if ([view.annotation isKindOfClass:[UBMWCustomAnnotation class]]) {
        UBMWCustomAnnotation* wca = (UBMWCustomAnnotation*)view.annotation;
        [self didSelectPointOverlay:wca];
    }
}
-(void)mapView:(MAMapView *)mapView didDeselectAnnotationView:(MAAnnotationView *)view{
    //处理轨迹路线
    if ([view.annotation isKindOfClass:[UBMWCustomAnnotation class]]) {
        UBMWCustomAnnotation* wca = (UBMWCustomAnnotation*)view.annotation;
        [self didDeselectPointOverlay:wca];
    }
}

-(MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id<MAAnnotation>)annotation{
    
    if ([annotation isKindOfClass:[UBMWCustomAnnotation class]]) {
        
        UBMWCustomAnnotation* wcAnnotation = (UBMWCustomAnnotation*)annotation;
        
        static NSString *pointReuseIndentifier = @"pointReuseIndentifier";
        UBMWCustomMAPinAnnotationView *annotationView = [[UBMWCustomMAPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:pointReuseIndentifier];
        
        if (wcAnnotation.pointType == ubmStartPoint) {
            annotationView.image = [UIImage imageFromUbiBundleWithName:@"ubm_startPointImg"];
            //偏移高度一半，让底部与点重合
            annotationView.centerOffset = CGPointMake(0, -annotationView.image.size.height/2.0+2);
            annotationView.zIndex = 1;
            
        }else if (wcAnnotation.pointType == ubmEndPoint){
            annotationView.image = [UIImage imageFromUbiBundleWithName:@"ubm_endPointImg"];
            //偏移高度一半，让底部与点重合
            annotationView.centerOffset = CGPointMake(0, -annotationView.image.size.height/2.0+2);
            annotationView.zIndex = 2;
            
        }else{
            annotationView.centerOffset = CGPointMake(0, 0);
            annotationView.zIndex = 3;
            [[SDWebImageManager sharedManager] loadImageWithURL:[NSURL URLWithString:wcAnnotation.eventIconUrl] options:SDWebImageRetryFailed progress:nil completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, SDImageCacheType cacheType, BOOL finished, NSURL * _Nullable imageURL) {
                if (image) {
                    //修改image的大小为需要展示的大小，annotationView的frame是根绝image的size自动计算的。
                    UIImage* newImg = [UBMViewTools scaleToSize:image size:CGSizeMake(20, 20)];
                    annotationView.image = newImg;
                }
            }];
            
        }
        
//        annotationView.canShowCallout= YES;       //设置气泡可以弹出，默认为NO
//        annotationView.animatesDrop = YES;        //设置标注动画显示，默认为NO
//        annotationView.pinColor = MAPinAnnotationColorPurple;
        
        annotationView.canShowCallout = NO; //不显示自带气泡
        
        return annotationView;
        
    }
    
    return nil;
}

- (MAOverlayRenderer *)mapView:(MAMapView *)mapView rendererForOverlay:(id <MAOverlay>)overlay
{
    if ([overlay isKindOfClass:[UBMWCustomMAMultiPolyline class]])
    {
        MAMultiPolyline* mov = (MAMultiPolyline*)overlay;
        MAMultiColoredPolylineRenderer *polylineRenderer = [[MAMultiColoredPolylineRenderer alloc] initWithMultiPolyline:mov];
        
        polylineRenderer.gradient = YES;
        polylineRenderer.lineWidth    = 6.f;
        
        //设置点对应颜色。
        UBMWCustomMAMultiPolyline* wOverlay = (UBMWCustomMAMultiPolyline*)overlay;
        if (wOverlay.wPolylineType == WDefultPolyline) {
            polylineRenderer.strokeColors = self.strokeColors;
            
        }else if (wOverlay.wPolylineType == WGrayPolyline){
            polylineRenderer.strokeColors = self.grayLineStrokeColors;
            
        }else if (wOverlay.wPolylineType == WSelectedPolyline){
            polylineRenderer.strokeColors = self.selectedLineStrokeColors;
        }
        
        
        polylineRenderer.lineJoinType = kMALineJoinRound;
        polylineRenderer.lineCapType  = kMALineCapRound;
        
        return polylineRenderer;
    }
    return nil;
}

#pragma mark - set方法，划线。有可能因为多次的延迟消息输入导致多次划线和打点，所以，划线前先移除。
-(void)setLinePointArray:(NSArray *)linePointArray{
    
    //有旧的，先移除旧的线。
    if (self.mapView.overlays && self.mapView.overlays.count>0) {
        [self.mapView removeOverlays:self.mapView.overlays];
    }
    
    if (linePointArray == nil || linePointArray.count == 0) {
        return;
    }
    
    _linePointArray = linePointArray;
    
    //构造折线数据对象  数据量对高德绘制无影响，但是不知道会不会影响自定义绘制。
    NSMutableArray* StyleIndexesArr = [[NSMutableArray alloc] init]; //drawStyleIndexes。
    self.strokeColors = [[NSMutableArray alloc] init];               //Indexes对应的颜色

    @bx_weakify(self);
    //先抽稀，大于20000则抽稀
    [self routPointVacuate:_linePointArray routArrayBloack:^(NSArray<RoutePoint *> *responseArray)
    {
        _linePointArray = responseArray;
        
        //准备绘制条件
        //CLLocationCoordinate2D commonPolylineCoords[linePointArray.count];//此初始化方法量大会崩溃，禁用。
        __block CLLocationCoordinate2D *commonPolylineCoords = (CLLocationCoordinate2D *)malloc(sizeof(CLLocationCoordinate2D) * responseArray.count);
        if (commonPolylineCoords == NULL)
        {
            //long count = responseArray.count;
            [UBMSLSLog addLogOfW:@"routPointMallocError"];
            Toast(@"路线绘制出了点问题哦，请返回重试！");
            return;
        }
        NSInteger theRuleNum = [self getTheRuleNum:responseArray.count];
        for (NSInteger i=0; i<responseArray.count; i++) {
            
            
            RoutePoint* rp = responseArray[i];
            
            CLLocationCoordinate2D ad = CLLocationCoordinate2DMake(rp.latitude,rp.longitude);
            if (self.linePointCoordinates != 1) {
                //坐标系转换
                ad = AMapCoordinateConvert(ad,AMapCoordinateTypeGPS);
            }
            
            commonPolylineCoords[i].latitude = ad.latitude;
            commonPolylineCoords[i].longitude = ad.longitude;
            
            //根据速度，计算颜色。缝theRuleNum取1作为index
            if (i%theRuleNum == 0) {
                [weak_self.strokeColors addObject:[weak_self velocity:rp.speedInKilometersPerHour]];
                [StyleIndexesArr addObject:[NSNumber numberWithInteger:i]];
            }
        }
        
        //构造折线对象
        UBMWCustomMAMultiPolyline *commonPolyline = [UBMWCustomMAMultiPolyline polylineWithCoordinates:commonPolylineCoords count:responseArray.count drawStyleIndexes:StyleIndexesArr];
        commonPolyline.wPolylineType = WDefultPolyline;
        dispatch_async(dispatch_get_main_queue(), ^{
            //在地图上添加折线对象
            [_mapView addOverlay: commonPolyline];
            
            //调整地图rect来让路线全部显示
            [_mapView setVisibleMapRect:commonPolyline.boundingMapRect edgePadding:UIEdgeInsetsMake(80, 40, 80, 40) animated:NO];

            free(commonPolylineCoords);
            commonPolylineCoords = NULL;
        });
    }];
}

#pragma mark - 选中路线处理
-(void)didSelectPointOverlay:(UBMWCustomAnnotation*)wca{
    
    if (wca.pointType == ubmEventPoint && wca.is_continuous == YES) {
        
        //灰色半透明覆盖线的处理
        if (self.grayLine) {
            [self.mapView addOverlay:self.grayLine];
        }else{
            
            NSMutableArray* StyleIndexesArr = [[NSMutableArray alloc] init]; //drawStyleIndexes。每个点都是。
            NSMutableArray* colorArr = [[NSMutableArray alloc] init];
            
            //准备绘制条件
            CLLocationCoordinate2D* commonPolylineCoords = (CLLocationCoordinate2D*)malloc(sizeof(CLLocationCoordinate2D)*self.linePointArray.count);
            if (commonPolylineCoords == NULL) {
                return;
            }
            
            for (int i=0; i<self.linePointArray.count; i++) {
                RoutePoint* rp = self.linePointArray[i];
                CLLocationCoordinate2D ad = CLLocationCoordinate2DMake(rp.latitude,rp.longitude);
                if (self.linePointCoordinates != 1) {
                    //坐标系转换
                    ad = AMapCoordinateConvert(ad,AMapCoordinateTypeGPS);
                }
                commonPolylineCoords[i].latitude = ad.latitude;
                commonPolylineCoords[i].longitude = ad.longitude;
            }
            [colorArr addObject:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.35]];
            [StyleIndexesArr addObject:[NSNumber numberWithUnsignedLong:self.linePointArray.count-1]];
            self.grayLineStrokeColors = colorArr;
            
            //构造折线对象
            UBMWCustomMAMultiPolyline *commonPolyline = [UBMWCustomMAMultiPolyline polylineWithCoordinates:commonPolylineCoords count:self.linePointArray.count drawStyleIndexes:StyleIndexesArr];
            commonPolyline.wPolylineType = WGrayPolyline;
            self.grayLine = commonPolyline;
            
            //在地图上添加折线对象
            [_mapView addOverlay: commonPolyline];
            
            free(commonPolylineCoords);
            commonPolylineCoords = NULL;
        }
        
        
        //事件线的处理。首先根据时间戳截取出事件的点数组。然后绘制。
        double startTimetmp = [wca.dataDic[@"start_timestamp"] doubleValue];
        double endTimetmp = [wca.dataDic[@"end_timestamp"] doubleValue];
        NSMutableArray* eventPointArr = [[NSMutableArray alloc] init];
        for (int i=0; i<self.linePointArray.count; i++) {
            RoutePoint* rp = self.linePointArray[i];
            if (rp.timestamp>=startTimetmp && rp.timestamp<=endTimetmp) {
                [eventPointArr addObject:rp];
            }
        }
        //绘制
        NSMutableArray* styleIndexesArr = [[NSMutableArray alloc] init]; //drawStyleIndexes。每个点都是。
        NSMutableArray* colorArr = [[NSMutableArray alloc] init];
        
        CLLocationCoordinate2D * commonPolylineCoords = (CLLocationCoordinate2D*)malloc(sizeof(CLLocationCoordinate2D)*eventPointArr.count);
        if (commonPolylineCoords == NULL) {
            return;
        }
        
        NSInteger theRuleNum = [self getTheRuleNum:eventPointArr.count];
        for (NSInteger i=0; i<eventPointArr.count; i++) {
            RoutePoint* rp = eventPointArr[i];
            CLLocationCoordinate2D ad = CLLocationCoordinate2DMake(rp.latitude,rp.longitude);
            if (self.linePointCoordinates != 1) {
                //坐标系转换
                ad = AMapCoordinateConvert(ad,AMapCoordinateTypeGPS);
            }
            commonPolylineCoords[i].latitude = ad.latitude;
            commonPolylineCoords[i].longitude = ad.longitude;
            
            //根据速度，计算颜色
            if (i%theRuleNum == 0) {
                [colorArr addObject:[self velocity:rp.speedInKilometersPerHour]];
                [styleIndexesArr addObject:[NSNumber numberWithInteger:i]];
            }
            
        }
        UBMWCustomMAMultiPolyline *commonPolyline = [UBMWCustomMAMultiPolyline polylineWithCoordinates:commonPolylineCoords count:eventPointArr.count drawStyleIndexes:styleIndexesArr];
        commonPolyline.wPolylineType = WSelectedPolyline;
        self.selectedLine = commonPolyline;
        self.selectedLineStrokeColors = colorArr;
        [_mapView addOverlay: commonPolyline];
        
        free(commonPolylineCoords);
        commonPolylineCoords = NULL;
    }
    
}
#pragma mark - 取消选中事件路线处理
-(void)didDeselectPointOverlay:(UBMWCustomAnnotation*)wca{
    
    if (wca.pointType == ubmEventPoint) {
        if (self.grayLine) {
            [self.mapView removeOverlay:self.grayLine];
        }
        if (self.selectedLine) {
            [self.mapView removeOverlay:self.selectedLine];
            self.selectedLineStrokeColors = nil;
        }
        
    }
    
}

#pragma mark - set方法，打点。
-(void)setEventArray:(NSArray *)eventArray{
    
    if (eventArray==nil || eventArray.count==0) {
        return;
    }
    
    _eventArray = eventArray;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if (_eventArray.count>0) {
            
            for (NSDictionary* item in eventArray) {
                
                UBMWCustomAnnotation* pointAnnotation = [[UBMWCustomAnnotation alloc] init];
                CGFloat lat = [item[@"start_lat"] floatValue]; //[item[@"lat"] floatValue];
                CGFloat lon = [item[@"start_lng"] floatValue];  // [item[@"lng"] floatValue];
                CLLocationCoordinate2D ad = CLLocationCoordinate2DMake(lat,lon);
                if ([self.coordinatesType isEqualToString:@"WGS84"]) {
                    ad = AMapCoordinateConvert(ad,AMapCoordinateTypeGPS);
                }
                pointAnnotation.coordinate = ad;
                pointAnnotation.pointType = ubmEventPoint;
                pointAnnotation.eventIconUrl = ChangeToString(item[@"icon_default"]);
                pointAnnotation.eventTouchIconURL = ChangeToString(item[@"icon_active"]);
                
                if ([item[@"is_continuous"] intValue] == 1) {
                    pointAnnotation.is_continuous = YES;
                }else{
                    pointAnnotation.is_continuous = NO;
                }
                
                NSString* eventx = ChangeToString(item[@"type_text"]);
                NSString* timp = item[@"start_timestamp"];
                NSString* timeResult = [UBMMTools timeSecondWithTimeIntervalString:timp]; //item[@"timestamp"]
                NSDictionary* dicxxx = @{@"event":eventx,@"des":item[@"desp"],  @"time":timeResult, @"start_timestamp":item[@"start_timestamp"], @"end_timestamp":item[@"end_timestamp"]};
                pointAnnotation.dataDic = dicxxx;
                
                [_mapView addAnnotation:pointAnnotation];
            }
            
        }
        
    });
}

-(void)setStatrtPoint:(NSDictionary *)statrtPoint{
    
    _statrtPoint = statrtPoint;
    if (statrtPoint && [statrtPoint isKindOfClass:[NSDictionary class]] && statrtPoint.allKeys.count>0) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UBMWCustomAnnotation* pointAnnotation = [[UBMWCustomAnnotation alloc] init];
            CGFloat lat = [statrtPoint[@"lat"] floatValue];
            CGFloat lon = [statrtPoint[@"lng"] floatValue];
            CLLocationCoordinate2D ad = CLLocationCoordinate2DMake(lat,lon);
            if ([self.coordinatesType isEqualToString:@"WGS84"]) {
                ad = AMapCoordinateConvert(ad,AMapCoordinateTypeGPS);
            }
            pointAnnotation.coordinate = ad;
            
            pointAnnotation.pointType = ubmStartPoint;
            
            [_mapView addAnnotation:pointAnnotation];
            
        });
        
        
    }
}

-(void)setEndPoint:(NSDictionary *)endPoint{
    
    _endPoint = endPoint;
    
    if (endPoint && [endPoint isKindOfClass:[NSDictionary class]] && endPoint.allKeys.count>0){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UBMWCustomAnnotation* pointAnnotation = [[UBMWCustomAnnotation alloc] init];
            CGFloat lat = [endPoint[@"lat"] floatValue];
            CGFloat lon = [endPoint[@"lng"] floatValue];
            CLLocationCoordinate2D ad = CLLocationCoordinate2DMake(lat,lon);
            if ([self.coordinatesType isEqualToString:@"WGS84"]) {
                ad = AMapCoordinateConvert(ad,AMapCoordinateTypeGPS);
            }
            pointAnnotation.coordinate = ad;
            
            pointAnnotation.pointType = ubmEndPoint;
            
            [_mapView addAnnotation:pointAnnotation];
            
        });
        
    }
    
}


#pragma mark - 计算颜色值
- (UIColor*)velocity:(float)curVelo{
    
//    rgba(137, 222, 0, 1) //绿
//    rgba(255, 121, 0, 1) //黄

    //填充轨迹渐变数组
    UIColor *xcolor;
    if(curVelo>0.){
        
        curVelo = ((curVelo < V_MIN) ? V_MIN : (curVelo  > V_MAX) ? V_MAX : curVelo);
        float hue = (curVelo-V_MIN)/(V_MAX-V_MIN);
        
        //黄的比较快 绿的比较慢
        //xcolor = [UIColor colorWithRed:(137+(255-137)*hue)/255.0 green:(222-(222-121)*hue)/255.0 blue:0/255.0 alpha:1];
        
        //黄的比较慢 绿的比较快
        xcolor = [UIColor colorWithRed:(255-(255-137)*hue)/255.0 green:(121+(222-121)*hue)/255.0 blue:0/255.0 alpha:1];
        
    }else{
        //xcolor = ColorSet(255, 255, 255, 1);
        xcolor = ColorSet(255, 121, 0, 1); //默认最红色。
    }
    
    return xcolor;
}


#pragma mark - 移除所有覆盖物和数据。
-(void)remoAllOverly{
    
    if (self.mapView.overlays && self.mapView.overlays.count>0) {
        [self.mapView removeOverlays:self.mapView.overlays];
    }
    
    if (self.mapView.annotations && self.mapView.annotations.count>0) {
        [self.mapView removeAnnotations:self.mapView.annotations];
    }
    
    
    
    _linePointArray = @[];
    _eventArray = @[];
    _statrtPoint = @{};
    _endPoint = @{};
    
}

-(NSInteger)getTheRuleNum:(NSInteger)count{
    
    NSInteger theRuleNum = 3;
    if (count>=20000 && count<=50000) {
        theRuleNum = 5;
    }else if(count>=50000 && count<=100000){
        theRuleNum = 7;
    }else if(count>100000 && count<=200000){
        theRuleNum = 9;
    }else if(count>200000 && count<=1000000){
        theRuleNum = 11;
    }else if(count>1000000){
        theRuleNum = 100;
    }
    
    return theRuleNum;
}

#pragma mark - 路线点抽稀。抽到2万点以内。取偶数位。
- (void)routPointVacuate:(NSArray<RoutePoint *>*)pointArray routArrayBloack:(void(^)(NSArray<RoutePoint *>* responseArray))routArrayBloack
{
    if (pointArray.count >= 20000)
    {
        [[UBMDouglasPeucker new] DouglasAlgorithm:pointArray threshold:50 routArrayBloack:^(NSArray *responseArray)
        {
            if (routArrayBloack != nil)
            {
                routArrayBloack(responseArray);
            }
        }];
    }
    else {
        if (routArrayBloack != nil)
        {
            routArrayBloack(pointArray);
        }
    }
}

@end
