//
//  UBMShareEventCell.m
//  LoveCarChannel_iOS
//
//  Created by 金鑫 on 2021/3/8.
//  Copyright © 2021 魏振伟. All rights reserved.
//

#import "UBMShareEventCell.h"
#import "UBMPrefixHeader.h"

@interface UBMShareEventCell ()
@property (nonatomic, assign) ShareEventCellType type;
@end

@implementation UBMShareEventCell
- (instancetype)init {
    return [self initWithType:ShareEventCellTypeSingle];
}

- (instancetype)initWithType:(ShareEventCellType)type {
    self = [super init];
    if (self) {
        self.type = type;
        [self setup];
    }
    return self;
}

- (void)setup {
    [self setupUI];
    [self setupLayout];
}

- (void)setupUI {
    [self addSubview:self.eventNum];
    [self addSubview:self.eventName];
}

- (UILabel *)eventNum {
    if (!_eventNum) {
        _eventNum = [[UILabel alloc] init];
        UIFont *font;
        if (self.type == ShareEventCellTypeSingle) {
            font = [UIFont systemFontOfSize:16 weight:UIFontWeightBold];
        } else {
            font = [UIFont systemFontOfSize:20 weight:UIFontWeightBold];
        }
        _eventNum.font = font;
        _eventNum.textColor = [UIColor ubmColorWithHexNum:0xFFFFFF];
        _eventNum.textAlignment = NSTextAlignmentCenter;
    }
    return _eventNum;
}

- (UILabel *)eventName {
    if (!_eventName) {
        _eventName = [[UILabel alloc] init];
        UIFont *font;
        if (self.type == ShareEventCellTypeSingle) {
            font = [UIFont systemFontOfSize:10 weight:UIFontWeightRegular];
        } else {
            font = [UIFont systemFontOfSize:10 weight:UIFontWeightMedium];
        }
        _eventName.font = font;
        _eventName.textColor = [UIColor ubmColorWithHexNum:0xFFFFFF alpha:0.85];
        _eventName.textAlignment = NSTextAlignmentCenter;
    }
    return _eventName;
}

- (void)setupLayout {
    [self.eventNum mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.eventNum.superview).offset(8);
        make.centerX.equalTo(self.eventNum.superview);
        make.width.mas_equalTo(28);
        if (self.type == ShareEventCellTypeSingle) {
            make.height.mas_equalTo(17);
        } else {
            make.height.mas_equalTo(22);
        }
    }];
    [self.eventName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.eventNum.mas_bottom).offset(4);
        make.centerX.equalTo(self.eventName.superview);
        make.bottom.equalTo(self.eventName.superview.mas_bottom).offset(-8);
        make.width.mas_equalTo(41);
        make.height.mas_equalTo(14);
        make.left.equalTo(self.eventName.superview).offset(6);
        make.right.equalTo(self.eventName.superview).offset(-6);
    }];
}

@end
