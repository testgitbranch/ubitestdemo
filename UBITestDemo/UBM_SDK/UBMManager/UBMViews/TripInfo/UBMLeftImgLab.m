//
//  UBMLeftImgLab.m
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/10/17.
//  Copyright © 2020 魏振伟. All rights reserved.
//

#import "UBMLeftImgLab.h"
#import "UBMPrefixHeader.h"

@interface UBMLeftImgLab()

@end

@implementation UBMLeftImgLab

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.layer.cornerRadius = 3;
        self.backgroundColor = ColorWithRGB(@"#4970FF");
        
        _leftImgView = [[UIImageView alloc] initWithFrame:CGRectMake(8, 0, 16, frame.size.height)];
        _leftImgView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_leftImgView];
        
        _contentLab = [[UILabel alloc] initWithFrame:CGRectMake(_leftImgView.right+8, 0, 10, frame.size.height)];
        _contentLab.textColor = UIColor.whiteColor;
        _contentLab.font = [UIFont systemFontOfSize:14];
        [self addSubview:_contentLab];
        
        
    }
    return self;
}

-(void)setImg:(UIImage *)leftImg andContentText:(NSString *)contentText{
    
    _leftImgView.image = leftImg;
    
    _contentLab.text = contentText;
    [_contentLab sizeToFit];
    
    _contentLab.height = self.frame.size.height;
    
    self.width = _leftImgView.right+8+_contentLab.width+8;
}

-(void)setContentText:(NSString *)contentText{
    _contentLab.text = contentText;
    [_contentLab sizeToFit];
    
    _contentLab.height = self.frame.size.height;
    
    self.width = _leftImgView.right+8+_contentLab.width+8;
}
@end
