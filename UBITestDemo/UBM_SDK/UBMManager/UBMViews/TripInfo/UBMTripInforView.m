//
//  UBMTripInforView.m
//  LoveCarChannel_iOS
//
//  Created by 王国松 on 2021/6/29.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

#import "UBMTripInforView.h"
#import "UBMPrefixHeader.h"
#import "UBMWaMapReGeocodeSearchRequest.h"
#import "UBMConfigure.h"

@implementation UBMTripInforView

- (id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.search = [[AMapSearchAPI alloc] init];
        self.search.delegate = self;
        [self setUp];
        [self setupLayout];
    }
    return self;
}

- (void)setUp{
    [self addSubview:self.tripContainerView];
    /// top
    [self.tripContainerView addSubview:self.tripTopContentView];
    [self.tripTopContentView addSubview:self.tripTitleLabel];
    [self.tripTopContentView addSubview:self.tripNightImage];
    [self.tripTopContentView addSubview:self.tripStartTimeLabel];
    /// middle
    [self.tripContainerView addSubview:self.tripMiddleContentView];
    [self.tripMiddleContentView addSubview:self.tripBeginPoint];
    [self.tripMiddleContentView addSubview:self.tripEndPoint];
    [self.tripMiddleContentView addSubview:self.tripLine];
    [self.tripMiddleContentView addSubview:self.tripBeginLabel];
    [self.tripMiddleContentView addSubview:self.tripEndLabel];
    /// bottom
    [self.tripContainerView addSubview:self.tripBottomContentView];
    [self.tripBottomContentView addSubview:self.tripDuration];
    [self.tripBottomContentView addSubview:self.tripDurationTitle];
    [self.tripBottomContentView addSubview:self.tripLine1];
    [self.tripBottomContentView addSubview:self.tripMileage];
    [self.tripBottomContentView addSubview:self.tripMileageTitle];
    [self.tripBottomContentView addSubview:self.tripLine2];
    [self.tripBottomContentView addSubview:self.tripAvgSpeed];
    [self.tripBottomContentView addSubview:self.tripAvgSpeedTitle];
}

- (void)setupLayout{
    [self.tripContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self.tripContainerView.superview);
        make.left.equalTo(self.tripContainerView.superview).offset(12);
        make.right.equalTo(self.tripContainerView.superview).offset(-12);
    }];
    /// 行程信息 Top
    [self.tripTopContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.equalTo(self.tripTopContentView.superview).offset(16);
        make.right.equalTo(self.tripTopContentView.superview).offset(-16);
    }];
    [self.tripTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.equalTo(self.tripTitleLabel.superview);
    }];
    [self.tripNightImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.tripTitleLabel);
        make.left.equalTo(self.tripTitleLabel.mas_right).offset(8);
        make.width.mas_equalTo(72);
        make.height.mas_equalTo(18);
    }];
    [self.tripStartTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.tripTitleLabel);
        make.right.equalTo(self.tripStartTimeLabel.superview);
    }];
    /// 行程信息 Middle
    [self.tripMiddleContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.tripTopContentView.mas_bottom).offset(20);
        make.left.equalTo(self.tripTopContentView.mas_left);
        make.right.equalTo(self.tripTopContentView.mas_right);
    }];
    [self.tripBeginLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.tripBeginLabel.superview);
        make.left.equalTo(self.tripBeginLabel.superview).offset(14);
    }];
    [self.tripEndLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.tripBeginLabel.mas_bottom).offset(16);
        make.left.equalTo(self.tripBeginLabel);
        make.bottom.equalTo(self.tripEndLabel.superview);
    }];
    [self.tripBeginPoint mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(6);
        make.centerY.equalTo(self.tripBeginLabel);
        make.left.equalTo(self.tripBeginPoint.superview);
    }];
    [self.tripEndPoint mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(6);
        make.centerY.equalTo(self.tripEndLabel);
        make.left.equalTo(self.tripEndPoint.superview);
    }];
    [self.tripLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(1);
        make.centerX.equalTo(self.tripBeginPoint);
        make.top.equalTo(self.tripBeginPoint.mas_bottom);
        make.bottom.equalTo(self.tripEndPoint.mas_top);
    }];
    /// 行程信息 Bottom
    [self.tripBottomContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.tripMiddleContentView.mas_bottom).offset(20);
        make.left.equalTo(self.tripMiddleContentView);
        make.right.equalTo(self.tripMiddleContentView);
        make.bottom.equalTo(self.tripBottomContentView.superview).offset(-16);
    }];
    [self.tripDurationTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.equalTo(self.tripDurationTitle.superview);
    }];
    [self.tripDuration mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.tripDuration.superview);
        make.bottom.equalTo(self.tripDurationTitle.mas_top).offset(-10);
        make.left.equalTo(self.tripDurationTitle);
    }];
    [self.tripLine1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(1);
        make.height.mas_equalTo(40);
        make.left.equalTo(self.tripLine1.superview).offset(105);
        make.centerY.equalTo(self.tripLine1.superview);
    }];
    [self.tripMileageTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.tripDurationTitle);
        make.left.equalTo(self.tripLine1).offset(12);
    }];
    [self.tripMileage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.tripMileageTitle);
        make.centerY.equalTo(self.tripDuration);
    }];
    [self.tripLine2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(1);
        make.height.mas_equalTo(40);
        make.left.equalTo(self.tripLine1.mas_right).offset(117);
        make.centerY.equalTo(self.tripLine1);
    }];
    [self.tripAvgSpeedTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.tripMileageTitle);
        make.left.equalTo(self.tripLine2).offset(12);
    }];
    [self.tripAvgSpeed mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.tripAvgSpeedTitle);
        make.centerY.equalTo(self.tripMileage);
    }];
}

//MARK:渲染数据
- (void)setRouteInforData:(NSDictionary *)routeInforData{
    _routeInforData = routeInforData;
    self.tripStartTimeLabel.text = [[NSDate dateFromTimestamp:routeInforData[@"start_time"] withFormat:@"yyyy-MM-dd HH:mm"] toTimestampWithFormat:@"yyyy年M月d日 HH:mm"];
    /// 夜间行驶
    NSArray *tags = routeInforData[@"driving_tags"];
    if (tags.count > 0) {
        self.isNightTrip = YES;
        [self.tripNightImage sd_setImageWithURL:tags.lastObject[@"driving_tag_link"]];
    }
    if([routeInforData[@"is_need_poi"] intValue] == 1){
        self.tripBeginLabel.text = @"";
        self.tripEndLabel.text = @"";

        //手动解析
        NSDictionary* sp = routeInforData[@"start_point"];
        NSString* coordinatesType = [NSString stringWithFormat:@"%@", routeInforData[@"coordinates"]];

        CLLocationCoordinate2D ad = CLLocationCoordinate2DMake([sp[@"lat"] floatValue],[sp[@"lng"] floatValue]);
        if ([coordinatesType isEqualToString:@"WGS84"]) {
            ad = AMapCoordinateConvert(ad,AMapCoordinateTypeGPS);
        }

        UBMWaMapReGeocodeSearchRequest *regeo = [[UBMWaMapReGeocodeSearchRequest alloc] init];
        regeo.location = [AMapGeoPoint locationWithLatitude:ad.latitude longitude:ad.longitude];
        regeo.requireExtension = YES;
        regeo.radius = 200;
        regeo.wtag = 1;
        [self.search AMapReGoecodeSearch:regeo];

        NSDictionary* ep = routeInforData[@"end_point"];
        CLLocationCoordinate2D ad2 = CLLocationCoordinate2DMake([ep[@"lat"] floatValue],[ep[@"lng"] floatValue]);
        if ([coordinatesType isEqualToString:@"WGS84"]){
            ad2 = AMapCoordinateConvert(ad2,AMapCoordinateTypeGPS);
        }

        UBMWaMapReGeocodeSearchRequest *regeo2 = [[UBMWaMapReGeocodeSearchRequest alloc] init];
        regeo2.location = [AMapGeoPoint locationWithLatitude:ad2.latitude longitude:ad2.longitude];
        regeo2.requireExtension = YES;
        regeo2.radius = 200;
        regeo2.wtag = 2;
        [self.search AMapReGoecodeSearch:regeo2];

    }else{
        self.tripBeginLabel.text = routeInforData[@"start_poi"];
        self.tripEndLabel.text = routeInforData[@"end_poi"];
    }
    self.tripDuration.text = [NSString stringWithFormat:@"%@", routeInforData[@"duration"]];
    self.tripMileage.text = [NSString stringWithFormat:@"%@ km", routeInforData[@"mileage_in_kilometre"]];
    [UBMViewTools markerLable:self.tripMileage changeString:@"km" andAllColor:UIColor.blackColor andMarkColor:ColorSet(0, 0, 0, 0.65) andMarkFondSize:[UIFont systemFontOfSize:12]];

    self.tripAvgSpeed.text = [NSString stringWithFormat:@"%@ km/h", routeInforData[@"avg_speed_in_kilometers_per_hour"]];
    [UBMViewTools markerLable:self.tripAvgSpeed changeString:@"km/h" andAllColor:UIColor.blackColor andMarkColor:ColorSet(0, 0, 0, 0.65) andMarkFondSize:[UIFont systemFontOfSize:12]];
    // 夜间行驶 UI
    if (self.isNightTrip == NO) {
        self.tripNightImage.hidden = YES;
    }
}

/* 逆地理编码回调. */
- (void)onReGeocodeSearchDone:(AMapReGeocodeSearchRequest *)request response:(AMapReGeocodeSearchResponse *)response
{
    if (response.regeocode != nil)
    {
        UBMWaMapReGeocodeSearchRequest* wa = (UBMWaMapReGeocodeSearchRequest*)request;
        NSString* address = response.regeocode.formattedAddress;
        
        //展示优化
        NSString* customAdress = [NSString stringWithString:address];
        NSString* shengshi = response.regeocode.addressComponent.province;
        NSString* shiqu = response.regeocode.addressComponent.city;
        NSString* qu = response.regeocode.addressComponent.district;
        NSString* jiedao = response.regeocode.addressComponent.township;
        
        if (shengshi) {
            customAdress = [customAdress stringByReplacingOccurrencesOfString:shengshi withString:@""];
        }
        if (shiqu) {
            customAdress = [customAdress stringByReplacingOccurrencesOfString:shiqu withString:@""];
        }
        if (qu) {
            customAdress = [customAdress stringByReplacingOccurrencesOfString:qu withString:@""];
        }
        if (jiedao) {
            customAdress = [customAdress stringByReplacingOccurrencesOfString:jiedao withString:@""];
        }
        if (customAdress.length>1) {
            address = customAdress;
        }
        
        NSMutableDictionary* dicx = [[NSMutableDictionary alloc] initWithDictionary:self.routeInforData];
        
        if (wa.wtag == 1) {
            //开始点
            self.tripBeginLabel.text = address;
            [dicx setValue:address forKey:@"start_poi"];
            [dicx setValue:@"2" forKey:@"is_need_poi"];
            
        }else{
            //结束点
            self.tripEndLabel.text = address;
            [dicx setValue:address forKey:@"end_poi"];
            [dicx setValue:@"2" forKey:@"is_need_poi"];
        }
        
        _routeInforData = dicx;
        
        [self updataStartAndEndPoi];
    }
}

/* 上传编码结果 */
- (void)updataStartAndEndPoi {
    if (self.tripBeginLabel.text.length>0 && self.tripEndLabel.text.length>0) {
        
        [[UBMNetWorkTool shareNetWorkTool] requestWithBaseUrl:[UBMDomainName.shareDomain ubmURL] UrlString:UBMUpdataStartAndEndPoi parameters:@{@"userId":[UBITracking shared].userID, @"tripId":self.routeInforData[@"trip_id"], @"startPoi":self.tripBeginLabel.text, @"endPoi":self.tripEndLabel.text} method:@"POST" success:^(id response) {
            
            if ([response[@"code"] intValue] == 1) {
                NSLog(@"逆地里编码上报成功");
            }
            
        } error:^(id response) {
            ;
        } isShowHUD:NO];
    }
}

- (UIView *)tripContainerView {
    if (!_tripContainerView) {
        _tripContainerView = [[UIView alloc] init];
    }
    return _tripContainerView;
}

#pragma mark - 行程信息 Top Content View
- (UIView *)tripTopContentView {
    if (!_tripTopContentView) {
        _tripTopContentView = [[UIView alloc] init];
        _tripContainerView.layer.cornerRadius = 8;
        _tripContainerView.backgroundColor = [UIColor ubmColorWithHexNum:0xFFFFFF];

    }
    return _tripTopContentView;
}

- (UILabel *)tripTitleLabel {
    if (!_tripTitleLabel) {
        _tripTitleLabel = [[UILabel alloc] init];
        _tripTitleLabel.text = @"行程信息";
        _tripTitleLabel.backgroundColor = [UIColor ubmColorWithHexNum:0xFFFFFF];
        _tripTitleLabel.textColor = [UIColor ubmColorWithHexNum:0x000000];
        _tripTitleLabel.textAlignment = NSTextAlignmentLeft;
        _tripTitleLabel.font = [UIFont systemFontOfSize:18 weight:UIFontWeightSemibold];
    }
    return _tripTitleLabel;
}

- (UIImageView *)tripNightImage {
    if (!_tripNightImage) {
        _tripNightImage = [[UIImageView alloc] init];
    }
    return _tripNightImage;
}

- (UILabel *)tripStartTimeLabel {
    if (!_tripStartTimeLabel) {
        _tripStartTimeLabel = [[UILabel alloc] init];
        _tripStartTimeLabel.backgroundColor = [UIColor ubmColorWithHexNum:0xFFFFFF];
        _tripStartTimeLabel.textColor = [UIColor ubmColorWithHexNum:0x000000 alpha:0.65];
        _tripStartTimeLabel.textAlignment = NSTextAlignmentRight;
        _tripStartTimeLabel.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
    }
    return _tripStartTimeLabel;
}

#pragma mark - 行程信息 Middle Content View
- (UIView *)tripMiddleContentView {
    if (!_tripMiddleContentView) {
        _tripMiddleContentView = [[UIView alloc] init];
    }
    return _tripMiddleContentView;
}

- (UIView *)tripBeginPoint {
    if (!_tripBeginPoint) {
        _tripBeginPoint = [[UIView alloc] init];
        _tripBeginPoint.backgroundColor = [UIColor ubmColorWithHexNum:0x52C41A];
        _tripBeginPoint.layer.cornerRadius = 3;
        _tripBeginPoint.layer.masksToBounds = YES;
    }
    return _tripBeginPoint;
}

- (UIView *)tripEndPoint {
    if (!_tripEndPoint) {
        _tripEndPoint = [[UIView alloc] init];
        _tripEndPoint.backgroundColor = [UIColor ubmColorWithHexNum:0xFF4B41];
        _tripEndPoint.layer.cornerRadius = 3;
        _tripEndPoint.layer.masksToBounds = YES;
    }
    return _tripEndPoint;
}

- (UIView *)tripLine {
    if (!_tripLine) {
        _tripLine = [[UIView alloc] init];
        _tripLine.backgroundColor = [UIColor ubmColorWithHexNum:0xD8D8D8];
    }
    return _tripLine;
}

- (UILabel *)tripBeginLabel {
    if (!_tripBeginLabel) {
        _tripBeginLabel = [[UILabel alloc] init];
        _tripBeginLabel.backgroundColor = [UIColor ubmColorWithHexNum:0xFFFFFF];
        _tripBeginLabel.textColor = [UIColor ubmColorWithHexNum:0x000000 alpha:0.85];
        _tripBeginLabel.textAlignment = NSTextAlignmentLeft;
        _tripBeginLabel.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
    }
    return _tripBeginLabel;
}

- (UILabel *)tripEndLabel {
    if (!_tripEndLabel) {
        _tripEndLabel = [[UILabel alloc] init];
        _tripEndLabel.backgroundColor = [UIColor ubmColorWithHexNum:0xFFFFFF];
        _tripEndLabel.textColor = [UIColor ubmColorWithHexNum:0x000000 alpha:0.85];
        _tripEndLabel.textAlignment = NSTextAlignmentLeft;
        _tripEndLabel.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
    }
    return _tripEndLabel;
}

#pragma mark - 行程信息 Bottom Content View
- (UIView *)tripBottomContentView {
    if (!_tripBottomContentView) {
        _tripBottomContentView = [[UIView alloc] init];
    }
    return _tripBottomContentView;
}

- (UILabel *)tripDuration {
    if (!_tripDuration) {
        _tripDuration = [[UILabel alloc] init];
        _tripDuration.backgroundColor = [UIColor ubmColorWithHexNum:0xFFFFFF];
        _tripDuration.textColor = [UIColor ubmColorWithHexNum:0x000000 alpha:0.85];
        _tripDuration.textAlignment = NSTextAlignmentLeft;
        _tripDuration.font = [UIFont systemFontOfSize:20 weight:UIFontWeightBold];
    }
    return _tripDuration;
}

- (UILabel *)tripDurationTitle {
    if (!_tripDurationTitle) {
        _tripDurationTitle = [[UILabel alloc] init];
        _tripDurationTitle.text = @"驾驶时长";
        _tripDurationTitle.backgroundColor = [UIColor ubmColorWithHexNum:0xFFFFFF];
        _tripDurationTitle.textColor = [UIColor ubmColorWithHexNum:0x000000 alpha:0.85];
        _tripDurationTitle.textAlignment = NSTextAlignmentLeft;
        _tripDurationTitle.font = [UIFont systemFontOfSize:13 weight:UIFontWeightRegular];
    }
    return _tripDurationTitle;
}

- (UIView *)tripLine1 {
    if (!_tripLine1) {
        _tripLine1 = [[UIView alloc] init];
        _tripLine1.backgroundColor = [UIColor ubmColorWithHexNum:0x000000 alpha:0.06];
    }
    return _tripLine1;
}

- (UILabel *)tripMileage {
    if (!_tripMileage) {
        _tripMileage = [[UILabel alloc] init];
        _tripMileage.backgroundColor = [UIColor ubmColorWithHexNum:0xFFFFFF];
        _tripMileage.textColor = [UIColor ubmColorWithHexNum:0x000000 alpha:0.85];
        _tripMileage.textAlignment = NSTextAlignmentCenter;
        _tripMileage.font = [UIFont systemFontOfSize:20 weight:UIFontWeightBold];
    }
    return _tripMileage;
}

- (UILabel *)tripMileageTitle {
    if (!_tripMileageTitle) {
        _tripMileageTitle = [[UILabel alloc] init];
        _tripMileageTitle.text = @"行驶里程";
        _tripMileageTitle.backgroundColor = [UIColor ubmColorWithHexNum:0xFFFFFF];
        _tripMileageTitle.textColor = [UIColor ubmColorWithHexNum:0x000000 alpha:0.85];
        _tripMileageTitle.textAlignment = NSTextAlignmentLeft;
        _tripMileageTitle.font = [UIFont systemFontOfSize:13 weight:UIFontWeightRegular];
    }
    return _tripMileageTitle;
}

- (UIView *)tripLine2 {
    if (!_tripLine2) {
        _tripLine2 = [[UIView alloc] init];
        _tripLine2.backgroundColor = [UIColor ubmColorWithHexNum:0x000000 alpha:0.06];
    }
    return _tripLine2;
}

- (UILabel *)tripAvgSpeed {
    if (!_tripAvgSpeed) {
        _tripAvgSpeed = [[UILabel alloc] init];
        _tripAvgSpeed.backgroundColor = [UIColor ubmColorWithHexNum:0xFFFFFF];
        _tripAvgSpeed.textColor = [UIColor ubmColorWithHexNum:0x000000 alpha:0.85];
        _tripAvgSpeed.textAlignment = NSTextAlignmentCenter;
        _tripAvgSpeed.font = [UIFont systemFontOfSize:20 weight:UIFontWeightBold];
    }
    return _tripAvgSpeed;
}

- (UILabel *)tripAvgSpeedTitle {
    if (!_tripAvgSpeedTitle) {
        _tripAvgSpeedTitle = [[UILabel alloc] init];
        _tripAvgSpeedTitle.text = @"平均速度";
        _tripAvgSpeedTitle.backgroundColor = [UIColor ubmColorWithHexNum:0xFFFFFF];
        _tripAvgSpeedTitle.textColor = [UIColor ubmColorWithHexNum:0x000000 alpha:0.85];
        _tripAvgSpeedTitle.textAlignment = NSTextAlignmentLeft;
        _tripAvgSpeedTitle.font = [UIFont systemFontOfSize:13 weight:UIFontWeightRegular];
    }
    return _tripAvgSpeedTitle;
}


@end
