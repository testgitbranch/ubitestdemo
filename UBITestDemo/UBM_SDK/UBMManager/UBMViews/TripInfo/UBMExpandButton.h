//
//  BXExpandButton.h
//  LoveCarChannel_iOS
//
//  Created by 金鑫 on 2021/3/12.
//  Copyright © 2021 魏振伟. All rights reserved.
//

/*
 扩大点击范围的button
 */

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UBMExpandButton : UIButton

@end

NS_ASSUME_NONNULL_END
