//
//  UBMWBaseAlertView.m
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/10/26.
//  Copyright © 2020 魏振伟. All rights reserved.
//

#import "UBMWBaseAlertView.h"
#import "UBMPrefixHeader.h"

@implementation UBMWBaseAlertView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.touchBackDismiss = YES;
        self.backgroundColor = ColorSet(0, 0, 0, MainAlphe);
        
        UIButton* backgroundBton = [UIButton buttonWithType:UIButtonTypeCustom];
        backgroundBton.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
        [backgroundBton addTarget:self action:@selector(touchBacAction) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:backgroundBton];
        
        [self addSomeViews];
    }
    return self;
}

-(void)addSomeViews{
    //子类继承
}

-(void)show{
    if(self){
        [[UBMMTools currentWindow] addSubview:self];
        //[UIApplication.sharedApplication.keyWindow addSubview:self]; //禁止使用keywindow
    }
}

-(void)touchBacAction{
    if (self.touchBackDismiss == YES) {
        [self dismiss];
    }
}

-(void)dismiss{
    [self removeFromSuperview];
    
    if (self.dismissBlock) {
        self.dismissBlock();
    }
}

@end
