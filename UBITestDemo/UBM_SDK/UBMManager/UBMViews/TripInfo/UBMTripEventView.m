//
//  UBMTripEventView.m
//  LoveCarChannel_iOS
//
//  Created by 王国松 on 2021/6/29.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

#import "UBMTripEventView.h"
#import "UBMPrefixHeader.h"

@implementation UBMTripEventView

- (id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.contentWidth = ScreenWidth - 32 * 2;
        [self setUp];
    }
    return self;
}

- (void)setUp{
    [self addSubview:self.eventContainerView];
    /// top
    [self.eventContainerView addSubview:self.eventTopContentView];
    [self.eventTopContentView addSubview:self.eventHiddenDangerLabel];
    [self.eventTopContentView addSubview:self.eventNumLabel];
    [self.eventTopContentView addSubview:self.eventUnitLabel];
    [self.eventTopContentView addSubview:self.questionButton];
    [self.eventContainerView addSubview:self.eventDisplayView];
}

- (void)setupLayout{
    [self.eventContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self.eventContainerView.superview);
        make.left.equalTo(self.eventContainerView.superview).offset(12);
        make.right.equalTo(self.eventContainerView.superview).offset(-12);
    }];
    [self.eventTopContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.equalTo(self.eventTopContentView.superview).offset(16);
        make.right.equalTo(self.eventTopContentView.superview).offset(-16);
    }];
    [self.eventHiddenDangerLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.equalTo(self.eventHiddenDangerLabel.superview);
    }];
    [self.eventNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(self.eventTotal < 10 ? 19 : 29);
        make.height.mas_equalTo(22);
        make.left.equalTo(self.eventHiddenDangerLabel.mas_right).offset(6);
        make.centerY.equalTo(self.eventHiddenDangerLabel);
    }];
    [self.eventUnitLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.eventNumLabel.mas_right).offset(6);
        make.centerY.equalTo(self.eventNumLabel);
    }];
    [self.questionButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.eventHiddenDangerLabel);
        make.right.equalTo(self.questionButton.superview);
    }];
    [self.eventDisplayView mas_makeConstraints:^(MASConstraintMaker *make) {
        CGFloat height = [self.eventDisplayView getHeightFromNumberOfEventType:self.eventModels.count];
        make.height.mas_equalTo(height);
        make.top.equalTo(self.eventTopContentView.mas_bottom).offset(20);
        make.left.right.equalTo(self.eventTopContentView);
        make.bottom.equalTo(self.eventDisplayView.superview).offset(-16);
    }];

}

//MARK:渲染数据
- (void)setRouteInforData:(NSDictionary *)routeInforData{
    _routeInforData = routeInforData;
    // 行程事件
    NSArray* gcDataArray = routeInforData[@"event_statistics"];
    if (gcDataArray && [gcDataArray isKindOfClass:[NSArray class]] && gcDataArray.count>0) {
        self.eventTotal = 0;
        self.eventModels = [UBMEventModel modelsWithDataArray:gcDataArray];
        for (UBMEventModel *model in self.eventModels) {
            self.eventTotal += model.eventCount;
        }
        if (self.eventTotal > 0 && self.eventModels.count > 0 && [self.routeInforData[@"score"] integerValue] < 100) {
            self.eventNumLabel.text = [NSString stringWithFormat:@"%zd", self.eventTotal];
            [self.eventContainerView addSubview:self.eventDisplayView];
        }
    }
    self.hidden = self.eventTotal <= 0;
    
}

- (UIView *)eventContainerView {
    if (!_eventContainerView) {
        _eventContainerView = [[UIView alloc] init];
        _eventContainerView.backgroundColor = [UIColor ubmColorWithHexNum:0xFFFFFF];
        _eventContainerView.layer.cornerRadius = 8;
    }
    return _eventContainerView;
}

- (UIView *)eventTopContentView {
    if (!_eventTopContentView) {
        _eventTopContentView = [[UIView alloc] init];
    }
    return _eventTopContentView;
}

- (UILabel *)eventHiddenDangerLabel {
    if (!_eventHiddenDangerLabel) {
        _eventHiddenDangerLabel = [[UILabel alloc] init];
        _eventHiddenDangerLabel.text = @"行程安全隐患";
        _eventHiddenDangerLabel.textColor = [UIColor ubmColorWithHexNum:0x000000];
        _eventHiddenDangerLabel.textAlignment = NSTextAlignmentLeft;
        _eventHiddenDangerLabel.font = [UIFont systemFontOfSize:18 weight:UIFontWeightSemibold];
    }
    return _eventHiddenDangerLabel;
}

- (UILabel *)eventNumLabel {
    if (!_eventNumLabel) {
        _eventNumLabel = [[UILabel alloc] init];
        _eventNumLabel.backgroundColor = [UIColor ubmColorWithHexNum:0xFF4B41];
        _eventNumLabel.textColor = [UIColor ubmColorWithHexNum:0xFFFFFF];
        _eventNumLabel.textAlignment = NSTextAlignmentCenter;
        _eventNumLabel.font = [UIFont systemFontOfSize:18 weight:UIFontWeightBold];
        _eventNumLabel.layer.cornerRadius = 4;
        _eventNumLabel.layer.masksToBounds = YES;
    }
    return _eventNumLabel;
}

- (UILabel *)eventUnitLabel {
    if (!_eventUnitLabel) {
        _eventUnitLabel = [[UILabel alloc] init];
        _eventUnitLabel.text = @"个";
        _eventUnitLabel.textColor = [UIColor ubmColorWithHexNum:0x000000];
        _eventUnitLabel.textAlignment = NSTextAlignmentLeft;
        _eventUnitLabel.font = [UIFont systemFontOfSize:18 weight:UIFontWeightSemibold];
    }
    return _eventUnitLabel;
}

- (UIButton *)questionButton {
    if (!_questionButton) {
        _questionButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_questionButton setTitle:@"安全隐患有哪些?" forState:UIControlStateNormal];
        [_questionButton setTitleColor:[UIColor ubmColorWithHexNum:0x000000 alpha:0.45] forState:UIControlStateNormal];
        _questionButton.titleLabel.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
        _questionButton.titleLabel.textAlignment = NSTextAlignmentRight;
    }
    return _questionButton;
}

- (UBMEventDisplayView *)eventDisplayView {
    if (!_eventDisplayView) {
        if (self.eventModels.count > 0) {
            _eventDisplayView = [UBMEventDisplayView instantiateWithContentViewWidth:self.contentWidth numberOfEventType:self.eventModels.count target:self];
            _eventDisplayView.backgroundColor = [UIColor ubmColorWithHexNum:0xFFFFFF];
        }
    }
    return _eventDisplayView;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.eventModels.count;
}

- (UBMEventCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UBMEventCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([UBMEventCell class]) forIndexPath:indexPath];
    
    cell.model = self.eventModels[indexPath.item];
    
    return cell;
}


@end
