//
//  UBMExpandButton.m
//  LoveCarChannel_iOS
//
//  Created by 金鑫 on 2021/3/12.
//  Copyright © 2021 魏振伟. All rights reserved.
//

#import "UBMExpandButton.h"

@implementation UBMExpandButton

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    CGRect newArea = CGRectMake(self.bounds.origin.x - 20, self.bounds.origin.y - 20, self.bounds.size.width + 40, self.bounds.size.height + 40);
    
    return CGRectContainsPoint(newArea, point);
}

@end
