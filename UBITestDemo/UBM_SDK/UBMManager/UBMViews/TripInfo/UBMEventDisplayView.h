//
//  UBMEventDisplayView.h
//  LoveCarChannel_iOS
//
//  Created by 金鑫 on 2021/3/2.
//  Copyright © 2021 魏振伟. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UBMEventDisplayView : UICollectionView
@property (nonatomic, assign) NSInteger eventTotal;

- (CGFloat)getHeightFromNumberOfEventType:(NSInteger)num;

+ (instancetype)instantiateWithContentViewWidth:(CGFloat)width numberOfEventType:(NSInteger)num target:(id<UICollectionViewDataSource>)target;

- (instancetype)initWithContentViewWidth:(CGFloat)width numberOfEventType:(NSInteger)num target:(id<UICollectionViewDataSource>)target;

@end

NS_ASSUME_NONNULL_END
