//
//  BXTripInforView.h
//  LoveCarChannel_iOS
//
//  Created by 王国松 on 2021/6/29.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AMapSearchKit/AMapSearchKit.h>
#import "UBMWaMapReGeocodeSearchRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface UBMTripInforView : UIView<AMapSearchDelegate>

@property(nonatomic, strong)AMapSearchAPI* search;

@property (nonatomic, strong) UIView *tripContainerView;
// 行程信息 Top Content View
@property (nonatomic, strong) UIView *tripTopContentView;
@property (nonatomic, strong) UILabel *tripTitleLabel;
@property (nonatomic, assign) BOOL isNightTrip;
@property (nonatomic, strong) UIImageView *tripNightImage;
@property (nonatomic, strong) UILabel *tripStartTimeLabel;
// 行程信息 Middle Content View
@property (nonatomic, strong) UIView *tripMiddleContentView;
@property (nonatomic, strong) UIView *tripBeginPoint;
@property (nonatomic, strong) UIView *tripEndPoint;
@property (nonatomic, strong) UIView *tripLine;
@property (nonatomic, strong) UILabel *tripBeginLabel;
@property (nonatomic, strong) UILabel *tripEndLabel;
// 行程信息 Bottom Content View
@property (nonatomic, strong) UIView *tripBottomContentView;
@property (nonatomic, strong) UILabel *tripDuration;
@property (nonatomic, strong) UILabel *tripDurationTitle;
@property (nonatomic, strong) UIView *tripLine1;
@property (nonatomic, strong) UILabel *tripMileage;
@property (nonatomic, strong) UILabel *tripMileageTitle;
@property (nonatomic, strong) UIView *tripLine2;
@property (nonatomic, strong) UILabel *tripAvgSpeed;
@property (nonatomic, strong) UILabel *tripAvgSpeedTitle;

//渲染数据
@property(nonatomic,strong)NSDictionary * routeInforData;

@end

NS_ASSUME_NONNULL_END
