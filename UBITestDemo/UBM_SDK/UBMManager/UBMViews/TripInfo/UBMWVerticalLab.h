//
//  UBMWVerticalLab.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/10/17.
//  Copyright © 2020 魏振伟. All rights reserved.
//
/*
 上下排列两个lable
 */

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UBMWVerticalLab : UIView

@property(nonatomic, copy)NSString* title;
@property(nonatomic, copy)NSString* lastTitle;

-(void)hidenRightLine;

-(void)markTitleWithText:(NSString*)markText markColor:(UIColor*)markColor markFont:(UIFont*)markFont;

@end

NS_ASSUME_NONNULL_END
