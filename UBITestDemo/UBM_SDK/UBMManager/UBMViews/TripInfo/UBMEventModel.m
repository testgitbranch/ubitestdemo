//
//  UBMEventModel.m
//  LoveCarChannel_iOS
//
//  Created by 金鑫 on 2021/3/2.
//  Copyright © 2021 魏振伟. All rights reserved.
//

#import "UBMEventModel.h"
#import "UBMPrefixHeader.h"

@implementation UBMEventModel
+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
        @"eventCount" : @"num",
        @"eventUnit" : @"type_unit",
        @"eventType" : @"type_text"
    };
}

+ (NSArray *)modelsWithDataArray:(NSArray *)dataArray {
    NSArray *models = [UBMEventModel mj_objectArrayWithKeyValuesArray:dataArray];
    
    return models;
}

@end
