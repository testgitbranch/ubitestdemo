//
//  UBMTripEventView.h
//  LoveCarChannel_iOS
//
//  Created by 王国松 on 2021/6/29.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UBMEventDisplayView.h"
#import "UBMEventModel.h"
#import "UBMEventCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface UBMTripEventView : UIView<UICollectionViewDataSource>

@property(nonatomic,assign)CGFloat  contentWidth;
@property (nonatomic, strong) UIView *eventContainerView;
@property (nonatomic, strong) UIView *eventTopContentView;
@property (nonatomic, strong) UILabel *eventHiddenDangerLabel;
@property (nonatomic, strong) UILabel *eventNumLabel;
@property (nonatomic, strong) UILabel *eventUnitLabel;
@property (nonatomic, strong) UIButton *questionButton;
@property (nonatomic, strong) UBMEventDisplayView *eventDisplayView;
@property (nonatomic, assign) NSInteger eventTotal;
@property (nonatomic, copy) NSArray <UBMEventModel *> *eventModels;

//数据
@property(nonatomic,strong)NSDictionary * routeInforData;

- (void)setupLayout;

@end

NS_ASSUME_NONNULL_END
