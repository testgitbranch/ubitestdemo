//
//  UBMWCustomMAMultiPolyline.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/12/7.
//  Copyright © 2020 魏振伟. All rights reserved.
//

/*
 为MAMultiPolyline增加类型。
 */

#import <UIKit/UIKit.h>
//#import <MAMapKit/MAMapKit.h>
#import <MapKit/MapKit.h>
#import <AMapNaviKit/MAMapKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef enum : NSUInteger {
    WDefultPolyline,
    WSelectedPolyline,
    WGrayPolyline,
} WPolylineType;

API_AVAILABLE(ios(13.0))
@interface UBMWCustomMAMultiPolyline : MAMultiPolyline

@property(nonatomic, assign)WPolylineType wPolylineType;

@end

NS_ASSUME_NONNULL_END
