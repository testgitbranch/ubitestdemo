//
//  UBMEventDisplayView.m
//  LoveCarChannel_iOS
//
//  Created by 金鑫 on 2021/3/2.
//  Copyright © 2021 魏振伟. All rights reserved.
//

#import "UBMEventDisplayView.h"
#import "UBMEventCell.h"
#import "UBMMTools.h"

static CGFloat const kMargin = 8;
static CGFloat const kItemH = 64;

@implementation UBMEventDisplayView

- (CGFloat)getHeightFromNumberOfEventType:(NSInteger)num {
    if (num == 0) {
        return 0;
        
    } else if (num >= 1 && num <= 4) {
        return kItemH;
        
    } else {
        return 2 * kItemH + kMargin;
    }
}

- (CGFloat)getItemWidthFromContentViewWidth:(CGFloat)width numberOfEventType:(NSInteger)num {
    if (num == 0) {
        return 0;
        
    } else if (num >= 1 && num <= 4) {
        return (width - (num - 1) * kMargin) / num;
    
    } else if (num == 5 || num == 6) {
        return (width - (3 - 1) * kMargin) / 3;
    
    } else if (num == 7 || num == 8) {
        return (width - (4 - 1) * kMargin) / 4;
    
    } else {
        return 0;
    }
}

+ (instancetype)instantiateWithContentViewWidth:(CGFloat)width numberOfEventType:(NSInteger)num target:(id<UICollectionViewDataSource>)target {
    return [[self alloc] initWithContentViewWidth:width numberOfEventType:num target:target];
}

- (instancetype)initWithContentViewWidth:(CGFloat)width numberOfEventType:(NSInteger)num target:(id<UICollectionViewDataSource>)target {
    CGFloat itemW = [self getItemWidthFromContentViewWidth:width numberOfEventType:num];
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.itemSize = CGSizeMake(itemW, kItemH);
    layout.minimumInteritemSpacing = kMargin;
    layout.minimumLineSpacing = kMargin;
    layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    self = [super initWithFrame:CGRectZero collectionViewLayout:layout];
    if (self) {
        [self registerNib:[UINib nibWithNibName:NSStringFromClass([UBMEventCell class]) bundle:[UBMMTools ubmBundle]] forCellWithReuseIdentifier:NSStringFromClass([UBMEventCell class])];
        self.dataSource = target;
        self.showsVerticalScrollIndicator = NO;
        self.showsHorizontalScrollIndicator = NO;
        self.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return self;
}

@end
