//
//  UBMScoreView.h
//  LoveCarChannel_iOS
//
//  Created by 王国松 on 2021/6/29.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UBMMeterView.h"

NS_ASSUME_NONNULL_BEGIN

@interface UBMScoreView : UIView

///评分图片
@property (nonatomic, strong) UIImageView *scoreStampImageView;
@property (nonatomic, strong) UIImageView *scoreMeterBorderImageView;
@property (nonatomic, strong) UIImageView *scoreMeterBaseImageView;
@property (nonatomic, strong) UIImageView *scoreMeterScaleImageView;
@property (nonatomic, strong) UBMMeterView *scoreMeterView;
@property (nonatomic, strong) UIView *scoreSafetyContentView;
@property (nonatomic, strong) UILabel *scoreSafety;
@property (nonatomic, strong) UILabel *scoreSafetyLabel;

//数据
@property(nonatomic,strong)NSDictionary * routeInforData;

@end

NS_ASSUME_NONNULL_END
