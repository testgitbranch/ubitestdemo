//
//  UBMAbortView.m
//  LoveCarChannel_iOS
//
//  Created by 王国松 on 2021/6/29.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

#import "UBMAbortView.h"
#import "UBMPrefixHeader.h"

@implementation UBMAbortView

- (id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setUp];
        [self setupLayout];
    }
    return self;
}

- (void)setUp{
    [self addSubview:self.abortContainerView];
    [self.abortContainerView addSubview:self.abortTitle];
    [self.abortContainerView addSubview:self.abortSubTitle];
    [self.abortContainerView addSubview:self.abortButtonBgView];
    [self.abortButtonBgView addSubview:self.abortButton];
}

- (void)setupLayout{
    [self.abortContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self.abortContainerView.superview);
        make.left.equalTo(self.abortContainerView.superview).offset(12);
        make.right.equalTo(self.abortContainerView.superview).offset(-12);
    }];
    [self.abortTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.abortTitle.superview).offset(14);
        make.left.equalTo(self.abortTitle.superview).offset(14);
    }];
    [self.abortSubTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.abortTitle.mas_bottom).offset(6);
        make.left.equalTo(self.abortTitle);
        make.width.mas_equalTo(160);
        make.bottom.equalTo(self.abortSubTitle.superview).offset(-14);
    }];
    [self.abortButtonBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(36);
        make.right.equalTo(self.abortButtonBgView.superview).offset(-14);
        make.centerY.equalTo(self.abortButtonBgView.superview);
    }];
    [self.abortButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.abortButton.superview);
    }];
}

//MARK:渲染数据
- (void)setRouteInforData:(NSDictionary *)routeInforData{
    _routeInforData = routeInforData;
    self.abortTitle.text = routeInforData[@"abnormal_trip_text"][@"title"];
    self.abortSubTitle.text = routeInforData[@"abnormal_trip_text"][@"desp"];
    [self.abortButton setTitle:routeInforData[@"abnormal_trip_text"][@"button"] forState:UIControlStateNormal];
}

- (UIView *)abortContainerView {
    if (!_abortContainerView) {
        _abortContainerView = [[UIView alloc] init];
        _abortContainerView.backgroundColor = [UIColor ubmColorWithHexNum:0xFFE4D1];
        _abortContainerView.layer.cornerRadius = 8;
    }
    return _abortContainerView;
}

- (UILabel *)abortTitle {
    if (!_abortTitle) {
        _abortTitle = [[UILabel alloc] init];
        _abortTitle.textColor = [UIColor ubmColorWithHexNum:0x000000];
        _abortTitle.textAlignment = NSTextAlignmentLeft;
        _abortTitle.font = [UIFont systemFontOfSize:16 weight:UIFontWeightSemibold];
    }
    return _abortTitle;
}

- (UILabel *)abortSubTitle {
    if (!_abortSubTitle) {
        _abortSubTitle = [[UILabel alloc] init];
        _abortSubTitle.textColor = [UIColor ubmColorWithHexNum:0x000000 alpha:0.45];
        _abortSubTitle.textAlignment = NSTextAlignmentLeft;
        _abortSubTitle.font = [UIFont systemFontOfSize:12 weight:UIFontWeightRegular];
        _abortSubTitle.numberOfLines = 0;
    }
    return _abortSubTitle;
}

- (UIView *)abortButtonBgView {
    if (!_abortButtonBgView) {
        _abortButtonBgView = [[UIView alloc] init];
        _abortButtonBgView.layer.cornerRadius = 18;
        _abortButtonBgView.layer.masksToBounds = YES;
    }
    return _abortButtonBgView;
}

- (UIButton *)abortButton {
    if (!_abortButton) {
        _abortButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _abortButton.titleLabel.font = [UIFont systemFontOfSize:15 weight:UIFontWeightRegular];
        _abortButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        [_abortButton setTintColor:[UIColor ubmColorWithHexNum:0xFFFFFF]];
    }
    return _abortButton;
}


@end
