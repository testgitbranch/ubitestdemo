//
//  UBMGradientView.m
//  LoveCarChannel_iOS
//
//  Created by 王国松 on 2021/6/30.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

#import "UBMGradientView.h"
#import "UBMPrefixHeader.h"

@implementation UBMGradientView

- (id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}

- (void)setupView{
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 17)];
    [self addSubview:view];
    view.backgroundColor = [UIColor whiteColor];
   
    UIView * gradientView = [[UIView alloc] initWithFrame:CGRectMake(0, 17, ScreenWidth, 133)];
    [self addSubview:gradientView];
    [UBMViewTools addGradualChangeBckColor:[UIColor ubmColorWithHexNum:0xFFFFFF] :[UIColor ubmColorWithHexNum:0xF2F2F2]  :1 to:gradientView];
}

@end
