//
//  UBMSpeedChartView.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/10/16.
//  Copyright © 2020 魏振伟. All rights reserved.
//

/*
 速度图标
 */

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UBMSpeedChartView : UIView

@property(nonatomic, assign)CGFloat maxSpeed; //最高速度
@property(nonatomic, strong)NSArray* speedPointArray;//速度曲线的数组

@end

NS_ASSUME_NONNULL_END
