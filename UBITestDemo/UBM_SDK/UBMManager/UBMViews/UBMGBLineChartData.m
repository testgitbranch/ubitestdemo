//
//  UBMGBLineChartData.m
//  GBChartDemo
//
//  Created by midas on 2018/12/21.
//  Copyright © 2018 Midas. All rights reserved.
//

#import "UBMGBLineChartData.h"

@implementation UBMGBLineChartDataItem

+ (id)dataItemWithY:(CGFloat)Y X:(CGFloat)X {
    
    UBMGBLineChartDataItem *item = [[UBMGBLineChartDataItem alloc] init];
    item.y = Y;
    item.x = X;
    return item;
}

+ (id)dataItemWithY:(CGFloat)Y {
    
    return [self dataItemWithY:Y X:0];
}

@end

@implementation UBMGBLineChartData

@end
