//
//  UBMHuanView.m
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/10/16.
//  Copyright © 2020 魏振伟. All rights reserved.
//

#import "UBMHuanView.h"
#import "UBMPrefixHeader.h"

@interface UBMHuanView()

@property (nonatomic, assign) CGFloat progress;

@property (nonatomic, strong) CAShapeLayer *progressLayer;

@property (nonatomic, strong) CALayer *gradientLayer;

@property (nonatomic, strong) UIView* maxSuduView;

@property (nonatomic, strong) UILabel* numLab;

@property (nonatomic, strong) UIImageView* whitePointImg;

@end

@implementation UBMHuanView


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        UIView* maxSuduView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.width)];
        [self addSubview:maxSuduView];
        self.maxSuduView = maxSuduView;
        
        //layer边框默认总是在最上层，会影响圆环的显示。所以自画。
//        CAShapeLayer *borderLayer=[CAShapeLayer layer];
//        CGPoint center = CGPointMake(frame.size.width/2.0, frame.size.width/2.0);
//        UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:center radius:frame.size.width/2.0-5 startAngle:0 endAngle:2*M_PI clockwise:YES];
//        borderLayer.path = [path CGPath];
//        borderLayer.fillColor= [UIColor clearColor].CGColor;
//        borderLayer.strokeColor= [ColorWithRGB(@"#F5F5F5") CGColor];
//        borderLayer.lineWidth= 4;
//        borderLayer.frame=self.bounds;
//        [maxSuduView.layer addSublayer:borderLayer];
        
        
        [self addOtherView];
        
    }
    return self;
}

- (void)addOtherView{
    UILabel* numLab = [[UILabel alloc] initWithFrame:CGRectMake(0, 35, self.width, 20)];
    numLab.textColor = UIColor.blackColor;
    numLab.font = [UIFont systemFontOfSize:24 weight:UIFontWeightBold];
    numLab.text = @"0.0";
    numLab.textAlignment = NSTextAlignmentCenter;
    [self addSubview:numLab];
    self.numLab = numLab;
    
    UILabel* numTextLab = [[UILabel alloc] initWithFrame:CGRectMake(0, numLab.bottom+5, self.width, 20)];
    numTextLab.textColor = ColorWithRGB(@"#595959");
    numTextLab.font = [UIFont systemFontOfSize:16 weight:UIFontWeightRegular];
    numTextLab.text = @"km/h";
    numTextLab.textAlignment = NSTextAlignmentCenter;
    [self addSubview:numTextLab];
    
    UIImageView* whitePointImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 6, 6)];
    whitePointImg.backgroundColor = UIColor.whiteColor;
    whitePointImg.layer.cornerRadius = 3;
    whitePointImg.layer.masksToBounds = YES;
    [self addSubview:whitePointImg];
    self.whitePointImg = whitePointImg;
}



-(void)setSpeed:(NSString *)speed{
    _speed = speed;
    
    self.numLab.text = speed;
    //环
    [self drawProgress:[_speed floatValue]];
}


- (void)drawProgress:(CGFloat )progress
{
    _progress = progress;
    [_progressLayer removeFromSuperlayer];
    [_gradientLayer removeFromSuperlayer];
    [self drawCycleProgress];
}


- (void)drawCycleProgress
{
    //背景灰环
    UIBezierPath *circlePath2 = [UIBezierPath bezierPathWithArcCenter:CGPointMake(_maxSuduView.width / 2, _maxSuduView.height / 2) radius:(_maxSuduView.width)/2.0-5 startAngle:0 endAngle:2*M_PI clockwise:YES];
    CAShapeLayer *bgLayer = [CAShapeLayer layer];
    bgLayer.frame = self.bounds;
    bgLayer.fillColor = [UIColor clearColor].CGColor;//填充色 -  透明色
    bgLayer.lineWidth = 5.f;
    bgLayer.strokeColor = ColorWithRGB(@"#F5F5F5").CGColor;//线条颜色
    bgLayer.strokeStart = 0;
    bgLayer.strokeEnd = 1;
    bgLayer.lineCap = kCALineCapRound;
    bgLayer.path = circlePath2.CGPath;
    [self.maxSuduView.layer addSublayer:bgLayer];
    
    
    //色环
    CGPoint center = CGPointMake(_maxSuduView.width/2.0, _maxSuduView.width/2.0);
    CGFloat radius = _maxSuduView.width/2.0-5;
    CGFloat startA = -(M_PI + M_PI_2/3*1);  //设置进度条起点位置
    CGFloat endA = M_PI_2/3*1;
//    CGFloat endA = -M_PI_2 + M_PI * 2 * _progress;  //设置进度条终点位置
    
    //计算终点 0-140
    if (_progress > 140.0) {
        _progress = 140.0;
    }
    endA = startA + (endA-startA)*(_progress/140.0);
    
    
    self.whitePointImg.center = [self getPicLocCenter:endA andCenter:self.maxSuduView.center radius:(_maxSuduView.width)/2.0-5];
    
    
    //获取环形路径（画一个圆形，填充色透明，设置线框宽度为10，这样就获得了一个环形）
    _progressLayer = [CAShapeLayer layer];//创建一个track shape layer
//    _progressLayer.backgroundColor = UIColor.whiteColor.CGColor;
    _progressLayer.frame = self.maxSuduView.bounds;
    _progressLayer.fillColor = [[UIColor clearColor] CGColor];  //填充色为无色
    _progressLayer.strokeColor = [UIColor.whiteColor CGColor]; //指定path的渲染颜色,这里可以设置任意不透明颜色
    _progressLayer.opacity = 1; //背景颜色的透明度
    _progressLayer.lineCap = kCALineCapRound;//指定线的边缘是圆的
    _progressLayer.lineWidth = 10;//线的宽度
    UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:center radius:radius startAngle:startA endAngle:endA clockwise:YES];//上面说明过了用来构建圆形
    _progressLayer.path =[path CGPath]; //把path传递給layer，然后layer会处理相应的渲染，整个逻辑和CoreGraph是一致的。
    [self.maxSuduView.layer insertSublayer:_progressLayer atIndex:1];
//    [self.maxSuduView.layer addSublayer:_progressLayer];
    
    
    //gradientLayer 上左边从下到上放一个渐变色 右边 从上到下放一个就按变色  最上面为中间值   中间颜色 和 起始颜色 还有结束颜色都从创建的时候传过来
    //这里黄色  255 255 0 到 红色 255  0  0  所以中间色是  255 255/2 0
    
    self.gradientLayer = [CAGradientLayer layer];
    
    //左边的渐变图层
    CAGradientLayer *leftGradientLayer = [CAGradientLayer layer];
    leftGradientLayer.frame = CGRectMake(0, 0, self.maxSuduView.width / 2, self.maxSuduView.height);
    [leftGradientLayer setColors:[NSArray arrayWithObjects:(id)_startColor.CGColor, (id)_middleColor.CGColor, nil]];
    [leftGradientLayer setLocations:@[@0,@0.9]];
    [leftGradientLayer setStartPoint:CGPointMake(0, 1)];
    [leftGradientLayer setEndPoint:CGPointMake(1, 0)];
    [_gradientLayer addSublayer:leftGradientLayer];

    CAGradientLayer *rightGradientLayer = [CAGradientLayer layer];
    rightGradientLayer.frame = CGRectMake(self.maxSuduView.width / 2, 0, self.maxSuduView.width / 2, self.maxSuduView.height);
    [rightGradientLayer setColors:[NSArray arrayWithObjects:(id)_middleColor.CGColor, (id)_endcolor.CGColor, nil]];
    [rightGradientLayer setLocations:@[@0.1, @1]];
    [rightGradientLayer setStartPoint:CGPointMake(0.5, 0)];
    [rightGradientLayer setEndPoint:CGPointMake(0.5, 1)];
    [_gradientLayer addSublayer:rightGradientLayer];
     
    [self.gradientLayer setMask:_progressLayer];
    self.gradientLayer.frame = self.maxSuduView.bounds;
    
    [self.maxSuduView.layer addSublayer:self.gradientLayer];
    
    
}

-(CGPoint)getPicLocCenter:(double)angle andCenter:(CGPoint)center radius:(float)radius{
    
    CGFloat x = 0.0;
    
    CGFloat y = 0.0;
    
    x = center.x + cosf(angle)*radius;
    
    y = center.y + sinf(angle)*radius;
    
    CGPoint p = CGPointMake(x, y);
    return p;
}

@end
