//
//  UBMWaMapReGeocodeSearchRequest.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/10/20.
//  Copyright © 2020 魏振伟. All rights reserved.
//

#import <AMapSearchKit/AMapSearchKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UBMWaMapReGeocodeSearchRequest : AMapReGeocodeSearchRequest

@property(nonatomic, assign)int wtag;

@end

NS_ASSUME_NONNULL_END
