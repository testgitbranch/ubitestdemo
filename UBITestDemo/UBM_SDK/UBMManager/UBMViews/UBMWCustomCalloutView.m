//
//  CustomCalloutView.m
//  CoreMotionOC
//
//  Created by wzw on 2020/10/13.
//  Copyright © 2020 LoveCarHome. All rights reserved.
//

#import "UBMWCustomCalloutView.h"
#import "UBMPrefixHeader.h"

#define kArrorHeight        10

#define kPortraitMargin     0
#define kPortraitWidth      70
#define kPortraitHeight     50

#define kTitleWidth         120
#define kTitleHeight        20

@interface UBMWCustomCalloutView ()

@property (nonatomic, strong) UIImageView *portraitView;
@property (nonatomic, strong) UILabel *subtitleLabel;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel* bottomLab;

@end

@implementation UBMWCustomCalloutView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.backgroundColor = UIColor.clearColor;
        [self initSubViews];
    }
    return self;
}

- (void)initSubViews
{
    // 添加图片，即商户图
//    self.portraitView = [[UIImageView alloc] initWithFrame:CGRectMake(kPortraitMargin, kPortraitMargin, kPortraitWidth, kPortraitHeight)];
//
//    self.portraitView.backgroundColor = [UIColor blackColor];
//    [self addSubview:self.portraitView];
    
    // 添加标题，即商户名
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 6, self.frame.size.width, 20)];
    self.titleLabel.font = [UIFont boldSystemFontOfSize:12];
    self.titleLabel.textColor = ColorSet(0, 0, 0, 0.85);;
    self.titleLabel.text = @"titletitletitletitle";
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.titleLabel];
    
    // 添加副标题，即商户地址
    self.subtitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.titleLabel.bottom, self.frame.size.width, 24)];
    self.subtitleLabel.font = [UIFont systemFontOfSize:10];
    self.subtitleLabel.textColor = ColorSet(0, 0, 0, 0.65);
    self.subtitleLabel.text = @"subtitleLabelsubtitleLabelsubtitleLabel";
    self.subtitleLabel.textAlignment = NSTextAlignmentCenter;
    self.subtitleLabel.numberOfLines = 0;
    [self addSubview:self.subtitleLabel];
    
    //第三个Lable
    UILabel* bottomLab = [[UILabel alloc] initWithFrame:CGRectMake(0, self.subtitleLabel.bottom, self.frame.size.width, 16)];
    bottomLab.font = [UIFont systemFontOfSize:10];
    bottomLab.textColor = ColorSet(0, 0, 0, 0.65);
    bottomLab.textAlignment = NSTextAlignmentCenter;
    [self addSubview:bottomLab];
    self.bottomLab = bottomLab;
}

- (void)drawRect:(CGRect)rect
{
    
    [self drawInContext:UIGraphicsGetCurrentContext()];
    
//    self.layer.shadowColor = [[UIColor whiteColor] CGColor];
//    self.layer.shadowOpacity = 1.0;
//    self.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    
}

- (void)drawInContext:(CGContextRef)context
{
    
    CGContextSetLineWidth(context, 2.0);
    CGContextSetFillColorWithColor(context, ColorSet(255, 255, 255, 0.85).CGColor);
    
    [self getDrawPath:context];
    CGContextFillPath(context);
    
}

- (void)getDrawPath:(CGContextRef)context
{
    CGRect rrect = self.bounds;
    CGFloat radius = 6.0;
    CGFloat minx = CGRectGetMinX(rrect),
    midx = CGRectGetMidX(rrect),
    maxx = CGRectGetMaxX(rrect);
    CGFloat miny = CGRectGetMinY(rrect),
    maxy = CGRectGetMaxY(rrect)-kArrorHeight;
    
    CGContextMoveToPoint(context, midx+kArrorHeight, maxy);
    CGContextAddLineToPoint(context,midx, maxy+kArrorHeight);
    CGContextAddLineToPoint(context,midx-kArrorHeight, maxy);
    
    CGContextAddArcToPoint(context, minx, maxy, minx, miny, radius);
    CGContextAddArcToPoint(context, minx, minx, maxx, miny, radius);
    CGContextAddArcToPoint(context, maxx, miny, maxx, maxx, radius);
    CGContextAddArcToPoint(context, maxx, maxy, midx, maxy, radius);
    CGContextClosePath(context);
}

- (void)setTitle:(NSString *)title
{
    self.titleLabel.text = title;
}

- (void)setSubtitle:(NSString *)subtitle
{
    self.subtitleLabel.text = subtitle;
    
//    self.subtitleLabel.text = @"很长很长很长很长很长很长很长很长很长很长很长很长很长很长";
}

- (void)setImage:(UIImage *)image
{
    self.portraitView.image = image;
}

-(void)setBottmTitle:(NSString *)bottmTitle{
    
    self.bottomLab.text = bottmTitle;
}
@end
