//
//  UBMHistoryItemBottomItemView.m
//  LoveCarChannel_iOS
//
//  Created by 王国松 on 2021/6/3.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

#import "UBMHistoryItemBottomItemView.h"
#import "UBMPrefixHeader.h"

@implementation UBMHistoryItemBottomItemView

- (id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}

- (void)setupView{
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.85];
    [self addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.equalTo(self);
    }];
    self.bottomButton = [[UBMHistoryBottomButton alloc] init];
    [self.bottomButton setTitleColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.85] forState:UIControlStateNormal];
    self.bottomButton.titleLabel.font = [UIFont systemFontOfSize:14];
    [self addSubview:self.bottomButton];
    [self.bottomButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.bottom.equalTo(self);
        make.top.equalTo(self.titleLabel.mas_bottom).offset(4);
        make.width.mas_equalTo(79);
    }];
}

@end
