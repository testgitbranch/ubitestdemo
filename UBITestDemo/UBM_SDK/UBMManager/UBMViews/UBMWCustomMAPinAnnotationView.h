//
//  wCustomMAPinAnnotationView.h
//  CoreMotionOC
//
//  Created by wzw on 2020/10/13.
//  Copyright © 2020 LoveCarHome. All rights reserved.
//

/*
 自定义MAPinAnnotationView，重写selected方法，控制calloutView的出现和消失
 */

#import "UBMWCustomCalloutView.h"
#import "UBMWCustomAnnotation.h"

NS_ASSUME_NONNULL_BEGIN

@interface UBMWCustomMAPinAnnotationView : MAAnnotationView

@property(nonatomic, strong) UBMWCustomCalloutView* calloutView;

@end

NS_ASSUME_NONNULL_END
