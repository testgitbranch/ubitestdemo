//
//  UBMRouteShareView.m
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/10/17.
//  Copyright © 2020 魏振伟. All rights reserved.
//

#import "UBMRouteShareView.h"
#import "UBMMeterView.h"
#import "UBMShareEventCell.h"
#import "UBMPrefixHeader.h"
#import "UIStackView+UBMSeparator.h"
#import "UBMMathTool.h"
#import "UBMConfigure.h"

@interface UBMRouteShareView()
// 外部参数
@property (nonatomic, strong) NSDictionary *dicx;
@property (nonatomic, strong) UIImage *mapView;
// 地图图片
@property (nonatomic, strong) UIImageView *mapmgv;
// 头像
@property (nonatomic, strong) UIImageView *imgv;
@property (nonatomic, strong) UILabel *userNameLab;
// 行程事件
@property (nonatomic, strong) UIView *eventsContainerView;
@property (nonatomic, strong) UIView *eventsLongSeparator;
@property (nonatomic, strong) UIView *eventsLongSeparatorBackMask;
@property (nonatomic, strong) UIStackView *eventsStackView;
@property (nonatomic, strong) UBMShareEventCell *eventCellView;
@property (nonatomic, copy) NSArray *eventArray;
@property (nonatomic, assign) BOOL hasEvent;
// 快慢线
@property (nonatomic, strong) UIView *tuliView;
@property (nonatomic, strong) UILabel *kuaiLab;
@property (nonatomic, strong) UILabel *manLab;
@property (nonatomic, strong) UIView *gLine;

// 主要信息
@property (nonatomic, strong) UIView *mainView;
@property (nonatomic, assign) BOOL isAbnormalTrip;
@property (nonatomic, assign) NSInteger evaluateLevel;
/// 驾驶分数
@property (nonatomic, strong) UIView *scoreContainerView;
@property (nonatomic, strong) UIImageView *scoreMeterBorderImageView;
@property (nonatomic, strong) UIImageView *scoreMeterBaseImageView;
@property (nonatomic, strong) UIImageView *scoreMeterScaleImageView;
@property (nonatomic, strong) UBMMeterView *scoreMeterView;
@property (nonatomic, strong) UIView *scoreSafetyContentView;
@property (nonatomic, strong) UILabel *scoreSafety;
@property (nonatomic, strong) UILabel *scoreSafetyLabel;
@property (nonatomic, strong) UIImageView *scoreStampImageView;
@property (nonatomic, assign) NSInteger score;
/// 时间信息
@property (nonatomic, strong) UILabel *timestampLabel;
/// 行程信息
@property (nonatomic, strong) UIView *tripContainerView;
@property (nonatomic, strong) UILabel *tripMileage;
@property (nonatomic, strong) UILabel *tripMileageTitle;
@property (nonatomic, strong) UIView *tripLine1;
@property (nonatomic, strong) UILabel *tripDuration;
@property (nonatomic, strong) UILabel *tripDurationTitle;
@property (nonatomic, strong) UIView *tripLine2;
@property (nonatomic, strong) UILabel *tripMaxSpeed;
@property (nonatomic, strong) UILabel *tripMaxSpeedTitle;
@property (nonatomic, strong) UIView *tripLine3;
@property (nonatomic, strong) UILabel *tripAvgSpeed;
@property (nonatomic, strong) UILabel *tripAvgSpeedTitle;
/// 地点信息
@property (nonatomic, strong) UIView *placeContainerView;
@property (nonatomic, strong) UIView *placeBeginPoint;
@property (nonatomic, strong) UIView *placeEndPoint;
@property (nonatomic, strong) UIView *placeLine;
@property (nonatomic, strong) UILabel *placeBeginLabel;
@property (nonatomic, strong) UILabel *placeEndLabel;
/// logo图片
@property (nonatomic, strong) UIView *logoContainerView;
@property (nonatomic, strong) UIImageView *logoImageView;
@end

@implementation UBMRouteShareView
- (instancetype)initWithFrame:(CGRect)frame andMapview:(UIImage *)mapview andOtherData:(NSDictionary *)dicx{
    self = [super initWithFrame:frame];
    if (self) {
        self.mapView = mapview;
        self.dicx = dicx;
        [self setup];
    }
    return self;
}

- (void)setup {
    [self setupModels];
    [self setupUI];
    [self setupLayout];
    [self setupAppearce];
    [self layoutIfNeeded];
}

- (void)setupUI {
    // 地图信息
    [self addSubview:self.mapmgv];
    [self addSubview:self.imgv];
    [self addSubview:self.userNameLab];
    [self addSubview:self.tuliView];
    [self.tuliView addSubview:self.manLab];
    [self.tuliView addSubview:self.gLine];
    [self.tuliView addSubview:self.kuaiLab];
    // 事件信息
    if (self.hasEvent == YES) {
        [self addSubview:self.eventsContainerView];
        [self.eventsContainerView addSubview:self.eventsStackView];
        [self.eventsContainerView addSubview:self.eventsLongSeparatorBackMask];
        [self.eventsContainerView addSubview:self.eventsLongSeparator];
    }
    // 主要信息
    [self addSubview:self.mainView];
    /// 驾驶评分
    [self.mainView addSubview:self.scoreContainerView];
    [self.scoreContainerView addSubview:self.scoreMeterBorderImageView];
    [self.scoreContainerView addSubview:self.scoreMeterBaseImageView];
    [self.scoreContainerView addSubview:self.scoreMeterView];
    [self.scoreContainerView addSubview:self.scoreMeterScaleImageView];
    [self.scoreContainerView addSubview:self.scoreSafetyContentView];
    [self.scoreSafetyContentView addSubview:self.scoreSafety];
    [self.scoreSafetyContentView addSubview:self.scoreSafetyLabel];
    [self.scoreContainerView addSubview:self.scoreStampImageView];
    /// 时间信息
    [self.mainView addSubview:self.timestampLabel];
    /// 行程信息
    [self.mainView addSubview:self.tripContainerView];
    [self.tripContainerView addSubview:self.tripMileage];
    [self.tripContainerView addSubview:self.tripMileageTitle];
    [self.tripContainerView addSubview:self.tripLine1];
    [self.tripContainerView addSubview:self.tripDuration];
    [self.tripContainerView addSubview:self.tripDurationTitle];
    [self.tripContainerView addSubview:self.tripLine2];
    [self.tripContainerView addSubview:self.tripMaxSpeed];
    [self.tripContainerView addSubview:self.tripMaxSpeedTitle];
    [self.tripContainerView addSubview:self.tripLine3];
    [self.tripContainerView addSubview:self.tripAvgSpeed];
    [self.tripContainerView addSubview:self.tripAvgSpeedTitle];
    /// 地点信息
    [self.mainView addSubview:self.placeContainerView];
    [self.placeContainerView addSubview:self.placeBeginPoint];
    [self.placeContainerView addSubview:self.placeEndPoint];
    [self.placeContainerView addSubview:self.placeLine];
    [self.placeContainerView addSubview:self.placeBeginLabel];
    [self.placeContainerView addSubview:self.placeEndLabel];
    /// logo图片
    [self.mainView addSubview:self.logoContainerView];
    [self.logoContainerView addSubview:self.logoImageView];
}

#pragma mark - 地图信息
- (UIImageView *)mapmgv {
    if (!_mapmgv) {
        _mapmgv = [[UIImageView alloc] init];
        _mapmgv.contentMode = UIViewContentModeScaleAspectFill;
        _mapmgv.image = self.mapView;
    }
    return _mapmgv;
}

- (UIImageView *)imgv {
    if (!_imgv) {
        _imgv = [[UIImageView alloc] init];
        _imgv.layer.cornerRadius = 16;
        _imgv.layer.masksToBounds = YES;
        _imgv.layer.borderWidth = 1;
        _imgv.layer.borderColor = [UIColor ubmColorWithHexNum:0xFFFFFF].CGColor;
        [_imgv sd_setImageWithURL:[NSURL URLWithString:UBMUserModel.shared.portrait]];
    }
    return _imgv;
}

- (UILabel *)userNameLab {
    if (!_userNameLab) {
        _userNameLab = [UBMViewTools lableWithText:@"--" font:[UIFont systemFontOfSize:18 weight:UIFontWeightSemibold] textColor:[UIColor ubmColorWithHexNum:0xFFFFFF]];
        _userNameLab.textAlignment = NSTextAlignmentLeft;
        _userNameLab.text = UBMUserModel.shared.nickname;
        [_userNameLab sizeToFit];
    }
    return _userNameLab;
}

- (UIView *)eventsContainerView {
    if (!_eventsContainerView) {
        _eventsContainerView = [[UIView alloc] init];
    }
    return _eventsContainerView;
}

- (UIView *)eventsLongSeparatorBackMask {
    if (!_eventsLongSeparatorBackMask) {
        _eventsLongSeparatorBackMask = [[UIView alloc] init];
        _eventsLongSeparatorBackMask.backgroundColor = [UIColor ubmColorWithHexNum:0x232E53 alpha:0.95];
    }
    return _eventsLongSeparatorBackMask;
}

- (UIView *)eventsLongSeparator {
    if (!_eventsLongSeparator) {
        _eventsLongSeparator = [[UIView alloc] init];
        _eventsLongSeparator.backgroundColor = [UIColor ubmColorWithHexNum:0x000000 alpha:0.25];
    }
    return _eventsLongSeparator;
}

- (UIStackView *)eventsStackView {
    if (!_eventsStackView) {
        _eventsStackView = [[UIStackView alloc] init];
        _eventsStackView.spacing = 0.5;
        _eventsStackView.axis = UILayoutConstraintAxisVertical;
        _eventsStackView.alignment = UIStackViewAlignmentCenter;
        _eventsStackView.distribution = UIStackViewDistributionEqualSpacing;
        _eventsStackView.separatorColor = [UIColor ubmColorWithHexNum:0xFFFFFF alpha:0.08];
        _eventsStackView.separatorThickness = 0.5;
        _eventsStackView.separatorLength = 40;
        
        NSInteger eventTotal = 0;
        UBMShareEventCell *totalCell = [[UBMShareEventCell alloc] initWithType:ShareEventCellTypeTotal];
        [_eventsStackView addArrangedSubview:totalCell];
        for (NSDictionary *dict in self.eventArray) {
            UBMShareEventCell *singleCell = [[UBMShareEventCell alloc] init];
            singleCell.eventNum.text = [NSString stringWithFormat:@"%@", dict[@"num"]];
            singleCell.eventName.text = dict[@"type_text"];
            [_eventsStackView addArrangedSubview:singleCell];
            eventTotal += [dict[@"num"] integerValue];
        }
        totalCell.eventNum.text = [NSString stringWithFormat:@"%ld", eventTotal];
        totalCell.eventName.text = @"安全隐患";
    }
    return _eventsStackView;
}

- (UIView *)tuliView {
    if (!_tuliView) {
        _tuliView = [[UIView alloc] init];
    }
    return _tuliView;
}

- (UILabel *)manLab {
    if (!_manLab) {
        _manLab = [[UILabel alloc] init];
        _manLab.text = @"慢";
        _manLab.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
        _manLab.textAlignment = NSTextAlignmentCenter;
        _manLab.textColor = [UIColor ubmColorWithHexNum:0xFFFFFF alpha:0.85];
    }
    return _manLab;
}

- (UILabel *)kuaiLab {
    if (!_kuaiLab) {
        _kuaiLab = [[UILabel alloc] init];
        _kuaiLab.text = @"快";
        _kuaiLab.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
        _kuaiLab.textAlignment = NSTextAlignmentCenter;
        _kuaiLab.textColor = [UIColor ubmColorWithHexNum:0xFFFFFF alpha:0.85];
    }
    return _kuaiLab;
}

- (UIView *)gLine {
    if (!_gLine) {
        _gLine = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 45, 2)];
        _gLine.layer.cornerRadius = 1.5;
        _gLine.layer.masksToBounds = YES;
        CAGradientLayer *gl = [CAGradientLayer layer];
        gl.frame = CGRectMake(0, 0, _gLine.width, 2);
        gl.startPoint = CGPointMake(0, 0.5);
        gl.endPoint = CGPointMake(1, 0.5);
        gl.colors = @[(__bridge id)[UIColor ubmColorWithHexNum:0xFF7900].CGColor, (__bridge id)[UIColor ubmColorWithHexNum:0x89DE00].CGColor];
        gl.locations = @[@(0), @(1.0f)];
        [_gLine.layer addSublayer:gl];
    }
    return _gLine;
}

#pragma mark - 主要信息
- (UIView *)mainView {
    if (!_mainView) {
        _mainView = [[UIView alloc] init];
    }
    return _mainView;
}

#pragma mark - 驾驶分数
- (UIView *)scoreContainerView {
    if (!_scoreContainerView) {
        _scoreContainerView = [[UIView alloc] init];
    }
    return _scoreContainerView;
}

- (UIImageView *)scoreMeterBorderImageView {
    if (!_scoreMeterBorderImageView) {
        _scoreMeterBorderImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ubm_score_meter_border_dark"]];
    }
    return _scoreMeterBorderImageView;
}

- (UIImageView *)scoreMeterBaseImageView {
    if (!_scoreMeterBaseImageView) {
        _scoreMeterBaseImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ubm_score_meter_base_dark"]];
    }
    return _scoreMeterBaseImageView;
}

- (UIImageView *)scoreMeterScaleImageView {
    if (!_scoreMeterScaleImageView) {
        _scoreMeterScaleImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ubm_score_meter_scale_dark"]];
    }
    return _scoreMeterScaleImageView;
}

- (UBMMeterView *)scoreMeterView {
    if (!_scoreMeterView) {
        _scoreMeterView = [[UBMMeterView alloc] initWithFrame:CGRectMake(0, 0, 120, 120)];
        if (self.evaluateLevel>=0 && self.evaluateLevel<=4) {
            [self drawScoreMeterProgressWithScoreValue:self.score];
        }
    }
    return _scoreMeterView;
}

- (void)drawScoreMeterProgressWithScoreValue:(NSInteger)value {
    [self.scoreMeterView drawProgressWithLineWidth:20 minAngle:[UBMMathTool radianFromDegree:155] maxAngle:[UBMMathTool radianFromDegree:25] fillColor:UIColor.clearColor strokeColor:UIColor.whiteColor progressValue:value fullValue:100];
    [self.scoreMeterView setColorGrad:[NSArray arrayWithObjects:(id)[UIColor ubmColorWithHexNum:0x37C6FF].CGColor, (id)[UIColor ubmColorWithHexNum:0x2B58FF].CGColor, nil]];
}

- (UIView *)scoreSafetyContentView {
    if (!_scoreSafetyContentView) {
        _scoreSafetyContentView = [[UIView alloc] init];
    }
    return _scoreSafetyContentView;
}

- (UILabel *)scoreSafety {
    if (!_scoreSafety) {
        _scoreSafety = [[UILabel alloc] init];
        _scoreSafety.text = (self.evaluateLevel>=0 && self.evaluateLevel<=4) == YES ? [NSString stringWithFormat:@"%ld", [self.dicx[@"score"] integerValue]] : @"- -";
        _scoreSafety.textColor = [UIColor ubmColorWithHexNum:0xFFFFFF];
        _scoreSafety.textAlignment = NSTextAlignmentCenter;
        _scoreSafety.font = [UIFont systemFontOfSize:36 weight:UIFontWeightBold];
    }
    return _scoreSafety;
}

- (UILabel *)scoreSafetyLabel {
    if (!_scoreSafetyLabel) {
        _scoreSafetyLabel = [[UILabel alloc] init];
        _scoreSafetyLabel.text = @"驾驶安全分";
        _scoreSafetyLabel.textColor = [UIColor ubmColorWithHexNum:0xFFFFFF];
        _scoreSafetyLabel.textAlignment = NSTextAlignmentCenter;
        _scoreSafetyLabel.font = [UIFont systemFontOfSize:12 weight:UIFontWeightRegular];
    }
    return _scoreSafetyLabel;
}

- (UIImageView *)scoreStampImageView {
    if (!_scoreStampImageView) {
        _scoreStampImageView = [[UIImageView alloc] init];
        if (self.evaluateLevel == 1) {
            _scoreStampImageView.image = [UIImage imageFromUbiBundleWithName:@"ubm_inforType3"];
        }else if (self.evaluateLevel == 2){
            _scoreStampImageView.image = [UIImage imageFromUbiBundleWithName:@"ubm_inforType2"];
        }else if (self.evaluateLevel == 3){
            _scoreStampImageView.image = [UIImage imageFromUbiBundleWithName:@"ubm_inforType4"];
        }else if (self.evaluateLevel == 4){
            _scoreStampImageView.image = [UIImage imageFromUbiBundleWithName:@"ubm_inforType1"];
        }else{
            _scoreStampImageView.image = [UIImage imageFromUbiBundleWithName:@"ubm_inforType5"];
        }
        BOOL isNeedShowMarkImageState = [_dicx[@"not_driver_mark"] integerValue] == 1;
        if (isNeedShowMarkImageState) {
            _scoreStampImageView.image = [UIImage imageFromUbiBundleWithName:@"ubm_inforType6"];
        }
    }
    return _scoreStampImageView;
}

#pragma mark - 时间信息
- (UILabel *)timestampLabel {
    if (!_timestampLabel) {
        _timestampLabel = [[UILabel alloc] init];
        _timestampLabel.text = [NSString stringWithFormat:@"行程时间: %@", [[NSDate dateFromTimestamp:self.dicx[@"start_time"] withFormat:@"yyyy-MM-dd HH:mm"] toTimestampWithFormat:@"yyyy年M月d日 HH:mm"]];
        _timestampLabel.textColor = [UIColor ubmColorWithHexNum:0xFFFFFF alpha:0.85];
        _timestampLabel.textAlignment = NSTextAlignmentRight;
        _timestampLabel.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
    }
    return _timestampLabel;
}

#pragma mark - 行程信息
- (UIView *)tripContainerView {
    if (!_tripContainerView) {
        _tripContainerView = [[UIView alloc] init];
    }
    return _tripContainerView;
}

- (UILabel *)tripMileage {
    if (!_tripMileage) {
        _tripMileage = [[UILabel alloc] init];
        _tripMileage.text = ChangeToString(self.dicx[@"mileage_in_kilometre"]);
        _tripMileage.textColor = [UIColor ubmColorWithHexNum:0xFFFFFF];
        _tripMileage.textAlignment = NSTextAlignmentCenter;
        _tripMileage.font = [UIFont systemFontOfSize:18 weight:UIFontWeightBold];
    }
    return _tripMileage;
}

- (UILabel *)tripMileageTitle {
    if (!_tripMileageTitle) {
        _tripMileageTitle = [[UILabel alloc] init];
        _tripMileageTitle.text = @"里程 km";
        _tripMileageTitle.textColor = [UIColor ubmColorWithHexNum:0xFFFFFF alpha:0.65];
        _tripMileageTitle.textAlignment = NSTextAlignmentCenter;
        _tripMileageTitle.font = [UIFont systemFontOfSize:12 weight:UIFontWeightRegular];
    }
    return _tripMileageTitle;
}

- (UIView *)tripLine1 {
    if (!_tripLine1) {
        _tripLine1 = [[UIView alloc] init];
        _tripLine1.backgroundColor = [UIColor ubmColorWithHexNum:0x000000 alpha:0.06];
    }
    return _tripLine1;
}

- (UILabel *)tripDuration {
    if (!_tripDuration) {
        _tripDuration = [[UILabel alloc] init];
        _tripDuration.text = ChangeToString(self.dicx[@"duration"]);
        _tripDuration.textColor = [UIColor ubmColorWithHexNum:0xFFFFFF];
        _tripDuration.textAlignment = NSTextAlignmentCenter;
        _tripDuration.font = [UIFont systemFontOfSize:18 weight:UIFontWeightBold];
    }
    return _tripDuration;
}

- (UILabel *)tripDurationTitle {
    if (!_tripDurationTitle) {
        _tripDurationTitle = [[UILabel alloc] init];
        _tripDurationTitle.text = @"时长";
        _tripDurationTitle.textColor = [UIColor ubmColorWithHexNum:0xFFFFFF alpha:0.65];
        _tripDurationTitle.textAlignment = NSTextAlignmentCenter;
        _tripDurationTitle.font = [UIFont systemFontOfSize:12 weight:UIFontWeightRegular];
    }
    return _tripDurationTitle;
}

- (UIView *)tripLine2 {
    if (!_tripLine2) {
        _tripLine2 = [[UIView alloc] init];
        _tripLine2.backgroundColor = [UIColor ubmColorWithHexNum:0x000000 alpha:0.06];
    }
    return _tripLine2;
}

- (UILabel *)tripMaxSpeed {
    if (!_tripMaxSpeed) {
        _tripMaxSpeed = [[UILabel alloc] init];
        _tripMaxSpeed.text = ChangeToString(self.dicx[@"max_speed_in_kilometers_per_hour"]);
        _tripMaxSpeed.textColor = [UIColor ubmColorWithHexNum:0xFFFFFF];
        _tripMaxSpeed.textAlignment = NSTextAlignmentCenter;
        _tripMaxSpeed.font = [UIFont systemFontOfSize:18 weight:UIFontWeightBold];
    }
    return _tripMaxSpeed;
}

- (UILabel *)tripMaxSpeedTitle {
    if (!_tripMaxSpeedTitle) {
        _tripMaxSpeedTitle = [[UILabel alloc] init];
        _tripMaxSpeedTitle.text = @"最高速 km/h";
        _tripMaxSpeedTitle.textColor = [UIColor ubmColorWithHexNum:0xFFFFFF alpha:0.65];
        _tripMaxSpeedTitle.textAlignment = NSTextAlignmentCenter;
        _tripMaxSpeedTitle.font = [UIFont systemFontOfSize:12 weight:UIFontWeightRegular];
    }
    return _tripMaxSpeedTitle;
}

- (UIView *)tripLine3 {
    if (!_tripLine3) {
        _tripLine3 = [[UIView alloc] init];
        _tripLine3.backgroundColor = [UIColor ubmColorWithHexNum:0x000000 alpha:0.06];
    }
    return _tripLine3;
}

- (UILabel *)tripAvgSpeed {
    if (!_tripAvgSpeed) {
        _tripAvgSpeed = [[UILabel alloc] init];
        _tripAvgSpeed.text = ChangeToString(self.dicx[@"avg_speed_in_kilometers_per_hour"]);
        _tripAvgSpeed.textColor = [UIColor ubmColorWithHexNum:0xFFFFFF];
        _tripAvgSpeed.textAlignment = NSTextAlignmentCenter;
        _tripAvgSpeed.font = [UIFont systemFontOfSize:18 weight:UIFontWeightBold];
    }
    return _tripAvgSpeed;
}

- (UILabel *)tripAvgSpeedTitle {
    if (!_tripAvgSpeedTitle) {
        _tripAvgSpeedTitle = [[UILabel alloc] init];
        _tripAvgSpeedTitle.text = @"均速 km/h";
        _tripAvgSpeedTitle.textColor = [UIColor ubmColorWithHexNum:0xFFFFFF alpha:0.65];
        _tripAvgSpeedTitle.textAlignment = NSTextAlignmentCenter;
        _tripAvgSpeedTitle.font = [UIFont systemFontOfSize:12 weight:UIFontWeightRegular];
    }
    return _tripAvgSpeedTitle;
}

#pragma mark - logo图片
- (UIImageView *)logoImageView {
    if (!_logoImageView) {
        _logoImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ubm_shareBottomImg"]];
        _logoImageView.contentMode = UIViewContentModeScaleToFill;
    }
    return _logoImageView;
}

#pragma mark - 地点信息
- (UIView *)placeContainerView {
    if (!_placeContainerView) {
        _placeContainerView = [[UIView alloc] init];
    }
    return _placeContainerView;
}

- (UIView *)placeBeginPoint {
    if (!_placeBeginPoint) {
        _placeBeginPoint = [[UIView alloc] init];
        _placeBeginPoint.backgroundColor = [UIColor ubmColorWithHexNum:0x52C41A];
        _placeBeginPoint.layer.cornerRadius = 3;
        _placeBeginPoint.layer.masksToBounds = YES;
    }
    return _placeBeginPoint;
}

- (UIView *)placeEndPoint {
    if (!_placeEndPoint) {
        _placeEndPoint = [[UIView alloc] init];
        _placeEndPoint.backgroundColor = [UIColor ubmColorWithHexNum:0xFF4B41];
        _placeEndPoint.layer.cornerRadius = 3;
        _placeEndPoint.layer.masksToBounds = YES;
    }
    return _placeEndPoint;
}

- (UIView *)placeLine {
    if (!_placeLine) {
        _placeLine = [[UIView alloc] init];
        _placeLine.backgroundColor = [UIColor ubmColorWithHexNum:0xD8D8D8];
    }
    return _placeLine;
}

- (UILabel *)placeBeginLabel {
    if (!_placeBeginLabel) {
        _placeBeginLabel = [[UILabel alloc] init];
        _placeBeginLabel.text = self.dicx[@"start_poi"];
        _placeBeginLabel.textColor = [UIColor ubmColorWithHexNum:0xFFFFFF];
        _placeBeginLabel.textAlignment = NSTextAlignmentLeft;
        _placeBeginLabel.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
    }
    return _placeBeginLabel;
}

- (UILabel *)placeEndLabel {
    if (!_placeEndLabel) {
        _placeEndLabel = [[UILabel alloc] init];
        _placeEndLabel.text = self.dicx[@"end_poi"];
        _placeEndLabel.textColor = [UIColor ubmColorWithHexNum:0xFFFFFF];
        _placeEndLabel.textAlignment = NSTextAlignmentLeft;
        _placeEndLabel.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
    }
    return _placeEndLabel;
}

- (UIView *)logoContainerView {
    if (!_logoContainerView) {
        _logoContainerView = [[UIView alloc] init];
        _logoContainerView.backgroundColor = UIColor.clearColor;
    }
    return _logoContainerView;
}

- (void)setupLayout {
    // 地图信息
    [self.mapmgv mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.mapmgv.superview);
        make.height.mas_equalTo(600);
    }];
    
    [self.imgv mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.imgv.superview).offset(30);
        make.left.equalTo(self.imgv.superview).offset(20);
        make.size.mas_equalTo(32);
    }];
    [self.userNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.imgv.mas_right).offset(10);
        make.centerY.equalTo(self.imgv);
    }];
    
    // 事件信息
    if (self.hasEvent == YES) {
        [self.eventsContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.eventsContainerView.superview).offset(30);
            make.right.equalTo(self.eventsContainerView.superview).offset(-14);
        }];
        [self.eventsStackView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.eventsStackView.superview).offset(6);
            make.bottom.equalTo(self.eventsStackView.superview).offset(-6);
            make.left.right.equalTo(self.eventsStackView.superview);
        }];
        [self.eventsLongSeparatorBackMask mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.eventsLongSeparator.superview).offset(62);
            make.left.right.equalTo(self.eventsLongSeparator.superview);
            make.height.mas_equalTo(0.5);
        }];
        [self.eventsLongSeparator mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self.eventsLongSeparatorBackMask);
            make.size.equalTo(self.eventsLongSeparatorBackMask);
        }];
    }
    
    [self.tuliView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.tuliView.superview).offset(458);
        make.right.equalTo(self.tuliView.superview).offset(-16);
    }];
    [self.kuaiLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.right.bottom.equalTo(self.kuaiLab.superview);
    }];
    [self.gLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(45);
        make.height.mas_equalTo(2);
        make.centerY.equalTo(self.kuaiLab);
        make.right.equalTo(self.kuaiLab.mas_left).offset(-6);
    }];
    [self.manLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.gLine);
        make.right.equalTo(self.gLine.mas_left).offset(-6);
    }];
    
    // 主要信息
    [self.mainView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mainView.superview).offset(496);
        make.left.right.bottom.equalTo(self.mainView.superview);
    }];
    /// 驾驶分数
    [self.scoreContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.scoreContainerView.superview);
    }];
    [self.scoreMeterView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(self.scoreMeterView.width);
        make.height.mas_equalTo(self.scoreMeterView.height);
        make.top.equalTo(self.scoreMeterView.superview).offset(-35);
        make.left.equalTo(self.scoreMeterView.superview).offset(26);
    }];
    [self.scoreMeterBorderImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(134);
        make.height.mas_equalTo(40);
        make.bottom.equalTo(self.scoreMeterBorderImageView.superview.mas_top);
        make.centerX.equalTo(self.scoreMeterView);
    }];
    [self.scoreMeterBaseImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(self.scoreMeterView);
        make.height.mas_equalTo(82);
        make.centerX.equalTo(self.scoreMeterView);
        make.top.equalTo(self.scoreMeterView);
    }];
    [self.scoreMeterScaleImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(self.scoreMeterView);
        make.height.mas_equalTo(86);
        make.centerX.equalTo(self.scoreMeterView);
        make.top.equalTo(self.scoreMeterView);
    }];
    [self.scoreSafetyContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(62);
        make.height.mas_equalTo(64);
        make.centerX.equalTo(self.scoreMeterView);
        make.top.equalTo(self.scoreSafetyContentView.superview);
        make.bottom.equalTo(self.scoreSafetyContentView.superview);
    }];
    [self.scoreSafety mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.scoreSafety.superview);
        make.centerX.equalTo(self.scoreSafety.superview);
    }];
    [self.scoreSafetyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.scoreSafetyLabel.superview);
        make.centerX.equalTo(self.scoreSafetyLabel.superview);
    }];
    [self.scoreStampImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.scoreStampImageView.superview);
        make.right.equalTo(self.scoreStampImageView.superview).offset(-16);
        make.width.mas_equalTo(70);
        make.height.mas_equalTo(56);
    }];
    /// 时间信息
    [self.timestampLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.scoreContainerView.mas_bottom).offset(15);
        make.left.equalTo(self.timestampLabel.superview).offset(16);
    }];
    /// 行程信息
    [self.tripContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.timestampLabel.mas_bottom).offset(18);
        make.left.equalTo(self.tripContainerView.superview).offset(24);
    }];
    [self.tripMileageTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.equalTo(self.tripMileageTitle.superview);
    }];
    [self.tripMileage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.tripMileage.superview);
        make.bottom.equalTo(self.tripMileageTitle.mas_top).offset(-8);
        make.centerX.equalTo(self.tripMileageTitle);
    }];
    [self.tripLine1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(1);
        make.height.mas_equalTo(38);
        make.left.equalTo(self.tripLine1.superview).offset(69);
        make.centerY.equalTo(self.tripLine1.superview);
    }];
    [self.tripDurationTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.tripMileageTitle.mas_right).offset(60);
        make.centerY.equalTo(self.tripMileageTitle);
    }];
    [self.tripDuration mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.tripDurationTitle);
        make.centerY.equalTo(self.tripMileage);
    }];
    [self.tripLine2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(1);
        make.height.mas_equalTo(38);
        make.left.equalTo(self.tripLine1.mas_right).offset(93);
        make.centerY.equalTo(self.tripLine1);
    }];
    [self.tripMaxSpeedTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.tripDurationTitle.mas_right).offset(47);
        make.centerY.equalTo(self.tripDurationTitle);
    }];
    [self.tripMaxSpeed mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.tripMaxSpeedTitle);
        make.centerY.equalTo(self.tripMileage);
    }];
    [self.tripLine3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(1);
        make.height.mas_equalTo(38);
        make.left.equalTo(self.tripLine2.mas_right).offset(93);
        make.centerY.equalTo(self.tripLine2);
    }];
    [self.tripAvgSpeedTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.tripMaxSpeedTitle.mas_right).offset(30);
        make.centerY.equalTo(self.tripMaxSpeedTitle);
    }];
    [self.tripAvgSpeed mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.tripAvgSpeedTitle);
        make.centerY.equalTo(self.tripMaxSpeed);
    }];
    /// 地点信息
    [self.placeContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.tripContainerView.mas_bottom).offset(18);
        make.left.equalTo(self.placeContainerView.superview).offset(16);
    }];
    [self.placeBeginLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.placeBeginLabel.superview);
        make.left.equalTo(self.placeBeginLabel.superview).offset(14);
    }];
    [self.placeEndLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.placeBeginLabel.mas_bottom).offset(16);
        make.left.equalTo(self.placeBeginLabel);
        make.bottom.equalTo(self.placeEndLabel.superview);
    }];
    [self.placeBeginPoint mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(6);
        make.centerY.equalTo(self.placeBeginLabel);
        make.left.equalTo(self.placeBeginPoint.superview);
    }];
    [self.placeEndPoint mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(6);
        make.centerY.equalTo(self.placeEndLabel);
        make.left.equalTo(self.placeEndPoint.superview);
    }];
    [self.placeLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(1);
        make.centerX.equalTo(self.placeBeginPoint);
        make.top.equalTo(self.placeBeginPoint.mas_bottom);
        make.bottom.equalTo(self.placeEndPoint.mas_top);
    }];
    /// logo图片
    [self.logoContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.placeContainerView.mas_bottom).offset(16);
        make.left.right.bottom.equalTo(self.logoContainerView.superview);
        make.height.mas_equalTo(64);
    }];
    [self.logoImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.equalTo(self.logoImageView.superview);
    }];
}

- (void)setupModels {
    if ([self.dicx[@"is_normal"] integerValue] == 1) {
        self.evaluateLevel = [self.dicx[@"level"] integerValue];
        
    }else{
        self.evaluateLevel = 5;
    }
    
    self.eventArray = self.dicx[@"event_statistics"];
    self.score = [self.dicx[@"score"] integerValue];
    if (self.eventArray && [self.eventArray isKindOfClass:[NSArray class]] && self.eventArray.count>0 && self.score<100) {
        self.hasEvent = YES;
    }
}

- (void)setupAppearce {
    self.backgroundColor = UIColor.blackColor;
    self.mainView.backgroundColor = [UIColor ubmColorWithHexNum:0x232E53];
    if (self.hasEvent == YES) {
        self.eventsContainerView.backgroundColor = [UIColor ubmColorWithHexNum:0x232E53 alpha:0.95];
        self.eventsStackView.backgroundColor = [UIColor ubmColorWithHexNum:0x232E53 alpha:0.95];
    }
    self.scoreContainerView.backgroundColor = [UIColor ubmColorWithHexNum:0x232E53];
    self.tripContainerView.backgroundColor = [UIColor ubmColorWithHexNum:0x232E53];
    self.logoContainerView.backgroundColor = [UIColor ubmColorWithHexNum:0x232E53];
    
    self.eventsContainerView.layer.cornerRadius = 8;
    self.mainView.layer.cornerRadius = 16;
    self.scoreContainerView.layer.cornerRadius = 16;
}

//MARK:分享钢印
- (void)setImage:(UIImage *)image{
    _image = image;
    _scoreStampImageView.image = image;
}

@end
