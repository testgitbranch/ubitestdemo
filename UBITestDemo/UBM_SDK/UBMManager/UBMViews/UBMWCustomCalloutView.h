//
//  CustomCalloutView.h
//  CoreMotionOC
//
//  Created by wzw on 2020/10/13.
//  Copyright © 2020 LoveCarHome. All rights reserved.
//

/*
 自定义气泡view
 */

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UBMWCustomCalloutView : UIView

//@property (nonatomic, strong) UIImage *image; //商户图
@property (nonatomic, copy) NSString *title; //标题
@property (nonatomic, copy) NSString *subtitle; //副标题
@property (nonatomic, copy) NSString *bottmTitle; //描述

@end

NS_ASSUME_NONNULL_END
