//
//  UBMGetDeviceSomeFrame.m
//  YO
//
//  Created by wzw on 2019/7/25.
//  Copyright © 2019 DeiDaiHuLianCompany. All rights reserved.
//



#import "UBMGetDeviceSomeFrame.h"
#import "UBMPrefixHeader.h"

@implementation UBMGetDeviceSomeFrame

+ (instancetype)share
{
    static UBMGetDeviceSomeFrame* getDeviceSomeFrameobj;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        getDeviceSomeFrameobj = [[UBMGetDeviceSomeFrame alloc] init];
        
        
        getDeviceSomeFrameobj.wIsiPhoneX = NO;
        getDeviceSomeFrameobj.wHeightOfSafeBottom = 0.0;
        
        if(@available(iOS 11.0, *))
        {
            UIWindow *window1 = [UBMMTools currentWindow];
            
            if(window1.safeAreaInsets.bottom>0)
            {
                getDeviceSomeFrameobj.wIsiPhoneX = YES;
            }
            
            getDeviceSomeFrameobj.wHeightOfSafeBottom = window1.safeAreaInsets.bottom;
        }
        
        
        getDeviceSomeFrameobj.wHeightOfStatusBar = UIApplication.sharedApplication.statusBarFrame.size.height;
        
        getDeviceSomeFrameobj.wHeightOfNavBar = ZXNavBarHeight; //UIApplication.sharedApplication.statusBarFrame.size.height + 44.0;
        
        getDeviceSomeFrameobj.wHeightOfTabBar = 49.0 + getDeviceSomeFrameobj.wHeightOfSafeBottom;
        
    });
    
    
    return getDeviceSomeFrameobj;
}

@end
