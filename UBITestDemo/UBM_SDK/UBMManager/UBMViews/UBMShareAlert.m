//
//  UBMShareAlert.m
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/10/17.
//  Copyright © 2020 魏振伟. All rights reserved.
//

#import "UBMShareAlert.h"
#import "UBMPrefixHeader.h"
#import "UBMGetDeviceSomeFrame.h"

@implementation UBMShareAlert

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = ColorSet(0, 0, 0, MainAlphe);
        
        UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss)];
        [self addGestureRecognizer:tap];
        
        UIView* sheetView = [[UIView alloc] initWithFrame:CGRectMake(0, frame.size.height-254-HeightSafeBottom, frame.size.width, 284+HeightSafeBottom)];
        sheetView.backgroundColor = UIColor.whiteColor;
        [self addSubview:sheetView];
        
        UILabel* title = [UBMViewTools lableWithText:@"分享至" font:[UIFont systemFontOfSize:18 weight:UIFontWeightMedium] textColor:ColorSet(0, 0, 0, 0.85)];
        title.frame = CGRectMake(0, 18, sheetView.width, 28);
        title.textAlignment = NSTextAlignmentCenter;
        [sheetView addSubview:title];
        
        CGFloat bwidth = frame.size.width/3.0;
        CGFloat bHieght = bwidth;
        UIButton* bton1 = [UBMViewTools buttonBimgframe:CGRectMake(0, title.bottom+10, bwidth, bHieght) title: @"微信好友" Color:ColorSet(0, 0, 0, 0.85) font:14 Img:@"ubm_sharSession" ImgSize:CGSizeMake(44, 44) distance:-10.0];
        bton1.tag = 777;
        [bton1 addTarget:self action:@selector(btonIndex:) forControlEvents:UIControlEventTouchUpInside];
        [sheetView addSubview:bton1];
        
        UIButton* bton2 = [UBMViewTools buttonBimgframe:CGRectMake(bwidth, title.bottom+10, bwidth, bHieght) title: @"朋友圈" Color:ColorSet(0, 0, 0, 0.85) font:14 Img:@"ubm_sharCircle" ImgSize:CGSizeMake(44, 44) distance:-10.0];
        bton2.tag = 778;
        [bton2 addTarget:self action:@selector(btonIndex:) forControlEvents:UIControlEventTouchUpInside];
        [sheetView addSubview:bton2];
        
        UIImageView* line = [[UIImageView alloc] initWithFrame:CGRectMake(0, bton1.bottom+20, frame.size.width, 10)];
        line.backgroundColor = ColorSet(245, 245, 245, 1);
        [sheetView addSubview:line];
        
        UIButton* cancelBton = [UIButton buttonWithType:UIButtonTypeCustom];
        cancelBton.frame = CGRectMake(0, line.bottom, sheetView.width, 52);
        [cancelBton setTitle:@"取消" forState:UIControlStateNormal];
        cancelBton.titleLabel.font = [UIFont systemFontOfSize:16];
        [cancelBton setBackgroundColor:UIColor.whiteColor];
        [cancelBton setTitleColor:ColorSet(0, 0, 0, 0.85) forState:UIControlStateNormal];
        [sheetView addSubview:cancelBton];
        [cancelBton addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
        
        CGFloat sheetHeight = cancelBton.bottom;
        sheetView.frame = CGRectMake(0, frame.size.height-sheetHeight-HeightSafeBottom, frame.size.width, sheetHeight+HeightSafeBottom);
        [UBMViewTools addCornerToview:sheetView.layer andWidth:10 andPosition:ubmPTop];
    }
    return self;
}

-(void)btonIndex:(UIButton*)bton{
    
    int index = 0;
    if (bton.tag == 778) {
        index=1;
    }
    
    if (self.selectBlock) {
        self.selectBlock(index);
    }
    
    [self dismiss];
}

-(void)show{
    if(self){
        [[UBMMTools currentWindow] addSubview:self];
    }
}

-(void)dismiss{
    [self removeFromSuperview];
    
    if (self.dismissBlock) {
        self.dismissBlock();
    }
}

@end
