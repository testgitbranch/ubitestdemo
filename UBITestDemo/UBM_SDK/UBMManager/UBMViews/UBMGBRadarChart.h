//
//  UBMGBRadarChart.h
//  GBChartDemo
//
//  Created by midas on 2018/12/11.
//  Copyright © 2018 Midas. All rights reserved.
//

/*
 雷达图
 */

#import <UIKit/UIKit.h>

@class UBMGBRadarChartDataItem;

typedef NS_ENUM(NSUInteger, GBRadarChartLabelStyle) {
    GBRadarChartLabelStyleCircle = 0, //圆环
    GBRadarChartLabelStyleHorizontal, //水平
    GBRadarChartLabelStyleHidden, //隐藏
};

@interface UBMGBRadarChart : UIView

/**
 初始化图表

 @param frame frame
 @param items 模型数组
 @param unitValue 均分值
 @return 对象
 */
- (id)initWithFrame:(CGRect)frame items:(NSArray <UBMGBRadarChartDataItem *> *)items valueDivider:(CGFloat)unitValue;

/** 绘制图表 */
- (void)strokeChart;

/**
 更新图表
 @param chartData 模型数组
 */
- (void)updateChartWithChartData:(NSArray <UBMGBRadarChartDataItem *> *)chartData;

/** Array of `RadarChartDataItem` objects, one for each corner. */
@property (nonatomic, strong) NSArray <UBMGBRadarChartDataItem *> *chartDataItems;
/** 展示的样式 */
@property (nonatomic, assign) GBRadarChartLabelStyle labelStyle;
/** The unit of this chart ,default is 1 */
@property (nonatomic, assign) CGFloat valueDivider;
/** The maximum for the range of values to display on the chart */
@property (nonatomic, assign) CGFloat maxValue;
/** Default is gray. 网格线颜色*/
@property (nonatomic, strong) UIColor *webColor;
/** Default is green , with an alpha of 0.7 折线填充色 */
@property (nonatomic, strong) UIColor *plotFillColor;
/** Default is green 折线颜色*/
@property (nonatomic, strong) UIColor *plotStrokeColor;

/** Default is black  title颜色 */
@property (nonatomic, strong) UIColor *fontColor;
/** Default is 12 */
@property (nonatomic, strong) UIFont* titleFont;

/** Default is orange 刻度值文字颜色*/
@property (nonatomic, strong) UIColor *graduationColor;

/** Tap the label will display detail value ,default is YES. */
@property (nonatomic, assign) BOOL canLabelTouchable;
/** is show graduation on the chart ,default is NO. 是否显示单位刻度*/
@property (nonatomic, assign) BOOL isShowGraduation;
/** is display animated, default is YES */
@property (nonatomic, assign) BOOL displayAnimated;
/** 是否是顺时针方向绘制，默认是YES*/
@property (nonatomic, assign) BOOL clockwise;

@property (nonatomic, assign) BOOL showZebraStripe; //展示明暗条纹

@end
