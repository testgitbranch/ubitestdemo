//
//  UBMScoreChart.swift
//  LoveCarChannel_iOS
//
//  Created by wzw on 2021/5/21.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

/*
 柱状图 图表（行程分在用）
 */
import UIKit

@objcMembers open class UBMScoreChart: UIView {
    public typealias PillarSelectBack = (_ index: Int) -> Void
    
    var graduationRangeOfX = 64.0; //x刻度距离
    var graduationFontOfX = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular) //x刻度字体
    var graduationRangeOfY = 44.0; //y刻度距离
    var graduationFontOfY = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular) //y刻度字体
    
    var pillarWisth = 10.0     //柱子宽度
    var defultPillarColor:UIColor = UIColor.ubmColorWith(hex: 0x206CFF)//默认柱子颜色
    var selectPillarColor:UIColor = UIColor.ubmColorWith(hex: 0xFFAB2D) //选中柱子颜色
    
    var dataArr:Array = Array<Any>.init() //数据
    
    var chartBackScrollView:UIScrollView = UIScrollView.init() //背景view，用来左右滑动
    var chartView = UIView.init() //图表
    
    var dataArray = NSArray.init()
    
    @objc public var callBack: PillarSelectBack?
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        setupBaseView()
    }
    
    func setupBaseView() {
        
        //y线
        let pv = UBMPillarView.init()
        graduationRangeOfY = Double(self.frame.size.height - pv.heightOfHeadLab - pv.heightOfGraduationLabOfX - 10)/4.0
        for i in 0...4  {
            
            let itemY = (pv.heightOfGraduationLabOfX+CGFloat(10.0))+CGFloat(graduationRangeOfY)*CGFloat(i);
            let line = UIImageView.init(frame: CGRect.init(x: 0.0, y: itemY, width: self.frame.size.width, height: 1))
            line.backgroundColor = UIColor.ubmColorWith(hex: 0xffffff, alpha: 0.06)
            self.addSubview(line)
        }
        
        
        chartBackScrollView.frame = CGRect.init(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)
        chartBackScrollView.showsHorizontalScrollIndicator = false
        self.addSubview(chartBackScrollView)
        chartView.frame = CGRect.init(x: 0, y: 0, width: chartBackScrollView.width, height: chartBackScrollView.height)
        chartBackScrollView.addSubview(chartView)
    
    }

}

extension UBMScoreChart{
    
    @objc public func updateDataArray(_ data:NSArray) -> Void {
        
        for viewx in chartView.subviews {
            viewx.removeFromSuperview()
        }
        
        chartBackScrollView.width = self.width
        chartView.width = chartBackScrollView.width
        chartBackScrollView.contentOffset = CGPoint.init(x: 0, y: 0)
        chartBackScrollView.contentSize = CGSize.init(width: chartView.width, height: chartView.height)
        
        if data.count == 0 {
            return
        }
        
        for i in 0..<data.count {
            
            let itemx:NSDictionary = data[i] as! NSDictionary
            
            let pillarData = UBMPillarData.init(textOfX: itemx["x"] as? String ?? "", showNum: itemx["score"] as? Double ?? 0.0, totalNum: 100.0, timrstr: itemx["start_time"] as? String ?? "")
            
            let Pillar = UBMPillarView.init(frame: CGRect.init(x: graduationRangeOfX*Double(i), y: 0.0, width: graduationRangeOfX, height: Double(chartView.height)))
            Pillar.callBackBlock = { [weak self] index in
                self?.didSelectPillar(index)
            }
            Pillar.index = i
            chartView.addSubview(Pillar)
            Pillar.updateDate(pillarData)
            
            chartView.width = Pillar.right
        }
        
        //且滑到最右边
        if chartView.width>chartBackScrollView.width{
            chartBackScrollView.contentSize = CGSize.init(width: chartView.width, height: chartView.height)
            chartBackScrollView.contentOffset = CGPoint.init(x: chartView.width-chartBackScrollView.width, y: 0)
        }
        
    }
    
}

extension UBMScoreChart{
    
    //选中回调
    func didSelectPillar(_ index:Int) -> () {
        
        for item in chartView.subviews {
            
            let pillerView = item as! UBMPillarView
            if pillerView.isSelect == true && pillerView.index != index {
                pillerView.pillarDeselect()
            }
        }
        
        if (callBack != nil) {
            callBack!(index)
        }

    }
    
    //传入选中态
    @objc public func changeToIndex(_ index:Int) -> () {
        
        var newPillerView = UBMPillarView()
        
        for item in chartView.subviews {
            
            let pillerView = item as! UBMPillarView
            
            //取消旧的选中态
            if pillerView.isSelect == true && pillerView.index != index {
                pillerView.pillarDeselect()
            }else if pillerView.isSelect == false && pillerView.index == index {
                pillerView.pillarSelect()
                newPillerView = pillerView
            }
        }
        
        //如果默认选中的柱子不在视野内，则调整contentOffset
        if newPillerView.x < chartBackScrollView.contentOffset.x {
            chartBackScrollView.contentOffset = CGPoint(x: newPillerView.x, y: 0)
        }
        
        
    }
}
