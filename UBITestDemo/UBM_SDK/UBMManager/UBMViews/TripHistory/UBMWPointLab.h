//
//  UBMWPointLab.h
//  GBChartDemo
//
//  Created by wzw on 2020/10/24.
//  Copyright © 2020 Midas. All rights reserved.
//

/*
 定义一个带背景色的lab。
 */

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UBMWPointLab : UILabel

@property(nonatomic, strong)UIImage* bacImg;

@property(nonatomic, assign)BOOL showBacImg;

@property(nonatomic, strong)UIColor* backColorOfShow;

@end

NS_ASSUME_NONNULL_END
