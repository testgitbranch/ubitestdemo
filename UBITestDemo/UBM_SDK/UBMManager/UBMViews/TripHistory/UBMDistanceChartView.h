//
//  UBMDistanceChartView.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/10/17.
//  Copyright © 2020 魏振伟. All rights reserved.
//

/*
 日月年的曲线图
 */



#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol UBMDistanceChartViewDelegate <NSObject>

-(void)didSelectThePoint:(int)index;

@end

@interface UBMDistanceChartView : UIScrollView

@property(nonatomic, weak)id<UBMDistanceChartViewDelegate> DCdelegate;

-(void)updateChartWithList:(NSArray*)dataArr;

//默认选中一个
-(void)defultSelectAtIndex:(long)defultIndex;

@end

NS_ASSUME_NONNULL_END
