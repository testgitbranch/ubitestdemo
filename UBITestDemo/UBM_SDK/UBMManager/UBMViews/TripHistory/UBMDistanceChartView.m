//
//  UBMDistanceChartView.m
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/10/17.
//  Copyright © 2020 魏振伟. All rights reserved.
//

#import "UBMDistanceChartView.h"
#import "UBMGBChart.h"
#import "UBMPrefixHeader.h"

@interface UBMDistanceChartView ()

@property(nonatomic, strong)UBMWgbLineChart *lineChart;
@property(nonatomic, strong)UBMGBLineChartData *lineData;

@property (nonatomic, strong)NSArray* dataArr;

@end

@implementation UBMDistanceChartView

-(void)updateChartWithList:(NSArray *)dataArr{
    self.dataArr = dataArr;
    
    NSMutableArray* arr1 = [[NSMutableArray alloc] init];//点的值
    NSMutableArray* arr2 = [[NSMutableArray alloc] init];//x坐标
    
    for (int i=0; i<dataArr.count; i++) {
        NSDictionary* item = dataArr[i];
        
        [arr1 addObject:item[@"mileage_in_kilometers"]];
        [arr2 addObject:item[@"x"]];
        
    }
    
    //给曲线赋y值。
    self.lineData.dataGetter = ^UBMGBLineChartDataItem *(NSInteger item) {
      
        return  [UBMGBLineChartDataItem dataItemWithY:[arr1[item] floatValue]];
//        return [UBMGBLineChartDataItem dataItemWithY:[arr1[item] floatValue] X:[arr2[item] floatValue]];
    };
    
    //计算y轴刻度的分布
    //找出最大y值
    float maxy = 0.0;
    for (NSString* kil in arr1) {
        if ([kil floatValue] > maxy ) {
            maxy = [kil floatValue];
        }
    }
    //找出合适的y基础刻度
    NSArray* baseYArr = @[@5, @10, @20, @30, @40, @50, @60, @70, @80, @90, @100, @140, @180, @200, @300, @400, @500, @600, @700, @800, @900, @1000, @1200, @1400, @1600, @1800, @2000, @2400, @2800, @3200, @3600, @4000, @5000, @6000, @7000, @8000];
    int baseY = [baseYArr[0] intValue];
    for (int i=0; i<baseYArr.count; i++) {
        int total = [baseYArr[i] intValue];
        if (total*5 > maxy) {
            baseY = total;
            break;
        }
    }
    
    
    NSMutableArray* yLableArr = [[NSMutableArray alloc] init];
    for (int i=0; i<6; i++) {
        [yLableArr addObject:[NSString stringWithFormat:@"%d", baseY*i]];
    }
    self.lineChart.yLabelTitles = yLableArr;
    
    
    
    //计算lineChart的width 每段的宽度*x个数+边界距离。
    CGFloat widthxx = (self.dataArr.count-1)*80+self.lineChart.chartMarginLeft+self.lineChart.chartMarginRight;
    if (widthxx<self.frame.size.width) {
        widthxx = self.frame.size.width;
    }
    self.lineChart.width = widthxx;
    self.contentSize = CGSizeMake(self.lineChart.width, self.frame.size.height);
    self.contentOffset =CGPointMake((self.lineChart.width-self.frame.size.width), 0);
    
    
    self.lineChart.xLabelTitles = arr2;
    self.lineChart.lineChartDatas.itemCount = arr2.count;
    [self.lineChart updateChartDatas:self.lineData];
    
}


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.showsHorizontalScrollIndicator = NO;
        [self setupAAChartView];
        
        
    }
    return self;
}

- (void)setupAAChartView {

    UBMWgbLineChart *chart = [[UBMWgbLineChart alloc] initWithFrame:CGRectMake(0, 0, self.width, self.height)];
    [self addSubview:chart];
    _lineChart = chart;
    
    chart.backgroundColor = UIColor.clearColor;
    
//    chart.xLabelTitles = @[@"0",@"10",@"20",@"30",@"40",@"50",@"60",@"70",@"80",@"90",@"100",];
    chart.xLabelTitles = @[@"0", @"0", @"0", @"0", @"0", @"0", @"0"];
    chart.yLabelTitles = @[@"0", @"20", @"40", @"60", @"80", @"100"];
    chart.xLabelRotationAngle = 0; //M_PI/6;
    chart.showCoordinateAxis = YES;
    chart.showYGridsLine = YES; //横线分割线
    chart.showYGridsLineDash = NO; //横线分割线 是否虚线
    chart.yGridsLineColor = ColorSet(255, 255, 255, 0.12);
    chart.xLabelAlignmentStyle = UBMWGLabelAlignmentStyleFitXAxis;
    chart.yAxisColor = ColorSet(255, 255, 255, 0.12);
    
    chart.showVerticalLine = NO;
    chart.verticalLineColor = [UIColor cyanColor];
    chart.verticalLineWidth = 1;
    chart.verticalLineXValue = 8.8;
    
    //点击了某个点的事件。
    __weak typeof(self) weakself = self;
    chart.selectBlock = ^(int xindex) {
        NSLog(@"点了 %d", xindex);
        
        if ([weakself.DCdelegate respondsToSelector:@selector(didSelectThePoint:)]) {
            [weakself.DCdelegate didSelectThePoint:xindex];
        }
    };
    
    NSMutableArray *array = [NSMutableArray array];
    [array addObjectsFromArray:@[@0.0,@0.0, @0.0, @0, @0, @0, @0, @0, @0, @0, @0]];
    
    NSMutableArray *xArray = chart.xLabelTitles.mutableCopy;
    
    /*
    if (chart.showVerticalLine) {
        NSInteger index = ceil(chart.verticalLineXValue/10.0);
        NSInteger count = 0;
        for (NSString *title in chart.xLabelTitles) {
            if ([title floatValue] != chart.verticalLineXValue) {
                count++;
            }
        }
        if (count == chart.xLabelTitles.count) {
            
            //如果是坐标上的端点 就不要insert
            [xArray insertObject:[NSString stringWithFormat:@"%0.0f", chart.verticalLineXValue] atIndex:index];
            
            CGFloat yvalue = ([array[index] floatValue] + [array[index+1] floatValue])/2;
            [array insertObject:[NSNumber numberWithFloat:yvalue] atIndex:index];
        }
    }
     */
     
    
    
    //曲线 包括曲线点数据和曲线UI
    UBMGBLineChartData *data = [UBMGBLineChartData new];
    data.lineAlpha = 1;
    data.lineColor = [UIColor colorWithRed:8/255.0 green:102/255.0 blue:255/255.0 alpha:1]; //[UIColor blueColor];
    data.lineWidth = 4;
    data.startIndex = 0;
    data.itemCount = chart.xLabelTitles.count; //index+1
    data.lineChartPointStyle = UBMGBLineChartPointStyleCircle;
    data.inflexionPointStrokeColor = [UIColor redColor];
    data.inflexionPointFillColor = [UIColor greenColor];
    data.inflexionPointWidth = 6;
    
    data.showGradientArea = NO;
    data.startGradientColor = [[UIColor redColor] colorWithAlphaComponent:0.3];
    data.endGradientColor = [[UIColor blueColor] colorWithAlphaComponent:0.3];
    
    data.showDash = NO; //曲线的风格，是否是虚线。
    data.lineDashPattern = @[@1,@1, @2, @3];
    
    data.showPointLabel = YES;
    data.pointLabelFont = [UIFont systemFontOfSize:12];
    data.pointLabelColor = [UIColor whiteColor];
    data.pointLabelFormat = @"%0.1fkm";
    
    data.dataGetter = ^UBMGBLineChartDataItem *(NSInteger item) {
      
        return [UBMGBLineChartDataItem dataItemWithY:[array[item] floatValue] X:[xArray[item] floatValue]];
    };
     
    //点的样式设置
    data.pointWidth = 10.0;
    data.borderWidth = 0.5;
    data.pointBorderColor = [UIColor colorWithRed:59/255.0 green:190/255.0 blue:255/255.0 alpha:1];
    data.pointBackColor = UIColor.whiteColor;
    
    data.selectedPointWidth = 16.0;
    data.selectedBorderWidth = 3.0;
    data.selectedPointBorderColor = ColorWithRGB(@"#206CFF");
    data.selectedPointBackColor = UIColor.whiteColor;
    self.lineData = data;
    
    chart.lineChartDatas =  data;
    chart.chartMarginLeft = 25;
    chart.yLabelBlockFormatter = ^NSString *(CGFloat value) {
        return [NSString stringWithFormat:@"%0.0f", value];
    };
    chart.showSmoothLines = YES;
    [chart strokeChart];
    
   
}

-(void)defultSelectAtIndex:(long)defultIndex{
    
    [self.lineChart defultSelectAtIndex:defultIndex];
    
    //如果在屏幕外，就设置下contentOffset，让它显示出来
    CGFloat nowx = defultIndex*80+self.lineChart.chartMarginLeft;
    if (nowx < self.contentOffset.x) {
        self.contentOffset = CGPointMake(nowx, 0);
    }
}


@end
