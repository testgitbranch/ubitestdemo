//
//  UBMTripUpLoadFileView.m
//  LoveCarChannel_iOS
//
//  Created by wzw on 2021/5/20.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

#import "UBMTripUpLoadFileView.h"
#import "UBMPrefixHeader.h"

@interface UBMTripUpLoadFileView()


@property(nonatomic, strong)UILabel* notTitleLab;
@property(nonatomic, strong)UILabel* notTimeLab;
@property(nonatomic, strong)UIButton* uploadBton;

@end

@implementation UBMTripUpLoadFileView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        UIImageView* lineImg1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, 10)];
        lineImg1.backgroundColor = ColorWithRGB(@"#F5F5F5");
        [self addSubview:lineImg1];
        
        UIImageView* notImg = [[UIImageView alloc] initWithFrame:CGRectMake(14, lineImg1.bottom+23, 36, 36)];
        notImg.image = [UIImage imageFromUbiBundleWithName:@"ubm_notEndTripImg"];
        [self addSubview:notImg];
        
        UILabel* notTitleLab = [UBMViewTools lableWithText:@"您有一条未上传的行程" font:[UIFont systemFontOfSize:16 weight:UIFontWeightBold] textColor:ColorSet(0, 0, 0, 0.85)];
        notTitleLab.frame = CGRectMake(notImg.right+5, lineImg1.bottom+18, frame.size.width-notImg.right+5, 24);
        [self addSubview:notTitleLab];
        self.notTitleLab = notTitleLab;
        
        UILabel* notTimeLab = [UBMViewTools lableWithText:@"行程时间" font:[UIFont systemFontOfSize:13 weight:UIFontWeightRegular] textColor:ColorSet(0, 0, 0, 0.45)];
        notTimeLab.frame = CGRectMake(notImg.right+5, notTitleLab.bottom, frame.size.width-notImg.right+5, 24);
        [self addSubview:notTimeLab];
        self.notTimeLab = notTimeLab;
        
        UIButton* uploadBton = [UIButton buttonWithType:UIButtonTypeCustom];
        uploadBton.frame = CGRectMake(frame.size.width-95-14, lineImg1.bottom+23, 95, 36);
        uploadBton.layer.cornerRadius = 18;
        uploadBton.layer.masksToBounds = YES;
        uploadBton.titleLabel.font = [UIFont systemFontOfSize:15];
        [uploadBton setBackgroundColor:ColorWithRGB(@"#4970FF")];
        [uploadBton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
        [uploadBton setTitle:@"立即上传" forState:UIControlStateNormal];
        [uploadBton addTarget:self action:@selector(checkAndupload) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:uploadBton];
        self.uploadBton = uploadBton;
        
    }
    return self;
}

-(void)checkAndupload{
    [self.uploadBton setTitle:@"上传中..." forState:UIControlStateNormal];
    [self.uploadBton setBackgroundColor:UIColor.grayColor];
    self.uploadBton.userInteractionEnabled = NO;
    
    if (self.uploadBtonBlock) {
        self.uploadBtonBlock();
    }
}

-(void)tripUploadEnd{
    
    [self.uploadBton setTitle:@"立即上传" forState:UIControlStateNormal];
    [self.uploadBton setBackgroundColor:ColorWithRGB(@"#4970FF")];
    self.uploadBton.userInteractionEnabled = YES;
    
}

-(void)setNotEndTripList:(NSArray*)tripList;{
    
    NSArray* notEndTripList = tripList;
    if (notEndTripList && notEndTripList.count>0) {
        
        if (notEndTripList.count == 1) {
            self.notTitleLab.text = @"您有一条未上传的行程";
        }else{
            self.notTitleLab.text = @"您有多条未上传的行程";
        }
        
        self.notTimeLab.text = [NSString stringWithFormat:@"行程时间 %@", [notEndTripList[notEndTripList.count-1] objectForKey:@"ctime"] ];

    }
}
@end
