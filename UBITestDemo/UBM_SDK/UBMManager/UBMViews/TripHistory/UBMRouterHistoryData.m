//
//  UBMRouterHistoryData.m
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/11/18.
//  Copyright © 2020 魏振伟. All rights reserved.
//

#import "UBMRouterHistoryData.h"

@implementation UBMRouterHistoryData

+(instancetype)shareRouterHistoryData{
    
    static id instance;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    
    return instance;
    
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.tripInfor = [[NSMutableDictionary alloc] init];
    }
    return self;
}

//可以在set方法中做存储操作，在get方法中做取出操作。


-(void)setTripList:(NSArray *)tripList{
    _tripList = tripList;
    
}
-(NSArray *)tripList{
    
    return _tripList;
}


-(void)setTripStatisticsData:(NSDictionary *)tripStatisticsData{
    _tripStatisticsData = tripStatisticsData;
    
}
-(NSDictionary *)tripStatisticsData{
    
    return _tripStatisticsData;
}


-(void)setTripStatisticsWeekData:(NSDictionary *)tripStatisticsWeekData{
    _tripStatisticsWeekData = tripStatisticsWeekData;
    
}
-(NSDictionary *)tripStatisticsWeekData{
    
    return _tripStatisticsWeekData;
}


-(void)setTripStatisticsMonthData:(NSDictionary *)tripStatisticsMonthData{
    _tripStatisticsMonthData = tripStatisticsMonthData;
    
}
-(NSDictionary *)tripStatisticsMonthData{
    
    return _tripStatisticsMonthData;
}


-(void)setTripInfor:(NSMutableDictionary *)tripInfor{
    _tripInfor = tripInfor;
    
}
-(NSMutableDictionary *)tripInfor{
    
    return _tripInfor;
}

@end
