//
//  UBMTripUpLoadFileView.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2021/5/20.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

/*
 未上传里程
 */

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^UploadBtonBlock)(void);

@interface UBMTripUpLoadFileView : UIView

@property(nonatomic, strong)UploadBtonBlock uploadBtonBlock;

-(void)tripUploadEnd;

-(void)setNotEndTripList:(NSArray*)tripList;

@end

NS_ASSUME_NONNULL_END
