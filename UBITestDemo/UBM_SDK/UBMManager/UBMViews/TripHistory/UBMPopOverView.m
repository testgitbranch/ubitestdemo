//
//  UBMPopOverView.m
//  LoveCarChannel_iOS
//
//  Created by wzw on 2021/5/20.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

#import "UBMPopOverView.h"
#import "UBMTableViewCell.h"
#import "UBMPrefixHeader.h"

static const CGFloat kRowHeight = 50.f;

@interface UBMPopOverView()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *titleArray;
@property (nonatomic, strong) NSArray *imageArray;
@property (nonatomic, strong) NSArray *imageUrls;

@property (nonatomic) CGPoint showOrigin;
@property (nonatomic, strong) UIButton *backgroundButton;
@property (nonatomic, strong) UIView* spinous;//尖角

@property (nonatomic, strong) UIView* relyView; //所依靠的view

@end

@implementation UBMPopOverView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.cellHeight = kRowHeight;
        self.defultSelected = NO;
    }
    return self;
}

- (instancetype)initWithtitles:(NSArray *)titles relyView:(UIView*)relyView
{
    self = [super init];
    if (self) {
        self.titleArray = titles;
        self.relyView = relyView;
        [self setMyViews];
        
    }
    return self;
}

- (instancetype)initWithtitles:(NSArray *)titles imgs:(NSArray *_Nullable)imgs orImgUrls:(NSArray *_Nullable)imgurls  relyView:(UIView*)relyView {
    
    self = [super init];
    if (self) {
        self.titleArray = titles;
        
        if (imgs && imgs.count>0) {
            self.imageArray = imgs;
        }else if(imgurls && imgurls.count>0){
            self.imageUrls = imgurls;
        }
        
        self.relyView = relyView;
        [self setMyViews];
        
    }
    return self;
}


- (void)setMyViews{
    
    self.frame = [self getViewFrame];
    self.backgroundColor = [UIColor whiteColor];
    self.layer.cornerRadius = 3;
    //[self setClipsToBounds:YES];
    
    self.spinous = [[UIView alloc] initWithFrame:CGRectMake(0, -7, 14, 14)];
    _spinous.layer.backgroundColor = UIColor.whiteColor.CGColor;
    _spinous.layer.cornerRadius = 14;
    [self addSubview:self.spinous];
    
    [self addSubview:self.tableView];
}

// 计算字符串宽 - 可写为分类
- (CGFloat)stringWidthOfString:(NSString *)str WithLimitHeight:(CGFloat)height fontSize:(CGFloat)fontSize
                otherAttributes:(NSDictionary *)otherAttributes
{
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithDictionary:@{NSFontAttributeName:[UIFont systemFontOfSize:fontSize]}];
    if (otherAttributes) {
        for (NSString *aKey in otherAttributes.allKeys) {
            id aValue = [otherAttributes objectForKey:aKey];
            [dictionary setObject:aValue forKey:aKey];
        }
    }
    CGRect rect = [str boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, height) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:dictionary context:nil];
    return rect.size.width;
}


// 具体多宽 请按需求在此修改
-(CGRect)getViewFrame
{
    UIWindow* window = [UBMMTools currentWindow];
    //计算relyView在屏幕上的位置
    CGRect rect = [self.relyView convertRect:self.relyView.bounds toView:window];
    self.showOrigin = rect.origin;
    
    CGFloat maxW = 0;
    CGFloat titleW = 0;
    
    for (NSString *str in self.titleArray) {
        [self stringWidthOfString:str WithLimitHeight:self.cellHeight fontSize:14 otherAttributes:nil];
        titleW = [self stringWidthOfString:str WithLimitHeight:self.cellHeight fontSize:14 otherAttributes:nil];
        if (titleW > maxW) {
            maxW = titleW;
        }
    }
    CGFloat x = self.showOrigin.x;
    CGFloat y = self.showOrigin.y;
    CGFloat h = [self.titleArray count] * self.cellHeight;
    
    if (([self.titleArray count] == [self.imageArray count]) || ([self.titleArray count] == [self.imageUrls count])) {
        maxW = 10 + 40 + 10 + maxW + 30;
    }else{
        maxW += 40;
    }
    
//    maxW = MIN(maxW, [UIScreen mainScreen].bounds.size.width-2*_showOrigin.x);
//    if (maxW < titleW) {
//        maxW = titleW+40;
//    }
    
    //不能超出屏幕右边
    if ( x+maxW > UIScreen.mainScreen.bounds.size.width) {
        x = UIScreen.mainScreen.bounds.size.width - maxW - 10;
    }
    
    //不能超出屏幕左边
    if ( x < 0) {
        x = 10;
    }
    
    CGFloat maxH = 0;
    maxH = MIN(h, self.cellHeight*6); //最高展示6行，再多可滑动
    CGRect frame = CGRectMake(x, y+self.relyView.frame.size.height+5, maxW, maxH);
    
    return frame;
}

-(void)show
{
    self.backgroundButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_backgroundButton setFrame:[UIScreen mainScreen].bounds];
    [_backgroundButton setBackgroundColor:[UIColor clearColor]];
    [_backgroundButton addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    [_backgroundButton addSubview:self];
    
    UIWindow *window = [UBMMTools currentWindow];
    [window addSubview:_backgroundButton];
    
    CGPoint arrowPoint = [self convertPoint:self.showOrigin fromView:_backgroundButton];
    self.layer.anchorPoint = CGPointMake(arrowPoint.x / self.frame.size.width, arrowPoint.y / self.frame.size.height);
    self.frame = [self getViewFrame];
    
    //尖角的位置。分为屏幕左右两边
    if (self.x < UIScreen.mainScreen.bounds.size.width/2.0) {
        self.spinous.centerX = self.frame.size.width/4.0;
    }else{
        self.spinous.centerX = self.frame.size.width/4.0*3;
    }
    
     //动画
    if(self.animation == YES){
        self.alpha = 0.f;
        self.transform = CGAffineTransformMakeScale(0.1f, 0.1f);
        [UIView animateWithDuration:0.2f delay:0.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.transform = CGAffineTransformMakeScale(1.05f, 1.05f);
            self.alpha = 1.f;
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.08f delay:0.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
                self.transform = CGAffineTransformIdentity;
            } completion:nil];
        }];
    }
    
}

-(void)dismiss
{
    //动画
   if(self.animation == YES){
       [UIView animateWithDuration:0.3f animations:^{
           self.transform = CGAffineTransformMakeScale(0.1f, 0.1f);
           self.alpha = 0.f;
       } completion:^(BOOL finished) {
           [self->_backgroundButton removeFromSuperview];
       }];
   }else{
       [_backgroundButton removeFromSuperview];
   }
}


#pragma mark - UITableView

-(UITableView *)tableView
{
    if (_tableView != nil) {
        return _tableView;
    }
    
    CGRect rect = self.bounds;
    rect.origin.x += 1 ;
    rect.origin.y += 1;
    rect.size.width -= 2*1;
    rect.size.height -= 2*1;
    self.tableView = [[UITableView alloc] initWithFrame:rect style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.alwaysBounceHorizontal = NO;
    _tableView.alwaysBounceVertical = NO;
    _tableView.showsHorizontalScrollIndicator = NO;
    _tableView.showsVerticalScrollIndicator = YES;
    _tableView.scrollEnabled = YES;
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.separatorColor = ColorSet(0, 0, 0, 0.06);
    _tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    
    return _tableView;
}

#pragma mark - UITableView DataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_titleArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"popCell";
    UBMTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[[UBMMTools ubmBundle] loadNibNamed:@"UBMTableViewCell" owner:nil options:nil] lastObject];
    }
    
    cell.backgroundView = [[UIView alloc] init];
    cell.backgroundView.backgroundColor = [UIColor clearColor];//RGB(57 , 56, 60);
    
    cell.selectedBackgroundView = [[UIView alloc] initWithFrame:cell.bounds];
    cell.selectedBackgroundView.backgroundColor = ColorWithRGB(@"#F2F2F2");
    
    cell.titleLable.textAlignment = NSTextAlignmentCenter;
    
    if ([_imageArray count] == [_titleArray count]) {
        cell.cellImage.hidden = NO;
        cell.cellImage.image = [UIImage imageFromUbiBundleWithName:[_imageArray objectAtIndex:indexPath.row]];
        cell.titleLable.textAlignment = NSTextAlignmentLeft;
        
    }else if([_imageUrls count] == [_titleArray count]){
        cell.cellImage.hidden = NO;
        [cell.cellImage sd_setImageWithURL:[NSURL URLWithString:[_imageUrls objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"jsBridge_pop_menu_placeholder"]];
        cell.titleLable.textAlignment = NSTextAlignmentLeft;
        
    }else{
        cell.cellImage.hidden = YES;
    }
    NSString *text = [_titleArray objectAtIndex:indexPath.row];
    cell.titleLable.text = text;
    
    //选中状态
    if (self.selectedIndex == indexPath.row && self.defultSelected == YES) {
        cell.titleLable.textColor = ColorWithRGB(@"#206CFF");
        cell.titleLable.font = [UIFont systemFontOfSize:14 weight:UIFontWeightSemibold];
    } else {
        cell.titleLable.textColor = ColorSet(0, 0, 0, 0.65);
        cell.titleLable.font = [UIFont systemFontOfSize:14];
    }
    
    cell.backgroundColor = [UIColor whiteColor];
    
    return cell;
}

#pragma mark - UITableView Delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if (self.selectRowAtIndex) {
        self.selectRowAtIndex(indexPath.row);
    }
    [self dismiss];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return self.cellHeight;
}


@end
