//
//  UBMRhInforCardView.m
//  LoveCarChannel_iOS
//
//  Created by wzw on 2021/5/20.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

#import "UBMRhInforCardView.h"
#import "UBMLeftImgLab.h"
#import "UBMWVerticalLab.h"
#import "UBMEventDisplayView.h"
#import "UBMEventCell.h"
#import "UBMEventModel.h"
#import "UBMPrefixHeader.h"

@interface UBMRhInforCardView()<UICollectionViewDelegate,UICollectionViewDataSource>

@property(nonatomic, strong)UILabel* scoreLab;
@property(nonatomic, strong)UBMLeftImgLab* dateLv;

@property(nonatomic, strong)UBMWVerticalLab* licehng;
@property(nonatomic, strong)UBMWVerticalLab* timeLab;
@property(nonatomic, strong)UBMWVerticalLab* averageSpeed;

@property(nonatomic, strong)UIView* anquanyinhuan; //安全隐患父view
@property(nonatomic, strong)UILabel* text1;
@property(nonatomic, strong)UILabel* errorLab; //安全隐患个数
@property(nonatomic, strong)UBMEventDisplayView *eventDisplayView; //安全隐患数据
@property(nonatomic, copy)NSArray <UBMEventModel *> *eventModels;
@property(nonatomic, strong)UIButton* errorBton; //展开详情按钮

@property(nonatomic, assign)BOOL isFold; //是否展开的状态

@end

@implementation UBMRhInforCardView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.isFold = NO;
        
        //里程
        UILabel* scoreLab = [UBMViewTools lableWithText:@"100 驾驶安全分" font:[UIFont systemFontOfSize:32 weight:UIFontWeightBold] textColor:UIColor.blackColor];
        scoreLab.frame = CGRectMake(14, 5, 300, 38);
        [UBMViewTools markerLable:scoreLab changeString:@"驾驶安全分" andAllColor:scoreLab.textColor andMarkColor:ColorSet(0, 0, 0, 0.65) andMarkFondSize:[UIFont systemFontOfSize:14 weight:UIFontWeightMedium]];
        [self addSubview:scoreLab];
        self.scoreLab = scoreLab;
        
        //几月行程
        UBMLeftImgLab* lv = [[UBMLeftImgLab alloc] initWithFrame:CGRectMake(14, scoreLab.bottom+10, 100, 24)];
        [lv setImg:[UIImage imageNamed:@"ubm_hRDistanceWhite"] andContentText:@"行程"];
        [self addSubview:lv];
        self.dateLv = lv;
        
        UBMWVerticalLab* licehng = [[UBMWVerticalLab alloc] initWithFrame:CGRectMake(0, lv.bottom+15, frame.size.width/3.0, 58)];
        licehng.title = @"0";
        licehng.lastTitle = @"行驶里程";
        [self addSubview:licehng];
        self.licehng = licehng;
    
        UBMWVerticalLab* timeLab = [[UBMWVerticalLab alloc] initWithFrame:CGRectMake(licehng.right, lv.bottom+15, frame.size.width/3.0, 58)];
        timeLab.title = @"0";
        timeLab.lastTitle = @"驾驶时长";
        [self addSubview:timeLab];
        self.timeLab = timeLab;
        
        UBMWVerticalLab* averageSpeed = [[UBMWVerticalLab alloc] initWithFrame:CGRectMake(timeLab.right, lv.bottom+15, frame.size.width/3.0, 58)];
        averageSpeed.title = @"0";
        averageSpeed.lastTitle = @"平均速度";
        [self addSubview:averageSpeed];
        self.averageSpeed = averageSpeed;
        
        
        //安全隐患
        UIView* anquanyinhuan = [[UIView alloc] initWithFrame:CGRectMake(0, averageSpeed.bottom+20, frame.size.width, 50)];
//        anquanyinhuan.translatesAutoresizingMaskIntoConstraints = YES;
        anquanyinhuan.clipsToBounds = YES;
        [self addSubview:anquanyinhuan];
        self.anquanyinhuan = anquanyinhuan;
        
        UIImageView* lineImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, anquanyinhuan.width, 0.5)];
        lineImg.backgroundColor = ColorSet(0, 0, 0, 0.06);
//        lineImg.translatesAutoresizingMaskIntoConstraints = YES;
        [anquanyinhuan addSubview:lineImg];
        
        
        __block UILabel* text1 = [[UILabel alloc] initWithFrame:CGRectMake(15, lineImg.bottom+15, 98, 23)];
        text1.textColor = ColorSet(0, 0, 0, 0.85);
        text1.translatesAutoresizingMaskIntoConstraints = YES;
        text1.font = [UIFont systemFontOfSize:16 weight:UIFontWeightSemibold];
        text1.text = @"行程安全隐患";
        [anquanyinhuan addSubview:text1];
        self.text1 = text1;
        
        UIView* errorBackView = [[UIView alloc] init];
        errorBackView.backgroundColor = ColorWithRGB(@"#FF4B41");
        errorBackView.layer.cornerRadius = 4;
        [anquanyinhuan addSubview:errorBackView];
        [errorBackView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(text1.mas_right).offset(6);
            make.top.equalTo(lineImg.mas_bottom).offset(16.5);
            make.height.mas_equalTo(20);
        }];
        
        UILabel* errorLab = [UBMViewTools lableWithText:@"0" font:[UIFont systemFontOfSize:16 weight:UIFontWeightBold] textColor:UIColor.whiteColor];
        errorLab.backgroundColor = ColorWithRGB(@"#FF4B41");
        errorLab.textAlignment = NSTextAlignmentCenter;
        [errorBackView addSubview:errorLab];
        self.errorLab = errorLab;
        [errorLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(errorBackView).inset(3);
            make.right.equalTo(errorBackView).inset(3);
            make.top.equalTo(errorBackView);
            make.height.mas_equalTo(20);
        }];
        
        UILabel* danwei = [UBMViewTools lableWithText:@"个" font:[UIFont systemFontOfSize:16 weight:UIFontWeightSemibold] textColor:ColorSet(0, 0, 0, 0.85)];
        [anquanyinhuan addSubview:danwei];
        [danwei mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(errorBackView.mas_right).offset(6);
            make.top.equalTo(lineImg.mas_bottom).offset(15);
            make.height.mas_equalTo(23);
            make.width.mas_equalTo(23);
        }];
        
        //安全隐患详情按钮
        UIButton* errorBton = [UBMViewTools buttonRimg:@"详情" Color:ColorSet(0, 0, 0, 0.65) font:14 Img:@"ubm_history_errorBton" ImgSize:CGSizeMake(14, 14)];
        [errorBton addTarget:self action:@selector(foldAction) forControlEvents:UIControlEventTouchUpInside];
        errorBton.frame = CGRectMake(frame.size.width-68, lineImg.bottom+10, 62, 34);
        [anquanyinhuan addSubview:errorBton];
        self.errorBton = errorBton;
        
        //[anquanyinhuan addSubview:self.eventDisplayView];
        
        self.height = anquanyinhuan.bottom+15;
    }
    return self;
}

-(void)updateUI:(NSDictionary*)item{
    
    self.scoreLab.text = [NSString stringWithFormat:@"%@ 驾驶安全分", item[@"score"]];
    [UBMViewTools markerLable:self.scoreLab changeString:@"驾驶安全分" andAllColor:self.scoreLab.textColor andMarkColor:ColorSet(0, 0, 0, 0.65) andMarkFondSize:[UIFont systemFontOfSize:14 weight:UIFontWeightMedium]];
    
    [self.dateLv setContentText:[NSString stringWithFormat:@"%@行程", item[@"x_desc"]]];
    
    self.licehng.title = [NSString stringWithFormat:@"%@ km", item[@"mileage_in_kilometers"]];
    [self.licehng markTitleWithText:@"km" markColor:ColorSet(0, 0, 0, 0.65) markFont:[UIFont systemFontOfSize:12]];
    self.timeLab.title = ChangeToString(item[@"duration"]);
    self.averageSpeed.title = [NSString stringWithFormat:@"%@ km/h", item[@"avg_speed_in_kilometers_per_hour"]];
    [self.averageSpeed markTitleWithText:@"km/h" markColor:ColorSet(0, 0, 0, 0.65) markFont:[UIFont systemFontOfSize:12]];
    
    self.errorLab.text = ChangeToString(item[@"event"][@"event_count"]);
    
    //切换即销毁-需要再重建
    if(_eventDisplayView){
        [_eventDisplayView removeFromSuperview];
        _eventDisplayView = nil;
    }
    
    //安全隐患
    if ([item[@"event_count"] intValue]>0) {
        self.errorBton.hidden = NO;
        
        NSArray* eventArray = item[@"event"][@"event_statistics"];
        NSLog(@"隐患个数：%ld",eventArray.count);
        self.eventModels = [UBMEventModel modelsWithDataArray:eventArray];
        
        [self.anquanyinhuan addSubview:self.eventDisplayView];
        
    }else{
        self.isFold = NO;
        self.errorBton.hidden = YES;
        self.eventModels = [NSArray array];
    }
    
    [self refreshHeight];
}

-(void)foldAction{
    self.isFold = !self.isFold;
    [self refreshHeight];
}

-(void)refreshHeight{
    if (self.isFold == YES) {
        self.anquanyinhuan.height = _eventDisplayView.bottom+10;
    }else{
        self.anquanyinhuan.height = self.errorLab.bottom+20;
    }
    
    self.height = self.anquanyinhuan.bottom+15.0;
    
    if (self.heightChangeBlock) {
        self.heightChangeBlock(self.height);
    }
}


#pragma mark - 安全隐患事件展示
- (UBMEventDisplayView *)eventDisplayView {
    if (!_eventDisplayView) {
        if (self.eventModels.count > 0) {
            _eventDisplayView = [UBMEventDisplayView instantiateWithContentViewWidth:self.anquanyinhuan.width-15*2 numberOfEventType:self.eventModels.count target:self];
            _eventDisplayView.backgroundColor = [UIColor ubmColorWithHexNum:0xFFFFFF];
            _eventDisplayView.frame = CGRectMake(15, self.text1.bottom+10, self.anquanyinhuan.width-15*2, [_eventDisplayView getHeightFromNumberOfEventType:self.eventModels.count]);
        }
    }
    return _eventDisplayView;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.eventModels.count;
}

- (UBMEventCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UBMEventCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([UBMEventCell class]) forIndexPath:indexPath];
    cell.model = self.eventModels[indexPath.item];
    return cell;
}

@end
