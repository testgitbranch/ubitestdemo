//
//  UBMNoDataPlaceholderView.swift
//  LoveCarChannel_iOS
//
//  Created by wzw on 2021/5/24.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

/*
 暂无行程数据占位图
 */

import UIKit

@objcMembers public class UBMNoDataPlaceholderView: UIView {

    var inforLab1 = UILabel.init()
    var inforText:String = "该时间内暂无行程数据" {
        didSet{
            inforLab1.text = inforText
        }
    }

    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        setupBaseView()
    }

    func setupBaseView() -> () {
        
        let imgView = UIImageView.init(image: UIImage.init(named: "ubm_history_noData"))
        self.addSubview(imgView)
        imgView.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.width.equalTo(160.0)
            make.height.equalTo(160.0)
        }
        
        let inforLab = UILabel.init()
        inforLab.font = .systemFont(ofSize: 14.0)
        inforLab.textAlignment = NSTextAlignment.center
//        inforLab.textColor = UIColor(hex: 0x000000, alpha: 0.45)
        inforLab.textColor = UIColor.ubmColorWith(hex: 0x000000, alpha: 0.45)
        inforLab.text = inforText
        self.inforLab1 = inforLab
        self.addSubview(inforLab)
        inforLab.snp.makeConstraints { (make) in
            make.top.equalTo(imgView.snp.bottom).offset(23.0)
            make.left.right.equalToSuperview()
        }
    }
}
