//
//  UBMPopOverView.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2021/5/20.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

/*
 带尖角的下拉框
 */

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UBMPopOverView : UIView

@property (nonatomic, assign) CGFloat cellHeight; //每一行的高度，默认50.0。图片高度是40*40

@property (nonatomic, assign) BOOL defultSelected; //是否需要默认选中
@property (nonatomic, assign) int selectedIndex;   //默认选中哪一项（0开始）

@property (nonatomic, assign) BOOL animation; //显示和隐藏时是否需要动画

@property (nonatomic, copy) void (^selectRowAtIndex)(NSInteger index); //点击回调，index：选中的第几项(0开始)

/*
 titles:标题数组
 relyView:依赖的view；（目前只支持在relyView的下面）
 */
- (instancetype)initWithtitles:(NSArray *)titles relyView:(UIView*)relyView;

/*
 titles:标题数组
 imgs:本地图片名字数组；
 imgurls:远程图片地址数组；
 relyView:依赖的view；（目前只支持在relyView的下面）
 */
- (instancetype)initWithtitles:(NSArray *)titles imgs:(NSArray *_Nullable)imgs orImgUrls:(NSArray *_Nullable)imgurls  relyView:(UIView*)relyView;

- (void)show;
- (void)dismiss;

@end

NS_ASSUME_NONNULL_END
