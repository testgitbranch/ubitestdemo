//
//  UBMWPointLab.m
//  GBChartDemo
//
//  Created by wzw on 2020/10/24.
//  Copyright © 2020 Midas. All rights reserved.
//

#import "UBMWPointLab.h"
#import "UBMPrefixHeader.h"

@interface UBMWPointLab()

@property(nonatomic, strong)UIImageView* jiaojiaoImg;


@end

@implementation UBMWPointLab

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
                
        UIImageView* jiaojiaoImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, self.frame.size.height, 10, 10)];
        jiaojiaoImg.layer.cornerRadius = 10;
//        jiaojiaoImg.layer.backgroundColor = ColorWithRGB(@"#206CFF").CGColor;
        [self addSubview:jiaojiaoImg];
        self.jiaojiaoImg = jiaojiaoImg;

        self.showBacImg = NO;
        
    }
    return self;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    self.layer.cornerRadius = self.frame.size.height/2.0;
    
    self.jiaojiaoImg.frame = CGRectMake(self.frame.size.width/2.0-5, self.frame.size.height-5, 10, 10);
}

-(void)setBacImg:(UIImage *)bacImg{
    _bacImg = bacImg;
    
    //self.bacImgView.image = bacImg;
}

- (void)setShowBacImg:(BOOL)showBacImg{
    _showBacImg = showBacImg;
    
    if (showBacImg == YES) {
        if (self.backColorOfShow) {
            self.layer.backgroundColor = self.backColorOfShow.CGColor;
            self.jiaojiaoImg.layer.backgroundColor = self.backColorOfShow.CGColor;
        }else{
            self.layer.backgroundColor = ColorWithRGB(@"#206CFF").CGColor;
            self.jiaojiaoImg.layer.backgroundColor = ColorWithRGB(@"#206CFF").CGColor;
        }
        self.jiaojiaoImg.hidden = NO;
    }else{
        self.layer.backgroundColor = UIColor.clearColor.CGColor;
        self.jiaojiaoImg.hidden = YES;
    }
}

@end
