//
//  UBMWgbLineChart.m
//  GBChartDemo
//
//  Created by wzw on 2020/10/23.
//  Copyright © 2020 Midas. All rights reserved.
//

/*
 单线 的 图表
 */

#import "UBMWgbLineChart.h"
#import "UBMWPointLab.h"
#import "UBMWUpstepBton.h"
#import "UBMPrefixHeader.h"

@interface UBMWgbLineChart()

@property(nonatomic) NSMutableArray *chartLineArray;  // Array Array[CAShapeLayer] save the line layer

@property(nonatomic) NSMutableArray *linePathArray; // 线的path数组。两点之间会有一个线。
@property(nonatomic) NSMutableArray *pointBtonArray;// 数据连接点的按钮数组.

@property (nonatomic) NSMutableArray *pointValueArray;
@property (nonatomic) NSMutableArray *pointLabelArray;

@property (nonatomic) NSMutableArray *yValueLabelArray;
@property (nonatomic) NSMutableArray *xValueLabelArray;
@property (nonatomic) NSMutableArray *gradientLayerArray;

@property (nonatomic, assign) CGFloat xStep;
@property (nonatomic, assign) CGFloat yStep;

@property (nonatomic) CGFloat yValueMax;
@property (nonatomic) CGFloat yValueMin;
@property (nonatomic) NSInteger yLabelNum;

@property (nonatomic, strong) CAAnimation *strokeEndAnimation;

/**
 坐标轴的高度
 */
@property (nonatomic) CGFloat chartCavanHeight;

/**
 坐标轴的宽度
 */
@property (nonatomic) CGFloat chartCavanWidth;


@property (nonatomic, assign)int selectedIndex; //选中点的位置。

@property (nonatomic, strong)UIImageView* whiteLine; //点到底部的竖线

@end

@implementation UBMWgbLineChart


#pragma mark - 初始化
- (id)initWithCoder:(NSCoder *)aDecoder {
    
    if (self = [super initWithCoder:aDecoder]) {
        [self configDefaultValues];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        [self configDefaultValues];
         
    }
    return self;
}

#pragma mark - 设置默认属性
- (void)configDefaultValues {
    
    self.backgroundColor = [UIColor whiteColor];
    self.clipsToBounds = YES;
    self.chartLineArray = [NSMutableArray new];
    self.pointBtonArray = [NSMutableArray new];
    self.linePathArray = [NSMutableArray new];
    self.pointLabelArray = [NSMutableArray new];
    self.pointValueArray = [NSMutableArray new];
    self.yValueLabelArray = [NSMutableArray new];
    self.xValueLabelArray = [NSMutableArray new];
    self.gradientLayerArray = [NSMutableArray new];
    
    _displayAnimated = YES;
    _showCoordinateAxis = YES;
    _showYGridsLineDash = YES;
    _showYLabels = YES;
    _coordinateAxisLineWidth = 1;
    _coordinateAxisColor = [UIColor darkGrayColor];
    _xAxisColor = _coordinateAxisColor;
    _yAxisColor = _coordinateAxisColor;
    _showYGridsLine = YES;
    _yGridsLineColor = [UIColor lightGrayColor];
    _yGridsLineWidth = 1;
    
    _chartMarginLeft = 25.0;
    _chartMarginRight = 25.0;
    _chartMarginTop = 0.0;
    _chartMarginBottom = 25.0;
    
    _xLabelColor = [UIColor grayColor];
    _xLabelFont = [UIFont systemFontOfSize:10];
    
    _yLabelFont = [UIFont systemFontOfSize:10];
    _yLabelColor = [UIColor grayColor];
    _yLabelFormat = @"%.0f";
    
    _verticalLineXValue = 0;
    _showVerticalLine = NO;
    _verticalLineWidth = 1;
    _verticalLineColor = [UIColor blackColor];
}

#pragma mark - setter方法
- (void)setCoordinateAxisColor:(UIColor *)coordinateAxisColor {
    _coordinateAxisColor = coordinateAxisColor;
    _xAxisColor = coordinateAxisColor;
    _yAxisColor = coordinateAxisColor;
}

#pragma mark - Public Method
#pragma mark - 画图表
- (void)strokeChart {
    
    [self calcuateChart];
    [self setXLabel];
    if (_showYLabels) {
        [self setYLabel];
    }
//    [self setGradientLayer];

    [self populateChartLines];
    
    if (_displayAnimated) {
        [self addAnimationIfNeeded];
    }
    [self createPointLabel];
    if (_showVerticalLine) {
        [self strokeVerticalLine];
    }
 
    [self setNeedsDisplay];
}

#pragma mark - 画竖线
- (void)strokeVerticalLine {
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    CGFloat x = [self getXPointWithXValue:_verticalLineXValue item:0];
    [path moveToPoint:CGPointMake(x, _chartMarginTop+_chartCavanHeight)];
    [path addLineToPoint:CGPointMake(x, _chartMarginTop)];
    
    CAShapeLayer *layer = [CAShapeLayer layer];
    layer.strokeColor = _verticalLineColor.CGColor;
    layer.lineWidth = _verticalLineWidth;
    layer.path = path.CGPath;
    [self.layer addSublayer:layer];
}

#pragma mark - 获取x方向的坐标(位置)
- (CGFloat)getXPointWithXValue:(CGFloat)xValue item:(NSInteger)item{
    
    CGFloat x;
    if (xValue == 0) {
        
        if (_xLabelAlignmentStyle == UBMWGLabelAlignmentStyleFullXAxis) {
            
            x = _chartMarginLeft + item*_xStep;
        } else {
            x = _chartMarginLeft  + _xStep/2 + item*_xStep;
        }
        return x;
    }
    if (_xLabelTitles.count == 1) {
        
        return _chartMarginLeft + _xStep/2;
    }
    
    CGFloat xMaxValue = [_xLabelTitles.lastObject floatValue];
    CGFloat xMinValue = [_xLabelTitles.firstObject floatValue];
    

    if (_xLabelAlignmentStyle == UBMWGLabelAlignmentStyleFullXAxis) {
        CGFloat position = (xValue-xMinValue) / (xMaxValue-xMinValue)* _chartCavanWidth;
        x = _chartMarginLeft + position;
    } else {
        CGFloat position = (xValue-xMinValue) / (xMaxValue-xMinValue)* _xStep*(_xLabelTitles.count-1);
        x = _chartMarginLeft  + _xStep/2 + position;
    }
    
    return x;
}

#pragma mark - 更新图表
- (void)updateChartDatas:(UBMGBLineChartData *)data {
    
    [self removeAllLayers];
    [self removeAllSubviews];
    [self removeAllObjects];
    self.selectedIndex = 0;
    _lineChartDatas = data;
    [self strokeChart];
}

#pragma mark - 创建点Label
- (void)createPointLabel {
    
    UBMGBLineChartData *chartData = _lineChartDatas;
    NSArray <NSValue *> *pointValueArray = _pointValueArray;
    if (chartData.showPointLabel) {//显示点
        
        NSInteger item = 0;
        NSMutableArray *pointLabelArray = [NSMutableArray array];
        for (NSValue *value in pointValueArray)
        {
            UBMWPointLab *label = [[UBMWPointLab alloc] init];
            label.font = chartData.pointLabelFont;
            label.textColor = chartData.pointLabelColor;
            label.textAlignment = NSTextAlignmentCenter;
            label.text = [NSString stringWithFormat:chartData.pointLabelFormat, chartData.dataGetter(item).y];
            label.bacImg = [UIImage imageNamed:@"ubm_selectPointBck"];
            
            CGPoint position = value.CGPointValue;
            CGSize size = [label.text sizeWithAttributes:@{NSFontAttributeName : label.font}];
            
            CGFloat width = size.width+20;
            CGFloat height = size.height+7;
            
            label.frame = CGRectMake(position.x - width/2, position.y - height -chartData.inflexionPointWidth-11, width, height);
            [self addSubview:label];
            
            [pointLabelArray addObject:label];
            item++;
        }
        self.pointLabelArray = pointLabelArray;
    }
}
#pragma mark - 添加动画
- (void)addAnimationIfNeeded {
    
    NSArray <CAShapeLayer *> *chartLineArr = _chartLineArray;
    for (CAShapeLayer *line in chartLineArr) {
        [line addAnimation:self.strokeEndAnimation forKey:@"ss"];
    }

    
}
#pragma mark - strokeEndAnimation
- (CAAnimation *)strokeEndAnimation {
    
    if (!_strokeEndAnimation) {
        CABasicAnimation *ani = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
        ani.fromValue = @0;
        ani.toValue = @1;
        ani.duration = 1;
        _strokeEndAnimation= ani;
    }
    return _strokeEndAnimation;
}

#pragma mark - 获取Y最大值最小值
- (void)getYValueMaxAndYValueMin {
    
    if (_yLabelTitles) {
        _yValueMax = [_yLabelTitles.lastObject floatValue];
        _yValueMin = [_yLabelTitles.firstObject floatValue];
        _yLabelNum = _yLabelTitles.count;
    } else {
        
        UBMGBLineChartData *chartData = self.lineChartDatas;
        for (int j = 0; j < chartData.itemCount; j++) {
            CGFloat yValue = chartData.dataGetter(j).y;
            _yValueMax = MAX(_yValueMax, yValue);
            _yValueMin = MIN(_yValueMin, yValue);
        }
        _yLabelNum = 6;
    }
}

#pragma mark - 计算。确定各元素位置
- (void)calcuateChart {
    
    _chartCavanWidth = self.frame.size.width - _chartMarginLeft - _chartMarginRight;
    _chartCavanHeight = self.frame.size.height - _chartMarginBottom - _chartMarginTop;
    
    [self getYValueMaxAndYValueMin];

    if (_xLabelAlignmentStyle == UBMWGLabelAlignmentStyleFullXAxis) {
        _xStep = _chartCavanWidth/(_xLabelTitles.count-1);
    } else {
        _xStep = _chartCavanWidth/_xLabelTitles.count;
    }
    _yStep = _chartCavanHeight/(_yLabelNum-1);
    
    CGFloat yAxisMax = _chartCavanHeight + _chartMarginTop;
    
    UBMGBLineChartData *chartData = _lineChartDatas;
    //点的数组
    NSMutableArray *pointValueArray = [NSMutableArray array];
    for (NSInteger item = chartData.startIndex; item < chartData.itemCount+chartData.startIndex; item++) {
        ///点的横坐标
    
        CGFloat center_x = [self getXPointWithXValue:chartData.dataGetter(item).x item:item];
        
        CGFloat yValue1 = chartData.dataGetter(item).y;
        if (yValue1 < _yValueMin || yValue1 > _yValueMax) {
            
            @throw [NSException exceptionWithName:NSInvalidArgumentException reason:@"值只能在最小值与最大值之间" userInfo:nil];
        }
        ///点的纵坐标
        CGFloat center_y =  yAxisMax - (yValue1-_yValueMin)/(_yValueMax-_yValueMin) * (_chartCavanHeight-_yStep);
        NSValue *pointValue = [NSValue valueWithCGPoint:CGPointMake(center_x, center_y)];
        [pointValueArray addObject:pointValue];
    }
    _pointValueArray = pointValueArray;

    //点
    NSMutableArray *pointPathArray = [NSMutableArray array];
    
    for (NSInteger item = chartData.startIndex; item < chartData.itemCount+chartData.startIndex; item++) {
        CGFloat center_x = [self getXPointWithXValue:chartData.dataGetter(item).x item:item];
        CGFloat center_y =  yAxisMax - (chartData.dataGetter(item).y-_yValueMin)/(_yValueMax-_yValueMin) * (_chartCavanHeight-_yStep);
        
        //圆的替换成bton
        UBMWUpstepBton* pointBton = [UBMWUpstepBton buttonWithType:UIButtonTypeCustom];
        
        if (chartData.lineChartPointStyle == UBMGBLineChartPointStyleCircle) {//圆
            
            pointBton.frame = CGRectMake(0, 0, chartData.pointWidth, chartData.pointWidth);
            pointBton.center = CGPointMake(center_x, center_y);
            pointBton.layer.cornerRadius = chartData.pointWidth/2.0;
            pointBton.layer.masksToBounds = YES;
            [pointBton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
            
            [pointPathArray addObject:pointBton];
      
        }
    }
    _pointBtonArray = pointPathArray;
    
    if (_showSmoothLines) { //曲线
        NSMutableArray *pointValueArray = _pointValueArray;
        if (pointValueArray.count==0) {
            return;
        }
        CGPoint startPoint = CGPointMake(0.0, 0.0);
        CGPoint endPoint = CGPointMake(0.0, 0.0);
        
        startPoint = CGPointMake(startPoint.x-_xStep, startPoint.y);
        endPoint = [pointValueArray.lastObject CGPointValue];
        endPoint = CGPointMake(endPoint.x+_xStep, endPoint.y);
        [pointValueArray insertObject:[NSValue valueWithCGPoint:startPoint] atIndex:0];
        [pointValueArray addObject:[NSValue valueWithCGPoint:endPoint]];
        
        NSMutableArray *chartLinePathArray = [NSMutableArray array];
        for (int j = 0; j < pointValueArray.count-3; j++) {
            
            CGPoint p0 = [pointValueArray[j] CGPointValue];
            CGPoint p1 = [pointValueArray[j+1] CGPointValue];
            CGPoint p2 = [pointValueArray[j+2] CGPointValue];
            CGPoint p3 = [pointValueArray[j+3] CGPointValue];
            
            UIBezierPath *path = [UIBezierPath bezierPath];
            [path moveToPoint:p1];
            if (p1.y == _chartCavanHeight + _chartMarginTop && p2.y == _chartCavanHeight + _chartMarginTop) {
                [self getLinePathx1:p2.x andy1:p2.y path:path];
            } else {
                [self getControlPointx0:p0.x andy0:p0.y x1:p1.x andy1:p1.y x2:p2.x andy2:p2.y x3:p3.x andy3:p3.y path:path];
            }
            [chartLinePathArray addObject:path];
        }
        
        [pointValueArray removeObjectAtIndex:0];
        [pointValueArray removeLastObject];
        
        _linePathArray = chartLinePathArray;
    }
#if 1
    else {//直线
        
        NSArray *pointValueArray = _pointValueArray;
        NSMutableArray *chartLinePathArray = [NSMutableArray array];
        for (int j = 0; j < pointValueArray.count-1; j++) {
            
            CGPoint point0 = [pointValueArray[j] CGPointValue];
            CGPoint point1 = [pointValueArray[j+1] CGPointValue];
            
            UIBezierPath *path = [UIBezierPath bezierPath];
            [path moveToPoint:point0];
            [path addLineToPoint:point1];
            [chartLinePathArray addObject:path];
        }
        _linePathArray = chartLinePathArray;
    }
    
#endif
}
#pragma mark - 布局折线和点
- (void)populateChartLines {
    //线
    UBMGBLineChartData *chartData = _lineChartDatas;
    NSMutableArray <UIBezierPath *> *linePathArray = _linePathArray;
    NSMutableArray *chartLineArray = [NSMutableArray array];
    for (UIBezierPath *path in linePathArray) {
        CAShapeLayer *line = [CAShapeLayer layer];
        line.lineCap = kCALineCapButt;
        line.lineJoin = kCALineJoinMiter;
        line.lineWidth = chartData.lineWidth;
        line.strokeColor = [chartData.lineColor colorWithAlphaComponent:chartData.lineAlpha].CGColor;
        line.path = path.CGPath;
        line.fillColor = [UIColor clearColor].CGColor;
        if (chartData.showDash) {
            line.lineDashPattern = chartData.lineDashPattern;
        }
        [self.layer addSublayer:line];
        [chartLineArray addObject:line];
    }
    _chartLineArray = chartLineArray;
    
    
    
    //点
    NSMutableArray *pointPathArray = _pointBtonArray;
    for (int x=0; x<pointPathArray.count; x++) {
        
        if ([pointPathArray[x] isKindOfClass:[UIButton class]]) {
            
            UBMWUpstepBton* bton = pointPathArray[x];
            bton.tag = 100+x;
            [bton setBackgroundColor: chartData.pointBackColor];
            bton.layer.borderWidth = chartData.borderWidth;
            bton.layer.borderColor = chartData.pointBorderColor.CGColor;
            bton.layer.masksToBounds = YES;
            [bton addTarget:self action:@selector(pointClick:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:bton];
            
        }
        
    }
}

#pragma mark - 移除
#pragma mark 移除所有layers
- (void)removeAllLayers {
    
    for (CALayer *layer in self.chartLineArray) {
        [layer removeAllAnimations];
        [layer removeFromSuperlayer];
    }

    for (CALayer *layer in self.gradientLayerArray) {
        [layer removeAllAnimations];
        [layer removeFromSuperlayer];
    }
    
}

#pragma mark 移除所有subviews
- (void)removeAllSubviews {
    
    for (UIButton* bton in _pointBtonArray) {
        [bton removeFromSuperview];
    }
    
    for (UIView *label in _pointLabelArray) {
        [label removeFromSuperview];
    }
    
    for (UILabel *label in _yValueLabelArray) {
        [label removeFromSuperview];
    }
    for (UILabel *label in _xValueLabelArray) {
        [label removeFromSuperview];
    }
}

#pragma mark 移除所有数据
- (void)removeAllObjects {
    
    [self.pointValueArray removeAllObjects];
    [self.pointLabelArray removeAllObjects];
    [self.chartLineArray removeAllObjects];
    [self.linePathArray removeAllObjects];
    [self.pointBtonArray removeAllObjects];
    [self.yValueLabelArray removeAllObjects];
    [self.xValueLabelArray removeAllObjects];
}

#pragma mark - 创建坐标轴的label
- (void)setYLabel {
    //从下往上布局
    CGFloat divideSeperetor = (_yValueMax - _yValueMin)/(_yLabelNum-1);
    for (int i = 0; i < _yLabelNum; i++) {
        
        UILabel *label = [[UILabel alloc] initWithFrame:[self frameForYLabelAtIndex:i]];
        label.font = _yLabelFont;
        label.textColor = _yLabelColor;
        NSString *value = [NSString stringWithFormat:_yLabelFormat, _yValueMin + divideSeperetor*i];
        if (_yLabelBlockFormatter) {
            value = _yLabelBlockFormatter(_yValueMin + divideSeperetor*i);
        }
        label.text = value;
        label.textAlignment = NSTextAlignmentRight;
        [self addSubview:label];
        [self.yValueLabelArray addObject:label];
    }
}

- (void)setXLabel{
    
    //从左到右
    for (int i = 0; i < _xLabelTitles.count; i++) {
        UILabel *label = [[UILabel alloc] initWithFrame:[self frameForXLabelAtIndex:i]];
        label.font = _xLabelFont;
        label.textColor = _xLabelColor;
        label.text = _xLabelTitles[i];
        label.textAlignment = NSTextAlignmentCenter;
        label.transform = CGAffineTransformMakeRotation(_xLabelRotationAngle);
        [self addSubview:label];
        [self.xValueLabelArray addObject:label];
    }
}


#pragma mark - YLabel Frame XLabel Frame
- (CGRect)frameForYLabelAtIndex:(NSInteger)index {
    
    CGFloat yAxisMax = _chartCavanHeight + _chartMarginTop;
    CGFloat w = _chartMarginLeft-4;
    _yLabelHeight = _chartCavanHeight / _yLabelNum;
    CGFloat x = 0;
    CGFloat y = yAxisMax - index* _yLabelHeight - _yLabelHeight/2;
    return CGRectMake(x, y, w, _yLabelHeight);
}

- (CGRect)frameForXLabelAtIndex:(NSInteger)index {
    
    CGFloat yAxisMax = _chartCavanHeight + _chartMarginTop;
    _xLabelWidth = [self getMaxXLabelWidth];
    CGFloat h = _chartMarginBottom;
    CGFloat x = 0;
    CGFloat y = yAxisMax;
    if (_xLabelAlignmentStyle == UBMWGLabelAlignmentStyleFullXAxis) {
        x = _chartMarginLeft + index * _xStep - _xStep/2;
    } else{
        x = _chartMarginLeft + index * _xStep;
    }
    return CGRectMake(x, y, _xLabelWidth, h);
}

- (CGFloat)getMaxXLabelWidth {
    
    CGFloat width = _chartCavanWidth/_xLabelTitles.count;
    for (NSString *text in _xLabelTitles) {
        CGFloat tempW = [text sizeWithAttributes:@{NSFontAttributeName : _xLabelFont}].width;
        width = MAX(width, tempW);
    }
    return ceil(width);
}

- (void)getLinePathx1:(CGFloat)x1 andy1:(CGFloat)y1
                 path:(UIBezierPath*)path {
    
    [path addLineToPoint:CGPointMake(x1, y1)];
    
}

- (void)getControlPointx0:(CGFloat)x0 andy0:(CGFloat)y0
                       x1:(CGFloat)x1 andy1:(CGFloat)y1
                       x2:(CGFloat)x2 andy2:(CGFloat)y2
                       x3:(CGFloat)x3 andy3:(CGFloat)y3
                     path:(UIBezierPath*)path{
    CGFloat smooth_value = 0.6;
    CGFloat ctrl1_x;
    CGFloat ctrl1_y;
    CGFloat ctrl2_x;
    CGFloat ctrl2_y;
    CGFloat xc1 = (x0 + x1) / 2.0;
    CGFloat yc1 = (y0 + y1) / 2.0;
    CGFloat xc2 = (x1 + x2) / 2.0;
    CGFloat yc2 = (y1 + y2) / 2.0;
    CGFloat xc3 = (x2 + x3) / 2.0;
    CGFloat yc3 = (y2 + y3) / 2.0;
    CGFloat len1 = sqrt(pow((x1-x0), 2) + pow(y1-y0, 2));
    CGFloat len2 = sqrt(pow((x2-x1), 2) + pow(y2-y1, 2));
    CGFloat len3 = sqrt(pow((x3-x2), 2) + pow((y3-y2), 2));
    CGFloat k1 = len1 / (len1 + len2);
    CGFloat k2 = len2 / (len2 + len3);
    CGFloat xm1 = xc1 + (xc2 - xc1) * k1;
    CGFloat ym1 = yc1 + (yc2 - yc1) * k1;
    CGFloat xm2 = xc2 + (xc3 - xc2) * k2;
    CGFloat ym2 = yc2 + (yc3 - yc2) * k2;
    ctrl1_x = xm1 + (xc2 - xm1) * smooth_value + x1 - xm1;
    ctrl1_y = ym1 + (yc2 - ym1) * smooth_value + y1 - ym1;
    ctrl2_x = xm2 + (xc2 - xm2) * smooth_value + x2 - xm2;
    ctrl2_y = ym2 + (yc2 - ym2) * smooth_value + y2 - ym2;
    [path addCurveToPoint:CGPointMake(x2, y2) controlPoint1:CGPointMake(ctrl1_x, ctrl1_y) controlPoint2:CGPointMake(ctrl2_x, ctrl2_y)];
}
#pragma mark - 绘制
- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    CGFloat yAxisMax = _chartCavanHeight + _chartMarginTop;
    CGFloat xAxisMax = _chartCavanWidth + _chartMarginLeft;
    
    
    if (_showCoordinateAxis) {
        
        //画坐标轴
        CGContextRef ctx = UIGraphicsGetCurrentContext();
        CGContextSetLineWidth(ctx, _coordinateAxisLineWidth);
        
        CGContextSetStrokeColorWithColor(ctx, _yAxisColor.CGColor);
        //画y轴三角形
        if (_hideYArrows != YES) {
            CGContextMoveToPoint(ctx, _chartMarginLeft, _chartMarginTop);
            CGContextAddLineToPoint(ctx, _chartMarginLeft-3, _chartMarginTop+5);
            CGContextMoveToPoint(ctx, _chartMarginLeft, _chartMarginTop);
            CGContextAddLineToPoint(ctx, _chartMarginLeft+3, _chartMarginTop+5);
        }

        //画y轴线
        CGContextMoveToPoint(ctx, _chartMarginLeft, _chartMarginTop);
        CGContextAddLineToPoint(ctx, _chartMarginLeft, yAxisMax);
        
        //绘制y轴分割点
        if(!_yStubWidth){
            _yStubWidth = 3;
        }
        for (int i = 0; i < _yLabelNum; i++) {
            
//            CGFloat y = _chartMarginTop + _yStep * i + _yStep;
            CGFloat y = yAxisMax - i* _yLabelHeight;
            CGContextMoveToPoint(ctx, _chartMarginLeft+_yStubWidth, y);
            CGContextAddLineToPoint(ctx, _chartMarginLeft-2, y);
        }
        
        CGContextStrokePath(ctx);

        CGContextSetStrokeColorWithColor(ctx, _xAxisColor.CGColor);
        
        //画x轴线
        CGContextMoveToPoint(ctx, _chartMarginLeft, yAxisMax);
        CGContextAddLineToPoint(ctx, xAxisMax, yAxisMax);
        
        //画x轴三角形(右箭头)
        if (_hideXArrows != YES) {
            CGContextAddLineToPoint(ctx, xAxisMax-5, yAxisMax-3);
            CGContextMoveToPoint(ctx, xAxisMax, yAxisMax);
            CGContextAddLineToPoint(ctx, xAxisMax-5, yAxisMax+3);
        }
        
        //绘制x轴分割点
        if(!_xStubHeight){
            _xStubHeight = 3;
        }
        CGFloat y = _chartCavanHeight + _chartMarginTop;
        for (NSInteger i = 0; i < _xLabelTitles.count; i++) {
            CGFloat x = _chartMarginLeft + (_xLabelAlignmentStyle == UBMWGLabelAlignmentStyleFullXAxis ? 0 : _xStep/2) + i * _xStep;
            CGContextMoveToPoint(ctx, x, y-_xStubHeight);
            CGContextAddLineToPoint(ctx, x, y);
        }
        CGContextStrokePath(ctx);
    }
    
    if (_showYGridsLine) {
        //绘制横线
        CGContextRef ctx = UIGraphicsGetCurrentContext();
        CGContextSetStrokeColorWithColor(ctx, _yGridsLineColor.CGColor);
        CGContextSetLineWidth(ctx, _yGridsLineWidth);
        if (_showYGridsLineDash) {
            CGFloat dash[] = {3,3};
            
            CGContextSetLineDash(ctx, 0.0, dash, 2  );
        }
    
        NSInteger index= 0;
        if (_showCoordinateAxis) {
            index = 1;
        }
        for (NSInteger i = index; i < _yLabelNum; i++) {
            
            
            //源代码的坐标没计算对。改之。
//            CGFloat y = _chartMarginTop + _yStep * i + _yStep;
            
//            NSLog(@"横线坐标点的y: %f", y);
            /*
            CGContextMoveToPoint(ctx, _chartMarginLeft, yAxisMax - _yStep * i);
            CGContextAddLineToPoint(ctx, xAxisMax, yAxisMax - _yStep * i);
            CGContextStrokePath(ctx);
             */
            
           CGFloat y = yAxisMax - i* _yLabelHeight;
            
           CGContextMoveToPoint(ctx, _chartMarginLeft, y);
           CGContextAddLineToPoint(ctx, xAxisMax, y);
           CGContextStrokePath(ctx);
        }
    }
}


#pragma mark - 默认选中一个
-(void)defultSelectAtIndex:(long)defultIndex{
    
    UIButton* selectBton = [self.pointBtonArray objectAtIndex:defultIndex];
    [self selectThePoint:selectBton];
}

#pragma mark - 点击事件。
//点击事件有两个任务：1传出事件；2设置数据lab的选中样式。3 点的选中样式。
-(void)pointClick:(UIButton*)bton{
    
    int index = (int)bton.tag-100;
    NSLog(@"点index： %ld", (long)bton.tag-100);
    
    if (self.selectBlock) {
        self.selectBlock(index);
    }
    
    [self selectThePoint:bton];
}

-(void)selectThePoint:(UIButton*)bton{
    
    int index = (int)bton.tag-100;
    
    if (self.selectedIndex >=0) {
        UBMWPointLab* dataLab = self.pointLabelArray[_selectedIndex];
        dataLab.showBacImg = NO;
        
        UIButton* pointBton = self.pointBtonArray[_selectedIndex];
        CGPoint center = pointBton.center;
        pointBton.layer.borderColor =  _lineChartDatas.pointBorderColor.CGColor;
        pointBton.layer.borderWidth = _lineChartDatas.borderWidth;
        pointBton.backgroundColor = _lineChartDatas.pointBackColor;
        pointBton.width = _lineChartDatas.pointWidth;
        pointBton.height = _lineChartDatas.pointWidth;
        pointBton.center = center;
        pointBton.layer.cornerRadius = pointBton.width/2.0;
        pointBton.layer.masksToBounds = YES;
        
    }
    
    self.selectedIndex = index;
    
    UBMWPointLab* dataLab = self.pointLabelArray[index];
    dataLab.showBacImg = YES;
    
    UIButton* pointBton = self.pointBtonArray[index];
    CGPoint center = pointBton.center;
    pointBton.layer.borderColor =  _lineChartDatas.selectedPointBorderColor.CGColor;
    pointBton.layer.borderWidth = _lineChartDatas.selectedBorderWidth;
    pointBton.backgroundColor = _lineChartDatas.selectedPointBackColor;
    pointBton.width = _lineChartDatas.selectedPointWidth;
    pointBton.height = _lineChartDatas.selectedPointWidth;
    pointBton.center = center;
    pointBton.layer.cornerRadius = pointBton.width/2.0;
    pointBton.layer.masksToBounds = YES;
    
    [self moveWhiteLineToindex:pointBton];
}

-(void)moveWhiteLineToindex:(UIButton*)bton{
    
    [self.whiteLine removeFromSuperview];
    self.whiteLine = nil;
    
    self.whiteLine = [[UIImageView alloc] initWithFrame:CGRectMake(bton.centerX-1, bton.centerY+8, 2, self.height-bton.centerY-34)];
    self.whiteLine.backgroundColor = UIColor.clearColor;
    [UBMViewTools addGradualChangeBckColor:ColorSet(255, 255, 255, 0.1) :UIColor.whiteColor :1 to:self.whiteLine];
    [self addSubview:self.whiteLine];
    
}

@end

