//
//  RouterHistoryData.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/11/18.
//  Copyright © 2020 魏振伟. All rights reserved.
//

/*
 内存缓存历史页面数据
 */

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UBMRouterHistoryData : NSObject
{
    //为了同时重写set\get方法
    NSArray* _tripList; //列表数据
    NSDictionary* _tripStatisticsData;//日数据
    NSDictionary* _tripStatisticsWeekData;//周数据
    NSDictionary* _tripStatisticsMonthData;//月数据
    NSMutableDictionary* _tripInfor; //详情
}

@property(nonatomic, strong)NSArray* tripList;
@property(nonatomic, strong)NSDictionary* tripStatisticsData;//日数据
@property(nonatomic, strong)NSDictionary* tripStatisticsWeekData;//周数据
@property(nonatomic, strong)NSDictionary* tripStatisticsMonthData;//月数据

@property(nonatomic, strong)NSMutableDictionary* tripInfor; //一个tripid一个key。

+(instancetype)shareRouterHistoryData;

@end

NS_ASSUME_NONNULL_END
