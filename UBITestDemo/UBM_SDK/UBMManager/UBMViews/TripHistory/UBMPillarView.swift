//
//  UBMPillarView.swift
//  LoveCarChannel_iOS
//
//  Created by wzw on 2021/5/22.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

/*
 柱状图 - 柱子
 */

import UIKit

struct UBMPillarData {
    var textOfX = "--"
    var showNum = 90.0
    var totalNum = 100.0
    var timrstr = "--"
}


@objcMembers public class UBMPillarView: UIView {
    public typealias PillarcallBackBlock = (_ index: Int) -> Void
    
    var pillarWisth = 10.0     //柱子宽度
    var defultPillarColor:UIColor = UIColor.ubmColorWith(hex: 0x206CFF)//默认柱子颜色
    var selectPillarColor:UIColor = UIColor.ubmColorWith(hex: 0xFFAB2D)//选中柱子颜色
    var graduationFontOfX = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular) //x刻度字体
    var graduationColorOfX:UIColor = UIColor.ubmColorWith(hex: 0xFFFFFF, alpha: 0.65) //x刻度字体颜色
    
    
    
    var heightOfHeadLab:CGFloat = 18.0
    var heightOfGraduationLabOfX:CGFloat = 30.0
    var heightOfPillarView:CGFloat = 0.0
    
    var graduationLabOfX:UILabel = UILabel.init()
    var pillarView:UIImageView = UIImageView.init()
    var headLab:UBMWPointLab = UBMWPointLab.init()
    
    var pillarData:UBMPillarData = UBMPillarData.init(textOfX: "--", showNum: 90.0, totalNum: 100.0, timrstr: "--")
    
    var isSelect = false //是否是选中状态
    var index:Int = 0
    var callBackBlock: PillarcallBackBlock?

    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupBaseView()
    }
    
    func setupBaseView() {
        
        heightOfPillarView = self.frame.size.height - heightOfHeadLab - heightOfGraduationLabOfX - 10
 
        //底部坐标文字
        graduationLabOfX = UILabel.init(frame: CGRect.init(x: 0, y: (self.frame.size.height-heightOfHeadLab), width: self.frame.size.width, height: heightOfHeadLab))
        graduationLabOfX.font = graduationFontOfX
        graduationLabOfX.textAlignment = NSTextAlignment.center
        graduationLabOfX.textColor = graduationColorOfX
        graduationLabOfX.text = "--"
        self.addSubview(graduationLabOfX)
        
        //柱子
        pillarView = UIImageView.init(frame: CGRect.init(x: (Double(self.frame.size.width)-pillarWisth)/2.0, y: Double(graduationLabOfX.frame.origin.y), width: pillarWisth, height: 0))
        pillarView.backgroundColor = defultPillarColor
        self.addSubview(pillarView)
        
        //头部的数据展示
        headLab = UBMWPointLab.init(frame: CGRect.init(x: 0, y: graduationLabOfX.y-heightOfHeadLab, width: 40, height: heightOfHeadLab))
        headLab.textColor = UIColor.white
        headLab.font = .systemFont(ofSize: 12)
        headLab.textAlignment = NSTextAlignment.center
        headLab.backColorOfShow = UIColor.ubmColorWith(hex: 0xFFAB2D)
        headLab.text = "0"
        headLab.showBacImg = true
        headLab.isHidden = true
        headLab.centerX = self.width/2.0;
        self.addSubview(headLab)
        
        let topBton = UIButton.init(type: UIButton.ButtonType.custom)
        topBton.frame = CGRect.init(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)
        topBton.addTarget(self, action: #selector(tapAction), for: UIControl.Event.touchUpInside)
        self.addSubview(topBton)
    }
    
}

extension UBMPillarView{
    
    func updateDate(_ data:UBMPillarData) -> () {
        
        pillarData = data
        
        //赋值
        graduationLabOfX.text = pillarData.textOfX
        headLab.text = String.init(pillarData.showNum)
        
        //计算位置
        UIView.animate(withDuration: 0.5) {
            
            self.pillarView.height = self.heightOfPillarView*CGFloat((self.pillarData.showNum/self.pillarData.totalNum))
            self.pillarView.y = self.graduationLabOfX.frame.origin.y - self.pillarView.height
            self.headLab.y = self.pillarView.y-self.headLab.height-10.0
            
        } completion: { (sucess) in
            
            let width = self.pillarView.width/2.0
            UBMViewTools.addCornerToview(self.pillarView.layer, andWidth: Float(width), andPosition: ubmPTop)
            
        }
        
    }
    
}

extension UBMPillarView{
    
    @objc func tapAction() {
        
        if isSelect == true {
            return //禁止重复选中 //pillarDeselect()
        }else{
            pillarSelect()
        }
        
        //回调
        if (callBackBlock != nil) {
            callBackBlock!(index)
        }
    }
    
   func pillarSelect() {
        isSelect = true
        
        pillarView.backgroundColor = selectPillarColor
        headLab.isHidden = false
    }
    
   func pillarDeselect() {
        isSelect = false
        
        pillarView.backgroundColor = defultPillarColor
        headLab.isHidden = true
    }
    
}
