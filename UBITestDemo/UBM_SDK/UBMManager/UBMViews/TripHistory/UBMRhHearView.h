//
//  UBMRhHearView.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/10/17.
//  Copyright © 2020 魏振伟. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol UBMRhHearViewDelegate <NSObject>

//日月周切换
-(void)tripStatisticsTypeChange:(int)index;

//立即上传
-(void)checkAndUpload;

//高度变换
-(void)heightChangeNotifay;

-(void)didSelectTime:(NSString*)startTime;

@end

@interface UBMRhHearView : UIView

@property(nonatomic, weak)id<UBMRhHearViewDelegate> delegate;

@property(nonatomic, strong)NSDictionary* tripStatisticsData; //头视图数据

@property(nonatomic, strong)NSArray* notEndTripList;  //未结束行程数组

@property(nonatomic, assign)BOOL notEndTripUploadEnd; //上传结束

@end

NS_ASSUME_NONNULL_END
