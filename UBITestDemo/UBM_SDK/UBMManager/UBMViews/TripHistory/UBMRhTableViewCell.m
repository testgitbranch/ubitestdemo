//
//  UBMRhTableViewCell.m
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/10/17.
//  Copyright © 2020 魏振伟. All rights reserved.
//

#import "UBMRhTableViewCell.h"
#import "UBMHistoryItemBottomItemView.h"
#import "UBMPrefixHeader.h"

@interface UBMRhTableViewCell ()

///第一类背景view
@property(nonatomic,strong)UIView * bottomTypeOneView;
///行驶里程
@property(nonatomic,strong)UIButton * distanceButton;
///安全隐患
@property(nonatomic,strong)UIButton * errorButton;


///第二类背景view
@property(nonatomic,strong)UIView * bottomTypeTwoView;
///行驶里程
@property(nonatomic,strong)UBMHistoryItemBottomItemView * distanceItem;
///安全隐患
@property(nonatomic,strong)UBMHistoryItemBottomItemView * errorItem;
///节能减排
@property(nonatomic,strong)UBMHistoryItemBottomItemView * greenItem;

@end

@implementation UBMRhTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupView];
}

- (void)setupView{
    [self addBottomTypeOne];
    [self addBottomTypeTwo];
}

//MARK:没有节能减排的底部
- (void)addBottomTypeOne{
    self.bottomTypeOneView = [[UIView alloc] init];
    [self.contentView addSubview:self.bottomTypeOneView];
    [self.bottomTypeOneView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.bottom.mas_equalTo(-12);
        make.height.mas_equalTo(36);
    }];
    CGFloat buttonW = (ScreenWidth) / 2.0;
    NSArray * buttonArray = @[self.distanceButton,self.errorButton];
    NSArray * imageNames = @[@"unm_hRDistanceBackImg",@"ubm_hRDangerBackImg"];
    NSArray * buttonNames = @[@"行程里程",@"安全隐患"];
    for ( int i = 0; i < imageNames.count; i ++) {
        UIButton * button = buttonArray[i];
        [self.bottomTypeOneView addSubview:button];
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(buttonW * i);
            make.bottom.mas_equalTo(0);
            make.height.mas_equalTo(36);
            make.width.mas_equalTo(buttonW);
        }];
        [button setImageEdgeInsets:UIEdgeInsetsMake(0, 0, -2, 10)];
        [button setTitle:buttonNames[i] forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont systemFontOfSize:14];
        [button setTitleColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.85] forState:UIControlStateNormal];
        [button setImage:[UIImage imageFromUbiBundleWithName:imageNames[i]] forState:UIControlStateNormal];
        if (i < imageNames.count - 1) {
            UIView * line = [[UIView alloc] init];
            line.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.06];
            [self.bottomTypeOneView addSubview:line];
            [line mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(button.mas_right).offset(-1);
                make.centerY.equalTo(button);
                make.height.mas_equalTo(22);
                make.width.mas_equalTo(1);
            }];
        }
    }
}

//MARK:附带节能减排的底部
- (void)addBottomTypeTwo{
    self.bottomTypeTwoView = [[UIView alloc] init];
    [self.contentView addSubview:self.bottomTypeTwoView];
    [self.bottomTypeTwoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.bottom.mas_equalTo(-14);
        make.height.mas_equalTo(52);
    }];
    CGFloat itemW = (ScreenWidth) / 3;
    NSArray * itemArray = @[self.distanceItem,self.errorItem,self.greenItem];
    NSArray * imageNames = @[@"unm_hRDistanceBackImg",@"ubm_hRDangerBackImg",@"ubm_history_list_green"];
    NSArray * buttonNames = @[@"行程里程",@"安全隐患",@"节能减排"];
    for (int i = 0; i < itemArray.count; i ++) {
        UBMHistoryItemBottomItemView * itemView = itemArray[i];
        [self.bottomTypeTwoView addSubview:itemView];
        [itemView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(itemW * i);
            make.width.mas_equalTo(itemW);
            make.height.mas_equalTo(52);
            make.bottom.mas_equalTo(0);
        }];
        itemView.titleLabel.text = @"";
        [itemView.bottomButton setTitle:buttonNames[i] forState:UIControlStateNormal];
        [itemView.bottomButton setImage:[UIImage imageFromUbiBundleWithName:imageNames[i]] forState:UIControlStateNormal];
        if (i < imageNames.count - 1) {
            UIView * line = [[UIView alloc] init];
            line.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.06];
            [self.bottomTypeTwoView addSubview:line];
            [line mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(itemView.mas_right).offset(-1);
                make.centerY.equalTo(itemView);
                make.height.mas_equalTo(22);
                make.width.mas_equalTo(1);
            }];
        }
    }
}

- (void)setDict:(NSDictionary *)dict{
    _dict = dict;
    self.timeLab.text = dict[@"start_time"];
    self.scoreLab.text = ChangeToString(dict[@"score"]);
    self.startLab.text = dict[@"start_poi"];
    self.endLab.text = dict[@"end_poi"];
    self.bottomTypeOneView.hidden = NO;
    self.bottomTypeTwoView.hidden = YES;
    if ([dict[@"is_normal"] integerValue] == 1) {
        self.abnormalImageView.hidden = YES;
        self.scoreTitleLab.hidden = NO;
        self.scoreLab.hidden = NO;
        
        [self.errorButton setTitle:[NSString stringWithFormat:@"安全隐患 %@ 次",dict[@"event_count"]] forState:UIControlStateNormal];
        [UBMViewTools markerLable:self.errorButton.titleLabel changeString:ChangeToString(dict[@"event_count"]) andAllColor:ColorSet(0, 0, 0, 0.85) andMarkColor:ColorSet(0, 0, 0, 0.85) andMarkFondSize:[UIFont systemFontOfSize:18 weight:UIFontWeightBold]];
        self.errorItem.titleLabel.text = [NSString stringWithFormat:@" %@ 次",dict[@"event_count"]];
        [UBMViewTools markerLable:self.errorItem.titleLabel changeString:ChangeToString(dict[@"event_count"]) andAllColor:ColorSet(0, 0, 0, 0.85) andMarkColor:ColorSet(0, 0, 0, 0.85) andMarkFondSize:[UIFont systemFontOfSize:18 weight:UIFontWeightBold]];
        //非自己驾驶的打标的行程
        if ([dict[@"not_driver"] integerValue] == 1) {
            self.abnormalImageView.hidden = NO;
            self.abnormalImageView.image = [UIImage imageFromUbiBundleWithName:@"ubm_inforType6"];
            self.scoreLab.hidden = YES;
            self.scoreTitleLab.hidden = YES;
        }else{
            self.bottomTypeOneView.hidden = YES;
            self.bottomTypeTwoView.hidden = NO;
        }
    } else {
        self.abnormalImageView.hidden = NO;
        self.abnormalImageView.image = [UIImage imageFromUbiBundleWithName:@"ubm_stamp_abnormal_trip_Incomplete"];
        self.scoreTitleLab.hidden = YES;
        self.scoreLab.hidden = YES;
        self.errorLab.text = [NSString stringWithFormat:@"安全隐患 - - 次"];
        [UBMViewTools markerLable:self.errorLab changeString:@"- -" andAllColor:ColorSet(0, 0, 0, 0.85) andMarkColor:ColorSet(0, 0, 0, 0.85) andMarkFondSize:[UIFont systemFontOfSize:18 weight:UIFontWeightBold]];
        self.errorItem.titleLabel.text = @" - - 次";
        [UBMViewTools markerLable:self.errorItem.titleLabel changeString:@"- -" andAllColor:ColorSet(0, 0, 0, 0.85) andMarkColor:ColorSet(0, 0, 0, 0.85) andMarkFondSize:[UIFont systemFontOfSize:18 weight:UIFontWeightBold]];
    }
    [self.distanceButton setTitle:[NSString stringWithFormat:@"行驶里程 %@ km", dict[@"mileage_in_kilometre"]] forState:UIControlStateNormal];
    [UBMViewTools markerLable:self.distanceButton.titleLabel changeString:ChangeToString(dict[@"mileage_in_kilometre"]) andAllColor:ColorSet(0, 0, 0, 0.85) andMarkColor:ColorSet(0, 0, 0, 0.85) andMarkFondSize:[UIFont systemFontOfSize:18 weight:UIFontWeightBold]];
    self.distanceItem.titleLabel.text = [NSString stringWithFormat:@" %@ km",dict[@"mileage_in_kilometre"]] ;
    [UBMViewTools markerLable:self.distanceItem.titleLabel changeString:ChangeToString(dict[@"mileage_in_kilometre"]) andAllColor:ColorSet(0, 0, 0, 0.85) andMarkColor:ColorSet(0, 0, 0, 0.85) andMarkFondSize:[UIFont systemFontOfSize:18 weight:UIFontWeightBold]];
    int energy = [dict[@"energy"] intValue];
   
    if (!self.bottomTypeTwoView.hidden) {
        self.greenItem.titleLabel.text = [NSString stringWithFormat:@"%d g",energy];
        [UBMViewTools markerLable:self.greenItem.titleLabel changeString:[NSString stringWithFormat:@"%d",energy] andAllColor:ColorSet(0, 0, 0, 0.85) andMarkColor:ColorSet(0, 0, 0, 0.85) andMarkFondSize:[UIFont systemFontOfSize:18 weight:UIFontWeightBold]];
    }
}

- (UIButton *)distanceButton{
    if (!_distanceButton) {
        _distanceButton = [[UIButton alloc] init];
    }
    return _distanceButton;
}

- (UIButton *)errorButton{
    if (!_errorButton) {
        _errorButton = [[UIButton alloc] init];
    }
    return _errorButton;
}

- (UBMHistoryItemBottomItemView *)distanceItem{
    if (!_distanceItem) {
        _distanceItem = [[UBMHistoryItemBottomItemView alloc] init];
    }
    return _distanceItem;
}

- (UBMHistoryItemBottomItemView *)errorItem{
    if (!_errorItem) {
        _errorItem = [[UBMHistoryItemBottomItemView alloc] init];
    }
    return _errorItem;
}

- (UBMHistoryItemBottomItemView *)greenItem{
    if (!_greenItem) {
        _greenItem = [[UBMHistoryItemBottomItemView alloc] init];
    }
    return _greenItem;
}

@end
