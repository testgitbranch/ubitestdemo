//
//  UBMWUpstepBton.m
//  LoveCarChannel_iOS
//
//  Created by wzw on 2021/5/21.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

#import "UBMWUpstepBton.h"

@implementation UBMWUpstepBton

-(BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    // 当前btn的大小
    CGRect btnBounds=self.bounds;
    // 扩大按钮的点击范围，改为负值
    btnBounds=CGRectInset(btnBounds, -20, -20);
    // 若点击的点在新的bounds里，就返回YES
    return CGRectContainsPoint(btnBounds, point);
}

@end
