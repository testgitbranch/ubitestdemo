//
//  UBMWUpstepBton.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2021/5/21.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

/*
 扩大按钮点击范围
 */

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UBMWUpstepBton : UIButton

@end

NS_ASSUME_NONNULL_END
