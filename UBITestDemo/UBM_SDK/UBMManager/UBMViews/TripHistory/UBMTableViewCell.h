//
//  UBMTableViewCell.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2021/9/15.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UBMTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *cellImage;
@property (weak, nonatomic) IBOutlet UILabel *titleLable;

@end

NS_ASSUME_NONNULL_END
