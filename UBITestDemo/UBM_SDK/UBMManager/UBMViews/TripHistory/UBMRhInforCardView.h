//
//  UBMRhInforCardView.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2021/5/20.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

/*
 历史页头部 - 数据详情展示view
 */

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^HeightChangeBlock)(CGFloat height);

@interface UBMRhInforCardView : UIView

@property(nonatomic, strong)HeightChangeBlock heightChangeBlock;

@property(nonatomic, strong)NSDictionary* tripStatisticsData; //传入的数据

-(void)updateUI:(NSDictionary*)item;


@end

NS_ASSUME_NONNULL_END
