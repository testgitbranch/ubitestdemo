//
//  UBMRhHearView.m
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/10/17.
//  Copyright © 2020 魏振伟. All rights reserved.
//

#import "UBMRhHearView.h"
#import "UBMDistanceChartView.h"
#import "UBMLeftImgLab.h"
#import "UBMPopOverView.h"
#import "UBMRhInforCardView.h"
#import "UBMTripUpLoadFileView.h"
#import "UBMPrefixHeader.h"
//#import <UBM_SDK/UBM_SDK-Swift.h>

@interface UBMRhHearView()<UBMDistanceChartViewDelegate, UIScrollViewDelegate>

@property(nonatomic, strong)UIButton* chartTypeSelectBton;
@property(nonatomic, strong)UIButton* timeTypeSelectBton;

@property(nonatomic, strong)UBMDistanceChartView* distanceChart;  //行驶里程 图表
@property(nonatomic, strong)UBMScoreChart* scoreChart; //行程分图标

@property(nonatomic, strong)UILabel* totalDistanceLab; //历史总行程
@property(nonatomic, strong)UBMRhInforCardView* inforCardView;
@property(nonatomic, strong)UBMTripUpLoadFileView* notEndTripView; //行程上传失败 卡片 视图
@property(nonatomic, strong)UIImageView* lineImg; //灰色分割线
@property(nonatomic, strong)UILabel* listTitltLab; //出行行程的标题
@property(nonatomic, strong)UIView* nowTotalView;

@property(nonatomic, assign)int chartType; //1:行驶里程 2：行程分
@property(nonatomic, assign)int timeType; //1:日 2：周 3 月

@property(nonatomic, assign)BOOL firstTag;//标记仅走一次的默认选中逻辑

@end

@implementation UBMRhHearView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.firstTag = YES;
        
        UIButton* chartTypeSelectBton = [UBMViewTools buttonRimg:@"行驶里程" Color:UIColor.whiteColor font:15 Img:@"ubm_history_errorBton2" ImgSize:CGSizeMake(14, 14)];
        chartTypeSelectBton.frame = CGRectMake(14, 10, 102, 32);
        chartTypeSelectBton.layer.backgroundColor = ColorSet(0, 0, 0, 0.15).CGColor;
        chartTypeSelectBton.layer.cornerRadius = 16;
        [chartTypeSelectBton addTarget:self action:@selector(selectChartType) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:chartTypeSelectBton];
        self.chartTypeSelectBton = chartTypeSelectBton;
        
        UIButton* timeTypeSelectBton = [UBMViewTools buttonRimg:@"每日" Color:UIColor.whiteColor font:15 Img:@"ubm_history_errorBton2" ImgSize:CGSizeMake(14, 14)];
        timeTypeSelectBton.frame = CGRectMake(frame.size.width-68-16, 10, 68, 32);
        timeTypeSelectBton.layer.backgroundColor = ColorSet(0, 0, 0, 0.15).CGColor;
        timeTypeSelectBton.layer.cornerRadius = 16;
        [timeTypeSelectBton addTarget:self action:@selector(selectTimeType) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:timeTypeSelectBton];
        self.timeTypeSelectBton = timeTypeSelectBton;
        
        //行程图表
        UBMDistanceChartView* dv = [[UBMDistanceChartView alloc] initWithFrame:CGRectMake(0, chartTypeSelectBton.bottom, frame.size.width, 240)];
        dv.backgroundColor = UIColor.clearColor;
        dv.DCdelegate = self;
        dv.delegate = self;
        [self addSubview:dv];
        self.distanceChart = dv;
        
        //分数图表
        self.scoreChart = [[UBMScoreChart alloc] initWithFrame:CGRectMake(0, chartTypeSelectBton.bottom, frame.size.width, 240)];
        @bx_weakify(self);
        self.scoreChart.callBack = ^(NSInteger index) {
            [weak_self didSelectThePiller:(int)index];
        };
        [self addSubview:_scoreChart];
        _scoreChart.hidden = YES;
        
        self.chartType = 1;
        self.timeType = 1;
        
        //总行程
        UILabel* totalDistanceLab = [UBMViewTools lableWithText:@"历史总行程 0 km" font:[UIFont systemFontOfSize:13] textColor:ColorSet(255, 255, 255, 0.65)];
        totalDistanceLab.frame = CGRectMake((frame.size.width-230)/2.0, _scoreChart.bottom+20, 230, 32);
        totalDistanceLab.backgroundColor = ColorSet(22, 33, 70, 1);
        totalDistanceLab.textAlignment = NSTextAlignmentCenter;
        totalDistanceLab.layer.cornerRadius = 16;
        totalDistanceLab.layer.masksToBounds = YES;
        [self addSubview:totalDistanceLab];
        self.totalDistanceLab = totalDistanceLab;
        
        UIView* nowTotalView = [[UIView alloc] initWithFrame:CGRectMake(0, totalDistanceLab.bottom+30, frame.size.width, 265)];
        nowTotalView.backgroundColor = UIColor.whiteColor;
        [self addSubview:nowTotalView];
        self.nowTotalView = nowTotalView;
        
        //头部的圆角
        UIImageView* yuanjiao = [[UIImageView alloc] initWithFrame:CGRectMake(0, -10, nowTotalView.width, 20)];
        yuanjiao.backgroundColor = UIColor.whiteColor;
        yuanjiao.layer.cornerRadius = 10;
        yuanjiao.layer.masksToBounds = YES;
        [nowTotalView addSubview:yuanjiao];
        
        //行程信息
        UBMRhInforCardView* inforCardView = [[UBMRhInforCardView alloc] initWithFrame:CGRectMake(0, 0, nowTotalView.width, 150.0)];
        [nowTotalView addSubview:inforCardView];
        inforCardView.heightChangeBlock = ^(CGFloat height) {
            [weak_self uploadMyViewFrames];
        };
        self.inforCardView = inforCardView;
        
        //未完成的行程
        UBMTripUpLoadFileView* notEndTripView = [[UBMTripUpLoadFileView alloc] initWithFrame:CGRectMake(0, inforCardView.bottom+20, frame.size.width, 0)];
        [nowTotalView addSubview:notEndTripView];
        notEndTripView.layer.masksToBounds = YES;
        notEndTripView.uploadBtonBlock = ^{
            [weak_self checkAndupload];
        };
        self.notEndTripView = notEndTripView;
        
        //灰色分割线
        UIImageView* lineImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, notEndTripView.bottom, frame.size.width, 10)];
        lineImg.backgroundColor = ColorWithRGB(@"#F5F5F5");
        [nowTotalView addSubview:lineImg];
        self.lineImg = lineImg;
        
        //出行行程标题
        UILabel* bl = [[UILabel alloc] initWithFrame:CGRectMake(14, lineImg.bottom+10, 78, 26)];
        bl.text = @"全部行程";
        bl.textColor = UIColor.blackColor;
        bl.font = [UIFont systemFontOfSize:18 weight:UIFontWeightMedium];
        [nowTotalView addSubview:bl];
        self.listTitltLab = bl;
        
    }
    return self;
}

#pragma mark - 更新UI高度
-(void)uploadMyViewFrames{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        self.notEndTripView.y = self.inforCardView.bottom;
        self.lineImg.y = self.notEndTripView.bottom;
        self.listTitltLab.y = self.lineImg.bottom+20;
        self.nowTotalView.height = self.listTitltLab.bottom+20.0;
        self.height = self.nowTotalView.bottom;
        
        [self.delegate heightChangeNotifay];
        
    });
    
}

#pragma mark - 更新了日、周、月数据
-(void)setTripStatisticsData:(NSDictionary *)tripStatisticsData{
    _tripStatisticsData = tripStatisticsData;
    
    //考虑 处理无数据的情况！！！！-日、周、月每次切换时如果无数据，则隐藏图表。
    if (tripStatisticsData == nil) {
        
        NSDictionary* defultItem = @{
            @"avg_speed_in_kilometers_per_hour" : @"--",
            @"score" : @"--",
            @"x" : @"--",
            @"event" : @{
                @"event_statistics" : @[],
                @"event_count" : @"--",
            },
            @"start_time" : @"--",
            @"mileage_in_kilometers" : @"-",
            @"max_speed_in_kilometers_per_hour" : @"--",
            @"duration" : @"--",
            @"event_count" : @"--"
        };
        
        [self.inforCardView updateUI:defultItem];
        
        [self setHistoryTotal:@"--"];
        
        self.distanceChart.hidden = YES;
        self.scoreChart.hidden = YES;
        
        return;
    }
    
    
    [self setHistoryTotal:ChangeToString(tripStatisticsData[@"total_mileage_in_kilometers"])];
    
    
    NSArray* list = tripStatisticsData[@"list"];
    if (list && [list isKindOfClass:[NSArray class]] && list.count>0) {
        
        //切换了时间类型数据后 默认选择最新的那个数据
        long defultIndex = list.count-1;
        
        //如果传入的有默认x值，则选择默认的；否则才选最后那个；
        if (!IsEmptyStr(ChangeToString(tripStatisticsData[@"default_x"])) && self.firstTag==YES) {
            if([tripStatisticsData[@"default_x"] intValue] >=0 && [tripStatisticsData[@"default_x"] intValue] < list.count){
                defultIndex = [tripStatisticsData[@"default_x"] intValue];
            }
        }
        
        NSDictionary* lastItem = list[defultIndex];
        
        [self.inforCardView updateUI:lastItem];
        
        [self.distanceChart updateChartWithList:list];
        [self.distanceChart defultSelectAtIndex:defultIndex];
        
        [self.scoreChart updateDataArray:list];
        [self.scoreChart changeToIndex:defultIndex];
        
        if (self.chartType == 1) {
            self.distanceChart.hidden = NO;
        }else{
            self.scoreChart.hidden = NO;
        }
        
        [self didSelectNew:(int)defultIndex];
    }
    
    //默认的时间选择类型；
    if (!IsEmptyStr(ChangeToString(tripStatisticsData[@"default_type"])) && self.firstTag==YES){
        self.timeType = [tripStatisticsData[@"default_type"] intValue];
    }
    
    self.firstTag = NO;
}


#pragma mark - 选中了柱子
-(void)didSelectThePiller:(int)index{
    
    [self didSelectNew:index];
    
    //保持曲线图同步
    [self.distanceChart defultSelectAtIndex:index];
}

#pragma mark - 点击了行程图表线上的点
-(void)didSelectThePoint:(int)index{
    
    [self didSelectNew:index];
    
    //保持柱状图同步
    [self.scoreChart changeToIndex:index];
}

-(void)didSelectNew:(int)index{
    NSArray* list = _tripStatisticsData[@"list"];
    NSDictionary* item = list[index];
    
    [self.inforCardView updateUI:item];
//    [MobClick event:@"trip_history_chart_action" attributes:[[NSDictionary alloc] initWithObjectsAndKeys:UserModel.shareUserModel.uid, @"uid", nil]];
    
    if ([self.delegate respondsToSelector:@selector(didSelectTime:)]) {
        if (item[@"start_time"]) {
            [self.delegate didSelectTime:item[@"start_time"]];
        }else{
            [self.delegate didSelectTime:@""];
        }
        
    }
}

#pragma mark - 更新了未结束行程数据
-(void)setNotEndTripList:(NSArray *)notEndTripList{
    _notEndTripList = notEndTripList;
    
    if (notEndTripList && notEndTripList.count>0) {
        [self.notEndTripView setNotEndTripList:notEndTripList];
        self.notEndTripView.height = 92;
        
//        [MobClick event:@"trip_batch_upload_show" attributes:[[NSDictionary alloc] initWithObjectsAndKeys:UserModel.shareUserModel.uid, @"uid", nil]];
    }else{
        self.notEndTripView.height = 0;
    }
    
    [self uploadMyViewFrames];
}

#pragma mark - 立即上传
-(void)checkAndupload{
    if ([self.delegate respondsToSelector:@selector(checkAndUpload)]) {
        [self.delegate checkAndUpload];
    }
//    [MobClick event:@"trip_batch_upload_click" attributes:[[NSDictionary alloc] initWithObjectsAndKeys:UserModel.shareUserModel.uid, @"uid", nil]];
}

#pragma mark - 上传结束
-(void)setNotEndTripUploadEnd:(BOOL)notEndTripUploadEnd{
    [self.notEndTripView tripUploadEnd];
}

#pragma mark - 设置历史总行程UI
-(void)setHistoryTotal:(NSString*)totalMileage{
    
    self.totalDistanceLab.text = [NSString stringWithFormat:@"历史总行程 %@ km",totalMileage];
    //可变字符串不同font上下居中
    NSString *bigString = totalMileage;
    NSString *smallString = @"历史总行程 ";
    NSString* smallString2 = @" km";
    NSString *fullString = [NSString stringWithFormat:@"%@%@%@", smallString, bigString, smallString2];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:fullString];

    NSRange bigStringRange = NSMakeRange(smallString.length, bigString.length);
    NSRange smallStringRange = NSMakeRange(0, smallString.length);
    NSRange smallStringRange2 = NSMakeRange(smallString.length+bigString.length, smallString2.length);
    [string beginEditing];
    //Set big string font and size
    [string addAttribute:NSFontAttributeName
            value:[UIFont systemFontOfSize:18 weight:UIFontWeightBold]
            range:bigStringRange];

    //set small string font and size
    [string addAttribute:NSFontAttributeName
            value:[UIFont systemFontOfSize:13.0]
            range:smallStringRange];
    [string addAttribute:NSFontAttributeName
            value:[UIFont systemFontOfSize:13.0]
            range:smallStringRange2];

    //Set small string baseline offset
    [string addAttribute:NSBaselineOffsetAttributeName
            value:[NSNumber numberWithFloat:1.5]
            range:smallStringRange];
    
    [string addAttribute:NSBaselineOffsetAttributeName
    value:[NSNumber numberWithFloat:1.5]
    range:smallStringRange2];

    [string endEditing];
    self.totalDistanceLab.attributedText = string;
}


#pragma mark - 切换表格类型
-(void)selectChartType{
    
    UBMPopOverView* chartTypeSelectView = [[UBMPopOverView alloc] initWithtitles:@[@"行驶里程", @"驾驶安全分"] relyView:self.chartTypeSelectBton];
    chartTypeSelectView.defultSelected = YES;
    chartTypeSelectView.selectedIndex = self.chartType-1;
    chartTypeSelectView.selectRowAtIndex = ^(NSInteger index) {
        if (index == 0) {
            self.chartType = 1;
        }else if (index == 1){
            self.chartType = 2;
        }
    };
    [chartTypeSelectView show];
}

-(void)setChartType:(int)chartType{
    _chartType = chartType;
    
    NSArray* typeArr = @[@"行驶里程", @"驾驶安全分"];
    [self.chartTypeSelectBton setTitle:typeArr[chartType-1] forState:UIControlStateNormal];
    
    if (self.chartType == 1) {
        self.distanceChart.hidden = NO;
        self.scoreChart.hidden = YES;
    }else if (self.chartType == 2){
        self.distanceChart.hidden = YES;
        self.scoreChart.hidden = NO;
    }
}

#pragma mark - 切换日月年
-(void)selectTimeType{
    
    UBMPopOverView* chartTypeSelectView = [[UBMPopOverView alloc] initWithtitles:@[@"每日", @"每周", @"每月"] relyView:self.timeTypeSelectBton];
    chartTypeSelectView.defultSelected = YES;
    chartTypeSelectView.selectedIndex = self.timeType-1;
    chartTypeSelectView.selectRowAtIndex = ^(NSInteger index) {
        if (index == 0) {
            self.timeType = 1;
        }else if (index == 1){
            self.timeType = 2;
        }else if (index == 2){
            self.timeType = 3;
        }
        
        if ([self.delegate respondsToSelector:@selector(tripStatisticsTypeChange:)]) {
            [self.delegate tripStatisticsTypeChange:self.timeType];
        }
        
    };
    [chartTypeSelectView show];
}

-(void)setTimeType:(int)timeType{
    _timeType = timeType;
    
    NSArray* timearr = @[@"每日", @"每周", @"每月"];
    [self.timeTypeSelectBton setTitle:timearr[timeType-1] forState:UIControlStateNormal];
    
}


-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
//    [MobClick event:@"trip_history_chart_action" attributes:[[NSDictionary alloc] initWithObjectsAndKeys:UserModel.shareUserModel.uid, @"uid", nil]];
}

@end
