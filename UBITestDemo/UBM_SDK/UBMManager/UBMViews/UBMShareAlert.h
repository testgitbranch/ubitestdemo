//
//  UBMShareAlert.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/10/17.
//  Copyright © 2020 魏振伟. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^selefIndex)(int index);
typedef void(^dismissBlock)(void);

NS_ASSUME_NONNULL_BEGIN

@interface UBMShareAlert : UIView

@property(nonatomic, copy)selefIndex selectBlock;
@property(nonatomic, copy)dismissBlock dismissBlock;
 
-(void)show;

@end

NS_ASSUME_NONNULL_END
