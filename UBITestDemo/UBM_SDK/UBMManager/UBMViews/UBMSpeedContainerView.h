//
//  UBMSpeedContainerView.h
//  LoveCarChannel_iOS
//
//  Created by 王国松 on 2021/6/28.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UBMSpeedChartView.h"

NS_ASSUME_NONNULL_BEGIN

@interface UBMSpeedContainerView : UIView


@property (nonatomic, strong) UIView *speedTopContentView;
@property (nonatomic, strong) UILabel *speedTitleLabel;
@property (nonatomic, strong) UILabel *maxSpeedTitleLabel;
@property (nonatomic, strong) UILabel *maxSpeedValueLabel;
@property(nonatomic, strong)UBMSpeedChartView* speedChartView;

@end

NS_ASSUME_NONNULL_END
