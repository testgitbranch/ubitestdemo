//
//  UBMHistoryBottomButton.m
//  LoveCarChannel_iOS
//
//  Created by 王国松 on 2021/6/3.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

#import "UBMHistoryBottomButton.h"

@implementation UBMHistoryBottomButton

- (CGRect)titleRectForContentRect:(CGRect)contentRect{
    return CGRectMake(14+3, 0, contentRect.size.width - 17, contentRect.size.height);
}

- (CGRect)imageRectForContentRect:(CGRect)contentRect{
    return CGRectMake(0, (contentRect.size.height - 14)/2.0, 14, 14);
}

@end
