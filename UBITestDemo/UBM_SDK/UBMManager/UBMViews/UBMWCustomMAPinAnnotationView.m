//
//  wCustomMAPinAnnotationView.m
//  CoreMotionOC
//
//  Created by wzw on 2020/10/13.
//  Copyright © 2020 LoveCarHome. All rights reserved.
//

#import "UBMWCustomMAPinAnnotationView.h"

@interface UBMWCustomMAPinAnnotationView ()

@end

@implementation UBMWCustomMAPinAnnotationView

#define kCalloutWidth       168.0
#define kCalloutHeight      80.0

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    if (self.selected == selected)
    {
        return;
    }
    
    if (selected)
    {
        //只有自定义的点才会显示气泡
        if ([self.annotation isKindOfClass:[UBMWCustomAnnotation class]]) {
            
            UBMWCustomAnnotation* wca = (UBMWCustomAnnotation*)self.annotation;
            [self handleImg:YES and:wca];
            
            //弹出弹窗
            if (self.calloutView == nil)
            {
                self.calloutView = [[UBMWCustomCalloutView alloc] initWithFrame:CGRectMake(0, 0, kCalloutWidth, kCalloutHeight)];
                self.calloutView.center = CGPointMake(CGRectGetWidth(self.bounds) / 2.f + self.calloutOffset.x,
                                                      -CGRectGetHeight(self.calloutView.bounds) / 2.f + self.calloutOffset.y);
            }
            if (wca.dataDic) {
                self.calloutView.title = wca.dataDic[@"event"];
                self.calloutView.subtitle = wca.dataDic[@"des"];
                self.calloutView.bottmTitle = wca.dataDic[@"time"];
                
                [self addSubview:self.calloutView];
            }
            
//            [MobClick event:@"trip_event_click" attributes:[[NSDictionary alloc] initWithObjectsAndKeys:UserModel.shareUserModel.uid, @"uid", nil]];
            
        }
        
    }
    else
    {
        if ([self.annotation isKindOfClass:[UBMWCustomAnnotation class]]) {
            UBMWCustomAnnotation* wca = (UBMWCustomAnnotation*)self.annotation;
            [self handleImg:NO and:wca];
        }
        
        //隐藏弹窗
        [self.calloutView removeFromSuperview];
        
//        [MobClick event:@"trip_map_action" attributes:[[NSDictionary alloc] initWithObjectsAndKeys:UserModel.shareUserModel.uid, @"uid", nil]];
    }
    
    [super setSelected:selected animated:animated];
}


-(void)handleImg:(BOOL )select and:(UBMWCustomAnnotation*)wcAnnotation{
    
    //开始和结束点是没有选择状态的。
    if (wcAnnotation.pointType == ubmStartPoint || wcAnnotation.pointType == ubmEndPoint ) {
        return;;
    }
    
    if (select == YES) {
        
        __weak typeof(self) weakself = self;
    
        [[SDWebImageManager sharedManager] loadImageWithURL:[NSURL URLWithString:wcAnnotation.eventTouchIconURL] options:SDWebImageRetryFailed progress:nil completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, SDImageCacheType cacheType, BOOL finished, NSURL * _Nullable imageURL) {
            if (image) {
                UIImage* newImg = [UBMViewTools scaleToSize:image size:CGSizeMake(20, 20)];
                weakself.image = newImg;
            }
        }];
        
        
    }else{
        
        __weak typeof(self) weakself = self;
        
        [[SDWebImageManager sharedManager] loadImageWithURL:[NSURL URLWithString:wcAnnotation.eventIconUrl] options:SDWebImageRetryFailed progress:nil completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, SDImageCacheType cacheType, BOOL finished, NSURL * _Nullable imageURL) {
            if (image) {
                UIImage* newImg = [UBMViewTools scaleToSize:image size:CGSizeMake(20, 20)];
                weakself.image = newImg;
            }
        }];
    }
    
}

@end
