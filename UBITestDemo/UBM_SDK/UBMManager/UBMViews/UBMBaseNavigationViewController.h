//
//  UBMBaseNavigationViewController.h
//  EngineeProject
//
//  Created by SuperMan on 2017/7/31.
//  Copyright © 2017年 mac. All rights reserved.
//
#import "ZXNavigationBarNavigationController.h"

#import <UIKit/UIKit.h>

@interface UBMBaseNavigationViewController : UINavigationController

@end
