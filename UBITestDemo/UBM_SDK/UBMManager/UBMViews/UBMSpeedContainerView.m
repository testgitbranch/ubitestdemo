//
//  UBMSpeedContainerView.m
//  LoveCarChannel_iOS
//
//  Created by 王国松 on 2021/6/28.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

#import "UBMSpeedContainerView.h"
#import "UBMPrefixHeader.h"

@implementation UBMSpeedContainerView

- (id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setUp];
        [self setupLayout];
    }
    return self;
}

- (void)setUp{
    [self addSubview:self.speedTopContentView];
    [self.speedTopContentView addSubview:self.speedTitleLabel];
    [self.speedTopContentView addSubview:self.maxSpeedTitleLabel];
    [self.speedTopContentView addSubview:self.maxSpeedValueLabel];
    [self addSubview:self.speedChartView];
}

- (void)setupLayout{
    [self.speedTopContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.equalTo(self).offset(16);
        make.right.equalTo(self).offset(-16);
    }];
    [self.speedTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.equalTo(self.speedTitleLabel.superview);
    }];
    [self.maxSpeedValueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.speedTitleLabel);
        make.right.equalTo(self.maxSpeedValueLabel.superview);
    }];
    [self.maxSpeedTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.speedTitleLabel);
        make.right.equalTo(self.maxSpeedValueLabel.mas_left).offset(-6);
    }];
    [self.speedChartView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(self.speedChartView.width);
        make.height.mas_equalTo(self.speedChartView.height);
        make.top.equalTo(self.speedTopContentView.mas_bottom).offset(24);
        make.left.equalTo(self.speedChartView.superview).offset(12);
        make.right.equalTo(self.speedChartView.superview).offset(-12);
    }];
}

- (UIView *)speedTopContentView {
    if (!_speedTopContentView) {
        _speedTopContentView = [[UIView alloc] init];
    }
    return _speedTopContentView;
}

- (UILabel *)speedTitleLabel {
    if (!_speedTitleLabel) {
        _speedTitleLabel = [[UILabel alloc] init];
        _speedTitleLabel.text = @"行程速度";
        _speedTitleLabel.textColor = [UIColor ubmColorWithHexNum:0x000000];
        _speedTitleLabel.textAlignment = NSTextAlignmentLeft;
        _speedTitleLabel.font = [UIFont systemFontOfSize:18 weight:UIFontWeightSemibold];
    }
    return _speedTitleLabel;
}

- (UILabel *)maxSpeedTitleLabel {
    if (!_maxSpeedTitleLabel) {
        _maxSpeedTitleLabel = [[UILabel alloc] init];
        _maxSpeedTitleLabel.text = @"最高速度";
        _maxSpeedTitleLabel.textColor = [UIColor ubmColorWithHexNum:0x000000 alpha:0.65];
        _maxSpeedTitleLabel.textAlignment = NSTextAlignmentRight;
        _maxSpeedTitleLabel.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
    }
    return _maxSpeedTitleLabel;
}

- (UILabel *)maxSpeedValueLabel {
    if (!_maxSpeedValueLabel) {
        _maxSpeedValueLabel = [[UILabel alloc] init];
        _maxSpeedValueLabel.textColor = [UIColor ubmColorWithHexNum:0x000000 alpha:0.65];
        _maxSpeedValueLabel.textAlignment = NSTextAlignmentRight;
        _maxSpeedValueLabel.font = [UIFont systemFontOfSize:14 weight:UIFontWeightSemibold];
    }
    return _maxSpeedValueLabel;
}

- (UBMSpeedChartView *)speedChartView {
    if (!_speedChartView) {
        _speedChartView = [[UBMSpeedChartView alloc] initWithFrame:CGRectMake(7, 0, ScreenWidth-24-14, 200)];
    }
    return _speedChartView;
}

@end
