//
//  UBMRouteShareView.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/10/17.
//  Copyright © 2020 魏振伟. All rights reserved.
//

/*
 用于生成分享图片的view
 */

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UBMRouteShareView : UIView

-(instancetype)initWithFrame:(CGRect)frame andMapview:(UIImage*)mapview andOtherData:(NSDictionary*)dicx;

@property(nonatomic,strong)UIImage * image;

@end

NS_ASSUME_NONNULL_END
