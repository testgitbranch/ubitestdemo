//
//  UBMGBLineChartData.h
//  GBChartDemo
//
//  Created by midas on 2018/12/21.
//  Copyright © 2018 Midas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, UBMGBLineChartPointStyle) {
    
    UBMGBLineChartPointStyleNone = 0,
    UBMGBLineChartPointStyleCircle = 1, //圆形
    UBMGBLineChartPointStyleSquare = 2, //方形
    UBMGBLineChartPointStyleTriangle = 3, //三角形
};

@interface UBMGBLineChartDataItem : NSObject

@property (nonatomic, assign) CGFloat y;

@property (nonatomic, assign) CGFloat x;

+ (id)dataItemWithY:(CGFloat)Y X:(CGFloat)X;

+ (id)dataItemWithY:(CGFloat)Y;

@end

typedef UBMGBLineChartDataItem * (^UBMGBLineChartDataGetter)(NSInteger item);

@interface UBMGBLineChartData : NSObject

@property (nonatomic, copy) UBMGBLineChartDataGetter dataGetter;

//@property (nonatomic, strong) NSArray* dataArray;//y的数据点。
@property (nonatomic, assign) NSInteger itemCount;  //数据点的数量

@property (nonatomic, strong) UIColor *lineColor;   //曲线颜色
@property (nonatomic, assign) CGFloat lineAlpha;    //曲线透明度
@property (nonatomic, assign) NSInteger startIndex; //开始绘制点
@property (nonatomic, assign) CGFloat lineWidth;    //曲线宽度

@property (nonatomic, assign) BOOL showPointLabel;  //是否显示数据点的lable
@property (nonatomic, strong) UIColor *pointLabelColor;
@property (nonatomic, strong) UIFont *pointLabelFont;
@property (nonatomic, strong) NSString *pointLabelFormat;

@property (nonatomic, assign) BOOL showDash;   //曲线是否是虚线。
@property (nonatomic, strong) NSArray <NSNumber *> *lineDashPattern; //虚线怎么虚的

@property (nonatomic, assign) UBMGBLineChartPointStyle lineChartPointStyle;   //点的形状
@property (nonatomic, assign) CGFloat inflexionPointWidth;                //点的宽度
@property (nonatomic, strong) UIColor *inflexionPointFillColor;           //点的填充颜色。
@property (nonatomic, strong) UIColor *inflexionPointStrokeColor;         //点的外围颜色 。

//自定义点的点击事件。
//@property (nonatomic, assign) int defultSelectedIndex; //默认选中第几个点。
@property (nonatomic, assign) BOOL allowClick;    //是否允许点击。

@property (nonatomic, assign) CGFloat pointWidth;          //按钮的宽度
@property (nonatomic, assign) CGFloat borderWidth;         //边框的宽度
@property (nonatomic, strong) UIColor* pointBackColor;   //按钮的背景色
@property (nonatomic, strong) UIColor* pointBorderColor; //按钮的边框颜色

@property (nonatomic, assign) CGFloat selectedPointWidth;           //选中的按钮的宽度
@property (nonatomic, assign) CGFloat selectedBorderWidth;        //选中的边框的宽度
@property (nonatomic, strong) UIColor* selectedPointBackColor;    //选中的按钮的背景色
@property (nonatomic, strong) UIColor* selectedPointBorderColor;  //选中的按钮的边框颜色



//渐变区域
/** 是否显示折线围成的渐变区域, 默认为0*/
@property (nonatomic, assign) BOOL showGradientArea;
/** 渐变开始的颜色 */
@property (nonatomic, strong) UIColor *startGradientColor;
/** 渐变结束的颜色 */
@property (nonatomic, strong) UIColor *endGradientColor;

@end
