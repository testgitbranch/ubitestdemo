//
//  UBITracking.m
//  BXUbmNewSDK
//
//  Created by 金鑫 on 2021/11/15.
//

#import "UBMManager.h"
#import "UBMUdKeys.h"
#import "UBITracking.h"
#import "UBMJugeBaseURLWIthRequest.h"

static UBMEnviType const kEnvType = UBMEnviSim;

@interface UBITracking ()
@property (nonatomic, assign, readonly, getter=isAutoRecordSwitchOpen) BOOL autoRecordSwitch;
@end

@implementation UBITracking

+ (instancetype)shared {
    static id instance;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    
    return instance;
}

- (BOOL)isAutoRecordSwitchOpen {
    NSNumber *sw = [[NSUserDefaults standardUserDefaults] objectForKey:UBMAutoRecordSwitchUDKey];
    return [sw integerValue] == 1 ? YES : NO;
}

- (void)enableSDK:(NSString * _Nonnull)userId provider:(UBICredentialProvider * _Nonnull)provider {
    self.userID = userId;
    self.provider = provider;
    //设置环境
    [[UBMEnviConfig shareEnvi] config:kEnvType];
    
    // 使用userId来初始化UBM
    [[UBMManager shared] setUserId:userId];
    // 接续15分钟内异常断开的行程
    [[UBMManager shared] keepLastTripWithInterval:15 keepLastTripResult:^(BOOL restart) {}];
    // 打开自动记录
    if (self.isAutoRecordSwitchOpen == YES) {
        [[UBMManager shared] startAutoRecord];
    }
    // 开始周期循环检查本地未上传行程，并予以上传的机制
    [[UBMManager shared] startCycleCheckAndUploadLocTrip];
}

- (void)startTrip {
    UBMStartOrEndModel* startModel = [[UBMStartOrEndModel alloc] init];
    startModel.origintype = ubmUserMovementType;
    [UBMManager.shared startRecord:startModel andResult:^(UBMResultModel * _Nonnull result) {}];
}

- (void)stopTrip:(void(^)(UBMResultModel* result))callback {
    UBMStartOrEndModel* endModel = [[UBMStartOrEndModel alloc] init];
    endModel.origintype = ubmUserMovementType;
    [[UBMManager shared] stopRecord:endModel andResult:^(UBMResultModel * _Nonnull result) {
        callback(result);
    }];
}

- (void)disableSDK:(void(^)(UBMResultModel* result))callback {
    //关闭定期检测未上传行程并予以上传机制
    [[UBMManager shared] stopCycleCheckAndUploadLocTrip];
    // 关闭行程
    UBMStartOrEndModel* endModel = [[UBMStartOrEndModel alloc] init];
    endModel.origintype = ubmUserMovementType;
    [[UBMManager shared] stopRecord:endModel andResult:^(UBMResultModel * _Nonnull result) {
        callback(result);
    }];
    // 关闭自动记录
    [[UBMManager shared] stopAutoRecord];
    // 删除UBMUserIdForResettingUDKey
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UBMUserIdForResettingUDKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    // 清除userId和passportToken
    [UBITracking shared].userID = nil;
    [UBITracking shared].provider = nil;
}

-(void)setUserToken:(NSString *)userToken {
    [UBMManager shared].userToken = userToken;
}

-(NSString *)userToken {
    return [UBMManager shared].userToken;
}

-(void)setUserId:(NSString*)userId {
    [[UBMManager shared] setUserId:userId];
}

/*
 如果数据要与车绑定，则需要调用本方法。vid只是作为一个行程的车辆参数。
 vid:车id。
 */
-(void)setVid:(NSString*)vid {
    [[UBMManager shared] setVid:vid];
}

/*
 自动拉起异常结束行程的最大时间。默认15分钟内。
 interval:分钟数。
 */
-(void)keepLastTripWithInterval:(int)interval keepLastTripResult: (keepLastTripResultBlock)lastTripResultBlock {
    [[UBMManager shared] keepLastTripWithInterval:interval keepLastTripResult:lastTripResultBlock];
}

//权限请求。可以选择第一次在哪里请求位置权限。当然，当开启之时也会默认先调用一次。
-(void)prepareCoreMotionCenter {
    [[UBMManager shared] prepareCoreMotionCenter];
}


//开启自动检测记录：会自动判断开车时机并开启记录。
-(void)startAutoRecord {
    [[UBMManager shared] startAutoRecord];
}

//关闭自动检测记录。比如：退出登录后。
-(void)stopAutoRecord {
    [[UBMManager shared] stopAutoRecord];
}


//开使收集数据。当isCollecting==yes，会返回失败。
-(void)startRecord:(UBMStartOrEndModel*)startModel andResult:(void(^)(UBMResultModel* result))startBack {
    [[UBMManager shared] startRecord:startModel andResult:startBack];
}

//停止收集数据。返回resultcode失败，代表没有获取到网络tripid。停止是绝对会的。
-(void)stopRecord:(UBMStartOrEndModel*)startModel andResult:(void(^)(UBMResultModel* result))endBack {
    [[UBMManager shared] stopRecord:startModel andResult:endBack];
}


//查找未结束的行程：hadStop==NO
-(NSArray*)getAllUnStopTrip {
    return [[UBMManager shared] getAllUnStopTrip];
}

//处理异常停止的trip并上传、校验、合并：hadStop==NO
-(void)stopAndUploadIncorrectTrip:(NSString*)locTripID {
    [[UBMManager shared] stopAndUploadIncorrectTrip:locTripID];
}


//查找本地UploadOver==NO的行程。（hadStop==YES && UploadOver==NO）
-(NSArray*)getAllUnUploadOverTrip {
    return [[UBMManager shared] getAllUnUploadOverTrip];
}

//检查并上传本地所有的未完全结束掉的trip。（hadStop==YES && UploadOver==NO）
-(void)checkAndUploadLocTrip:(void(^)(BOOL result))callBack {
    [[UBMManager shared] checkAndUploadLocTrip:callBack];
}


//上传指定tripIds的行程
-(void)uploadTheTripsOfTripids:(NSArray*)tripids andCallBack:(void(^)(BOOL result))upresult {
    [[UBMManager shared] uploadTheTripsOfTripids:tripids andCallBack:upresult];
}



//设置是否在导航中
-(void)setIsNaving:(BOOL)isNav {
    [[UBMManager shared] setIsNaving:isNav];
}

//设置是否在结束倒计时
-(void)setIsStoping:(BOOL)isStoping {
    [[UBMManager shared] setIsStoping:isStoping];
}

//关闭Ubm
- (void)closeUbm {
    [[UBMManager shared] stopAutoRecord];
    [UBMManager shared].userToken = @"";
    [[UBMManager shared] closeAutoRecordSwitch];
}

- (void)closeAutoRecordSwitch {
    [[UBMManager shared] closeAutoRecordSwitch];
}

- (void)openAutoRecordSwitch {
    [[UBMManager shared] openAutoRecordSwitch];
}

- (void)startCycleCheckAndUploadLocTrip {
    [[UBMManager shared] startCycleCheckAndUploadLocTrip];
}

- (void)stopCycleCheckAndUploadLocTrip {
    [[UBMManager shared] stopCycleCheckAndUploadLocTrip];
}

@end
