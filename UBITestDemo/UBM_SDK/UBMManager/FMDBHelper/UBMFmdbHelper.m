//
//  UBMFmdbHelper.m
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/12/12.
//  Copyright © 2020 魏振伟. All rights reserved.
//

#import "UBMFmdbHelper.h"
#import "UBMPbFileManger.h"

@interface UBMFmdbHelper()

@property (nonatomic, strong)FMDatabaseQueue* queue;

@property (strong, nonatomic)NSString *fileName;//数据库文件的路径
//@property (strong, nonatomic)FMDatabase *dataBase;//数据库对象

@end

@implementation UBMFmdbHelper

+(instancetype)sharedFMDBHelper
{
    static id instance;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    
    return instance;
}

-(void)createDBWithName:(NSString *)dbName{
    
    if (dbName.length == 0) {
        /**
         *  是防止用户直接传值为nil或者NULL
         */
        self.fileName = nil;
        /**注意：当返回值为nil或者NULL的时候，表示会在临时目录下创建一个空的数据库，当FMDatabase连接关闭的时候，文件也会被删除**/
    }else{
        /**
         *  判断用户是否为数据库文件添加后缀名
         */
        if ([dbName hasSuffix:@".sqlite"]) {
            self.fileName = dbName;
        }else{
             self.fileName = [dbName stringByAppendingString:@".sqlite"];
        }
    }
    
    //创建数据库
    /*
    if (!self.queue) {
        self.queue = [FMDatabaseQueue databaseQueueWithPath:[self DBPath]];
    }
    */
    self.queue = [FMDatabaseQueue databaseQueueWithPath:[self DBPath]];
}

#pragma mark - 插入一列
-(BOOL)addAColumn:(NSString *)columnName inTableWithName:(NSString *)tableName{
    
    __block BOOL result = YES;
    [self.queue inDatabase:^(FMDatabase * _Nonnull db) {
        
        if ([db open]) {
            
            BOOL sucess = YES;
            if (![db columnExists:columnName inTableWithName:tableName])
            {
                NSString *sql = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN '%@' TEXT", tableName, columnName];
                sucess = [db executeUpdate:sql];
            }
            
            [db close];//关闭数据库
        
            result = sucess;
        }else{
            [db close];//关闭数据库
            
            result = NO;
        }
        
    }];
    
    return result;
}

#pragma mark - 更新时使用了串行来搞
- (BOOL)notResustSetWithSql:(NSString*)sql{
    
    __block BOOL result = YES;
    [self.queue inDatabase:^(FMDatabase * _Nonnull db) {
        
        if ([db open]) {
            
            BOOL isUpdate = [db executeUpdate:sql];
            
            [db close];//关闭数据库
        
            result = isUpdate;
        }else{
            [db close];//关闭数据库
            
            result = NO;
        }
        
    }];
    
    return result;
}

#pragma mark - 查询
-(NSArray *)qureyWithSql:(NSString *)sql{
    
    //声明可变数组，用来存放所有的记录
    __block NSMutableArray* resultArr = [[NSMutableArray alloc] init];
    
    [self.queue inDatabase:^(FMDatabase * _Nonnull db) {
        
        //打开数据库
        if ([db open]) {
            //NSLog(@"打开数据库成功");
            
            //得到所有记录的结果集
            FMResultSet *resuluSet = [db executeQuery:sql];

            // 遍历结果集，取出每一条记录,并且存储到可变数组中
            while ([resuluSet next]) {
                //直接将一条记录转换为字典类型
                NSDictionary *dic =[resuluSet resultDictionary];
                [resultArr addObject:dic];
            }
            
            //释放结果集
            [resuluSet close];
            
            //关闭数据库
            //[db close];
            
        }else{
            NSLog(@"打开数据库失败");
            [db close];
        }
        
    }];
    
    return resultArr;
}

-(NSString *)DBPath{
    
    if (self.fileName) {
        
        //说明fileName有值
        //使用uidMD5之后的数据作为文件夹分类。
        NSString* savePath = [NSString stringWithFormat:@"%@/%@",[UBMPbFileManger libraryBasePath],self.fileName];
        
        return savePath;
        
    }else{
        return @"";
    }
}

@end
