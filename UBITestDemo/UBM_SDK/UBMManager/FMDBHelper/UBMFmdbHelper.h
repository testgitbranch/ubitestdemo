//
//  UBMFmdbHelper.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/12/12.
//  Copyright © 2020 魏振伟. All rights reserved.
//

#import <fmdb/FMDatabase.h>//数据库的类 相当于sqlite3* 句柄（一个FMDatabase对象就代表一个单独的SQLite数据库，用来执行SQL语句）。
#import <fmdb/FMResultSet.h>//伴随指针，使用FMDatabase执行查询后的结果集
#import <fmdb/FMDatabaseAdditions.h>//FMDatabase类的一个类目,是给FMDatabase的一个扩展
#import <fmdb/FMDatabaseQueue.h>//用与在多线程中执行多个查询或者更新，它能够保证线程是安全的。
#import <fmdb/FMDatabasePool.h>

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UBMFmdbHelper : NSObject

//单例
+ (instancetype)sharedFMDBHelper;

//创建一个表或者准备打开一个表。
- (void)createDBWithName:(NSString *)dbName;

//给一个表新增一个字段
- (BOOL)addAColumn:(NSString*)columnName inTableWithName:(NSString*)tableName;

//无返回结果集的操作：增删改
- (BOOL)notResustSetWithSql:(NSString*)sql;

//有返回结果集的操作：查
- (NSArray *)qureyWithSql:(NSString *)sql;


@end

NS_ASSUME_NONNULL_END
