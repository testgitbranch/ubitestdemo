//
//  UBMNotificationNameConst.m
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/12/10.
//  Copyright © 2020 魏振伟. All rights reserved.
//

#import "UBMNotificationNameConst.h"

NSString *const UBMStartNotification = @"UBMStartNotification";
NSString *const UBMStartResultNotification = @"UBMStartResultNotification";
NSString *const UBMEndNotification = @"UBMEndNotification";
NSString *const UBMEndResultNotification = @"UBMEndResultNotification";
NSString *const UBMUIdataUpdateNotification = @"UBMUIdataUpdateNotification";
NSString *const UBMSpeedAutoNotification = @"UBMSpeedAutoNotification";
