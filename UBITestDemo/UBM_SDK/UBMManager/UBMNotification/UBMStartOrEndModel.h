//
//  UBMStartOrEndModel.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/12/11.
//  Copyright © 2020 魏振伟. All rights reserved.
//

/*
 开始或者结束行程通知时的model
 */

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef enum : NSUInteger {
    ubmUserMovementType,  //手动触发类型
    ubmAutoType,          //自动触发类型
    ubmAbnormalType,      //异常开启类型
} UBMOrigintype;

@interface UBMStartOrEndModel : NSObject

@property(nonatomic, assign)UBMOrigintype origintype;
@property(nonatomic, copy)NSString* incorrectLocTripdId; //异常结束的LoctripID(本地tripid)。
@property(nonatomic, copy)NSString* tripId; //网络tripid

@end

NS_ASSUME_NONNULL_END
