//
//  UBMResultModel.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/12/12.
//  Copyright © 2020 魏振伟. All rights reserved.
//

/*
 行程结束结果的model
 */

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UBMResultModel: NSObject

@property(nonatomic, assign)BOOL resultCode;  //Yes: 成功 NO:失败
@property(nonatomic, copy, nullable)NSString* tripId;   //行程ID

@end

NS_ASSUME_NONNULL_END
