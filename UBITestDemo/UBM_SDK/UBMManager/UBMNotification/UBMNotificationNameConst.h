//
//  UBMNotificationNameConst.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/12/10.
//  Copyright © 2020 魏振伟. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

UIKIT_EXTERN NSString *const UBMStartNotification;  //通知ubmMannger开始收集。传入UBMStartOrEndModel
UIKIT_EXTERN NSString *const UBMStartResultNotification;    //开始收集结果回调。传出：UBMResultModel
UIKIT_EXTERN NSString *const UBMEndNotification;    //通知ubmMannger结束收集。传入UBMStartOrEndModel
UIKIT_EXTERN NSString *const UBMEndResultNotification;  //结束收集结果回调。传出：UBMResultModel
UIKIT_EXTERN NSString *const UBMUIdataUpdateNotification;   //实时UI数据更新通知 传出UBMTipUIModel
UIKIT_EXTERN NSString *const UBMSpeedAutoNotification;  //速度更新通知 传出UIDataModel
