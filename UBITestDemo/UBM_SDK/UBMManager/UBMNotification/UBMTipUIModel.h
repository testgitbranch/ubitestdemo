//
//  UBMTipUIModel.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/12/11.
//  Copyright © 2020 魏振伟. All rights reserved.
//

/*
 行程中实时数据model
 */

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UBMTipUIModel : NSObject

@property(atomic, copy)NSString* mileage;   //km
@property(atomic, copy)NSString* totalTime; //00：00：00
@property(atomic, copy)NSString* speed;     //km/h
@property(atomic, assign)double speedAccuracy; //速度精确度

@end

NS_ASSUME_NONNULL_END
