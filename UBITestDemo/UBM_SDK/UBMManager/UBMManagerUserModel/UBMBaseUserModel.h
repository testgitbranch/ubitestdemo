//
//  UBMBaseUserModel.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2021/1/18.
//  Copyright © 2021 魏振伟. All rights reserved.
//

/*
 ubm数据管理类的基础用户数据。用来区分存储用户行程数据。
 */

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UBMBaseUserModel : NSObject

+(instancetype)shared;

@property(nonatomic, copy)NSString* userId;  //用户id
@property(nonatomic, copy)NSString* vid;  //车id

@end

NS_ASSUME_NONNULL_END
