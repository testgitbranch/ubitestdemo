//
//  UBMUserModel.m
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/8/4.
//  Copyright © 2020 魏振伟. All rights reserved.
//

#import "UBMUserModel.h"
//#import "ClearCacheTool.h"
#import <WebKit/WebKit.h>
#import "UBMUserModel.h"
#import "UBMMainDataModel.h"
#import "UBMPrefixHeader.h"
#import "UBMNetWorkToolOfJson.h"
#import "UBITracking.h"

@implementation UBMUserModel

+(instancetype)shared
{
    static id instance;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    
    return instance;
}

- (id)init{
    self = [super init];
    if (self) {
        self.uid = @"";
        self.nickname = @"";
        self.portrait = @"";
        self.phone = @"";
    }
    return self;
}

@end
