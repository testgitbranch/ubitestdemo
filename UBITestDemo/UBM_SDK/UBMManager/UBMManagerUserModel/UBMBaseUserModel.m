//
//  UBMBaseUserModel.m
//  LoveCarChannel_iOS
//
//  Created by wzw on 2021/1/18.
//  Copyright © 2021 魏振伟. All rights reserved.
//

#import "UBMBaseUserModel.h"

@implementation UBMBaseUserModel

+(instancetype)shared
{
    static id instance;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    
    return instance;
}

- (id)init{
    self = [super init];
    if (self) {
        
        self.userId = @"";
        self.vid = @"";
    }
    return self;
}

@end
