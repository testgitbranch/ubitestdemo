//
//  UBMUserModel.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/8/4.
//  Copyright © 2020 魏振伟. All rights reserved.
//

/*
 用户信息单例
 */

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UBMUserModel : NSObject

@property(nonatomic, copy)NSString* uid;
@property(nonatomic, copy)NSString* name;
@property(nonatomic, copy)NSString* nickname;
@property(nonatomic, copy)NSString* phone;
@property(nonatomic, copy)NSString* portrait;  //头像

@property(nonatomic, copy)NSString* level;     //用户等级
@property(nonatomic, copy)NSString* levelName; //等级名称
@property(nonatomic, copy)NSString* levelIcon; //等级logo

@property(nonatomic, copy)NSString* biz_mga_status;  //是否是mga用户
@property(nonatomic, copy)NSString* biz_hz_status;   //是否是互助用户

@property(nonatomic, copy)NSString* card_no;         //银行卡号                 (独立接口获取)
@property(nonatomic, strong)NSArray* carArr;         //用户名下车辆信息          （独立接口获取）
@property(nonatomic, strong)NSArray* oldCarArr;      //用户名下有效车辆信息       （独立接口获取）
@property(nonatomic, copy)NSString* has_ali_token;   //是否有支付宝授权：1是  0否  （独立接口获取）

+ (instancetype)shared;

@end

NS_ASSUME_NONNULL_END
