//
//  UBITracking.h
//  UbmSDK
//
//  Created by 金鑫 on 2021/11/15.
//

#import <Foundation/Foundation.h>
#import "UBMResultModel.h"
#import "UBMStartOrEndModel.h"
#import "UBMTripTableModel.h"
#import "UBMManager.h"
#import "UBICredentialProvider.h"
// FIXME: jinxin
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UBITracking : NSObject
@property (nonatomic, copy, nullable) NSString *userID;
@property(nonatomic, strong, nullable) UBICredentialProvider *provider; //Passprot Token
@property(atomic, assign)BOOL isCollecting; //是否收集中
@property(atomic, copy)NSString* nowTripid; //收集中的tripId;

+ (instancetype)shared;

- (void)enableSDK:(NSString * _Nonnull)userId provider:(UBICredentialProvider * _Nonnull)provider;

- (void)startTrip;

- (void)stopTrip:(void(^)(UBMResultModel* result))callback;

- (void)disableSDK:(void(^)(UBMResultModel* result))callback;

//- (void)setUserToken:(NSString *)userToken;

//- (NSString *)userToken;

//- (void)setUserId:(NSString*)userId;

// FIXME: jinxin 去掉不必要的Api
/*
 如果数据要与车绑定，则需要调用本方法。vid只是作为一个行程的车辆参数。
 vid:车id。
 */
//-(void)setVid:(NSString*)vid;

/*
 自动拉起异常结束行程的最大时间。默认15分钟内。
 interval:分钟数。
 */
//-(void)keepLastTripWithInterval:(int)interval keepLastTripResult: (keepLastTripResultBlock)lastTripResultBlock;

//权限请求。可以选择第一次在哪里请求位置权限。当然，当开启之时也会默认先调用一次。
//-(void)prepareCoreMotionCenter;


//开启自动检测记录：会自动判断开车时机并开启记录。
-(void)startAutoRecord;

//关闭自动检测记录。比如：退出登录后。
-(void)stopAutoRecord;


//开使收集数据。当isCollecting==yes，会返回失败。
//-(void)startRecord:(UBMStartOrEndModel*)startModel andResult:(void(^)(UBMResultModel* result))startBack;

//停止收集数据。返回resultcode失败，代表没有获取到网络tripid。停止是绝对会的。
//-(void)stopRecord:(UBMStartOrEndModel*)startModel andResult:(void(^)(UBMResultModel* result))endBack;


//查找未结束的行程：hadStop==NO
//-(NSArray*)getAllUnStopTrip;

//处理异常停止的trip并上传、校验、合并：hadStop==NO
//-(void)stopAndUploadIncorrectTrip:(NSString*)locTripID;


//查找本地UploadOver==NO的行程。（hadStop==YES && UploadOver==NO）
-(NSArray*)getAllUnUploadOverTrip;

//检查并上传本地所有的未完全结束掉的trip。（hadStop==YES && UploadOver==NO）
-(void)checkAndUploadLocTrip:(void(^)(BOOL result))callBack;;


//上传指定tripIds的行程
-(void)uploadTheTripsOfTripids:(NSArray*)tripids andCallBack:(void(^)(BOOL result))upresult;



//设置是否在导航中
//-(void)setIsNaving:(BOOL)isNav;

//设置是否在结束倒计时
//-(void)setIsStoping:(BOOL)isStoping;

//关闭Ubm
//-(void)closeUbm;

//打开自动记录开关
//-(void)openAutoRecordSwitch;

//关闭自动记录开关
//-(void)closeAutoRecordSwitch;

//开启定期检测未上传行程并予以上传机制
//- (void)startCycleCheckAndUploadLocTrip;

//关闭定期检测未上传行程并予以上传机制
//- (void)stopCycleCheckAndUploadLocTrip;

@end

NS_ASSUME_NONNULL_END
