//
//  UBMConfigure.h
//  UBM_SDK
//
//  Created by 金鑫 on 2021/11/24.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UBMConfigure : NSObject

@property (nonatomic, copy) NSString *uid;

@property (nonatomic, copy) NSString *nickname;

@property (nonatomic, copy) NSString *portrait;   //头像

@property (nonatomic, copy) NSString *phone;

+ (instancetype)shared;

@end

NS_ASSUME_NONNULL_END
