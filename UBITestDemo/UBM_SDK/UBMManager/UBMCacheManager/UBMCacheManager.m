//
//  UBMCacheManager.m
//  LoveCarChannel_iOS
//
//  Created by wzw on 2021/4/15.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

#import "UBMCacheManager.h"
#import "UBMPbFileManger.h"
#import "UBMSqliteHelper.h"
#import "UBMBaseUserModel.h"

@implementation UBMCacheManager

+(void)removeCacheBeforeOfDays:(int)days{
    
    //取出数据库中所有已经上传成功的数据
    NSArray* roteArray = [UBMSqliteHelper.sharedHelper getUBMTripTableModelOfUploadOver:YES];
    if (roteArray == nil || roteArray.count == 0) {
        return;
    }
    
    //取出days之前的数据数据
    NSDate* date = [NSDate dateWithTimeIntervalSinceNow:0];
    NSTimeInterval time= [date timeIntervalSince1970]; //s
    
    for (int i=0; i<roteArray.count; i++) {
        UBMTripTableModel* model = roteArray[i];
        NSTimeInterval itemTime = [model.lastUpdateTime doubleValue];
        
        if ((time-itemTime) > days*24.0*60.0*60.0) {
            
            //删除这些数据对应的本地pb文件
            [UBMCacheManager removeLocalRouteData:model];
            
        }
        
    }
    
}

+(void)removeLocalRouteData:(UBMTripTableModel*)tripModel{
    
    if (tripModel.tripPBList == nil || tripModel.tripPBList.count==0 ) {
        return;
    }
    
    NSArray* pbfileArr = [UBMPbFileManger getAllFileAtPath:[UBMPbFileManger pbFolderPathWithTripId:tripModel.tripID]];
    if (pbfileArr == nil || pbfileArr.count==0 ) {
        NSLog(@"文件夹已为空");
        return;
    }
    
    NSString* folderPath = [UBMPbFileManger pbFolderPathWithLocTripId:tripModel.locTripID];
    
    for (int x=0; x<pbfileArr.count; x++) {
        
        NSString* pbFileName = [NSString stringWithFormat:@"%@", pbfileArr[x]];
        NSString* filePath = [NSString stringWithFormat:@"%@/%@", folderPath, pbFileName];
        
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            
            [UBMPbFileManger deleteFileAtPath:filePath];
            
        });
        
    }
    
}

@end
