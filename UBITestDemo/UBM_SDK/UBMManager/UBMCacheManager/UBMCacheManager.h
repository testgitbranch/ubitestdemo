//
//  UBMCacheManager.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2021/4/15.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UBMCacheManager : NSObject

//移除 days 天之前的pb数据文件(只删除已上传成功的)
+(void)removeCacheBeforeOfDays:(int)days;

@end

NS_ASSUME_NONNULL_END
