//
//  PrefixHeader.pch
//  Ubm
//
//  Created by 金鑫 on 2021/10/14.
//

#ifndef PrefixHeader_pch
#define PrefixHeader_pch

#import <UIKit/UIkit.h>
#import <objc/runtime.h>
#import "UBMBaseUserModel.h"
#import "UBMUserModel.h"
#import "UBMViewTools.h"
#import "UBMMTools.h"
#import "UBMUdKeys.h"
#import "UBMUrls.h"
#import "UBMNetWorkTool.h"
#import "UBMDomains.h"
#import "UBMManager.h"
//#import "ShareClass.h"
#import "UBMSLSLog.h"
#import "UBMUaGet.h"
#import "UBMNotifications.h"
#import "UBMJugeBaseURLWIthRequest.h"
#import "UBMToastObjc.h"
#import "UBMWCustomAlert.h"
#import "UIView+UBMLayout.h"
#import "UBMUiUtil.h"
#import "NSDate+UBMExtension.h"
#import "UBMNotificationNameConst.h"
#import "UBMResultModel.h"
#import "UBMStartOrEndModel.h"
#import "UIColor+UBMHex.h"
#import <AVFAudio/AVAudioSession.h>
#import <AliyunLogProducer/AliyunLogProducer.h>
#import <SDWebImage/SDWebImage.h>
#import <Masonry/Masonry.h>
#import <MJExtension/MJExtension.h>
#import <ZXNavigationBar/ZXNavigationBar.h>
#import <AMapNaviKit/AMapNaviKit.h>
#import <WebKit/WKWebView.h>
#import "UBMWJProgressHUD.h"
#import "UBMWBaseAlertView.h"
#import "UBMUdKeys.h"
#import "UIImage+UBMBundle.h"
#import "UBICredentialProvider.h"
#import "UBITracking.h"
#import "UBMGetDeviceSomeFrame.h"

//#import "UIStackView+UBMSeparator.h"
// Include any system framework and library headers here that should be included in all compilation units.
// You will also need to set the Prefix Header build setting of one or more of your targets to reference this file.

//#import "ToastObjc.h"

//#define BaseUrl  [UBMDomainName.shareDomain baseURL]                 //APP前端 地址
//#define HiveBaseUrl  [UBMDomainName.shareDomain hiveURL]         //大修车公众号前端 地址
//#define RepairBaseUrl  [UBMDomainName.shareDomain repairURL]     //小修宝公众号前端 地址
//#define PayBaseUrl  [UBMDomainName.shareDomain payURL]           //接口 pay 地址
//#define ActBaseUrl  [UBMDomainName.shareDomain actURL]           //接口 act 地址
//#define UBMBaseUrl  [UBMDomainName.shareDomain ubmURL]           //接口 ubm 地址
//#define AthenaBaseUrl  [UBMDomainName.shareDomain athenaURL]      //接口 Athena 地址
//#define UCBaseUrl   [UBMDomainName.shareDomain ucURL]   
//
//
//#pragma mark - 第三方数据
//#define JPUSHAppKey  @"f94fc4c6d8bab8451293ee5e"  //极光appkey
//
//#define CUSTOMER_SERVICE @"400-6680122"    //申请修车客服电话
//
//#pragma mark - 尺寸
//#define ScreenWidth    [UIScreen mainScreen].bounds.size.width
//#define ScreenHeight   [UIScreen mainScreen].bounds.size.height
//#define kViewWidth     self.view.frame.size.width
//#define kViewHeight    self.view.frame.size.height
//
//#define kIs_iPhoneX         [UBMGetDeviceSomeFrame share].wIsiPhoneX
//#define HeightStatusBar     [UBMGetDeviceSomeFrame share].wHeightOfStatusBar  //状态栏高度
//#define HeightNavBar        [UBMGetDeviceSomeFrame share].wHeightOfNavBar     //导航栏+状态栏高度
//#define HeightTabBar        [UBMGetDeviceSomeFrame share].wHeightOfTabBar     //tabar高度
//#define HeightSafeBottom    [UBMGetDeviceSomeFrame share].wHeightOfSafeBottom //安全区域底部高度
//
//#define vw(width)           ([[UBMUiUtil shareInit] vw:width])                //根据屏宽计算宽高
//
//
//#pragma mark - 字符串判空
//#define  IsEmptyStr(string) (string == nil || string == NULL ||[string isKindOfClass:[NSNull class]]|| [string isEqualToString:@""] || [string isEqualToString:@"<null>"] || [string isEqualToString:@"(null)"] || [[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0 ? YES : NO)          //空字符串 = yes
//#define ChangeToString(idStr) [UBMMTools changeToString:idStr]  //强制转成字符串
//
//
//#pragma mark - 系统版本判断
//#define IS_IOS_VERSION   floorf([[UIDevice currentDevice].systemVersion floatValue])
//
//#pragma mark - 色值
//#define ColorSet(x,y,z,al) [UIColor colorWithRed:x/255.0 green:y/255.0 blue:z/255.0 alpha:al]
//#define ColorWithRGB(x) [UBMMTools colorWithRGB:x]
//#define kMainColor      [UIColor colorWithRed:255/255.0 green:121/255.0 blue:24/255.0 alpha:1]  //APP橘黄风格基础色值
//#define MainAlphe 0.55    //提示框背景黑色透明度
//
//#pragma mark - toast提示
//#define Toast(title)        [UBMToastObjc toastWithText:title andtime:3]
//#define ToastWithTime(title, time)        [ToastObjc toastWithText:title andtime:time]
//#define ToastSuccess(title) [ToastObjc toastSuccessWithText:title andtime:3]
//#define ToastFile(title)    [ToastObjc toastFailWithText:title andtime:3]
//
//
//#pragma mark - 定义常用字符串
//#define ErrorHint             @"数据出了点问题哦，请稍后再试"
////#define NetErrorHint          @"服务器错误，请检查网络或稍后再试"
//#define NoNetError            @"无法获取网络，请检查网络连接后重试"
//
//
//#pragma mark - nslog
//#ifndef __OPTIMIZE__
//#define NSLog(...) NSLog(__VA_ARGS__)
//#else
//#define NSLog(...) {}
//#endif
//
//#ifndef strongify
//#if DEBUG
//#if __has_feature(objc_arc)
//#define bx_strongify(object) autoreleasepool{} __typeof__(object) object = weak##_##object;
//#else
//#define bx_strongify(object) autoreleasepool{} __typeof__(object) object = block##_##object;
//#endif
//#else
//#if __has_feature(objc_arc)
//#define bx_strongify(object) try{} @finally{} __typeof__(object) object = weak##_##object;
//#else
//#define bx_strongify(object) try{} @finally{} __typeof__(object) object = block##_##object;
//#endif
//#endif
//#endif
//
//#ifndef weakify
//#if DEBUG
//#if __has_feature(objc_arc)
//#define bx_weakify(object) autoreleasepool{} __weak __typeof__(object) weak##_##object = object;
//#else
//#define bx_weakify(object) autoreleasepool{} __block __typeof__(object) block##_##object = object;
//#endif
//#else
//#if __has_feature(objc_arc)
//#define bx_weakify(object) try{} @finally{} {} __weak __typeof__(object) weak##_##object = object;
//#else
//#define bx_weakify(object) try{} @finally{} {} __block __typeof__(object) block##_##object = object;
//#endif
//#endif
//#endif

#endif /* PrefixHeader_pch */
