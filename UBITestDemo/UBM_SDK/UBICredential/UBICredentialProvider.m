//
//  UBICredentialProvider.m
//  LoveCarChannel_iOS
//
//  Created by 金鑫 on 2021/12/1.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

#import "UBICredentialProvider.h"

@implementation UBICredentialProvider

- (instancetype)initWithPassportToken:(NSString *)token delegate:(id<UBICredentialProviderDelegate>)delegate {
    self = [super init];
    if (self) {
        self.passportToken = token;
        self.delegate = delegate;
    }
    return self;
}

@end
