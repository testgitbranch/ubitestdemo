//
//  UBICredentialProvider.h
//  LoveCarChannel_iOS
//
//  Created by 金鑫 on 2021/12/1.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol UBICredentialProviderDelegate <NSObject>
@required
- (NSString * _Nullable)UBICredentialRefreshPassportToken;

@end

@interface UBICredentialProvider : NSObject
@property (nonatomic, weak) id<UBICredentialProviderDelegate> delegate;
@property (nonatomic, copy) NSString *passportToken;

- (instancetype)initWithPassportToken:(NSString * _Nonnull)token delegate:(id<UBICredentialProviderDelegate> _Nonnull) delegate;

@end

NS_ASSUME_NONNULL_END
