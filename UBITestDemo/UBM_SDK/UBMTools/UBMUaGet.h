//
//  UBMUaGet.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2021/8/20.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UBMUaGet : NSObject

+(NSString*)getUAContent;

@end

NS_ASSUME_NONNULL_END
