//
//  UBMUiUtil.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/7/16.
//  Copyright © 2020 魏振伟. All rights reserved.
//

/*
长度适配：根据设计稿屏宽跟实际屏宽计算当前长度。
*/

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UBMUiUtil : NSObject

+(instancetype)shareInit;

-(int)vw:(float)width;
-(int)vh:(float)height;

@end

NS_ASSUME_NONNULL_END
