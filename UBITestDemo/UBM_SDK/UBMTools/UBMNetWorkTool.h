//
//  UBMNetWorkTool.h
//  EngineeProject
//
//  Created by Edog on 2017/6/5.
//  Copyright © 2017年 mac. All rights reserved.
//

#import <AFNetworking/AFHTTPSessionManager.h>

typedef void(^sucessBlock)(id response);
typedef void(^errorBlock)(id response);

typedef void(^sucessBlockOfData)(NSData* response);

@interface UBMNetWorkTool : AFHTTPSessionManager

#pragma mark - 初始化函数，必须调用
+(instancetype)shareNetWorkTool;

//签名算法，可用于继承重写。
-(NSDictionary*)encryptParameters:parameters url:baseUrl;

/**
 推荐(默认表单格式上传参数)
 */
#pragma mark - post请求1：返回解析data后的字典数据
-(void)postRequestBaseUrl:(NSString*)baseUrl lastUrl:(NSString*)lastUrl par:(id)parameters showHUD:(BOOL)show showError:(BOOL)showError backInMainQ:(BOOL)inMainQ success:(sucessBlock)success error:(errorBlock)errorx;

/**
 推荐(默认表单格式上传参数)
 */
#pragma mark - post请求2：返回未解析的原nsdata数据
-(void)postRequestBaseUrl:(NSString*)baseUrl lastUrl:(NSString*)lasturl par:(id)parameters showHUD:(BOOL)show showError:(BOOL)showError backInMainQ:(BOOL)inMainQ originalData:(sucessBlockOfData)success error:(errorBlock)errorx;



#pragma mark - 比较完全的POST网络请求方法
-(void)postRequestWithBaseUrl:(NSString*)baseUrl lastUrl:(NSString*)URLString parameters:(id)parameters needShowHUD:(BOOL)showHUD needShowError:(BOOL)showError needBackInMainQueue:(BOOL)shouldBackInMainQueue success:(sucessBlock)success error:(errorBlock)errorx;

#pragma mark - 可选择请求方式的网络请求方法："POST"、"GET"
-(void)requestWithBaseUrl:(NSString*)baseUrl UrlString: (NSString*)URLString parameters: (id)parameters method:(NSString*)method success:(sucessBlock)success error:(errorBlock)errorx isShowHUD:(BOOL)showHUD;

#pragma mark - 上传文件 “POST”
-(void)uplodImgWithBaseUrl:(NSString *)baseUrl UrlString:(NSString *)URLString andParameters:(id)parameters andFileData:(NSData *)fileData FileName:(NSString*)fileName method:(NSString *)method success:(sucessBlock)success error:(errorBlock)errorx isSync:(BOOL)showHUD;

#pragma mark - 下载一个文件
-(void)downLoadFileOfPath:(NSString*)filePath success:(sucessBlock)success error:(errorBlock)errorx;

#pragma mark - 取消网络请求
- (void) cancelRequest;

@end
