//
//  UBMUrls.h
//  LoveCarChannel_iOS
//
//  Created by 金鑫 on 2021/6/22.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//
/*
 url地址管理
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

UIKIT_EXTERN NSString * const UBMChannelURL;    //原APP网页      主页地址
UIKIT_EXTERN NSString * const UBMHiveURL;       //大修车公众号    主页地址
UIKIT_EXTERN NSString * const UBMRepairURL;     //小修车公众号    主页地址

#pragma mark - web地址

UIKIT_EXTERN NSString * const UBMPublicityListURL;     //公示列表
UIKIT_EXTERN NSString * const UBMPublicityURL;         // 公示详情
UIKIT_EXTERN NSString * const UBMFactoryUrl;           //修理厂
UIKIT_EXTERN NSString * const UBMHelpEventList;        //我的修车
UIKIT_EXTERN NSString * const UBMPlanList;             //我的计划列表
UIKIT_EXTERN NSString * const UBMPlanInfor;      //计划详情
UIKIT_EXTERN NSString * const UBMOpenCity;       //开放城市
UIKIT_EXTERN NSString * const UBMMsgUrl;         //消息
UIKIT_EXTERN NSString * const UBMCpuGame;        //猜杯子游戏页面
UIKIT_EXTERN NSString * const UBMMyShareDetail;  //分摊明细
UIKIT_EXTERN NSString * const UBMMyCaseDetail;   //事件详情
UIKIT_EXTERN NSString * const UBMReportCase;     //服务保障说明
UIKIT_EXTERN NSString * const UBMBankCard;       //银行卡页面

UIKIT_EXTERN NSString * const UBMRedPacket;  //ubm活动-优惠券页面
UIKIT_EXTERN NSString * const UBMRank;       //ubm活动-排名
UIKIT_EXTERN NSString * const UBMPK;         //ubm活动-pk赛
UIKIT_EXTERN NSString * const UBMRules;      //ubm活动-规则
UIKIT_EXTERN NSString * const UBMAutoRecordSetting; //ubm自动记录设置

UIKIT_EXTERN NSString * const UBMBaikeURl;   //产品百科
UIKIT_EXTERN NSString * const UBMGongyue;    //平台公约
UIKIT_EXTERN NSString * const UBMZhangCheng;            //计划章程
UIKIT_EXTERN NSString * const UBMPrivacyAgreement;       //隐私协议
UIKIT_EXTERN NSString * const UBMUserAgreement;          //用户协议
UIKIT_EXTERN NSString * const UBMNonAccidentRescueAgreement;  //非事故道路救援服务细则
UIKIT_EXTERN NSString * const UBMZbAuthorization;            //《武汉众邦银行委托扣款授权书》

UIKIT_EXTERN NSString * const UBMAgreement;    //共享修车计划协议地址
UIKIT_EXTERN NSString * const UBMComplianceExam;  //合规答题（邀请好友）
UIKIT_EXTERN NSString * const UBMComplianceExam2; //合规答题（提现）

UIKIT_EXTERN NSString * const UBMFeedback;   //问题反馈
UIKIT_EXTERN NSString * const UBMOpenAcount; //开户流程

UIKIT_EXTERN NSString * const UBMPlanJoinUrl;     //计划页面立即加入
UIKIT_EXTERN NSString * const UBMInsurHomeUrl;    //“服务”->保险页面
UIKIT_EXTERN NSString * const UBMInsurMyClaims;   //'理赔记录'页面
UIKIT_EXTERN NSString * const UBMInsurMyPolicy;   //'我的保单'页面
UIKIT_EXTERN NSString * const UBMPrizepointRule;  //奖励分规则

UIKIT_EXTERN NSString* const UBMBenefits; //福利页地址

UIKIT_EXTERN NSString* const UBMNewsCenter; //新闻中心
UIKIT_EXTERN NSString* const UBMServiceNetwork; //服务网络

#pragma mark - 接口地址

//登录/注册
UIKIT_EXTERN NSString * const UBMWXAuthLogin;  //微信授权登录
UIKIT_EXTERN NSString * const UBMWxBindPhone;  //微信与手机号绑定
UIKIT_EXTERN NSString * const UBMPhoneLogin;   //手机号登录
UIKIT_EXTERN NSString * const UBMGetPhoneSMS;  //获取手机验证码
UIKIT_EXTERN NSString * const UBMReviewLogin; //审核期新旧登录判断接口
UIKIT_EXTERN NSString * const UBMUserLogout;   //用户退出登录

//首页接口
UIKIT_EXTERN NSString * const UBMVersionMangeURL;    //是否更新
UIKIT_EXTERN NSString * const UBMMedialInfo;        //获取某个坑位信息
UIKIT_EXTERN NSString * const UBMHomeRecommend;     //首页精选推荐
UIKIT_EXTERN NSString * const UBMShowOpenAcount;    //是否展示开户弹窗
UIKIT_EXTERN NSString * const UBMUnclaimedBenefits; // 未领取的福利
UIKIT_EXTERN NSString * const UBMCardInfo;     //Ubm卡片
UIKIT_EXTERN NSString * const UBMVehicleInfo;     //车辆信息
UIKIT_EXTERN NSString * const UBMQuestionDataURL; //常见问题
UIKIT_EXTERN NSString * const UBMGetRewardPopup;  //获取自动记录奖励金接口
UIKIT_EXTERN NSString * const UBMDynamicNews; //新闻中心
//UIKIT_EXTERN NSString * const NetServe;   //网络服务
UIKIT_EXTERN NSString * const UBMNewsCategory;  //新闻分类接口
UIKIT_EXTERN NSString * const UBMMediaAllByPitkeySortList;  //瀑布流list接口

//活动 页面
UIKIT_EXTERN NSString * const UBMGetMyGradeURL;          //我的战绩接口-勋章等级
UIKIT_EXTERN NSString * const UBMGetUserRewardListURL;   //每日问答轮播消息
UIKIT_EXTERN NSString * const UBMGetDailyQuestioURL;     //每日问答题目
UIKIT_EXTERN NSString * const UBMCheckAnswerRightURL;    //答题
UIKIT_EXTERN NSString * const UBMGetdoLotteryURL;        //问答 抽奖接口-是否中奖
UIKIT_EXTERN NSString * const UBMGetofferRewardURL;      // 每日问答 抽奖-领奖

UIKIT_EXTERN NSString * const UBMGetMySignURL; //获取签到信息
UIKIT_EXTERN NSString * const UBMWeekSiginURL; //获取7天的签到信息
UIKIT_EXTERN NSString * const UBMDoSignURL;    //签到
UIKIT_EXTERN NSString * const UBMGetEggURL;   //砸蛋领奖。  如果是第七天签到直接调这个接口领奖。然后出蛋。
UIKIT_EXTERN NSString * const UBMDianZan;     //点赞


//我的 页面
UIKIT_EXTERN NSString * const UBMGetUserinfor;  //获取用户信息
UIKIT_EXTERN NSString * const UBMUserProfile;   //新的获取用户信息（2.9.0）
UIKIT_EXTERN NSString * const UBMGetBankInfor;  //获取用户银行卡信息
UIKIT_EXTERN NSString * const UBMNewMessage;    //是否有新消息
UIKIT_EXTERN NSString * const UBMGetAliStatus;  //是否已获得支付宝授权
 
UIKIT_EXTERN NSString * const UBMUpdateUserDevice; //更新用户极光ID

UIKIT_EXTERN NSString * const UBMRewardType;    //奖励类型
//UIKIT_EXTERN NSString * const RewardList;    //奖励列表
//UIKIT_EXTERN NSString * const RewardAcount;  //奖励账户金额
//UIKIT_EXTERN NSString * const RewardWithdraw;  //活动奖励提现
//UIKIT_EXTERN NSString * const RewardGetSign;   //获取支付宝授权参数URL
//
//UIKIT_EXTERN NSString * const SignAcount;      //获取用户账户签约状态
//UIKIT_EXTERN NSString * const GetSignPathURL;  //获取签约链接
//UIKIT_EXTERN NSString * const MoneyTixian;     //通过签约账户提现
//
//UIKIT_EXTERN NSString * const IntegralType;     //积分类型
//UIKIT_EXTERN NSString * const IntegralAcount;   //总积分
//UIKIT_EXTERN NSString * const IntegralList;     //积分列表
//UIKIT_EXTERN NSString * const GetShareList;     //获取分摊列表
//
//UIKIT_EXTERN NSString * const GetShareImage;      //邀请图片
//UIKIT_EXTERN NSString * const GetExaminationList; //获取合规信息接口
//
UIKIT_EXTERN NSString * const UBMGetCarList;   //获取车辆列表信息 2.5.0新(融合了计划和保险车辆) //= @"api/v1/Vehicle/getVehicleList";
UIKIT_EXTERN NSString * const UBMOldCarData;   //以前的获取车辆列表信息2.0.3之前
//UIKIT_EXTERN NSString * const GetCarInfor;  // 获取车辆信息 2.0.8更换 // = @"api/v1/Vehicle/getVehicleInfo"; // 获取车辆信息
//UIKIT_EXTERN NSString * const MgaGetCardInfo; //获取保险车辆列表接口
UIKIT_EXTERN NSString * const UBMPlanJoin;      //获取是否显示立即加入banner
//
//
////计划页
UIKIT_EXTERN NSString * const UBMGongShiInfor;  //首页公示信息
//UIKIT_EXTERN NSString * const eventState;    //车辆维修状态
UIKIT_EXTERN NSString * const UBMReloadPlan;    //恢复计划
//
//UIKIT_EXTERN NSString * const KongKimUrl;       //获取金刚位
UIKIT_EXTERN NSString * const UBMKengWeiInfor;     //获取坑位信息
//UIKIT_EXTERN NSString * const TapKengWeiEvent;   //点击坑位统计接口
//UIKIT_EXTERN NSString * const TimeStatistics;    //统计时长
//UIKIT_EXTERN NSString * const HomeOperating;     //运营弹框数据
//
//UIKIT_EXTERN NSString * const PingZhengInfor;    //获取共享修车凭证信息
//UIKIT_EXTERN NSString * const PingZhengImg;      //获取共享修车凭证图片
//
UIKIT_EXTERN NSString * const UBMPhoneBookReport; //保存成功，上传接口
UIKIT_EXTERN NSString * const UBMPhoneBookdata;   //保存手机号到通讯录
//
////ubm
UIKIT_EXTERN NSString * const UBMGetTripId;   //获取行程id
UIKIT_EXTERN NSString * const UBMUpLoadPB;    //上传pb文件
UIKIT_EXTERN NSString * const UBMTheTripFileList; //请求所有本行程的pb文件名和md5，同本地做验证。
UIKIT_EXTERN NSString * const UBMTripInofr;      //详情接口
UIKIT_EXTERN NSString * const UBMTripList;       //行程列表
UIKIT_EXTERN NSString * const UBMTripStatistics; //日月周统计
UIKIT_EXTERN NSString * const UBMUpdataStartAndEndPoi; //上报逆地理编码结果
UIKIT_EXTERN NSString * const UBMTripMerge; //行程结束通知
//UIKIT_EXTERN NSString * const WeekTrip;  //周数据统计，ubm首页用
UIKIT_EXTERN NSString * const UBMGetUnFinishTripList; //获取未完结的行程列表接口
UIKIT_EXTERN NSString * const UBMTripMark;   //非驾驶行为打标
//UIKIT_EXTERN NSString * const Carbon;     //碳中和
//UIKIT_EXTERN NSString * const TripHistory;     //行程历史信息
//UIKIT_EXTERN NSString * const SafeDays;     //安全行驶天数
//
UIKIT_EXTERN NSString * const UBMOssPolicy;    //获取oss上传凭证
UIKIT_EXTERN NSString * const UBMUploadNotify; //上传完成通知接口
//UIKIT_EXTERN NSString * const WeeklyDetail; //周报详情
//UIKIT_EXTERN NSString * const WeeklyHistoryList;// 周报历史列表
//
////ubm活动接口
UIKIT_EXTERN NSString * const UBMGetEventUserInfo;  //ubm用户入口信息
UIKIT_EXTERN NSString * const UBMGetReachRules;     //获取达标规则信息接口
UIKIT_EXTERN NSString * const UBMJionPlan;         //开启计划回调接口
UIKIT_EXTERN NSString * const UBMGuideBeginner;       //新手指引完成回调接口
UIKIT_EXTERN NSString * const UBMDeclarationShowed;    //展示宣言书回调接口
UIKIT_EXTERN NSString * const UBMGetThemeCarouselText; //获取主题区轮播文案接口
UIKIT_EXTERN NSString * const UBMGetEventCoordinateConfig;  //获取坐标轴活动配置信息接口
UIKIT_EXTERN NSString * const UBMGetEventPopUpInfo;        //获取弹窗信息接口
UIKIT_EXTERN NSString * const UBMWithdrawEventReward;      //红色主题现金活动提现接口
UIKIT_EXTERN NSString * const UBMGetEventMoneyText;        //机器人轮播
UIKIT_EXTERN NSString * const UBMCloseFloatPit;  //坑位浮窗关闭接口
//
//
////双录
//UIKIT_EXTERN NSString * const ShuangluUsersig;    //获取客户端签名(TRTC)
//UIKIT_EXTERN NSString * const ShuangluCos;        //获取客户端临时密钥(cos)
//UIKIT_EXTERN NSString * const ShuangluRoomid;     //申请进入
//UIKIT_EXTERN NSString * const ShuangluLiveTesting;    //活体检测
//UIKIT_EXTERN NSString * const ShuangluUpdateStatus;   //进入房间状态上报
//
////配置接口
//UIKIT_EXTERN NSString * const AppConfigSwitch;    //获取客户端配置信息接口
//UIKIT_EXTERN NSString * const TabbarData;    //获取tabbar配置接口
//
//// 新坑位
//UIKIT_EXTERN NSString * const AllPits;      // 新坑位接口
