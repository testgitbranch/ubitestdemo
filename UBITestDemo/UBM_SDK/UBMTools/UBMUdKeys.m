//
//  UBMUdKeys.m
//  LoveCarChannel_iOS
//
//  Created by 金鑫 on 2021/6/21.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

#import "UBMUdKeys.h"

// UBM自动记录开关
NSString * const UBMAutoRecordSwitchUDKey = @"UBMAutoRecordSwitch";

// 首页TopView中自动记录权限引导设置的时间戳
NSString * const UBMHomeTopSettingTimestampUDKey = @"UBMHomeTopTimestamp";

// 判断是否打开手机权限
NSString * const UBMJugeSavePhoneUDKey = @"jugeSavePhone";

// Car ID
NSString * const UBMCarIdUDKey = @"CarID";

// 客服联系方式
NSString * const UBMContactsUDKey = @"contacts";

// 机车id
NSString * const UBMCarVidUDKey = @"carVid";

// Vin码
NSString * const UBMCarVinUDKey = @"carVin";

// 用户引导
NSString * const UBMHadGuidedActivityUDKey = @"hadGuidedActivity";

// 签到
NSString * const UBMShouldSiginUDKey = @"shouldSigin";

// 主用户信息
NSString * const UBMMainUserInforUDKey = @"mainUserInfor";

// 用户Uid
NSString * const UBMUserUidUDKey = @"userUid";

// 用户重新设置Uid时，可以结束开始的行程
NSString * const UBMUserIdForResettingUDKey = @"UBMUserIdForResettingUDKey";

// web判断是否打开手机权限
NSString * const UBMWebJugeSavePhoneUDKey = @"webJugeSavePhone";

// web客服联系方式
NSString * const UBMWebContactsUDKey = @"webContacts";

// 隐私政策弹窗
NSString * const UBMLCCPrivacyPolicyAlertViewShowFlagUDKey = @"LCCPrivacyPolicyAlertViewShowFlag-2";

// 新用户代理
NSString * const UBMNewUserAgentUDKey = @"newUserAgent";

// 用户代理
NSString * const UBMUserAgentUDKey = @"UserAgent";

// 权限显示时间戳
NSString * const UBMPermissionTimeUDKey = @"permission_time_key";
