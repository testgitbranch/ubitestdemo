//
//  UBMSLSLog.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2021/2/3.
//  Copyright © 2021 魏振伟. All rights reserved.
//

/**
 *业务性日志的封装：上传到阿里云sls
 *可使用 _tag 来区分不同业务的日志
 *目前有：normal、trip
 */

#import "UBMSLSLogManagement.h"

NS_ASSUME_NONNULL_BEGIN

@interface UBMSLSLog : UBMSLSLogManagement


//一般性日志
+ (void)addLog:(NSString*)message;

//警告性日志
+ (void)addLogOfW:(NSString*)message;

#pragma mark - UBM相关。trip

//ubm一般性日志
+ (void)addUBMLog:(NSString*)message;
+ (void)addUBMLog:(NSString*)message andTripID:(NSString*)tripId;


//ubm警告性日志
+ (void)addUBMLogOfW:(NSString*)message;
+ (void)addUBMLogOfW:(NSString*)message andTripID:(NSString*)tripId;


@end

NS_ASSUME_NONNULL_END
