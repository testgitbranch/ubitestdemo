//
//  UBMSLSLog.m
//  LoveCarChannel_iOS
//
//  Created by wzw on 2021/2/3.
//  Copyright © 2021 魏振伟. All rights reserved.
//

#import "UBMSLSLog.h"
#import "UBMMTools.h"
#import <AliyunLogProducer/AliyunLogProducer.h>
#import "UBMSLSLogManagement.h"

@implementation UBMSLSLog

+(void)addLog:(NSString *)message{
    
    if ([UBMMTools IsEmptyStr:message]) {
        return;
    }
    
    unsigned int nowTime = [[NSDate date] timeIntervalSince1970];
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        Log *log = [[Log alloc] init];
        [log SetTime:nowTime];
        [UBMSLSLog.sharedInstance setNormalParameter:log];
        [log PutContent:@"_tag"       value:@"normal"];
        [log PutContent:@"_msg"       value:message];
        [log PutContent:@"level"      value:@"v"];
        LogProducerResult res = [UBMSLSLog.sharedInstance.HKSLSClient AddLog:log];
        [UBMSLSLog.sharedInstance addLogResult:res];
        
    });
    
}

+(void)addLogOfW:(NSString *)message{
    
    if ([UBMMTools IsEmptyStr:message]) {
        return;
    }
    
    unsigned int nowTime = [[NSDate date] timeIntervalSince1970];
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        Log *log = [[Log alloc] init];
        [log SetTime:nowTime];
        [UBMSLSLog.sharedInstance setNormalParameter:log];
        [log PutContent:@"_tag"       value:@"normal"];
        [log PutContent:@"_msg"       value:message];
        [log PutContent:@"level"      value:@"w"];
        LogProducerResult res = [UBMSLSLog.sharedInstance.HKSLSClient AddLog:log];
        [UBMSLSLog.sharedInstance addLogResult:res];
        
    });
    

}

+(void)addUBMLog:(NSString *)message{
    
    if ([UBMMTools IsEmptyStr:message]) {
        return;
    }
    
    unsigned int nowTime = [[NSDate date] timeIntervalSince1970];
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        Log *log = [[Log alloc] init];
        [log SetTime:nowTime];
        [UBMSLSLog.sharedInstance setNormalParameter:log];
        [log PutContent:@"_tag"       value:@"trip"];
        [log PutContent:@"_msg"       value:message];
        [log PutContent:@"level"      value:@"v"];
        LogProducerResult res = [UBMSLSLog.sharedInstance.HKSLSClient AddLog:log];
        [UBMSLSLog.sharedInstance addLogResult:res];
        
    });
    
}

+(void)addUBMLog:(NSString *)message andTripID:(NSString *)tripId{
    
    if ([UBMMTools IsEmptyStr:message]) {
        return;
    }
    
    if ([UBMMTools IsEmptyStr:tripId]) {
        tripId = @"";
    }
    
    unsigned int nowTime = [[NSDate date] timeIntervalSince1970];
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        Log *log = [[Log alloc] init];
        [log SetTime:nowTime];
        [UBMSLSLog.sharedInstance setNormalParameter:log];
        [log PutContent:@"_tag"       value:@"trip"];
        [log PutContent:@"tripId"     value:tripId];
        [log PutContent:@"_msg"       value:message];
        [log PutContent:@"level"      value:@"v"];
        LogProducerResult res = [UBMSLSLog.sharedInstance.HKSLSClient AddLog:log];
        [UBMSLSLog.sharedInstance addLogResult:res];
        
    });
    
}

+(void)addUBMLogOfW:(NSString *)message{
    
    if ([UBMMTools IsEmptyStr:message]) {
        return;
    }
    unsigned int nowTime = [[NSDate date] timeIntervalSince1970];
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        

        Log *log = [[Log alloc] init];
        [log SetTime:nowTime];
        [UBMSLSLog.sharedInstance setNormalParameter:log];
        [log PutContent:@"_tag"       value:@"trip"];
        [log PutContent:@"_msg"       value:message];
        [log PutContent:@"level"      value:@"w"];
        LogProducerResult res = [UBMSLSLog.sharedInstance.HKSLSClient AddLog:log];
        [UBMSLSLog.sharedInstance addLogResult:res];
        
    });
    
}

+(void)addUBMLogOfW:(NSString *)message andTripID:(NSString *)tripId{
    
    if ([UBMMTools IsEmptyStr:message]) {
        return;
    }
    
    if ([UBMMTools IsEmptyStr:tripId]) {
        tripId = @"";
    }
    
    unsigned int nowTime = [[NSDate date] timeIntervalSince1970];
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        Log *log = [[Log alloc] init];
        [log SetTime:nowTime];
        [UBMSLSLog.sharedInstance setNormalParameter:log];
        [log PutContent:@"_tag"       value:@"trip"];
        [log PutContent:@"tripId"     value:tripId];
        [log PutContent:@"_msg"       value:message];
        [log PutContent:@"level"      value:@"w"];
        LogProducerResult res = [UBMSLSLog.sharedInstance.HKSLSClient AddLog:log];
        [UBMSLSLog.sharedInstance addLogResult:res];
        
    });
    
}

@end
