//
//  UBMSLSLogManagement.h
//  LoveCarChannel_iOS
//
//  Created by 马翔 on 2021/1/20.
//  Copyright © 2021 魏振伟. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AliyunLogProducer/AliyunLogProducer.h>

NS_ASSUME_NONNULL_BEGIN
typedef NSString *HKSLSLogTag NS_STRING_ENUM;

FOUNDATION_EXPORT HKSLSLogTag const _Nonnull UBMCrashLogTag;
FOUNDATION_EXPORT HKSLSLogTag const _Nonnull UBMPushLogTag;
FOUNDATION_EXPORT HKSLSLogTag const _Nonnull UBMNetworkLogTag;
FOUNDATION_EXPORT HKSLSLogTag const _Nonnull UBMTripLogTag;
FOUNDATION_EXPORT HKSLSLogTag const _Nonnull UBMCommonLogTag;
/*
  AliSLS 日志的处理类
  levle的类型: v、d、i、w、e ：w:wearing e:error。
 */
@interface UBMSLSLogManagement : NSObject

@property (nonatomic, strong) LogProducerClient *HKSLSClient;

+ (instancetype)sharedInstance;

- (void)setNormalParameter:(Log *)log;

- (void)addLogResult:(LogProducerResult)result;

//网络日志
- (void)addNetworkLogItem:(NSString *)httpReqUrl
             httpRespCode:(NSString *)httpRespCode
               httpTookMs:(NSString *)httpTookMs
            httpException:(NSString *)httpException
                    level:(NSString *)level
                  message:(NSString *)message;

//崩溃日志
- (void)addCrashLogItem:(NSString *)exceptionReason
                   name:(NSString *)exceptionName
       callStackSymbols:(NSString *)exceptionStack;
@end

NS_ASSUME_NONNULL_END
