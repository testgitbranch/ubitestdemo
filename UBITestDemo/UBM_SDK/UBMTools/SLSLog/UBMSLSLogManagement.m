//
//  UBMSLSLogManagement.m
//  LoveCarChannel_iOS
//
//  Created by 马翔 on 2021/1/20.
//  Copyright © 2021 魏振伟. All rights reserved.
//

#import "UBMSLSLogManagement.h"
#import "UBMFile.h"
#import "UBMBaseInforManagerment.h"
#import "UBMPrefixHeader.h"

NSString* UBMEndpoint = @"cn-beijing.log.aliyuncs.com";
NSString* UBMProject  = @"client-log-service";
NSString* UBMLogstore = @"client-log-service";
NSString* UBMAccesskeyid     = @"LTAI4GCVqc16gbX8sqyPBu8X";
NSString* UBMAccesskeysecret = @"8D14lb89JEFDWMoVK4n50uKcVJpJ5p";

HKSLSLogTag const _Nonnull UBMCrashLogTag   = @"crash";
HKSLSLogTag const _Nonnull UBMPushLogTag    = @"push";
HKSLSLogTag const _Nonnull UBMNetworkLogTag = @"network";
HKSLSLogTag const _Nonnull UBMTripLogTag    = @"trip";
HKSLSLogTag const _Nonnull UBMCommonLogTag  = @"common";

#define kLogDir      @"SLSLogs"
#define kLogFileName @"SLSLog.dat"
@interface  UBMSLSLogManagement()

@end

@implementation UBMSLSLogManagement

//可继承单例
+ (instancetype)sharedInstance
{
    Class selfClass = [self class];
    id instance = objc_getAssociatedObject(selfClass, @"UBMSLSLogManagement");

    @synchronized (self) {
        
        if (!instance){
            instance = [[super allocWithZone:NULL] init];
            NSLog(@"单例创建=====%@=====",instance);
            objc_setAssociatedObject(selfClass, @"UBMSLSLogManagement", instance, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        }
    }
    return instance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        [self initClient];
        
    }
    return self;
}

- (void)initClient
{
    LogProducerConfig* config = [[LogProducerConfig alloc] initWithEndpoint:UBMEndpoint project:UBMProject logstore:UBMLogstore accessKeyID:UBMAccesskeyid accessKeySecret:UBMAccesskeysecret];
    [config SetTopic:@"haochezhu_topic"];
    [config AddTag:@"haochezhu" value:@"haochezhu_tag"];
    [config SetPacketLogBytes:1024*1024];
    [config SetPacketLogCount:1024];
    [config SetPacketTimeout:3000];
    [config SetMaxBufferLimit:64*1024*1024];
    [config SetSendThreadCount:1];
    [config SetPersistent:1];
    [config SetPersistentFilePath:[[self checkOrCreateFilePath] stringByAppendingString:kLogFileName]];
    [config SetPersistentForceFlush:1];
    [config SetPersistentMaxFileCount:10];
    [config SetPersistentMaxFileSize:1024*1024];
    [config SetPersistentMaxLogCount:65536];
    
    //[config SetConnectTimeoutSec:10];
    //[config SetSendTimeoutSec:10];
    //[config SetDestroyFlusherWaitSec:1];
    //[config SetDestroySenderWaitSec:1];
    //[config SetCompressType:1];
    //[config SetNtpTimeOffset:1];
    //[config SetMaxLogDelayTime:7*24*3600];
    //[config SetDropDelayLog:1];
    //[config SetDropUnauthorizedLog:0];
    _HKSLSClient = [[LogProducerClient alloc] initWithLogProducerConfig:config callback:UBMOnLogSendDone];
}

void UBMOnLogSendDone(const char * config_name, log_producer_result result, size_t log_bytes, size_t compressed_bytes, const char * req_id, const char * message, const unsigned char * raw_buffer, void * userparams)
{
    if (result == LOG_PRODUCER_OK) {
        printf("send success, config : %s, result : %d, log bytes : %d, compressed bytes : %d, request id : %s \n", config_name, (result), (int)log_bytes, (int)compressed_bytes, req_id);
    } else {
        printf("send fail   , config : %s, result : %d, log bytes : %d, compressed bytes : %d, request id : %s \n, error message : %s\n", config_name, (result), (int)log_bytes, (int)compressed_bytes, req_id, message);
    }
}

- (NSString *)checkOrCreateFilePath
{
    NSString *userUid = [UBMBaseUserModel shared].userId;
    NSString *LogDict = kLogDir;
    NSString *md5Str  = [UBMMTools MD5HexDigest:userUid];
    NSString *LogPath = [UBMFile ensureDirectoryInCache:[NSString stringWithFormat:@"%@/%@", md5Str, LogDict]];

    return LogPath;
}

- (void)setNormalParameter:(Log *)log
{
    [log PutContent:@"_plt"     value:@"2"];
    [log PutContent:@"_uid"     value:[UBMBaseUserModel shared].userId];
    [log PutContent:@"_vid"     value:[[NSUserDefaults standardUserDefaults] objectForKey:UBMCarIdUDKey]];
    [log PutContent:@"_verName" value:[UBMBaseInforManagerment sharedInstance].appVersionCode];
    [log PutContent:@"_buildCode" value:[UBMBaseInforManagerment sharedInstance].appBuildVersion];
    [log PutContent:@"_verCode" value:@""];
    [log PutContent:@"_sysVer"  value:[UBMBaseInforManagerment sharedInstance].osVersion];
    [log PutContent:@"_sysSdk"  value:[UBMBaseInforManagerment sharedInstance].sdkVersion];
    [log PutContent:@"_manu"    value:@"Apple"];
    [log PutContent:@"_model"   value:[UBMBaseInforManagerment sharedInstance].model];
    [log PutContent:@"_process" value:[UBMBaseInforManagerment sharedInstance].appProcessName];
    //[log PutContent:@"_thread"  value:[NSString stringWithFormat:@"%@", [NSThread currentThread]]];
    [log PutContent:@"_proj"    value:@"huzhu"];
    
    NSString* timei = [UBMMTools timeWithTimeIntervalString:[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]]];
    [log PutContent:@"_time"    value:timei];
}

- (void) addNetworkLogItem:(NSString *)httpReqUrl
             httpRespCode:(NSString *)httpRespCode
               httpTookMs:(NSString *)httpTookMs
            httpException:(NSString *)httpException
                    level:(NSString *)level
                  message:(NSString *)message
{
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        Log *log = [[Log alloc] init];
        [log SetTime:[[NSDate date] timeIntervalSince1970]];
        [self setNormalParameter:log];
        [log PutContent:@"httpReqUrl"    value:httpReqUrl];
        [log PutContent:@"httpRespCode"  value:httpRespCode];
        [log PutContent:@"httpTookMs"    value:httpTookMs];
        [log PutContent:@"httpException" value:httpException];
        [log PutContent:@"_tag"          value:@"network"];
        [log PutContent:@"_level"        value:level];
        [log PutContent:@"_msg"          value:message];
        LogProducerResult res = [self.HKSLSClient AddLog:log];
        [self addLogResult:res];
        
    });
    
}

- (void)addTripLogItem:(NSString *)tripId tripStatus:(NSString *)tripStatus level:(NSString *)level message:(NSString *)message;
{
    Log *log = [[Log alloc] init];
    [log SetTime:[[NSDate date] timeIntervalSince1970]];
    [self setNormalParameter:log];
    [log PutContent:@"tripId"     value:tripId];
    [log PutContent:@"tripStatus" value:tripStatus];
    [log PutContent:@"_tag"       value:@"trip"];
    [log PutContent:@"_msg"       value:message];
    [log PutContent:@"level"      value:level];
    LogProducerResult res = [self.HKSLSClient AddLog:log];
    [self addLogResult:res];
}

- (void)addCrashLogItem:(NSString *)exceptionReason name:(NSString *)exceptionName callStackSymbols:(NSString *)exceptionStack
{
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        Log *log = [[Log alloc] init];
        [log SetTime:[[NSDate date] timeIntervalSince1970]];
        [self setNormalParameter:log];
        [log PutContent:@"_tag"   value:@"crash"];
        [log PutContent:@"_msg"   value:@""];
        [log PutContent:@"exceptionReasonf" value:exceptionReason];
        [log PutContent:@"exceptionName"    value:exceptionName];
        [log PutContent:@"exceptionStack"   value:exceptionStack];
        LogProducerResult res = [self.HKSLSClient AddLog:log];
        [self addLogResult:res];
        
    });
    
}

- (void)addLogResult:(LogProducerResult)result
{
    switch (result) {
        case LogProducerOK:
        {
            NSLog(@"LogProducerOK");
        }
            break;
        case LogProducerInvalid:
        {
            NSLog(@"LogProducerInvalid");
        }
            break;
        case LogProducerWriteError:
        {
            NSLog(@"LogProducerWriteError");
        }
            break;
        case LogProducerDropError:
        {
            NSLog(@"LogProducerDropError");
        }
            break;
        case LogProducerSendNetworkError:
        {
            NSLog(@"LogProducerSendNetworkError");
        }
            break;
        case LogProducerSendQuotaError:
        {
            NSLog(@"LogProducerSendQuotaError");
        }
            break;
        case LogProducerSendUnauthorized:
        {
            NSLog(@"LogProducerSendUnauthorized");
        }
            break;
        case LogProducerSendServerError:
        {
            NSLog(@"LogProducerSendServerError");
        }
            break;
        case LogProducerSendDiscardError:
        {
            NSLog(@"LogProducerSendDiscardError");
        }
            break;
        case LogProducerSendTimeError:
        {
            NSLog(@"LogProducerSendTimeError");
        }
            break;
        case LogProducerSendExitBufferdF:
        {
            NSLog(@"LogProducerSendExitBufferdF");
        }
            break;
        case LogProducerPERSISTENT_Error:
        {
            NSLog(@"LogProducerPERSISTENT_Error");
        }
            break;
        default:
            break;
    }
}
@end
