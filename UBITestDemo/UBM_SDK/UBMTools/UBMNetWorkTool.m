//
//  UBMNetWorkTool.m
//  EngineeProject
//
//  Created by Edog on 2017/6/5.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "UBMNetWorkTool.h"
#import "UBMNetEncyptOfOC.h"
#import "UBMWJProgressHUD.h"
#import "UBMPrefixHeader.h"

static NSString * const AFHTTPClientBaseURLString = @"https://www.baidu.com/";
static NSInteger const kPassportTokenRefreshCount = 1;

@interface UBMNetWorkTool ()
//@property (nonatomic, strong) dispatch_semaphore_t refreshPassportTokenSignal;
@end

@implementation UBMNetWorkTool

+(instancetype)shareNetWorkTool
{
    /*
    static id instance;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    
    return instance;
    */
    
    //可继承单例
    Class selfClass = [self class];
    id instance = objc_getAssociatedObject(selfClass, @"UBMNetWorkTool");
    @synchronized (self) {
        if (!instance) {
            instance = [[super allocWithZone:NULL] init];
            NSLog(@"单例创建=====%@=====",instance);
            objc_setAssociatedObject(selfClass, @"UBMNetWorkTool", instance, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        }
    }
    return instance;
     
}

- (id)init{
    self = [super init];
    if (self) {
        
        self = [self initWithBaseURL:[NSURL URLWithString:AFHTTPClientBaseURLString]];
        self.requestSerializer.timeoutInterval = 90;
        self.requestSerializer = [AFHTTPRequestSerializer serializer];
        self.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalCacheData;//忽略本地缓存
        
        self.responseSerializer = [AFHTTPResponseSerializer serializer];
        self.responseSerializer.acceptableContentTypes=[NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", @"text/plain",@"application/octet-stream", @"application/xml", nil];
        //[self.requestSerializer setValue:@"application/x-www-form-urlencoded; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
        [self.requestSerializer setValue:@"hcz" forHTTPHeaderField:@"appfrom"];
        [self.reachabilityManager startMonitoring];
        
//        self.refreshPassportTokenSignal = dispatch_semaphore_create(1);
    }
    return self;
}

//取消网络请求
- (void) cancelRequest
{
    [self.operationQueue cancelAllOperations];
}




#pragma mark - 网络请求1
-(void)postRequestWithBaseUrl:(NSString*)baseUrl lastUrl:(NSString*)lastUrl parameters:(id)parameters needShowHUD:(BOOL)showHUD needShowError:(BOOL)showError needBackInMainQueue:(BOOL)shouldBackInMainQueue success:(sucessBlock)success error:(errorBlock)errorx{
    
    [self postRequestBaseUrl:baseUrl lastUrl:lastUrl par:parameters showHUD:showHUD showError:showError backInMainQ:shouldBackInMainQueue success:^(id response) {
        if (success)
            success(response);
    } error:^(id response) {
        if (errorx) {
            errorx(response);
        }
    }];
}

#pragma mark - 网络请求2
-(void)requestWithBaseUrl:(NSString *)baseUrl UrlString:(NSString *)URLString parameters:(id)parameters method:(NSString *)method success:(sucessBlock)success error:(errorBlock)errorx isShowHUD:(BOOL)showHUD
{
    //get 请求方法
    if ([method isEqualToString:@"GET"])
    {
        [self getRequestBaseurl:baseUrl lastUrl:URLString par:parameters showHUD:showHUD showError:YES backInMainQ:YES success:^(id response) {
            if (success) {
                success(response);
            }
        } error:^(id response) {
            if (errorx) {
                errorx(response);
            }
        }];
    }
    else if ([method isEqualToString:@"POST"])//POST 请求方法
    {
        [self postRequestBaseUrl:baseUrl lastUrl:URLString par:parameters showHUD:showHUD showError:YES backInMainQ:YES success:^(id response) {
            if (success) {
                success(response);
            }
        } error:^(id response) {
            if (errorx) {
                errorx(response);
            }
        }];
    }
}

- (void)refreshPassportTokenWith:(NSString *)baseUrl lastUrl:(NSString *)lastUrl par:(id)parameters showHUD:(BOOL)show showError:(BOOL)showError backInMainQ:(BOOL)inMainQ success:(sucessBlock)success error:(errorBlock)errorx {
    
    __block dispatch_semaphore_t refreshPassportTokenSignal = dispatch_semaphore_create(1);
    __block BOOL refreshFlag = YES;
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        for (NSInteger refreshCount = kPassportTokenRefreshCount; refreshCount>0; refreshCount--) {
            NSLog(@"refreshTokenCount:%ld", (long)refreshCount);
            // 同一时刻只有一个刷新PassportToken的操作
            intptr_t ret = dispatch_semaphore_wait(refreshPassportTokenSignal, DISPATCH_TIME_FOREVER);
            if (!ret) { // 没有超时
                NSLog(@"refreshToken 没有超时");
                
            } else { // 超时
                NSLog(@"refreshToken 超时");
            }
            if (refreshFlag == NO) {
                dispatch_semaphore_signal(refreshPassportTokenSignal);
                NSLog(@"refreshToken break signal");
                break;
            }
            if ([UBITracking shared].provider.delegate && [[UBITracking shared].provider.delegate respondsToSelector:@selector(UBICredentialRefreshPassportToken)]) {
                // 刷新 PassprotToken
                NSString *token = [[UBITracking shared].provider.delegate UBICredentialRefreshPassportToken];
                if (token) {
                    [UBITracking shared].provider.passportToken = token;
                    // 刷新PassprotToken后，继续网络请求
                    [self postRequestBaseUrl:baseUrl lastUrl:lastUrl par:parameters showHUD:show showError:showError backInMainQ:inMainQ isRefreshToken:YES originalData:^(NSData *response) {
                        
                        if(!response){
                            if (errorx) {
                                errorx(nil);
                            }
                            refreshFlag = NO;
                            dispatch_semaphore_signal(refreshPassportTokenSignal);
                            NSLog(@"refreshToken signal errorx");
                        }
                        else{
                            if (success) {
                                success(response);
                            }
                            refreshFlag = NO;
                            dispatch_semaphore_signal(refreshPassportTokenSignal);
                            NSLog(@"refreshToken signal success");
                        }
                        
                    } error:^(id response) {
                        if (errorx) {
                            errorx(response);
                        }
                        refreshFlag = NO;
                        dispatch_semaphore_signal(refreshPassportTokenSignal);
                        NSLog(@"refreshToken signal error");
                    }];
                    
                } else {
                    dispatch_semaphore_signal(refreshPassportTokenSignal);
                    NSLog(@"refreshToken signal token is NULL");
                }
                
            } else {
                @throw
                [NSException exceptionWithName:NSInternalInconsistencyException
                                        reason:@"You must implement delegate method UBICredentialRefreshPassportToken."
                                      userInfo:nil];
            }
        }
    });
}

#pragma mark - post网络封装请求
#pragma mark -返回解析data后的字典数据
-(void)postRequestBaseUrl:(NSString *)baseUrl lastUrl:(NSString *)lastUrl par:(id)parameters showHUD:(BOOL)show showError:(BOOL)showError backInMainQ:(BOOL)inMainQ success:(sucessBlock)success error:(errorBlock)errorx{
    
    [self postRequestBaseUrl:baseUrl lastUrl:lastUrl par:parameters showHUD:show showError:showError backInMainQ:inMainQ isRefreshToken:NO originalData:^(id response) {
        
        NSDictionary * resultDic = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:nil];
        
        if(!resultDic){
            if (errorx)
                errorx(nil);
        }
        else{
            if (success) {
                success(resultDic);
            }
        }
        
    } error:^(id response) {
        if (errorx) {
            errorx(response);
        }
    }];
    
}

#pragma mark -返回未解析的原data数据
-(void)postRequestBaseUrl:(NSString *)baseUrl lastUrl:(NSString *)lastUrl par:(id)parameters showHUD:(BOOL)show showError:(BOOL)showError backInMainQ:(BOOL)inMainQ isRefreshToken:(BOOL)isRefreshToken originalData:(sucessBlockOfData)success error:(errorBlock)errorx{
    
    if(show == YES){
       [UBMWJProgressHUD show];
    }
    
    // UBI PassportToken for header
    NSString *token = [UBITracking shared].provider.passportToken;
    [self.requestSerializer setValue:token forHTTPHeaderField:@"X-Token"];
    
    NSString* urlstr = [NSString stringWithFormat:@"%@%@",baseUrl,lastUrl];
    NSLog(@"URL: %@\n parameters = %@",urlstr,parameters);
    //计算并追加公共参数
    NSDictionary* paDicx = [self encryptParameters:parameters url:baseUrl];
    NSTimeInterval reqBeginTime = [[[NSDate alloc] init] timeIntervalSince1970];
    
    @bx_weakify(self);
    [self POST:urlstr parameters:paDicx progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {

        if (show == YES) {
            [UBMWJProgressHUD dismiss];
        }
        
        if (inMainQ == YES) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if(success){
                   success(responseObject);
                }
            });
        }else{
            if(success){
               success(responseObject);
            }
        }
        
        //网络日志
        [weak_self jugeLogOfResponse:responseObject anbeginTime:reqBeginTime and:urlstr];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (show == YES) {
            [UBMWJProgressHUD dismiss];
        }
        
        NSData * data = error.userInfo[@"com.alamofire.serialization.response.error.data"];
        NSString * str = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        NSLog(@"%@ 服务器的错误原因:%@", urlstr, str);
        
        // for PassportToken，不是RefreshToken后再继续的请求，才执行刷新
        if (isRefreshToken == NO) {
            NSHTTPURLResponse *response = error.userInfo[@"com.alamofire.serialization.response.error.response"];
            if (response && response.statusCode == 403) {
                NSLog(@"PassportToken过期");
                [self refreshPassportTokenWith:baseUrl lastUrl:lastUrl par:parameters showHUD:show showError:showError backInMainQ:inMainQ success:success error:errorx];
            }
        }
            
        if (showError == YES) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if([AFNetworkReachabilityManager sharedManager].networkReachabilityStatus == AFNetworkReachabilityStatusNotReachable){
//                   Toast(NoNetError);
                }else{
                   //Toast(ErrorHint);
                }
            });
        }
        
        if (inMainQ == YES) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if([AFNetworkReachabilityManager sharedManager].networkReachabilityStatus == AFNetworkReachabilityStatusNotReachable){
                    if (errorx) {
//                        errorx(NoNetError);
                    }
                }else{
                    if(errorx){
                        errorx(error);
                    }
                }
            });
            
        }else{
            
            if([AFNetworkReachabilityManager sharedManager].networkReachabilityStatus == AFNetworkReachabilityStatusNotReachable){
                if (errorx) {
                    errorx(NoNetError);
                }
            }else{
                if(errorx){
                    errorx(error);
                }
            }
        }
    
        //网络日志
        NSTimeInterval reqEndTime = [[[NSDate alloc] init] timeIntervalSince1970];
        NSInteger      timeSpace  = (reqEndTime - reqBeginTime) * 1000;
        [weak_self addErrorSLSLogOfPath:urlstr timeInterval:[NSString stringWithFormat:@"%ld", (long)timeSpace] resCode:@"null" resMsg:ChangeToString(error)];
        
    }];
    
}

#pragma mark - get网络请求封装
#pragma mark -返回解析data后的字典数据
-(void)getRequestBaseurl:(NSString *)baseUrl lastUrl:(NSString *)URLString par:(id)parameters showHUD:(BOOL)show showError:(BOOL)showError backInMainQ:(BOOL)inMainQ success:(sucessBlock)success error:(errorBlock)errorx{
    
    [self getRequestBaseurl:baseUrl lastUrl:URLString par:parameters showHUD:show showError:showError backInMainQ:inMainQ originalData:^(id response) {
        
        NSDictionary * resultDic = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:nil];
        if(!resultDic){
            if (errorx)
                errorx(nil);
        }
        else{
            if (success)
                success(resultDic);
        }
        
    } error:^(id response) {
        if (errorx)
            errorx(response);
    }];
}

#pragma mark -返回未解析的原data数据
-(void)getRequestBaseurl:(NSString *)baseUrl lastUrl:(NSString *)URLString par:(id)parameters showHUD:(BOOL)show showError:(BOOL)showError backInMainQ:(BOOL)inMainQ originalData:(sucessBlock)success error:(errorBlock)errorx{
    
    if(show == YES){
       [UBMWJProgressHUD show];
    }
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:UBMNewUserAgentUDKey] != nil){
        NSString* newUserAgent = [[NSUserDefaults standardUserDefaults] objectForKey:UBMNewUserAgentUDKey];
        [self.requestSerializer setValue:newUserAgent forHTTPHeaderField:@"User-Agent"];
    }else{
        NSString* useragent = [UBMUaGet getUAContent];
        [self.requestSerializer setValue:useragent forHTTPHeaderField:@"User-Agent"];
    }
        
    NSString* urlstr = [NSString stringWithFormat:@"%@%@",baseUrl,URLString];
    NSDictionary* paDicx = [self encryptParameters:parameters url:baseUrl];
    NSLog(@"URL: %@\n parameters = %@",urlstr,paDicx);
    NSTimeInterval reqBeginTime = [[[NSDate alloc] init] timeIntervalSince1970];
    
    @bx_weakify(self);
    [self GET:urlstr parameters:paDicx progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (show == YES) {
            [UBMWJProgressHUD dismiss];
        }
        
        if (success) {
            success(responseObject);
        }
        
        //网络日志
        [weak_self jugeLogOfResponse:responseObject anbeginTime:reqBeginTime and:urlstr];
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (show == YES) {
            [UBMWJProgressHUD dismiss];
        }
        
        if([AFNetworkReachabilityManager sharedManager].networkReachabilityStatus == AFNetworkReachabilityStatusNotReachable){
           Toast(NoNetError);
           errorx(NoNetError);
        }else{
            if(errorx)
                errorx(error);
            
            NSLog(@"%@报错, %@", urlstr ,error);
        }
        
        //网络日志
        NSTimeInterval reqEndTime = [[[NSDate alloc] init] timeIntervalSince1970];
        NSInteger      timeSpace  = (reqEndTime - reqBeginTime) * 1000;
        [weak_self addErrorSLSLogOfPath:urlstr timeInterval:[NSString stringWithFormat:@"%ld", (long)timeSpace] resCode:@"" resMsg:ChangeToString(error)];
        
    }];
}


#pragma mark - file格式上传文件
-(void)uplodImgWithBaseUrl:(NSString *)baseUrl UrlString:(NSString *)URLString andParameters:(id)parameters andFileData:(NSData *)fileData FileName:(NSString*)fileName method:(NSString *)method success:(sucessBlock)success error:(errorBlock)errorx isSync:(BOOL)showHUD
{
    if(showHUD == YES){
        [UBMWJProgressHUD show];
    }
    
    NSString* urlstr = [NSString stringWithFormat:@"%@%@",baseUrl,URLString];
    NSLog(@"%@",urlstr);
    NSDictionary* paDicx = [self encryptParameters:parameters url:baseUrl];
    NSTimeInterval reqBeginTime = [[[NSDate alloc] init] timeIntervalSince1970];
    
    @bx_weakify(self);
    if([method isEqualToString:@"POST"])
    {
        [self POST:urlstr parameters:paDicx constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
            
            NSString* tpex = @"application/octet-stream";
            [formData appendPartWithFileData:fileData name:@"file" fileName:fileName mimeType:tpex];
            
        } progress:^(NSProgress * _Nonnull uploadProgress) {
            ;
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            if (showHUD == YES) {
                [UBMWJProgressHUD dismiss];
            }
            
            NSTimeInterval reqEndTime = [[[NSDate alloc] init] timeIntervalSince1970];
            NSInteger      timeSpace  = (reqEndTime - reqBeginTime) * 1000;
            
            NSDictionary * resultDic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
            if(!resultDic)
            {
                NSString * str = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
                if(str.length>0)
                {NSLog(@"json解析失败:%@",str);}
                else
                {NSLog(@"%@",resultDic);}
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    if(errorx)
                        errorx(nil);
                });
                
                [weak_self addErrorSLSLogOfPath:urlstr timeInterval:[NSString stringWithFormat:@"%ld", (long)timeSpace] resCode:@"null" resMsg:ChangeToString(str)];
            }
            else
            {
                NSLog(@"文件上传成功： %@",resultDic);
                dispatch_async(dispatch_get_main_queue(), ^{
                    if(success)
                        success(resultDic);
                });
                
                [weak_self addNormalSLSLogOfPath:urlstr timeInterval:[NSString stringWithFormat:@"%ld", (long)timeSpace] resCode:ChangeToString(resultDic[@"code"]) resMsg:ChangeToString(resultDic[@"msg"])];
            }

    
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            if (showHUD == YES) {
                [UBMWJProgressHUD dismiss];
            }
            
            NSData * data = error.userInfo[@"com.alamofire.serialization.response.error.data"];
            NSString * str = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
            NSLog(@"服务器的错误原因:%@",str);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if(errorx)
                    errorx(error);
            });
            
            //网络日志
            NSTimeInterval reqEndTime = [[[NSDate alloc] init] timeIntervalSince1970];
            NSInteger      timeSpace  = (reqEndTime - reqBeginTime) * 1000;
            [weak_self addErrorSLSLogOfPath:urlstr timeInterval:[NSString stringWithFormat:@"%ld", (long)timeSpace] resCode:@"" resMsg:ChangeToString(error)];
            
        }];
    }
}

#pragma mark - 下载一个文件
-(void)downLoadFileOfPath:(NSString *)filePath success:(sucessBlock)success error:(errorBlock)errorx{
    
    NSTimeInterval reqBeginTime = [[[NSDate alloc] init] timeIntervalSince1970];
    
    @bx_weakify(self);
    [self GET:filePath parameters:@{} progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
       if(success)
            success(responseObject);
        
        //网络日志
        NSTimeInterval reqEndTime = [[[NSDate alloc] init] timeIntervalSince1970];
        NSInteger      timeSpace  = (reqEndTime - reqBeginTime) * 1000;
        [weak_self addNormalSLSLogOfPath:filePath timeInterval:[NSString stringWithFormat:@"%ld", (long)timeSpace] resCode:@"0" resMsg:@"下载成功"];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"%@", error);
        if(errorx){
            errorx(error);
        }
        
        //网络日志
        NSTimeInterval reqEndTime = [[[NSDate alloc] init] timeIntervalSince1970];
        NSInteger      timeSpace  = (reqEndTime - reqBeginTime) * 1000;
        [weak_self addErrorSLSLogOfPath:filePath timeInterval:[NSString stringWithFormat:@"%ld", (long)timeSpace] resCode:@"" resMsg:ChangeToString(error)];
        
    }];
    
}

-(NSDictionary*)encryptParameters:parameters url:baseUrl {
    return [UBMNetEncyptOfOC encryptParameters:parameters url:baseUrl];
}

#pragma mark - 添加网络日志
//判断返回结果并添加网络日志
-(void)jugeLogOfResponse:(id)responseObject anbeginTime:(NSInteger)reqBeginTime and:(NSString*)urlstr{
    
    NSTimeInterval reqEndTime = [[[NSDate alloc] init] timeIntervalSince1970];
    NSInteger      timeSpace  = (reqEndTime - reqBeginTime) * 1000;
    NSDictionary * resultDic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
    if(!resultDic){
        [self addErrorSLSLogOfPath:urlstr timeInterval:[NSString stringWithFormat:@"%ld", (long)timeSpace] resCode:@"null" resMsg:ChangeToString(responseObject)];
    }
    else{
        
        //打印结果
        NSLog(@"%@ 结果： %@", urlstr, resultDic);
        if ([resultDic isKindOfClass:[NSDictionary class]] && resultDic[@"msg"]) {
            NSLog(@"%@",[UBMMTools replaceUnicode:resultDic[@"msg"]]);
        }
        
        //网络日志
        if ([resultDic isKindOfClass:[NSDictionary class]]) {
            [self addNormalSLSLogOfPath:urlstr timeInterval:[NSString stringWithFormat:@"%ld", (long)timeSpace] resCode:ChangeToString(resultDic[@"code"]) resMsg:ChangeToString(resultDic[@"msg"])];
        }else{
            [self addNormalSLSLogOfPath:urlstr timeInterval:[NSString stringWithFormat:@"%ld", (long)timeSpace] resCode:@"" resMsg:@""];
        }
    }
}

-(void)addNormalSLSLogOfPath:(NSString*)path timeInterval:(NSString*)time resCode:(NSString*)code resMsg:(NSString*)msg{
    //一般性日志
    [[UBMSLSLogManagement sharedInstance] addNetworkLogItem:path httpRespCode:code httpTookMs:time httpException:msg level:@"v" message:@"Succeed"];
}
     
-(void)addErrorSLSLogOfPath:(NSString*)path timeInterval:(NSString*)time resCode:(NSString*)code resMsg:(NSString*)msg{
    //错误性日志
    [[UBMSLSLogManagement sharedInstance] addNetworkLogItem:path httpRespCode:code httpTookMs:time httpException:msg level:@"e" message:@"Error"];
    
}

@end
