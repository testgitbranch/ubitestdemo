//
//  UBMBaseInforManagerment.m
//  LoveCarChannel_iOS
//
//  Created by 马翔 on 2020/12/12.
//  Copyright © 2020 魏振伟. All rights reserved.
//

#import "UBMBaseInforManagerment.h"
#include <sys/param.h>
#include <sys/mount.h>
#include <mach/mach.h>

#import <sys/types.h>
#import <sys/sysctl.h>
#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import "NSString+UBMCatepory.h"
#import <CoreTelephony/CTCallCenter.h>
#import <CoreTelephony/CTCall.h>
#import "UBMPrefixHeader.h"

typedef enum : NSUInteger
{
    UBM_HK_MEMEMORY_TYPE_TOTAL   ,
    UBM_HK_MEMEMORY_TYPE_FREE    ,
    UBM_HK_MEMEMORY_TYPE_WIRED   ,
    UBM_HK_MEMEMORY_TYPE_ACTIVE  ,
    UBM_HK_MEMEMORY_TYPE_INACTIVE,
    UBM_HK_MEMEMORY_TYPE_RESIDENT
} UBMHKMemorySizeType;

@interface UBMBaseInforManagerment ()
@property(nonatomic, strong)CTCallCenter *callCenter;
@end

@implementation UBMBaseInforManagerment

+ (instancetype)sharedInstance
{
    static id _instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[self alloc] init];
    });
    return _instance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.callCenter = [[CTCallCenter alloc] init];
        [self getStaticInfor];
    }
    return self;
}

- (void)getStaticInfor
{
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    
    self.model          = [UBMMTools getDeviceName];
    self.osName         = [UIDevice currentDevice].systemName;
    self.osVersion      = [UIDevice currentDevice].systemVersion;
    self.sdkVersion     = [infoDictionary objectForKey:@"DTSDKBuild"];;
    self.manufacturer   = @"Apple";
    self.appVersionCode = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    
    self.appVersionName = [infoDictionary objectForKey:@"CFBundleDisplayName"];
    self.appBuildVersion= [infoDictionary objectForKey:@"CFBundleVersion"];
    self.appProcessName = [infoDictionary objectForKey:@"CFBundleExecutable"];
    //self.deviceMemory   = [self MemoryInfo:UBM_HK_MEMEMORY_TYPE_TOTAL];
    //self.deviceDisk     = [self getTotalDiskSpace];
    @bx_weakify(self);
    self.callCenter.callEventHandler=^(CTCall* call)
    {
        @bx_strongify(self);
        if (call.callState == CTCallStateDisconnected)
        {
            NSLog(@"Call has been disconnected");
            self.callCenterStatus = ubmCallStateDisconnected;
        }
        else if (call.callState == CTCallStateConnected)
        {
            NSLog(@"Call has just been connected");
            self.callCenterStatus = ubmCallStateConnected;
        }
        else if(call.callState == CTCallStateIncoming)
        {
            NSLog(@"Call is incoming");
            self.callCenterStatus = ubmCallStateIncoming;
        }
        else if (call.callState == CTCallStateDialing)
        {
            NSLog(@"call is dialing");
            self.callCenterStatus = ubmCallStateDialing;
        }
        else
        {
            NSLog(@"Nothing is done");
            self.callCenterStatus = ubmCallNotingIsDone;
        }
    };
}

- (NSString *)getAppContext
{
    self.networkType      = [self getNetConnectType];
    self.batteryAvailable = [NSString stringWithFormat:@"%.2f", [UIDevice currentDevice].batteryLevel];
    NSMutableString *paramString = [NSMutableString stringWithCapacity:1];
    
    [paramString appendFormat:@"%@ %@\n", @"osName",          self.osName];
    [paramString appendFormat:@"%@ %@\n", @"osVersion",       self.osVersion];
    [paramString appendFormat:@"%@ %@\n", @"sdkVersion",      self.sdkVersion];
    [paramString appendFormat:@"%@ %@\n", @"model",           self.model];
    [paramString appendFormat:@"%@ %@\n", @"manufacturer",    self.manufacturer];
    [paramString appendFormat:@"%@ %@\n", @"appVersionCode",  self.appVersionCode];
    [paramString appendFormat:@"%@ %@\n", @"appVersionName",  self.appVersionName];
    [paramString appendFormat:@"%@ %@\n", @"appBuildVersion", self.appBuildVersion];
    //[paramString appendFormat:@"%@ %@\n", @"deviceMemory",    self.deviceMemory];
    //[paramString appendFormat:@"%@ %@\n", @"deviceDisk",      self.deviceDisk];
    
    [paramString appendFormat:@"%@ %@\n", @"networkType",     self.networkType];
    //[paramString appendFormat:@"%@ %@\n", @"availableDisk",   self.availableDisk];
    //[paramString appendFormat:@"%@ %@\n", @"availableMemory", self.availableMemory];
    [paramString appendFormat:@"%@ %@\n", @"batteryAvailable",self.batteryAvailable];
    return paramString;
}

- (NSString *)getTotalDiskSpace
{
    float        totalSpace = 0.0;
    NSError      * error;
    NSDictionary * infoDic = [[NSFileManager defaultManager] attributesOfFileSystemForPath:NSHomeDirectory() error: &error];
    if (infoDic)
    {
        NSNumber * fileSystemSizeInBytes = [infoDic objectForKey: NSFileSystemSize];
        totalSpace = [fileSystemSizeInBytes floatValue];
        return [NSString getFileSizeString:totalSpace];
    }
    else {
        NSLog(@"Error Obtaining System Memory Info: Domain = %@, Code = %ld", [error domain], (long)[error code]);
        return nil;
    }
}

- (NSString *)freeDiskSpaceInBytes
{
    struct statfs buf;
    long long freespace = -1;
    if(statfs("/var", &buf) >= 0){
        freespace = (long long)(buf.f_bsize * buf.f_bfree);
    }
    return [NSString getFileSizeString:freespace];
}

- (NSString *)usedMemory
{
    task_basic_info_data_t taskInfo;

    mach_msg_type_number_t infoCount;

    kern_return_t kernReturn = task_info(mach_task_self(), TASK_BASIC_INFO, (task_info_t)&taskInfo, &infoCount);

    if (kernReturn != KERN_SUCCESS)
    {
        return nil;
    }
    double availableMB = taskInfo.resident_size;
    return [NSString getFileSizeString:availableMB];
}

- (NSString *)MemoryInfo:(UBMHKMemorySizeType)memoryType
{
    int mib[6];
    mib[0] = CTL_HW;
    mib[1] = HW_PAGESIZE;
    
    int pagesize;
    size_t length;
    length = sizeof (pagesize);
    if (sysctl (mib, 2, &pagesize, &length, NULL, 0) < 0)
    {
        fprintf (stderr, "getting page size");
    }
    
    mach_msg_type_number_t count = HOST_VM_INFO_COUNT;
    
    vm_statistics_data_t vmstat;
    if (host_statistics (mach_host_self (), HOST_VM_INFO, (host_info_t) &vmstat, &count) != KERN_SUCCESS)
    {
        fprintf (stderr, "Failed to get VM statistics.");
    }
    task_basic_info_64_data_t info;
    unsigned size = sizeof (info);
    task_info (mach_task_self (), TASK_BASIC_INFO_64, (task_info_t) &info, &size);
    
    double unit     = 1;
    double total    = (vmstat.wire_count + vmstat.active_count + vmstat.inactive_count + vmstat.free_count) * pagesize / unit;
    double wired    = vmstat.wire_count * pagesize / unit;
    double active   = vmstat.active_count * pagesize / unit;
    double inactive = vmstat.inactive_count * pagesize / unit;
    double free     = vmstat.free_count * pagesize / unit;
    double resident = info.resident_size / unit;
    
    switch(memoryType)
    {
        case UBM_HK_MEMEMORY_TYPE_TOTAL:
        {
            return [NSString getFileSizeString:total];
        }
        break;
        case UBM_HK_MEMEMORY_TYPE_FREE:
        {
            return [NSString getFileSizeString:free];
        }
            break;
        case UBM_HK_MEMEMORY_TYPE_WIRED:
        {
            return [NSString getFileSizeString:wired];
        }
            break;
        case UBM_HK_MEMEMORY_TYPE_ACTIVE:
        {
            return [NSString getFileSizeString:active];
        }
            break;
        case UBM_HK_MEMEMORY_TYPE_INACTIVE:
        {
            return [NSString getFileSizeString:inactive];
        }
            break;
        case UBM_HK_MEMEMORY_TYPE_RESIDENT:
        {
            return [NSString getFileSizeString:resident];
        }
            break;
        default:
            return @"0";
    }
}

- (NSString *)getNetConnectType
{
    NSString *netconnType = @"";
    
    AFNetworkReachabilityStatus netStatus = [AFNetworkReachabilityManager sharedManager].networkReachabilityStatus;
    if (netStatus == AFNetworkReachabilityStatusUnknown) {
        netconnType = @"Unknown";
    }else if (netStatus == AFNetworkReachabilityStatusNotReachable) {
        netconnType = @"NotReachable";
    }else if (netStatus == AFNetworkReachabilityStatusReachableViaWWAN) {
        netconnType = @"WWAN";
    }else if (netStatus == AFNetworkReachabilityStatusReachableViaWiFi) {
        netconnType = @"WiFi";
    }
    
    return netconnType;
}

@end
