//
//  UBMMathTool.m
//  LoveCarChannel_iOS
//
//  Created by 金鑫 on 2021/3/12.
//  Copyright © 2021 魏振伟. All rights reserved.
//

#import "UBMMathTool.h"

@implementation UBMMathTool

+ (CGFloat)radianFromDegree:(CGFloat)degree {
    return degree / 180.0 * M_PI;
}

+ (CGFloat)degreeFromRadian:(CGFloat)radian {
    return radian * (180.0 / M_PI);
}

@end
