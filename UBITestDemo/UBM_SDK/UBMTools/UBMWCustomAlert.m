//
//  UBMWCustomAlert.m
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/7/21.
//  Copyright © 2020 魏振伟. All rights reserved.
//

#import "UBMWCustomAlert.h"
#import "UBMViewTools.h"
#import "UBMPrefixHeader.h"

@interface UBMWCustomAlert()

@property(nonatomic, strong)UIButton* lefbton; //左按钮
@property(nonatomic, strong)UIButton* rightBton; //右按钮
@property(nonatomic, strong)UILabel* contentLab; //内容

@end

@implementation UBMWCustomAlert

-(instancetype)initWithFrame:(CGRect)frame andTitle:(NSString *)title andContent:(NSString *)contend andleftBton:(NSString *)lefttitle andRightBton:(NSString *)rightTitle{
    if(self = [super initWithFrame:frame]){
        
        self.backgroundColor = ColorSet(0, 0, 0, MainAlphe);
        
        UIView* cardView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth-34*2, 0)];
        cardView.backgroundColor = UIColor.whiteColor;
        cardView.layer.cornerRadius = 6;
        [self addSubview:cardView];
        
        CGFloat topHeight = 20;
        if(!IsEmptyStr(title)){
            UILabel* titlelab = [UBMViewTools lableWithText:title font:[UIFont systemFontOfSize:17 weight:UIFontWeightMedium] textColor:UIColor.blackColor];
            titlelab.frame = CGRectMake(0, 0, cardView.width, 64);
            titlelab.textAlignment = NSTextAlignmentCenter;
            [cardView addSubview:titlelab];
            topHeight = titlelab.bottom+10;
        }
        
        UILabel* contentLab = [UBMViewTools lableWithText:contend font:[UIFont systemFontOfSize:15] textColor:ColorSet(0, 0, 0, 0.85)];
        contentLab.numberOfLines = 0;
        [UBMViewTools setLabelSpace:contentLab withSpace:5 withFont:[UIFont systemFontOfSize:15]];
        contentLab.frame = CGRectMake(20, topHeight, cardView.frame.size.width-20*2, [UBMViewTools getHeightLable:contentLab withWidth:(cardView.width-20*2)] );
        [cardView addSubview:contentLab];
        self.contentLab = contentLab;
        
        UIImageView* line1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, contentLab.bottom+20, cardView.frame.size.width, 0.5)];
        line1.backgroundColor = ColorSet(0, 0, 0, 0.06);
        [cardView addSubview:line1];
        
        if(!IsEmptyStr(rightTitle) ){
            
            UIButton* lefbton = [UIButton buttonWithType:UIButtonTypeCustom];
            lefbton.frame = CGRectMake(0, line1.bottom, cardView.frame.size.width/2.0, vw(50));
            [lefbton setTitleColor:ColorSet(0, 0, 0, 0.85) forState:UIControlStateNormal];
            [lefbton setTitle:lefttitle forState:UIControlStateNormal];
            lefbton.titleLabel.font = [UIFont systemFontOfSize:vw(16) weight:UIFontWeightMedium];
            [lefbton addTarget:self action:@selector(lefAction) forControlEvents:UIControlEventTouchUpInside];
            [cardView addSubview:lefbton];
            self.lefbton = lefbton;
            
            UIImageView* line2 = [[UIImageView alloc] initWithFrame:CGRectMake(lefbton.frame.size.width, line1.bottom, 0.5, vw(50))];
            line2.backgroundColor = ColorSet(0, 0, 0, 0.06);
            [cardView addSubview:line2];
            
            
            UIButton* rightBton = [UIButton buttonWithType:UIButtonTypeCustom];
            rightBton.frame = CGRectMake(line2.right, line1.bottom, cardView.width/2.0, vw(50));
            [rightBton setTitleColor:ColorSet(0, 0, 0, 0.85) forState:UIControlStateNormal];
            rightBton.titleLabel.font = [UIFont systemFontOfSize:vw(16) weight:UIFontWeightMedium];
            [rightBton setTitle:rightTitle forState:UIControlStateNormal];
            [rightBton addTarget:self action:@selector(rightAction) forControlEvents:UIControlEventTouchUpInside];
            [cardView addSubview:rightBton];
            self.rightBton = rightBton;
            
            cardView.frame = CGRectMake(vw(33), (frame.size.height-rightBton.bottom)/2.0, frame.size.width-vw(33)*2, rightBton.bottom);
        }else{
            UIButton* rightBton = [UIButton buttonWithType:UIButtonTypeCustom];
            rightBton.frame = CGRectMake(0, line1.bottom, cardView.width, vw(50));
            [rightBton setTitleColor:ColorSet(255, 121, 24, 1) forState:UIControlStateNormal];
            rightBton.titleLabel.font = [UIFont systemFontOfSize:vw(16) weight:UIFontWeightMedium];
            [rightBton setTitle:lefttitle forState:UIControlStateNormal];
            [rightBton addTarget:self action:@selector(lefAction) forControlEvents:UIControlEventTouchUpInside];
            [cardView addSubview:rightBton];
            
            cardView.frame = CGRectMake(vw(33), (frame.size.height-rightBton.bottom)/2.0, frame.size.width-vw(33)*2, rightBton.bottom);
        }
        
        
    }
    return self;
}

-(void)show{
    if(self){
        [[[UIApplication sharedApplication] delegate].window addSubview:self];
        //[UIApplication.sharedApplication.keyWindow addSubview:self]; //禁止使用keywindow
    }
}

-(void)dismiss{
    [self removeFromSuperview];
}

-(void)lefAction{
    if(self.leftActionBlock){
        self.leftActionBlock();
    }
    [self dismiss];
}

-(void)rightAction{
    
    if(self.rightActionBlock){
        self.rightActionBlock();
    }
    
    [self dismiss];
}

-(void)setRightBtonColor:(UIColor *)rightBtonColor{
    _rightBtonColor = rightBtonColor;
    
    [self.rightBton setTitleColor:rightBtonColor forState:UIControlStateNormal];
}

-(void)setLeftBtonColor:(UIColor *)leftBtonColor{
    _leftBtonColor = leftBtonColor;
    
    [self.lefbton setTitleColor:leftBtonColor forState:UIControlStateNormal];
}

-(void)setCaAligint:(UBMContentAligint)caAligint{
    _caAligint = caAligint;
    if (caAligint == ubmWCAleft) {
        self.contentLab.textAlignment = NSTextAlignmentLeft;
    }else if (caAligint == ubmWCAright){
        self.contentLab.textAlignment = NSTextAlignmentRight;
    }else{
        self.contentLab.textAlignment = NSTextAlignmentCenter;
    }
}

@end
