//
//  UBMMTools.h
//  YO
//
//  Created by SuperMan on 2018/10/24.
//  Copyright © 2018年 DeiDaiHuLianCompany. All rights reserved.
//

/*
 工具集合类
 */

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <UIKit/UIKit.h>

@interface UBMMTools : NSObject

/* 正则表达式判断手机号是否符合标准 */
+ (BOOL)valiMobile:(NSString *)mobile;


/* 手机号中间加密 */
+ (NSString*)encryptPhoneNum:(NSString*)phoneNum;


#pragma mark - 获取日
+ (NSInteger)day:(NSDate *)date;
#pragma mark - 获取月
+ (NSInteger)month:(NSDate *)date;
#pragma mark - 获取年
+ (NSInteger)year:(NSDate *)date;
#pragma mark - 获取当月第一天周几
+ (NSInteger)firstWeekdayInThisMonth:(NSDate *)date;
#pragma mark - 获取当前月有多少天
+ (NSInteger)totaldaysInMonth:(NSDate *)date;

#pragma mark - 时间戳转时间(ms) 到分
+ (NSString *)timeWithTimeIntervalString:(NSString *)timeString withFormat:(NSString*)format;

+ (NSString *)timeWithTimeIntervalString:(NSString *)timeString;
+ (NSString *)timeSecondWithTimeIntervalString:(NSString *)timeString;

+ (NSString* )currentTimeStr;//当前时间时间戳
+ (NSTimeInterval)timeInterval:(NSString*)beginTimestamp sinceDate:(NSString*)endTimestamp;//时间差（秒）

#pragma mark - 百度转火星（GCJ-02）坐标
+ (CLLocationCoordinate2D)GCJ02FromBD09:(CLLocationCoordinate2D)coor;
#pragma mark - 火星转百度坐标
+ (CLLocationCoordinate2D)BD09FromGCJ02:(CLLocationCoordinate2D)coor;

//世界标准地理坐标(WGS-84) 转换成 中国国测局地理坐标（GCJ-02）<火星坐标>
//+ (CLLocationCoordinate2D)wgs84ToGcj02:(CLLocationCoordinate2D)location;

#pragma mark - 世界标准地理坐标(WGS-84) 转换成 百度地理坐标（BD-09)
+ (CLLocationCoordinate2D)wgs84ToBd09:(CLLocationCoordinate2D)location;

#pragma mark - 计算两位置之前的距离
+(double)distanceBetweenOrderBy:(double) lat1 :(double) lat2 :(double) lng1 :(double) lng2;

#pragma mark - 颜色宏定义
+ (UIColor *)colorWithRGB:(NSString*)color;

#pragma mark - 导航
+ (void)daohangLag:(NSString*)Lng andlat:(NSString*)lat andname:(NSString*)str;

#pragma mark - 米转化成千米
+ (NSString*)mToKmWithString:(NSString*)mm;

#pragma mark - 当前app版本
+ (NSString *)appVersion;

#pragma mark - 文件夹大小（M）
+ (float)folderSizeAtPath:(NSString *)folderPath;

#pragma mark - 根据日月计算星座
+ (NSString *)getAstroWithMonth:(NSInteger)m day:(NSInteger)d;

#pragma mark - 根据生日计算年龄。@"yyyy-MM-dd"
+ (NSString*)ageWithBirsday:(NSString*)bir;


#pragma mark - 字典转json
+ (NSString*)JsonWithDictionary:(NSDictionary*)dict;

#pragma mark - 将数组转换成json格式字符串,不含\n这些符号
+ (NSString *)JsonWithArrary:(NSArray *)arrJson;

#pragma mark - json转对象（字典和数组）
+(id)jsonToObject:(NSString *)json;

#pragma mark - 转成字符串
+ (NSString*)changeToString:(id)idStr;

#pragma mark - 比较两个版本号 版本号相等,返回0; v1小于v2,返回-1; 否则返回1.
+ (NSInteger)compareVersion:(NSString *)v1 to:(NSString *)v2;

#pragma mark - 千分位
+ (NSString*)positiveFormat:(NSString *)text andSymbol:(NSString*)symbol;

#pragma mark - unicode转中文
+(NSString *)replaceUnicode:(NSString*)unicodeStr;

#pragma mark - 获取当前viewcontroller
+ (UIViewController *)currentActiveController;

#pragma mark - 获取当前设备名称
+ (NSString*)getDeviceName;

///获取设备UUID
+ (NSString *)getUUID;

#pragma mark - md5
+ (NSString *)MD5HexDigest:(NSString *)input;

#pragma mark - emptyStr
+(BOOL)IsEmptyStr:(NSString *)string;

+ (UIWindow *)currentWindow;

+ (NSBundle *)ubmBundle;

@end
