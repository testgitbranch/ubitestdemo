//
//  UBMWJProgressHUD.m
//  WJ
//
//  Created by mac on 14/12/9.
//  Copyright (c) 2014年 ecan. All rights reserved.
//

#import "UBMWJProgressHUD.h"
#import "UBMMTools.h"
#import "UBMViewTools.h"
#import "UBMPrefixHeader.h"

#define bacDefultAlpha 0   //默认透明度是0

@interface UBMWJProgressHUD()

@property (nonatomic,strong)UIView *backgroundView;

//gif
@property (nonatomic,strong)UIImageView *contentView;

//系统菊花图
@property (nonatomic,strong)UIActivityIndicatorView* juhuaView;
@property (nonatomic, strong)UILabel* juhuaLab;

// 三个点
@property(nonatomic, strong)UIImageView* pointOne;
@property(nonatomic, strong)UIImageView* pointTwo;
@property(nonatomic, strong)UIImageView* pointThree;



//模式
@property(nonatomic, assign)UBMHudModel model;

@property(nonatomic, assign)CGFloat backAlpe; //背景透明度
@property(nonatomic, strong)UIColor* bacColor; //背景色


@end

@implementation UBMWJProgressHUD

+ (void)show{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[UBMWJProgressHUD instance] showViewWithColor:[UBMWJProgressHUD instance].bacColor andAlpha:[UBMWJProgressHUD instance].backAlpe];
    });
    
}

+ (void)showWithType:(UBMHudModel)model andBacAlpha:(CGFloat)bacAlpha andBacColor:(UIColor *)bacColor{
    [UBMWJProgressHUD instance].model = model;
    [UBMWJProgressHUD instance].backAlpe = bacAlpha;
    [UBMWJProgressHUD instance].bacColor = bacColor;
    [UBMWJProgressHUD show];
}

+ (void)showActivityIndicatorWithText:(NSString *)text{
    [UBMWJProgressHUD instance].model = ubmActivityIndicator;
    [UBMWJProgressHUD instance].bacColor = ColorSet(0, 0, 0, MainAlphe);
    [UBMWJProgressHUD instance].backAlpe = 1;
    [UBMWJProgressHUD instance].juhuaLab.text = text;
    [UBMWJProgressHUD instance].juhuaLab.hidden = NO;
    [UBMWJProgressHUD show];
}

+ (void)dismiss{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[UBMWJProgressHUD instance] dismissView];
    });
    
}

+(void)setTpye:(UBMHudModel)model{
    [UBMWJProgressHUD instance].model = model;
}

+(void)setBacAlpha:(CGFloat)bacAlpha{
    [UBMWJProgressHUD instance].backAlpe = bacAlpha;
}

+(void)setBacColor:(UIColor *)bacColor{
    [UBMWJProgressHUD instance].bacColor = bacColor;
}


+ (UBMWJProgressHUD *)instance{
    static dispatch_once_t once;
    static UBMWJProgressHUD *progressView;
    dispatch_once(&once, ^{
        progressView = [[self alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    });
    return progressView;
}


- (instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        self.bacColor = UIColor.blackColor;
        self.backAlpe = bacDefultAlpha;

        
        self.backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height )];
        self.backgroundView.alpha = 0.0f;
        [self addSubview:self.backgroundView];
        
        UIView *closeView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, 64)];
        [self addSubview:closeView];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissView)];
        [closeView addGestureRecognizer:tap];
        
        //自定义三点动画
        CGFloat pointWidth = 8.0;
        CGFloat pointInterspace = 12.0;
        
        _pointOne = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.size.width/2-pointWidth/2-pointInterspace-pointWidth, self.frame.size.height/2-pointWidth/2, pointWidth, pointWidth)];
        _pointOne.backgroundColor = ColorWithRGB(@"#FF4B41");
        _pointOne.layer.cornerRadius = _pointOne.frame.size.width/2.0;
        _pointOne.layer.masksToBounds = YES;
        _pointOne.alpha = 0;
        [self addSubview:_pointOne];
        
        
        _pointTwo = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.size.width/2-pointWidth/2, self.frame.size.height/2-pointWidth/2, pointWidth, pointWidth)];
        _pointTwo.backgroundColor = ColorWithRGB(@"#FFAB2D");
        _pointTwo.layer.cornerRadius = _pointTwo.frame.size.width/2.0;
        _pointTwo.layer.masksToBounds = YES;
        _pointTwo.alpha = 0;
        [self addSubview:_pointTwo];
        
        
        _pointThree = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.size.width/2+pointInterspace+pointWidth/2, self.frame.size.height/2-pointWidth/2, pointWidth, pointWidth)];
        _pointThree.backgroundColor = ColorWithRGB(@"#52C41A");
        _pointThree.layer.cornerRadius = _pointThree.frame.size.width/2.0;
        _pointThree.layer.masksToBounds = YES;
        _pointThree.alpha = 0;
        [self addSubview:_pointThree];
        

    }
    return self;
}

#pragma mark - gif动画
-(UIImageView *)contentView{
    if (!_contentView) {

       CGFloat contentViewWidth = self.frame.size.width/4;
       CGFloat contentViewHeight = contentViewWidth * 74 / 133;

         NSMutableArray *images = [NSMutableArray array];
         for (int i = 1; i<=15; i++) {
             UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"loading_%i",i]];
             [images addObject:image];
         }
         
        _contentView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, contentViewWidth, contentViewHeight)];
        _contentView.center = self.center;
        _contentView.image = [UIImage imageNamed:@"loading_1"];
        [_contentView setAnimationImages:images];
        [_contentView setAnimationDuration:3];
        [self addSubview:_contentView];
     }

    return _contentView;
}

#pragma mark - 系统菊花
- (UIActivityIndicatorView *)juhuaView{
    if (!_juhuaView) {
        _juhuaView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleLarge];
        _juhuaView.center = self.center;
        [self addSubview:_juhuaView];
    }
    
    return _juhuaView;
}

//-(UILabel *)juhuaLab{
//    if (_juhuaLab == nil) {
//
//        _juhuaLab = [UBMViewTools lableWithText:@"" font:[UIFont systemFontOfSize:15] textColor:UIColor.whiteColor];
//        _juhuaLab.frame = CGRectMake(0, self.center.y+5, self.frame.size.width, 100);
//        _juhuaLab.numberOfLines = 0;
//        _juhuaLab.textAlignment = NSTextAlignmentCenter;
//        _juhuaLab.hidden = YES;
////        _juhuaLab.backgroundColor= UIColor.orangeColor;
//        [self addSubview:_juhuaLab];
//    }
//    return _juhuaLab;
//}

#pragma mark - 开启动画
-(void)startPointAniMation{
    [self pointAnimation:self.pointOne];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self pointAnimation:self.pointTwo];
    });
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self pointAnimation:self.pointThree];
    });
}

#pragma mark - 停止动画
-(void)stopAniMation{
    [self.pointOne.layer removeAllAnimations];
    [self.pointTwo.layer removeAllAnimations];
    [self.pointThree.layer removeAllAnimations];
}

-(void)pointAnimation:(UIView*)view{
    CGFloat aniHeight = 4;
    CAKeyframeAnimation* animation = [CAKeyframeAnimation animationWithKeyPath:@"transform.translation.y"];
    CGFloat currentTx = view.transform.ty;
    animation.duration = 0.8;
    animation.values = @[@(currentTx), @(currentTx-aniHeight), @(currentTx)];
    animation.keyTimes = @[@(0), @(0.4), @(1)];
    animation.repeatCount = 5000;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    [view.layer addAnimation:animation forKey:@"kViewShakerAnimationKey"];
}



#pragma mark - 开始动画
- (void)showViewWithColor:(UIColor *)color andAlpha:(CGFloat)alpha
{
    if (![self superview]) {
        
        [[UBMMTools currentWindow] addSubview:self];
//        NSLog(@"%f,%f,%f",UIWindowLevelNormal,UIWindowLevelStatusBar,UIWindowLevelAlert);
        
    }
    
    
    self.pointOne.alpha = 0.0;
    self.pointTwo.alpha = 0.0;
    self.pointThree.alpha = 0.0;
    
    [UIView animateWithDuration:0.4
                         delay:0
         usingSpringWithDamping:0
          initialSpringVelocity:0
                       options:UIViewAnimationOptionCurveLinear
                     animations:^{
                     
                         self.backgroundView.alpha = alpha;
                         self.backgroundView.backgroundColor = color;
                     
                         if (self.model == ubmDefultModel || self.model == ubmPointAnimation) {
                             self.pointOne.alpha = 0.5;
                             self.pointTwo.alpha = 0.5;
                             self.pointThree.alpha = 0.5;
                             
                         }else if (self.model == ubmActivityIndicator) {
                             self.juhuaView.alpha = 1.0;
                         }
                     
                       } completion:^(BOOL finished) {
                           if (self.model == ubmDefultModel || self.model == ubmPointAnimation) {
                               
                               self.pointOne.alpha = 1;
                               self.pointTwo.alpha = 1;
                               self.pointThree.alpha = 1;
                               [self startPointAniMation];
                               
                           }else if (self.model == ubmImgGif){
                               
                               [self.contentView startAnimating];
                               
                           }else if (self.model == ubmActivityIndicator){
                               
                               [self.juhuaView startAnimating];
                               self.juhuaView.center = self.center;
                           }
                       }];

    
    
}

#pragma mark - 结束动画
- (void)dismissView{
    
    if (![self superview]) {
        return;
    }
    
    [self removeFromSuperview];
    
    if (self.model == ubmDefultModel || self.model == ubmPointAnimation) {
        [self stopAniMation];
        
    }else if (self.model == ubmImgGif){
        [self.contentView stopAnimating];
        
    }else if (self.model == ubmActivityIndicator){
        [self.juhuaView stopAnimating];
    }
    
    //恢复默认数值
    [UBMWJProgressHUD instance].model = ubmDefultModel;
    [UBMWJProgressHUD instance].backAlpe = bacDefultAlpha;
    [UBMWJProgressHUD instance].bacColor = UIColor.blackColor;

    if (_juhuaLab) {
        [UBMWJProgressHUD instance].juhuaLab.hidden = YES;
    }
    
    
    /*
    [UIView animateWithDuration:0.0
                          delay:0
         usingSpringWithDamping:0
          initialSpringVelocity:0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         
                         self.backgroundView.alpha = 0.0;
                         self.backgroundView.backgroundColor = [UIColor whiteColor];
                             
                         if (self.model == ubmDefultModel || self.model == ubmPointAnimation){
                            self.pointOne.alpha = 0.0;
                            self.pointTwo.alpha = 0.0;
                            self.pointThree.alpha = 0.0;
                             
                         }else if (self.model == ubmActivityIndicator){
                            self.juhuaView.alpha = 0.0;
                         }

                     } completion:^(BOOL finished) {
                         
                             if (self.model == ubmDefultModel || self.model == ubmPointAnimation) {
                                 [self stopAniMation];
                                 
                             }else if (self.model == ubmImgGif){
                                 [self.contentView stopAnimating];
                                 
                             }else if (self.model == ubmActivityIndicator){
                                 [self.juhuaView stopAnimating];
                             }
                             
                             //恢复默认数值
                             [UbmWJProgressHUD instance].model = ubmDefultModel;
                             [UbmWJProgressHUD instance].backAlpe = bacDefultAlpha;
                             [UbmWJProgressHUD instance].bacColor = UIColor.blackColor;
                         
                             if (_juhuaLab) {
                                 [UbmWJProgressHUD instance].juhuaLab.hidden = YES;
                             }
                             
                             
                             [self removeFromSuperview];
                             
                         }];
*/

}

@end
