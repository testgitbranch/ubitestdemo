//
//  UBMNetEncyptOfOC.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2021/5/13.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

/*
 处理请求参数：拼接公共参数、请求参数加密
 */

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UBMNetEncyptOfOC : NSObject

+(NSDictionary*)encryptParameters:(NSDictionary*)defultParameters url:(NSString*)url;

//此签名方法去掉了对特殊字符的encode。签名算法与pay一致。
+(NSDictionary*)encryptJsonParameters:(NSDictionary*)defultParameters url:(NSString*)url;

@end

NS_ASSUME_NONNULL_END
