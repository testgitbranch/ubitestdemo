//
//  UBMNetEncyptOfOC.m
//  LoveCarChannel_iOS
//
//  Created by wzw on 2021/5/13.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

#import "UBMNetEncyptOfOC.h"
#include <CommonCrypto/CommonDigest.h>
#import "UBMPrefixHeader.h"

static const NSString * appkey = @"3gkcyhreglmp7chvg6k8ntkf5ymo1ark";   //pay key
static const NSString * appkey2 = @"e74cb4423b4c4c23bc3d19437f52958a";  //act key

@implementation UBMNetEncyptOfOC

#pragma mark - 处理请求参数
+(NSDictionary*)encryptParameters:(NSDictionary*)defultParameters url:(NSString*)url{
    
    NSDictionary* paDicx = defultParameters;
    if ([url isEqualToString:PayBaseUrl] || [url isEqualToString:[UBMDomainName.shareDomain ubmURL]] || [url isEqualToString:AthenaBaseUrl]) {
        paDicx = [self handlePayParameters:defultParameters];
    }else if([url isEqualToString:ActBaseUrl]){
        paDicx = [self handleActParameters:defultParameters];
    }else{
        paDicx = [self handleParameters:defultParameters];
    }
    
    NSLog(@"%@",paDicx);
    
    return paDicx;
}

+(NSDictionary*)encryptJsonParameters:(NSDictionary*)defultParameters url:(NSString*)url{
    NSDictionary* praDic = [self addPayCommenPar:defultParameters];
    
    NSString* wappkey = [appkey copy];
    NSString* sigin = [self getNewSign:praDic andAppKey:wappkey];
    
    NSMutableDictionary* resultPar = [[NSMutableDictionary alloc] initWithDictionary:praDic copyItems:YES];
    [resultPar setObject:sigin forKey:@"sign"];
    
    return resultPar;
}

#pragma mark - pay接口加密
+(NSDictionary *)handlePayParameters:(NSDictionary*)defultParameters{
    
    NSDictionary* praDic = [self addPayCommenPar:defultParameters];
    
    NSString* wappkey = [appkey copy];
    NSString* sigin = [self getSign:praDic andAppKey:wappkey];
    
    NSMutableDictionary* resultPar = [[NSMutableDictionary alloc] initWithDictionary:praDic copyItems:YES];
    [resultPar setObject:sigin forKey:@"sign"];
    
    return resultPar;
}

//pay接口添加公共参数
+(NSDictionary*)addPayCommenPar:(NSDictionary*)defultParameters{
    
//    NSString* app_Version = [NSString stringWithFormat:@"%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
    NSDictionary* defultPraDic = @{@"appid":@"210001", @"ver":@"1", @"plat":@"3", @"v":@"1.0.1"};
    NSMutableDictionary* paDicx = [[NSMutableDictionary alloc] initWithDictionary:defultParameters copyItems:YES];
    
    NSArray* keys = defultPraDic.allKeys;
    for (int i=0; i<keys.count; i++) {
        NSString* value = [defultPraDic objectForKey:keys[i]];
        [paDicx setObject:value forKey:keys[i]];
    }

    return paDicx;
}

#pragma mark - act接口加密
+(NSDictionary *)handleActParameters:(NSDictionary*)defultParameters{
    
//    NSString* app_Version = [NSString stringWithFormat:@"%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
    NSDictionary* defultPraDic = @{@"appid":@"992001", @"ver":@"1", @"dv":@"1", @"plat":@"3", @"v":@"1.0.1"};
    NSString* wappkey = [appkey2 copy];

    NSMutableDictionary* paDicx = [[NSMutableDictionary alloc] initWithDictionary:defultParameters];
    
    NSArray* keys = defultPraDic.allKeys;
    for (int i=0; i<keys.count; i++) {
        NSString* value = [defultPraDic objectForKey:keys[i]];
        [paDicx setObject:value forKey:keys[i]];
    }
    
    NSString* sigin = [self getSign:paDicx andAppKey:wappkey];
    
    [paDicx setObject:sigin forKey:@"sign"];
    
    return paDicx;
}


#pragma mark - 其他地址接口加密
+(NSDictionary *)handleParameters:(NSDictionary *)defultParameters{
    
    return defultParameters;
}

#pragma mark - 计算签名
+(NSString* )getSign:(NSDictionary*)dataDic andAppKey:(NSString*)wappkey{
    if(dataDic == nil){
        return @"";
    }
    
    NSArray* allkeys = dataDic.allKeys;
    NSArray *result = [allkeys sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        return [obj1 compare:obj2]; //升序
    }];
    allkeys = result;
    
    NSMutableString* str1 = [[NSMutableString alloc] init];
    for (int i=0; i<allkeys.count; i++) {
        if(dataDic[allkeys[i]] == nil){
            continue;
        }else{
//            NSString* jsonString2 = [dataDic[allkeys[i]] stringByReplacingOccurrencesOfString:@"\\" withString:@""];
            NSString* encodeStr = [dataDic[allkeys[i]] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet characterSetWithCharactersInString:@"\"#%/:<>?@[\\]^`{|}(), "].invertedSet];
//            [NSCharacterSet characterSetWithCharactersInString:@":,[{}]\""]
            //NSLog(@"encodeStr: %@ %@", dataDic[allkeys[i]] ,encodeStr);
            NSString* nstr = [NSString stringWithFormat:@"&%@=%@", allkeys[i], encodeStr];
            [str1 appendString:nstr];
        }
    }
    
    NSString* str2 = [str1 substringFromIndex:1];
    NSString* str3 = [NSString stringWithFormat:@"%@%@", str2, wappkey];
    NSString* str4 = [self MD5HexDigest:[str3 dataUsingEncoding:NSUTF8StringEncoding]];
    
    return str4;
}

#pragma mark - 计算签名2：不对特殊字符encode
+(NSString* )getNewSign:(NSDictionary*)dataDic andAppKey:(NSString*)wappkey{
    if(dataDic == nil){
        return @"";
    }
    
    NSArray* allkeys = dataDic.allKeys;
    NSArray *result = [allkeys sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        return [obj1 compare:obj2]; //升序
    }];
    allkeys = result;
    
    NSMutableString* str1 = [[NSMutableString alloc] init];
    for (int i=0; i<allkeys.count; i++) {
        if(dataDic[allkeys[i]] == nil){
            continue;
        }else{
            NSString* value = dataDic[allkeys[i]];
            NSString* nstr = [NSString stringWithFormat:@"&%@=%@", allkeys[i], value];
            [str1 appendString:nstr];
        }
    }
    
    NSString* str2 = [str1 substringFromIndex:1];
    NSString* str3 = [NSString stringWithFormat:@"%@%@", str2, wappkey];
    NSString* str4 = [self MD5HexDigest:[str3 dataUsingEncoding:NSUTF8StringEncoding]];
    
    return str4;
}

//MD5加密返回Nsstring  %02x小写   %02X大写
+(NSString *)MD5HexDigest:(NSData *)input {
     unsigned char result[CC_MD5_DIGEST_LENGTH];
     CC_MD5(input.bytes, (CC_LONG)input.length, result);
     NSMutableString *ret = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH*2];
     for (int i = 0; i<CC_MD5_DIGEST_LENGTH; i++)  {
         [ret appendFormat:@"%02x",result[i]];
     }
     return ret;
}


@end
