//
//  UBMNetWorkToolOfJson.m
//  LoveCarChannel_iOS
//
//  Created by wzw on 2021/9/23.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

#import "UBMNetWorkToolOfJson.h"
#import "UBMNetEncyptOfOC.h"

@implementation UBMNetWorkToolOfJson

+ (instancetype)shareNetWorkTool{
    
    UBMNetWorkToolOfJson* instance = [super shareNetWorkTool];
    
    instance.requestSerializer = [AFJSONRequestSerializer serializer];
    
    return instance;
}

-(NSDictionary *)encryptParameters:(id)parameters url:(id)baseUrl{
    
    return [UBMNetEncyptOfOC encryptJsonParameters:parameters url:baseUrl];
}

@end
