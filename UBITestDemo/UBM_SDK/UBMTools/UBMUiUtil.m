//
//  UBMUiUtil.m
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/7/16.
//  Copyright © 2020 魏振伟. All rights reserved.
//

#import "UBMUiUtil.h"
#import "UBMPrefixHeader.h"

@interface UBMUiUtil()
@property (nonatomic) float width;
@property (nonatomic) float height;
@end

@implementation UBMUiUtil

+(instancetype) shareInit
{
    static id instance;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    
    return instance;
}

-(id)init{
    if(self = [super init]){
        _width=ScreenWidth;
        _height=ScreenHeight;
        
        //当前手机的最大宽度是1242/2。超过最大尺寸，就不再适配了，保持xsmax的适配方案。
        if(_width> 1242/2 + 50){
            _width = 1242/2;
        }
    }
    return self;
}

-(int)vw:(float)width{
    return (int)width*(_width/375);
}
-(int)vh:(float)height{
    return (int)height*(_height/667);
}

@end
