//
//  UBMWJProgressHUD.h
//  WJ
//
//  Created by mac on 14/12/9.
//  Copyright (c) 2014年 ecan. All rights reserved.
//

/*
 自定义等待遮挡动画
 */

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    ubmDefultModel,        //同 ubmPointAnimation
    ubmImgGif,             //一组图片动画。动画暂时没有写入接口，内部写死。
    ubmActivityIndicator,  //系统菊花
    ubmPointAnimation      //三点动画
} UBMHudModel;



@interface UBMWJProgressHUD : UIView

+ (UBMWJProgressHUD *)instance;

#pragma mark - 下面三个方法是为了显示特殊的HUD，默认不用任何设置。

/*
 可以选择上面三种类型，当前默认DefultModel 指向 ubmPointAnimation。可选。
 除DefultModel外，其他的model设置之后只会被执行一次样式。
 */
+ (void)setTpye:(UBMHudModel)model;

/*
 设置背景透明度，也是仅一次有效，然后恢复默认透明度。
 */
+ (void)setBacAlpha:(CGFloat)bacAlpha;

/*
设置背景色，也是仅一次有效，然后恢复默认背景色。
*/
+ (void)setBacColor:(UIColor*)bacColor;

+ (void)showWithType:(UBMHudModel)model andBacAlpha:(CGFloat)bacAlpha andBacColor:(UIColor*)bacColor;

/*
 系统菊花+文字
 */
+ (void)showActivityIndicatorWithText:(NSString*)text;

#pragma mark - 通用方法，必须调用。

/*
 展示
 */
+ (void)show;

/*
移除
*/
+ (void)dismiss;


@end
