//
//  UBMToastObjc.h
//  EngineeProject
//
//  Created by wei on 2017/6/6.
//  Copyright © 2017年 mac. All rights reserved.
//

/*
 自定义toast提示
 */

#import <Foundation/Foundation.h>

@interface UBMToastObjc : NSObject

+(void)toastWithText:(NSString *)title;

+(void)toastWithText:(NSString*)title andtime:(int)tim;

//失败样式的提示框
+(void)toastFailWithText:(NSString*)title andtime:(int)tim;

//成功样式的提示框
+(void)toastSuccessWithText:(NSString*)title andtime:(int)tim;


+(void)toastWithTitle:(NSString*)title andTime:(int)tim andType:(int)type;


//带菊花loading、
+ (void)toastLoadingWithText:(NSString *)text time:(int)time;
+ (void)toastLoadingHiden;

@end
