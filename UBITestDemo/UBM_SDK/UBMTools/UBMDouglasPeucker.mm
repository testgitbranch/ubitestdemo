//
//  UBMDouglasPeucker.m
//  DouglasPeuckerDemo
//
//  Created by 杨磊 on 2018/7/18.
//

#import "UBMDouglasPeucker.h"
#import "Route.pbobjc.h"
#include <set>
#include <stack>

#define kPointArraySpace 2000

@interface UBMDouglasPeucker ()
{
    std::set<NSUInteger> maxIndexSet;
}
@end

@implementation UBMDouglasPeucker

- (instancetype)init
{
    self = [super init];
    if (self) {
        _maxIndexArray = [NSMutableArray arrayWithCapacity:1000];
    }
    return self;
}

- (void)DouglasAlgorithm:(NSArray <RoutePoint *>*)coordinateList threshold:(CGFloat)threshold routArrayBloack:(void(^)(NSArray *responseArray))routArrayBloack
{
    if (coordinateList == nil || coordinateList.count == 0) {
        return ;
    }
    NSUInteger queneCount = coordinateList.count / kPointArraySpace;
    NSUInteger listInterCount = coordinateList.count / queneCount;
    dispatch_group_t group = dispatch_group_create();
    for (NSUInteger i = 0; i < queneCount; i++)
    {
        NSString *queueName = [NSString stringWithFormat:@"quene.group.%lu", (unsigned long)i];
        dispatch_queue_t quene = dispatch_queue_create([queueName UTF8String], DISPATCH_QUEUE_CONCURRENT);
        dispatch_group_async(group, quene, ^
        {
            NSUInteger end = (i+1)*listInterCount - 1;
            if (i == queneCount - 1) {
                end = coordinateList.count - 1;
            }
            [self DouglasAlgorithm:coordinateList begin:i*listInterCount end:end threshold:threshold];
        });
    }
    dispatch_group_notify(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^
    {
        for(std::set<NSUInteger>::iterator it = maxIndexSet.begin(); it!= maxIndexSet.end(); it++)
        {
            [self.maxIndexArray addObject:coordinateList[*it]];
        }
        if (routArrayBloack != nil)
        {
            routArrayBloack(self.maxIndexArray);
        }
    });
}

- (void)DouglasAlgorithm:(NSArray *)coordinateList begin:(NSUInteger)begin end:(NSUInteger)end threshold:(CGFloat)threshold
{
    double dist = 0.0;
    NSUInteger position = 0; //最大距离的点的序号
    std::stack<NSUInteger> tempIndexStack; //STL实现的栈
    tempIndexStack.push(end);
    
    do
    {
        NSArray *array = [self getMaxDistance:coordinateList begin:begin end:end threshold:threshold];
        dist = [array[0] floatValue];
        position = [array[1] unsignedIntegerValue];
        if (dist > threshold)
        {
            @synchronized (self)
            {
                maxIndexSet.insert(position);
            }
            end = position; //更新B值
            tempIndexStack.push(end); //记录最大距离点，放入栈中存储
            continue;
        }
        else
        {
            if (!tempIndexStack.empty() && end != tempIndexStack.top())   //判断后一点是否和当前栈顶相等
            {
                begin = end;
                end   = tempIndexStack.top();
                continue;
            }
            else
            {
                if (end != coordinateList.count)      //判断最后一个点是否和当前线段的B点重合
                {
                    begin = end;
                    if (!tempIndexStack.empty())
                    {
                        NSInteger count = tempIndexStack.size();
                        if (count > 1) //栈中至少还有两个点
                        {
                            NSUInteger temp = tempIndexStack.top();
                            tempIndexStack.pop();
                            end = tempIndexStack.top();
                            tempIndexStack.push(temp);
                        }
                        else if (count == 1)  //栈中只有一个点
                        {
                            end = tempIndexStack.top();
                        }
                        tempIndexStack.pop();
                        continue;
                    }
                }
            }
        }
        
    } while (!tempIndexStack.empty());
}

- (void)douglasAlgorithm:(NSArray *)coordinateList begin:(NSUInteger)begin end:(NSUInteger)end threshold:(CGFloat)threshold
{
    NSUInteger mid = 0;
    if (end - begin > 1)
    {
        NSArray *array = [self getMaxDistance:coordinateList begin:begin end:end threshold:threshold];
        if ([array[0] floatValue] > threshold)
        {
            mid = [array[1] unsignedIntegerValue];
            maxIndexSet.insert(mid);
            [self douglasAlgorithm:coordinateList begin:begin end:mid threshold:threshold];
            [self douglasAlgorithm:coordinateList begin:mid   end:end threshold:threshold];
        }
    }
    else {
        return;
    }
}

- (NSArray *)getMaxDistance:(NSArray *)coordinateList begin:(NSUInteger)begin end:(NSUInteger)end threshold:(CGFloat)threshold
{
    CGFloat maxDistance = -1;
    NSInteger position  = -1;
    CGFloat distance = getDistanceCFunction(coordinateList[begin], coordinateList[end]);
    
    for(NSInteger i = begin; i <= end; i++)
    {
        CGFloat firstSide = getDistanceCFunction(coordinateList[begin], coordinateList[i]);//中间点距离起点的距离
        if(firstSide < threshold)
        {//斜边小于阈值 直角边即这个点距离首尾连线的距离肯定小于阈值 直接跳过这个点 省去了后面的运算
            continue;
        }
        
        CGFloat lastSide = getDistanceCFunction(coordinateList[end], coordinateList[i]);
        if(lastSide < threshold)
        {//斜边小于阈值 直角边即这个点距离首尾连线的距离肯定小于阈值 直接跳过这个点 省去了后面的运算
            continue;
        }
        // 使用海伦公式求距离
        CGFloat p   = (distance + firstSide + lastSide) / 2.0;
        CGFloat dis = sqrt(p * (p - distance) * (p - firstSide) * (p - lastSide)) / distance * 2;
        
        if(dis > maxDistance)
        {
            maxDistance = dis;
            position    = i;
        }
    }
    return @[@(maxDistance),@(position)];
}

- (NSUInteger)getMaxDistIndex:(NSMutableArray *)coordinateList begin:(NSUInteger)begin end:(NSUInteger)end threshold:(CGFloat)threshold
{
    CGFloat maxDistance = -1;
    NSInteger position  = -1;
    CGFloat distance = [self getDistance:coordinateList[begin] lastEntity:coordinateList[end]];
    for(NSInteger i = begin; i <= end; i++)
    {
        CGFloat firstSide = [self getDistance:coordinateList[begin] lastEntity:coordinateList[i]];//中间点距离起点的距离
        if(firstSide < threshold)
        {//斜边小于阈值 直角边即这个点距离首尾连线的距离肯定小于阈值 直接跳过这个点 省去了后面的运算
            continue;
        }
        
        CGFloat lastSide = [self getDistance:coordinateList[end] lastEntity:coordinateList[i]];
        if(lastSide < threshold)
        {//斜边小于阈值 直角边即这个点距离首尾连线的距离肯定小于阈值 直接跳过这个点 省去了后面的运算
            continue;
        }
        // 使用海伦公式求距离
        CGFloat p   = (distance + firstSide + lastSide) / 2.0;
        CGFloat dis = sqrt(p * (p - distance) * (p - firstSide) * (p - lastSide)) / distance * 2;
        
        if(dis > maxDistance)
        {
            maxDistance = dis;
            position = i;
        }
    }
    return position;
}

/**
 * 道格拉斯算法，处理coordinateList序列
 * 先将首末两点加入points序列中，然后在coordinateList序列找出距离首末点连线距离的最大距离值dmax并与阈值threshold进行比较，
 * 若大于阈值则将这个点加入points序列，重新遍历points序列。否则将两点间的所有点(coordinateList)移除
 * LatLngEntity是经纬度model
 * @return 返回经过道格拉斯算法后得到的点的序列
 */
- (NSArray*)douglasAlgorithm:(NSArray <RoutePoint *>*)coordinateList threshold:(CGFloat)threshold
{
    // 将首末两点加入到序列中
    NSMutableArray *points = [NSMutableArray array];
    NSMutableArray *list = [NSMutableArray arrayWithArray:coordinateList];
    
    [points addObject:list[0]];
    [points addObject:coordinateList[coordinateList.count - 1]];
    
    for (NSInteger i = 0; i<points.count - 1; i++)
    {
        NSUInteger start = (NSUInteger)[list indexOfObject:points[i]];
        NSUInteger end = (NSUInteger)[list indexOfObject:points[i+1]];
        if ((end - start) == 1)
        {//起始点之间没有其他点 直接跳过
            continue;
        }
        NSArray *values = [self getMaxDistance:list startIndex:start endIndex:end threshold:threshold];
        CGFloat maxDist = [values[0] floatValue];
        
        //大于阈值 将点加入到points数组
        if (maxDist >= threshold)
        {
            NSInteger position = [values[1] integerValue];
            [points insertObject:list[position] atIndex:i+1];
            // 将循环变量i初始化,即重新遍历points序列
            i = -1;
        }else
        {//将两点间的点全部删除
            NSInteger removePosition = start + 1;
            for (NSInteger j = 0; j < end - start - 1; j++)
            {
                [list removeObjectAtIndex:removePosition];
            }
        }
    }
    return points;
}

/**
 * 根据给定的始末点，计算出距离始末点直线的最远距离和点在coordinateList列表中的位置
 * @param startIndex 遍历coordinateList起始点
 * @param endIndex 遍历coordinateList终点
 * @return maxDistance 返回最大距离 position 在原始数组中位置
 */
- (NSArray *)getMaxDistance:(NSArray <RoutePoint *>*)coordinateList startIndex:(NSInteger)startIndex endIndex:(NSInteger)endIndex threshold:(CGFloat)threshold
{
    CGFloat maxDistance = -1;
    NSInteger position = -1;
    CGFloat distance = [self getDistance:coordinateList[startIndex] lastEntity:coordinateList[endIndex]];//首末两点的距离
    
    for(NSInteger i = startIndex; i < endIndex; i++)
    {
        CGFloat firstSide = [self getDistance:coordinateList[startIndex] lastEntity:coordinateList[i]];//中间点距离起点的距离
        if(firstSide < threshold)
        {//斜边小于阈值 直角边即这个点距离首尾连线的距离肯定小于阈值 直接跳过这个点 省去了后面的运算
            continue;
        }
        
        CGFloat lastSide = [self getDistance:coordinateList[endIndex] lastEntity:coordinateList[i]];
        if(lastSide < threshold)
        {//斜边小于阈值 直角边即这个点距离首尾连线的距离肯定小于阈值 直接跳过这个点 省去了后面的运算
            continue;
        }
        
        // 使用海伦公式求距离
        CGFloat p   = (distance + firstSide + lastSide) / 2.0;
        CGFloat dis = sqrt(p * (p - distance) * (p - firstSide) * (p - lastSide)) / distance * 2;
        
        if(dis > maxDistance)
        {
            maxDistance = dis;
            position = i;
        }
    }
    return @[@(maxDistance),@(position)];
}

// 两点间距离公式
- (CGFloat)getDistance:(RoutePoint*)firstEntity lastEntity:(RoutePoint*)lastEntity
{
    CLLocation *firstLocation = [[CLLocation alloc] initWithLatitude:firstEntity.latitude longitude:firstEntity.longitude];
    CLLocation *lastLocation = [[CLLocation alloc] initWithLatitude:lastEntity.latitude longitude:lastEntity.longitude];
    
    CGFloat  distance  = [firstLocation distanceFromLocation:lastLocation];
    return  distance;
}

// 两点间距离公式
UIKIT_STATIC_INLINE double getDistanceCFunction(RoutePoint *firstEntity, RoutePoint *lastEntity)
{
    double latitude  = (firstEntity.latitude  - lastEntity.latitude) * 100000;
    double longitude = (firstEntity.longitude - lastEntity.longitude) * 110000;
    double distance  = sqrt(latitude * latitude + longitude * longitude);
    return  distance;
};
@end
