//
//  UBMGetLoctionInfor.h
//  EngineeProject
//
//  Created by Edog on 2017/6/9.
//  Copyright © 2017年 mac. All rights reserved.
//

/*
 
 获取用户地理位置信息，并反编码出用户位置
 
 */

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

typedef void(^CallBackBlcok) (NSString *text,CLLocation* loction);//1

@interface UBMGetLoctionInfor : NSObject

-(void)getMyLoctionNow;//首先获取位置，后callback

@property(nonatomic, copy)CallBackBlcok callBackBlock;


@end
