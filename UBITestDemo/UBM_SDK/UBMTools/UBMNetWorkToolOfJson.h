//
//  UBMNetWorkToolOfJson.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2021/9/23.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

/*
 此单例继承自NetWorkTool，目的是把请求参数格式设置成json；
 签名算法目前与pay保持大概一致（去掉了特殊字符的encode）；
 请求参数要求是json格式上传时，用此类；
 */

#import "UBMNetWorkTool.h"

NS_ASSUME_NONNULL_BEGIN

@interface UBMNetWorkToolOfJson : UBMNetWorkTool

@end

NS_ASSUME_NONNULL_END
