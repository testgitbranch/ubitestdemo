//
//  UBMDomains.h
//  LoveCarChannel_iOS
//
//  Created by 金鑫 on 2021/8/13.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

/*
 基础域名定义
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

///MARK: 测试环境
UIKIT_EXTERN NSString * const UBMTBaseURL;       //APP前端 h5
UIKIT_EXTERN NSString * const UBMTHiveBaseURL;   //大互助公众号前端 h5
UIKIT_EXTERN NSString * const UBMTRepairBaseURL; //小修宝公众号前端 h5
UIKIT_EXTERN NSString * const UBMTBaiXingKeFu;   //官网域名 h5

UIKIT_EXTERN NSString * const UBMTPayBaseURL;    //接口 pay
UIKIT_EXTERN NSString * const UBMTActBaseURL;    //接口 act
UIKIT_EXTERN NSString * const UBMTUBMBaseURL;    //ubm
UIKIT_EXTERN NSString * const UBMTAthenaBaseURL; //athena
UIKIT_EXTERN NSString * const UBMTUcBaseURL;     //uc

///MARK: QA环境
UIKIT_EXTERN NSString * const UBMQABaseURL;
UIKIT_EXTERN NSString * const UBMQAHiveBaseURL;
UIKIT_EXTERN NSString * const UBMQARepairBaseURL;
UIKIT_EXTERN NSString * const UBMQABaiXingKeFu;

UIKIT_EXTERN NSString * const UBMQAPayBaseURL;
UIKIT_EXTERN NSString * const UBMQAActBaseURL;
UIKIT_EXTERN NSString * const UBMQAUBMBaseURL;
UIKIT_EXTERN NSString * const UBMQAAthenaBaseURL;
UIKIT_EXTERN NSString * const UBMQAUcBaseURL;

///MARK: 仿真环境
UIKIT_EXTERN NSString * const UBMSBaseURL;
UIKIT_EXTERN NSString * const UBMSHiveBaseURL;
UIKIT_EXTERN NSString * const UBMSRepairBaseURL;
UIKIT_EXTERN NSString * const UBMSBaiXingKeFu;

UIKIT_EXTERN NSString * const UBMSPayBaseURL;
UIKIT_EXTERN NSString * const UBMSActBaseURL;
UIKIT_EXTERN NSString * const UBMSUBMBaseURL;
UIKIT_EXTERN NSString * const UBMSAthenaBaseURL;
UIKIT_EXTERN NSString * const UBMSUcBaseURL;

///MARK: 生产环境
UIKIT_EXTERN NSString * const UBMPBaseURL;
UIKIT_EXTERN NSString * const UBMPHiveBaseURL;
UIKIT_EXTERN NSString * const UBMPRepairBaseURL;
UIKIT_EXTERN NSString * const UBMPBaiXingKeFu;

UIKIT_EXTERN NSString * const UBMPPayBaseURL;
UIKIT_EXTERN NSString * const UBMPActBaseURL;
UIKIT_EXTERN NSString * const UBMPUBMBaseURL;
UIKIT_EXTERN NSString * const UBMPAthenaBaseURL;
UIKIT_EXTERN NSString * const UBMPUcBaseURL;
