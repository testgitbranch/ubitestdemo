//
//  UBMJugeBaseURLWIthRequest.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/8/19.
//  Copyright © 2020 魏振伟. All rights reserved.
//

/*
 基础URL判断
 */

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef enum : NSUInteger {
    UBMEnviTest,
    UBMEnviQA,
    UBMEnviSim,
    UBMEnviProduct,
} UBMEnviType;


@interface UBMEnviConfig: NSObject

@property (nonatomic, assign, readonly) UBMEnviType currentEnvi;//当前配置环境

+ (instancetype)shareEnvi;

- (void)config:(UBMEnviType)envi;

@end

@interface UBMDomainName : NSObject

@property (nonatomic, copy, readonly) NSString * baseURL;
@property (nonatomic, copy, readonly) NSString * hiveURL;
@property (nonatomic, copy, readonly) NSString * repairURL;
@property (nonatomic, copy, readonly) NSString * baiXingKeFuURL;
@property (nonatomic, copy, readonly) NSString * payURL;
@property (nonatomic, copy, readonly) NSString * actURL;
@property (nonatomic, copy, readonly) NSString * ubmURL;
@property (nonatomic, copy, readonly) NSString * athenaURL;
@property (nonatomic, copy, readonly) NSString * ucURL;

+ (instancetype)shareDomain;

- (void)configDomainWithEnvi:(UBMEnviType)envi;

@end

@interface UBMJugeBaseURLWIthRequest : NSObject

//用于 域名 是否走接口配置
+ (void)requestDomainNameEnviConfig:(void(^)(BOOL isSucc))completeBlock;


@end

NS_ASSUME_NONNULL_END
