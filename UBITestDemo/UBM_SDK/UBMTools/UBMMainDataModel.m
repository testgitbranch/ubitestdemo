//
//  UBMMainDataModel.m
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/11/12.
//  Copyright © 2020 魏振伟. All rights reserved.
//

#import "UBMMainDataModel.h"
//#import <AFNetworking/AFNetworking.h>
#import "UBMUrls.h"
#import "UBMNetWorkTool.h"

@interface UBMMainDataModel()

//把请求成功的数据保存下来，如果没网，则返回之。相当于只有内存缓存，没有磁盘缓存。APP不退出，则会一直存在。退出了，没网是需要去登录的。
@property(nonatomic, strong)NSDictionary* GetReachRulesDic;
@property(nonatomic, strong)NSArray* lastRouteInfor;
@property(nonatomic, strong)NSArray* lunboArr;
@property(nonatomic, strong)NSDictionary* kengweiInfor;
@property(nonatomic, strong)NSDictionary* jindutiaoInfor;
@property(nonatomic, strong)NSArray* eventMoneyArr;//机器人轮播数据

@end

@implementation UBMMainDataModel

+(instancetype)shareUBMMainDataModel
{
    static id instance;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    
    return instance;
}

-(void)getDataWithBaseUrl:(NSString *)baseURl URLName:(NSString *)urlName andParameter:(NSDictionary *)param andResult:(void (^)(id _Nonnull))result ansError:(nonnull void (^)(void))error{
    
    //无网取本地。
    if ([[AFNetworkReachabilityManager sharedManager] networkReachabilityStatus] == AFNetworkReachabilityStatusNotReachable) {
        
        id localResult = nil;
        
        //可以考虑动态添加属性。
        if ([urlName isEqualToString: UBMGetReachRules] && self.GetReachRulesDic != nil) {
            localResult = self.GetReachRulesDic;
        }else if ([urlName isEqualToString: UBMTripList] && self.lastRouteInfor != nil){
            localResult = self.lastRouteInfor;
        }else if ([urlName isEqualToString: UBMGetThemeCarouselText] && self.lunboArr != nil){
            localResult = self.lunboArr;
        }else if ([urlName isEqualToString: UBMKengWeiInfor] && self.kengweiInfor != nil){
            localResult = self.kengweiInfor;
        }else if ([urlName isEqualToString: UBMGetEventCoordinateConfig] && self.jindutiaoInfor != nil){
            localResult = self.jindutiaoInfor;
        }else if ([urlName isEqualToString: UBMGetEventMoneyText] && self.eventMoneyArr != nil){
            localResult = self.eventMoneyArr;
        }
        
        
        if (localResult !=nil) {
            if (result) {
                result(localResult);
            }
            return;
        }
        
    }
    
    __weak typeof(self) weakself = self;
    
    [[UBMNetWorkTool shareNetWorkTool] requestWithBaseUrl:baseURl UrlString:urlName parameters:param method:@"POST" success:^(id response) {
        
        if (response[@"code"] && [response[@"code"] intValue]==1) {
            
            //更新本地数据
            if ([urlName isEqualToString:UBMGetReachRules]) {
                weakself.GetReachRulesDic = response[@"data"];
            }else if ([urlName isEqualToString: UBMTripList]){
                weakself.lastRouteInfor = response[@"data"];
            }else if ([urlName isEqualToString: UBMGetThemeCarouselText]){
                weakself.lunboArr = response[@"data"];
            }else if ([urlName isEqualToString: UBMKengWeiInfor]){
                weakself.kengweiInfor = response[@"data"];
            }else if ([urlName isEqualToString: UBMGetEventCoordinateConfig]){
                weakself.jindutiaoInfor = response[@"data"];
            }else if ([urlName isEqualToString: UBMGetEventMoneyText]){
                weakself.eventMoneyArr = response[@"data"];
            }
            
            
            if (result) {
                result(response[@"data"]);
            }
            
        }else{
            
            if (error) {
                error();
            }
            
        }
        
    } error:^(id response) {
        
        if (error) {
            error();
        }
        
    } isShowHUD:NO];
    
}

- (void)clearModel {
    self.GetReachRulesDic = nil;
    self.lastRouteInfor = nil;
    self.lunboArr = nil;
    self.kengweiInfor = nil;
    self.jindutiaoInfor = nil;
    self.eventMoneyArr = nil;
}


@end
