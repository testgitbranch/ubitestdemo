//
//  UBMUaGet.m
//  LoveCarChannel_iOS
//
//  Created by wzw on 2021/8/20.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

#import "UBMUaGet.h"

@implementation UBMUaGet

+(NSString*)getUAContent{
    
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *app_Version = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    NSString *ua = [NSString stringWithFormat:@" appfrom=hcz;appbrand=Apple hcz/%@", app_Version];
    
    return ua;
}

@end
