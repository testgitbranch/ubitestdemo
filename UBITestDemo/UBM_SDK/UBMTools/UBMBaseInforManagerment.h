//
//  UBMBaseInforManagerment.h
//  LoveCarChannel_iOS
//
//  Created by 马翔 on 2020/12/12.
//  Copyright © 2020 魏振伟. All rights reserved.
//

/*
  提供基础信息类
 */

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef enum : NSUInteger {
    ubmCallStateDialing,
    ubmCallStateIncoming,
    ubmCallStateConnected,
    ubmCallStateDisconnected,
    ubmCallNotingIsDone
} UBMHKCallState;

@interface UBMBaseInforManagerment : NSObject
/** 固定信息 */
@property (nonatomic, copy) NSString *model;          /** 设备名称 **/
@property (nonatomic, copy) NSString *osName;         /** 系统名称:  IOS */
@property (nonatomic, copy) NSString *osVersion;      /** 系统版本 */
@property (nonatomic, copy) NSString *sdkVersion;     /** sdk版本  */
@property (nonatomic, copy) NSString *manufacturer;   /** 设备制造商 */
@property (nonatomic, copy) NSString *appVersionCode; /** App的版本号 */
@property (nonatomic, copy) NSString *appVersionName; /** App的版本名 */
@property (nonatomic, copy) NSString *appProcessName; /** App的进程名 */
@property (nonatomic, copy) NSString *appBuildVersion;/** App的build版本号 */
//@property (nonatomic, copy) NSString *deviceMemory;   /** 设备的内存大小 */
//@property (nonatomic, copy) NSString *deviceDisk;     /** 设备的硬盘大小 */
/** 当前即时信息 */
@property (nonatomic, copy) NSString *gpsLevel;        /** gps信号强度 */
@property (nonatomic, copy) NSString *networkType;     /** 网络类型 */
@property (nonatomic, copy) NSString *networkLevel;    /** 网络信号强度 */
//@property (nonatomic, copy) NSString *availableDisk;   /** 当前可用的硬盘的大小 */
//@property (nonatomic, copy) NSString *availableMemory; /** 当前可用的内存的大小 */
@property (nonatomic, copy) NSString *batteryAvailable;/** 可用的电量 */
@property (atomic, assign) UBMHKCallState callCenterStatus; /**可使用kvo监听本状态的改变*/


+ (instancetype)sharedInstance;

/**
 *  获取App运行时，基本硬件环境参数
 *
 *  @return 多个参数拼接成的string，每项参数以回车(\n)做为结束符
 */
- (NSString *)getAppContext;

@end

NS_ASSUME_NONNULL_END
