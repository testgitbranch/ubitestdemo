//
//  UBMWCustomAlert.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/7/21.
//  Copyright © 2020 魏振伟. All rights reserved.
//

/*
 自定义弹出框
 没指定弹窗框样式的情况下，默认只用此样式
 */
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef enum : NSUInteger {
    ubmWCAleft,
    ubmWCAright,
    ubmWCAcenter,
} UBMContentAligint;

typedef void(^leftAction)(void);
typedef void(^rightAction)(void);

@interface UBMWCustomAlert : UIView

@property(nonatomic, strong)UIColor* leftBtonColor;
@property(nonatomic, strong)UIColor* rightBtonColor;
@property(nonatomic, assign)UBMContentAligint caAligint;

@property(atomic, copy)leftAction leftActionBlock;
@property(atomic, copy)rightAction rightActionBlock;

//只有一个按钮时，rightTitle传@”“，默认黄色； 回调lefAction
-(instancetype)initWithFrame:(CGRect)frame andTitle:(NSString*)title andContent:(NSString*)contend andleftBton:(NSString*)lefttitle andRightBton:(NSString*)rightTitle;


-(void)show;



@end

NS_ASSUME_NONNULL_END
