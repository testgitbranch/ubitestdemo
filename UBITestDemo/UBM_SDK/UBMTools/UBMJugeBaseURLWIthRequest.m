//
//  UBMJugeBaseURLWIthRequest.m
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/8/19.
//  Copyright © 2020 魏振伟. All rights reserved.
//

#import "UBMDomains.h"
#import "UBMJugeBaseURLWIthRequest.h"
#import "UBMMTools.h"
#import "UBMNetWorkTool.h"

@interface UBMEnviConfig ()

@property (nonatomic, assign, readwrite) UBMEnviType currentEnvi;


@end

@implementation UBMEnviConfig

+ (instancetype)shareEnvi {
    static id instance;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    
    return instance;
}

- (void)config:(UBMEnviType)envi {
    self.currentEnvi = envi;
    [UBMDomainName.shareDomain configDomainWithEnvi:envi];
}


@end

@interface UBMDomainName ()

@property (nonatomic, copy, readwrite) NSString * baseURL;
@property (nonatomic, copy, readwrite) NSString * hiveURL;
@property (nonatomic, copy, readwrite) NSString * repairURL;
@property (nonatomic, copy, readwrite) NSString * baiXingKeFuURL;
@property (nonatomic, copy, readwrite) NSString * payURL;
@property (nonatomic, copy, readwrite) NSString * actURL;
@property (nonatomic, copy, readwrite) NSString * ubmURL;
@property (nonatomic, copy, readwrite) NSString * athenaURL;
@property (nonatomic, copy, readwrite) NSString * ucURL;

@end

@implementation UBMDomainName

+ (instancetype)shareDomain {
    static id instance;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    
    return instance;
}

- (void)configDomainWithEnvi:(UBMEnviType)envi {
    
    switch (envi) {
        case UBMEnviTest:
        {
            self.baseURL = UBMTBaseURL;
            self.hiveURL = UBMTHiveBaseURL;
            self.repairURL = UBMTRepairBaseURL;
            self.baiXingKeFuURL = UBMTBaiXingKeFu;
            self.payURL = UBMTPayBaseURL;
            self.actURL = UBMTActBaseURL;
            self.ubmURL = UBMTUBMBaseURL;
            self.athenaURL = UBMTAthenaBaseURL;
            self.ucURL = UBMTUcBaseURL;
        }
            break;
        case UBMEnviQA:
        {
            self.baseURL = UBMQABaseURL;
            self.hiveURL = UBMQAHiveBaseURL;
            self.repairURL = UBMQARepairBaseURL;
            self.baiXingKeFuURL = UBMQABaiXingKeFu;
            self.payURL = UBMQAPayBaseURL;
            self.actURL = UBMQAActBaseURL;
            self.ubmURL = UBMQAUBMBaseURL;
            self.athenaURL = UBMQAAthenaBaseURL;
            self.ucURL = UBMQAUcBaseURL;
        }
            break;
        case UBMEnviSim:
        {
            self.baseURL = UBMSBaseURL;
            self.hiveURL = UBMSHiveBaseURL;
            self.repairURL = UBMSRepairBaseURL;
            self.baiXingKeFuURL = UBMSBaiXingKeFu;
            self.payURL = UBMSPayBaseURL;
            self.actURL = UBMSActBaseURL;
            self.ubmURL = UBMSUBMBaseURL;
            self.athenaURL = UBMSAthenaBaseURL;
            self.ucURL = UBMSUcBaseURL;
        }
            break;
        case UBMEnviProduct:
        {
            self.baseURL = UBMPBaseURL;
            self.hiveURL = UBMPHiveBaseURL;
            self.repairURL = UBMPRepairBaseURL;
            self.baiXingKeFuURL = UBMPBaiXingKeFu;
            self.payURL = UBMPPayBaseURL;
            self.actURL = UBMPActBaseURL;
            self.ubmURL = UBMPUBMBaseURL;
            self.athenaURL = UBMPAthenaBaseURL;
            self.ucURL = UBMPUcBaseURL;
        }
            break;
        default:
            self.baseURL = @"";
            self.hiveURL = @"";
            self.repairURL = @"";
            self.baiXingKeFuURL = @"";
            self.payURL = @"";
            self.actURL = @"";
            self.ubmURL = @"";
            self.athenaURL = @"";
            self.ucURL = @"";
            break;
    }
}


@end

@implementation UBMJugeBaseURLWIthRequest

+ (void)requestDomainNameEnviConfig:(void(^)(BOOL isSucc))completeBlock {
    
    NSString* timenow = [UBMMTools currentTimeStr];
    
    NSDictionary * params = @{
        @"t": timenow,
    };
    
    [UBMNetWorkTool.shareNetWorkTool requestWithBaseUrl:UBMDomainName.shareDomain.baseURL UrlString:@"apk/aichejia_ios" parameters:params method:@"GET" success:^(id response) {
        if (response) {
            NSDictionary* dic = response;
            NSString* version = dic[@"version"];
            //获取当前版本信息
            NSString* localVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
            //先比较本地版本与线上版本的区别
            NSInteger t = [UBMMTools compareVersion:localVersion to:version];
            
            if (t == 0) {
                [UBMEnviConfig.shareEnvi config:UBMEnviTest];
            } else {
                [UBMEnviConfig.shareEnvi config:UBMEnviTest];
            }
        } else {
            
        }
    } error:^(id response) {
        
    } isShowHUD:NO];
}

@end
