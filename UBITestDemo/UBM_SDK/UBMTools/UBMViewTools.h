//
//  UBMViewTools.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/7/15.
//  Copyright © 2020 魏振伟. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

#pragma mark - 添加圆角
typedef enum : NSUInteger {
    ubmPTop,
    ubmPBottom,
    ubmPLeft,
    ubmPRight,
    ubmPTopLeft,
    ubmPTopRight,
    ubmPBottomLeft,
    ubmPBottomRight,
} UBMBorderPosition;

@interface UBMViewTools : NSObject

+(void)addCornerToview:(CALayer*)layer andWidth:(float)width andPosition:(UBMBorderPosition)position;


//#pragma mark -  简化初始化lable
+(UILabel*)lableWithText:(id)tTitile font:(UIFont*)tFont  textColor:(UIColor*)tTextColor;


#pragma mark - 左文右图的button
+(UIButton *)buttonRimg:(NSString *)title Color:(UIColor *)co font:(int)fo Img:(NSString *)iname ImgSize:(CGSize)dsize;


#pragma mark - 上图下文的button
+(UIButton *)buttonBimgframe:(CGRect)rect title:(NSString *)title Color:(UIColor *)co font:(int)fo Img:(NSString *)iname ImgSize:(CGSize)dsize distance:(float)distance;


#pragma mark - 改变lab中某个字符串大小和颜色
//简单标记，不可叠加。
//+ (void)messageAction:(UILabel *)theLab changeString:(NSString *)change andAllColor:(UIColor *)allColor andMarkColor:(UIColor *)markColor andMarkFondSize:(float)fontSize;
//
////只可标记某一个字符串的颜色。
+ (void)markerLable:(UILabel *)theLab changeString:(NSString *)change andAllColor:(UIColor *)allColor andMarkColor:(UIColor *)markColor andMarkFondSize:(UIFont*)font;
//
////可多次叠加标记。（推荐）
+ (void)markerLableOverlay:(UILabel *)theLab ofString:(NSString *)change markColor:(UIColor *)markColor markFondSize:(UIFont*)font;

//可多次叠加标记。且文字上下居中对其
+ (void)markerLableOverlayCenter:(UILabel *)theLab ofString:(NSString *)change markColor:(UIColor *)markColor markFond:(UIFont*)font defultFontSize:(int)defultSize markFondSize:(int)markSize;

//#pragma mark - 设置行间距
+(void)setLabelSpace:(UILabel*)label withSpace:(CGFloat)space withFont:(UIFont*)fontL;


//#pragma mark - 设置行间距并返回计算之后的label的高度。宽度不变。
//+ (float)Line:(UILabel *)label andLineSpace:(CGFloat)line;
//
//
//#pragma mark - 通过宽度，自适应高度
+(float)getHeightLable:(UILabel*)wlab withWidth:(float)width;


//#pragma mark - 通过高度，计算宽度，(by fontSize)
//+ (CGFloat)getWidthWithText:(NSString *)text height:(CGFloat)height fontSize:(CGFloat)size;
//
//#pragma mark - 通过高度，计算宽度，(by font)支持字重
//+ (CGFloat)getWidthWithText:(NSString *)text height:(CGFloat)height font:(UIFont *)font;
//
//#pragma mark - 生成纯色图片
//+ (UIImage *)imageWithColor:(UIColor *)color;
//
//
//#pragma mark - 设置锚点并恢复位置
//+ (void)setAnchorPoint:(CGPoint)anchorPoint forView:(UIView *)view;
//
//
//#pragma mark - 加载HTML富文本
//+ (void)loadHtmlTOLabel:(UILabel*)lab andHtmlStr:(NSString*)htmlStr;
//
//
//#pragma mark - 添加内阴影
//+(void)addInShadow:(UIView*)viewx;
//
//
#pragma mark - 生成image
+(UIImage *)getmakeImageWithView:(UIView *)view andWithSize:(CGSize)size;

#pragma mark - 添加渐变色背景。direction：1：上-下 2 左-右
+(void)addGradualChangeBckColor:(UIColor*)startColor :(UIColor*)endColor :(int)direction to:(UIView*)view;

#pragma mark - 改变图片大小
+ (UIImage *)scaleToSize:(UIImage *)img size:(CGSize)newsize;

#pragma mark - 判断view是否全都显示在屏幕上
+ (BOOL)isDisplayedInSceenWithView:(UIView *)view;

@end

NS_ASSUME_NONNULL_END
