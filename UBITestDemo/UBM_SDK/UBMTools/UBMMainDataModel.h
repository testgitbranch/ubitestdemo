//
//  UBMMainDataModel.h
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/11/12.
//  Copyright © 2020 魏振伟. All rights reserved.
//

/*
 处理请求数据。中间层。就是为了临时存储一些用到的数据。
 可作为一个中转层，请求下来的数据做持久化，当没网时吐出数据。
 */

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UBMMainDataModel : NSObject

@property(nonatomic, strong)NSDictionary* weekInfor; //周数据。做了单独设置，因为要用它区分用户。
//@property(nonatomic, strong)NSArray* 

+(instancetype)shareUBMMainDataModel;

-(void)getDataWithBaseUrl:(NSString*)baseURl URLName:(NSString*)urlName andParameter:(NSDictionary*)param andResult:(void(^)(id response))result ansError:(void(^)(void))error;

- (void)clearModel;

@end

NS_ASSUME_NONNULL_END
