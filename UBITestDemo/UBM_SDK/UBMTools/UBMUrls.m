//
//  UBMUrls.m
//  LoveCarChannel_iOS
//
//  Created by 金鑫 on 2021/6/22.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

#import "UBMUrls.h"

#pragma mark - 前端页面地址

NSString * const UBMChannelURL  = @"channel/index.html#/";    //原APP网页      主页地址
NSString * const UBMHiveURL     = @"hive/index.html#/";       //大修车公众号    主页地址
NSString * const UBMRepairURL   = @"repair/index.html#/";     //小修车公众号    主页地址

NSString * const UBMVersionMangeURL  = @"apk/iosVersionMange.json";  //是否更新

NSString * const UBMPublicityListURL = @"mine/publicity";     //公示列表
NSString * const UBMPublicityURL  = @"mine/publicityDetail";  // 公示详情
NSString * const UBMFactoryUrl = @"allFactory";         //修理厂
NSString * const UBMHelpEventList  = @"mine/case";      //我的修车
NSString * const UBMPlanList   = @"mine/plan";            //我的计划列表
NSString * const UBMPlanInfor  = @"mine/planDetail";    //计划详情
NSString * const UBMOpenCity   = @"mine/openCity";      //开放城市
NSString * const UBMMsgUrl     = @"notify";             //消息
NSString * const UBMCpuGame    = @"signin/CupGameForApp";  //猜杯子游戏页面
NSString * const UBMMyShareDetail = @"mine/shareDetail";   //分摊明细
NSString * const UBMMyCaseDetail  = @"mine/myCaseDetail";  //事件详情
NSString * const UBMReportCase  = @"reportCase";          //服务保障说明
NSString * const UBMBankCard  = @"bankCard";   //银行卡页面

NSString * const UBMRedPacket  = @"act/driving/redpacket"; //ubm活动-优惠券页面
NSString * const UBMRank    = @"act/driving/rank";      //ubm活动-排名
NSString * const UBMPK      = @"act/driving/pk";        //ubm活动-pk赛
NSString * const UBMRules   = @"act/driving/rule";      //ubm活动-规则
NSString * const UBMAutoRecordSetting    = @"act/driving/setting"; //ubm自动记录设置

NSString * const UBMBaikeURl   = @"https://www.kancloud.cn/achejia/wiki_haochezhu/1576533";  //产品百科
NSString * const UBMGongyue = @"https://h5.haochezhu.club/hive/platform_convention.html";    //平台公约
NSString * const UBMZhangCheng = @"https://h5.haochezhu.club/hive/statutes.html";            //计划章程
NSString * const UBMPrivacyAgreement = @"https://h5.haochezhu.club/hive/privacy.html";       //隐私协议
NSString * const UBMUserAgreement = @"https://h5.haochezhu.club/hive/service_agreement.html";//用户协议
NSString * const UBMNonAccidentRescueAgreement = @"https://h5.haochezhu.club/channel/rescue.html";  //非事故道路救援服务细则
NSString * const UBMZbAuthorization = @"https://h5.haochezhu.club/hive/zb_deduction.html";       //《武汉众邦银行委托扣款授权书》

NSString * const UBMAgreement = @"channel/good_drive.html"; //共享修车计划协议地址
NSString * const UBMComplianceExam = @"contractor/index.html#/exam/hcz?type=1"; //合规答题（邀请好友）
NSString * const UBMComplianceExam2 = @"contractor/index.html#/exam/server?type=2"; //合规答题（提现）

NSString * const UBMFeedback = @"feedback/new"; //问题反馈
NSString * const UBMOpenAcount = @"zb/openAccount"; //开户流程

NSString * const UBMPlanJoinUrl = @"hive/index.html#/app/joinPlan";    //计划页面立即加入
NSString * const UBMInsurHomeUrl = @"hive/index.html#/insur";          //“服务”->保险页面
NSString * const UBMInsurMyClaims = @"hive/index.html#/insur/myClaims"; //'理赔记录'页面
NSString * const UBMInsurMyPolicy = @"hive/index.html#/insur/myPolicy"; //'我的保单'页面
NSString * const UBMPrizepointRule = @"channel/prize_point_rule.html";  //奖励分规则

NSString* const UBMBenefits = @"channel/index.html#/benefits"; //福利页地址

NSString* const UBMNewsCenter = @"mobile/newsCenter.html";         //新闻中心
NSString* const UBMServiceNetwork = @"mobile/serviceNetwork.html"; //服务网络

#pragma mark - 接口地址

NSString * const UBMQuestionDataURL = @"hive/importantQa.json"; //常见问题

NSString * const UBMGetMyGradeURL = @"Question/getMyGrade";     //我的战绩接口-勋章等级

NSString * const UBMGetUserRewardListURL = @"Question/getUserRewardList";   //每日问答轮播消息
NSString * const UBMGetDailyQuestioURL = @"Question/dailyQuestion";          //每日问答题目
NSString * const UBMCheckAnswerRightURL = @"Question/checkAnswerRight";     //答题
NSString * const UBMGetdoLotteryURL = @"Question/doLottery";                //问答 抽奖接口-是否中奖
NSString * const UBMGetofferRewardURL = @"Question/offerReward";            // 每日问答 抽奖-领奖

NSString * const UBMGetMySignURL = @"Reward/getMySign";       //获取签到信息
NSString * const UBMWeekSiginURL = @"Reward/getMyPeriodSign"; //获取7天的签到信息
NSString * const UBMDoSignURL    = @"Reward/doSign";          //签到
NSString * const UBMGetEggURL    = @"Question/golden_eggs";   //砸蛋领奖。  如果是第七天签到直接调这个接口领奖。然后出蛋。

NSString * const UBMGetUserinfor = @"api/v1/user/getUserInfo";      //获取用户信息
NSString * const UBMUserProfile  = @"ucv2/api/v1/user/profile";      //新的获取用户信息（2.9.0）
NSString * const UBMGetBankInfor = @"api/v1/user/getUserBankInfo";  //获取用户银行卡信息
NSString * const UBMNewMessage   = @"api/v1/user/getUnreadNum";     //是否有新消息
NSString * const UBMGetAliStatus   = @"api/v1/user/getalistatus";   //是否已获得支付宝授权

NSString * const UBMUpdateUserDevice  =  @"ucv2/api/v1/user/device"; //更新用户极光ID

NSString * const UBMRewardType   = @"api/v1/reward/type";    //奖励类型
//NSString * const RewardList   = @"api/v1/reward/list";    //奖励列表
//NSString * const RewardAcount = @"api/v1/reward/account"; //奖励账户金额
//NSString * const RewardWithdraw   = @"api/v1/activityWithdraw/pay";  //活动奖励提现
//NSString * const RewardGetSign = @"api/v1/activityWithdraw/getSign"; //获取支付宝授权参数URL
//
//NSString * const SignAcount   = @"channel/sign/account";  //获取用户账户签约状态
//NSString * const GetSignPathURL  = @"channel/sign/sign";  //获取签约链接
//NSString * const MoneyTixian = @"channel/withdraw/pay";   //通过签约账户提现
//
//NSString * const IntegralType    = @"api/v1/integral/type";     //积分类型
//NSString * const IntegralAcount  = @"api/v1/integral/account";  //总积分
//NSString * const IntegralList    = @"api/v1/integral/list";     //积分列表
//
//NSString * const GetShareList = @"api/v1/Cooperation/getShareList";   //获取分摊列表

NSString * const UBMGetCarList   = @"api/v2/Vehicle/getVehicleList"; //获取车辆列表信息 2.5.0新(融合了计划和保险车辆) //= @"api/v1/Vehicle/getVehicleList";
NSString * const UBMOldCarData   = @"api/v1/Vehicle/getVehicleData"; //以前的获取车辆列表信息2.0.3之前
//NSString * const GetCarInfor  = @"api/v2/Vehicle/getVehicleInfo"; // 获取车辆信息 2.0.8更换 // = @"api/v1/Vehicle/getVehicleInfo"; // 获取车辆信息
//NSString * const MgaGetCardInfo = @"mga-api/api/v1/home/policy/getCardInfo"; //获取保险车辆列表接口

NSString * const UBMPlanJoin = @"api/v2/Banner/showOpenRegisterBanner"; //获取是否显示立即加入banner


//计划页接口
NSString * const UBMGongShiInfor  = @"api/v1/Cooperation/getPublicInfo";  //首页公示信息
//NSString * const eventState    = @"api/v1/Cooperation/getMutualMoney"; //车辆维修状态
NSString * const UBMReloadPlan    = @"Plan/reloadPlan";                  //恢复计划

//NSString * const KongKimUrl    = @"api/v1/home/getKingKongData";         //获取金刚位
NSString * const UBMKengWeiInfor  = @"Media/getMediaAll";       //获取坑位信息
//NSString * const TapKengWeiEvent = @"Media/clickStatistics"; //点击坑位统计接口
//NSString * const TimeStatistics  = @"DataCollection.php";    //统计时长
//NSString * const HomeOperating  = @"Media/getHomePopup";    //运营弹框数据
//
//NSString * const PingZhengInfor = @"api/v1/Cooperation/getMutualPlanElectVou"; //获取共享修车凭证信息
//NSString * const PingZhengImg  = @"api/v1/Cooperation/getElectVouResImg";      //获取共享修车凭证图片
//
//NSString * const GetShareImage = @"channel/User/getShareImage";  //邀请图片

NSString * const UBMPhoneBookReport = @"api/v1/phoneBook/report";        //保存成功，上传接口
NSString * const UBMPhoneBookdata   = @"api/v1/phoneBook/checkStatus";  //保存手机号到通讯录

NSString * const UBMDianZan = @"ladder/api/media/praise";     //点赞

//NSString * const GetExaminationList = @"Examination/getExaminationList"; //获取合规信息接口

//ubm
NSString * const UBMGetTripId = @"trip/getTripId"; //获取行程id
NSString * const UBMUpLoadPB  = @"trip/upload";    //上传pb文件
NSString * const UBMTheTripFileList = @"trip/getTripFileList"; //请求所有本行程的pb文件名和md5，同本地做验证。
NSString * const UBMTripInofr = @"trip/getTripDetail";  //详情接口
NSString * const UBMTripList  = @"trip/getTripList";    //行程列表
NSString * const UBMTripStatistics = @"history/tripStatistics"; //日月周统计
NSString * const UBMUpdataStartAndEndPoi = @"trip/updateStartAndEndPoi"; //上报逆地理编码结果
NSString * const UBMTripMerge = @"trip/tripMerge"; //行程结束通知
//NSString * const WeekTrip = @"history/getWeekTripData";  //周数据统计，ubm首页用
NSString * const UBMGetUnFinishTripList = @"trip/getUnFinishTripList"; //获取未完结的行程列表接口
NSString * const UBMTripMark = @"trip/mark";//非驾驶行为打标
//NSString * const Carbon = @"channel/index.html#/act/carbon";//碳中和

NSString * const UBMOssPolicy = @"oss/ossPolicy";  //获取oss上传凭证
NSString * const UBMUploadNotify = @"trip/uploadNotify"; //上传完成通知接口
//NSString * const WeeklyDetail = @"event/week/weekDetail"; //周报详情
//NSString * const WeeklyHistoryList = @"event/week/weekList"; //周报历史列表
//NSString * const TripHistory = @"history/getStatistics";   //行程历史信息
//NSString * const SafeDays = @"garage-api/capi/v1/mutual/aids/getAidsByUid";   //安全行驶天数

//ubm活动接口
NSString * const UBMGetEventUserInfo = @"event/home/getEventUserInfo";  //ubm用户入口信息
NSString * const UBMGetReachRules = @"event/home/getReachRules"; //获取达标规则信息接口
NSString * const UBMJionPlan = @"event/home/joinPlan"; //开启计划回调接口
NSString * const UBMGuideBeginner = @"event/home/guideBeginner"; //新手指引完成回调接口
NSString * const UBMDeclarationShowed = @"event/home/declarationShowed"; //展示宣言书回调接口
NSString * const UBMGetThemeCarouselText = @"event/home/getThemeCarouselText"; //获取主题区轮播文案接口
NSString * const UBMGetEventCoordinateConfig = @"event/home/getEventCoordinateConfig";  //获取坐标轴活动配置信息接口
NSString * const UBMGetEventPopUpInfo = @"event/home/getEventPopUpInfo"; //获取弹窗信息接口
NSString * const UBMWithdrawEventReward = @"event/home/withdrawEventReward"; //红色主题现金活动提现接口
NSString * const UBMGetEventMoneyText = @"event/home/getEventMoneyText"; //机器人轮播

//获取自动记录奖励金接口
NSString * const UBMGetRewardPopup = @"ladder/api/task/getRewardPopup";

//登录/注册
NSString * const UBMWXAuthLogin = @"channel/auth/wechat";  //微信授权登录
NSString * const UBMWxBindPhone = @"channel/auth/bind";    //微信与手机号绑定
NSString * const UBMPhoneLogin = @"channel/auth/login";    //手机号登录
NSString * const UBMGetPhoneSMS = @"channel/auth/sendcode";    //获取手机验证码
NSString * const UBMReviewLogin = @"apk/review.json";
NSString * const UBMUserLogout = @"auth/logout";    //用户退出登录

//首页接口
NSString * const UBMMedialInfo = @"ladder/api/media/getMediaInfo";    //获取某个坑位信息
NSString * const UBMHomeRecommend = @"ladder/api/user/getMediaRecommend";    //首页精选推荐
NSString * const UBMShowOpenAcount = @"rtc/user/isPopup";    //是否展示开户弹窗
NSString * const UBMUnclaimedBenefits = @"ladder/api/task/getUnclaimedBenefits";    //未领取的福利
NSString * const UBMCardInfo = @"event/eventInfo/getUbmCardInfo";    //Ubm卡片
NSString * const UBMVehicleInfo = @"User/getVehicleByVid";    //车辆信息
NSString * const UBMDynamicNews = @"ladder/api/media/getMediaAllByPitkeySortListHasPage";  //新闻
//NSString * const NetServe = @"api/media/getMediaAllByPitkey"; // 网络服务
NSString * const UBMNewsCategory = @"ladder/api/media/PitCatList";  //新闻分类接口
NSString * const UBMMediaAllByPitkeySortList = @"ladder/api/media/getMediaAllByPitkeySortListHasPage";  //瀑布流list接口

//坑位接口
NSString * const UBMCloseFloatPit = @"ladder/api/media/closeMedia";    //坑位浮窗关闭接口

//双录
//NSString * const ShuangluUsersig = @"qcloud/userSig";    //获取客户端签名(TRTC)
//NSString * const ShuangluCos = @"qcloud/tempKeys";    //获取客户端临时密钥(cos)
//NSString * const ShuangluRoomid = @"rtc/user/askForIn";    //申请进入
//NSString * const ShuangluLiveTesting = @"rtc/user/faceid";    //活体检测
//NSString * const ShuangluUpdateStatus = @"rtc/user/report";    //进入房间状态上报
//
////配置接口
//NSString * const AppConfigSwitch = @"appConfig/switch";    //获取客户端配置信息接口
//NSString * const TabbarData = @"api/v2/bar/GetBarListByKey";    //获取客户端配置信息接口
//
//// 新坑位
//NSString * const AllPits = @"ladder/api/media/getMediaAllByPitkey";    // 新坑位接口
