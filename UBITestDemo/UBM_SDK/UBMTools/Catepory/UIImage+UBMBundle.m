//
//  UIImage+UBMBundle.m
//  UBM_SDK
//
//  Created by 金鑫 on 2021/12/8.
//

#import "UIImage+UBMBundle.h"
#import "UBMMTools.h"

@implementation UIImage (UBMBundle)

+ (UIImage *)imageFromUbiBundleWithName:(NSString *)imageName {
    return [UIImage imageNamed:imageName inBundle:[UBMMTools ubmBundle] compatibleWithTraitCollection:nil];
}

@end
