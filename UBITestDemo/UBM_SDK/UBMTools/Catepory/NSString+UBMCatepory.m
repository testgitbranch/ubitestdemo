//
//  NSString+UBMCatepory.m
//  LoveCarChannel_iOS
//
//  Created by 马翔 on 2020/12/10.
//  Copyright © 2020 魏振伟. All rights reserved.
//

#import "NSString+UBMCatepory.h"

@implementation NSString (UBMCatepory)

+ (NSString *)dateFormatStringWithTimestamp:(NSString *)FormatStr timestamp:(uint64_t)timestamp
{
    NSDate *date = [[NSDate alloc]initWithTimeIntervalSince1970:((double)timestamp) / 1000];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:FormatStr];
    NSString *TimeString = [dateFormatter stringFromDate:date];
    return TimeString;
}

+ (NSString *)getFileSizeString:(CGFloat)size
{
    if (size > 1024*1024*1024)
    {
        return [NSString stringWithFormat:@"%.1fGB",size/1024/1024/1024]; //大于1G，则转化成G单位的字符串
    }
    else if(size < 1024*1024*1024 && size >= 1024*1024) //大于1M，则转化成M单位的字符串
    {
        return [NSString stringWithFormat:@"%.1fMB",size/1024/1024];
    }
    else if(size >= 1024 && size < 1024*1024) //不到1M,但是超过了1KB，则转化成KB单位
    {
        return [NSString stringWithFormat:@"%.1fKB",size/1024];
    }
    else //剩下的都是小于1K的，则转化成B单位
    {
        return [NSString stringWithFormat:@"%.1fB",size];
    }
}
@end
