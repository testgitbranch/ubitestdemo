//
//  UIImage+UBMBundle.h
//  UBM_SDK
//
//  Created by 金鑫 on 2021/12/8.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImage (UBMBundle)

+ (UIImage *)imageFromUbiBundleWithName:(NSString *)imageName;

@end

NS_ASSUME_NONNULL_END
