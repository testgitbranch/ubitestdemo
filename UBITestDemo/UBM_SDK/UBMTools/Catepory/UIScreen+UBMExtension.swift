//
//  UIScreen+Extension.swift
//  LoveCarChannel_iOS
//
//  Created by 金鑫 on 2021/4/19.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

import Foundation

extension UIScreen {
    
    static var ubmWidth : CGFloat  {
        return UIScreen.main.bounds.size.width
    }
    
    static var ubmHeight : CGFloat  {
        return UIScreen.main.bounds.size.height
    }
    
    static var ubmSize : CGSize  {
        return UIScreen.main.bounds.size
    }
    
    var ubmIsFullScreen: Bool {
        return bounds.height / bounds.width > 2
    }
}
