//
//  UIColor+Extension.swift
//  LoveCarChannel_iOS
//
//  Created by 金鑫 on 2021/4/21.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

import Foundation

@objc public extension UIColor {
    
    @objc class func ubmColorWith(r: CGFloat, g: CGFloat, b: CGFloat, a: CGFloat) -> UIColor {
        return UIColor(red: r / 255.0, green: g / 255.0, blue: b / 255.0, alpha: a)
    }
//    convenience init(r: CGFloat, g: CGFloat, b: CGFloat, a: CGFloat = 1) {
//        self.init(red: r / 255.0, green: g / 255.0, blue: b / 255.0, alpha: a)
//    }
    
    /// 16进制颜色
    @objc class func ubmColorWith(hex: Int, alpha: CGFloat = 1) -> UIColor {
        let r = CGFloat((hex & 0xFF0000) >> 16) / 255
        let g = CGFloat((hex & 0xFF00) >> 8) / 255
        let b = CGFloat(hex & 0xFF) / 255
        return UIColor(red: r, green: g, blue: b, alpha: alpha)
    }
    
    /// 16进制颜色
//    convenience init(hex: Int, alpha: CGFloat = 1) {
//        let red = CGFloat((hex & 0xFF0000) >> 16) / 255
//        let green = CGFloat((hex & 0xFF00) >> 8) / 255
//        let blue = CGFloat(hex & 0xFF) / 255
//        self.init(red: red, green: green, blue: blue, alpha: alpha)
//    }
    
//    @objc class func rgba(_ r:CGFloat, _ g:CGFloat, _ b:CGFloat, _ a:CGFloat) -> UIColor {
//        return UIColor(red: r/255, green: g/255, blue: b/255, alpha: a)
//    }
    
}
    // for oc
//    @objc class func hex(_ color: Int) -> UIColor {
//        return UIColor.init(hex: color, alpha: 1)
//    }
    
    /// 灰度值
//    static func rgbColor(rgb: CGFloat) -> UIColor {
//        return UIColor(r: rgb, g: rgb, b: rgb)
//    }
//
//    /// 随机颜色
//    static var random: UIColor {
//        let hue = CGFloat(arc4random() % 256) / 256 // use 256 to get full range from 0.0 to 1.0
//        let saturation = CGFloat(arc4random() % 128) / 256 + 0.5 // from 0.5 to 1.0 to stay away from white
//        let brightness = CGFloat(arc4random() % 128) / 256 + 0.5 // from 0.5 to 1.0 to stay away from black
//
//        return UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: 1)
//    }
//
//    /**
//     *  将自身变化到某个目标颜色，可通过参数progress控制变化的程度，最终得到一个纯色
//     *  @param toColor 目标颜色
//     *  @param progress 变化程度，取值范围0.0f~1.0f
//     */
//    func transition(to color: UIColor, progress: CGFloat) -> UIColor {
//        return UIColor.color(from: self, to: color, progress: progress)
//    }
//
//    /**
//     *  将颜色A变化到颜色B，可通过progress控制变化的程度
//     *  @param fromColor 起始颜色
//     *  @param toColor 目标颜色
//     *  @param progress 变化程度，取值范围0.0f~1.0f
//     */
//    static func color(from fromColor: UIColor, to toColor: UIColor, progress: CGFloat) -> UIColor {
//        let progress = min(progress, 1.0)
//        let fromRed = fromColor.redValue
//        let fromGreen = fromColor.greenValue
//        let fromBlue = fromColor.blueValue
//        let fromAlpha = fromColor.alphaValue
//
//        let toRed = toColor.redValue
//        let toGreen = toColor.greenValue
//        let toBlue = toColor.blueValue
//        let toAlpha = toColor.alphaValue
//
//        let finalRed = fromRed + (toRed - fromRed) * progress
//        let finalGreen = fromGreen + (toGreen - fromGreen) * progress
//        let finalBlue = fromBlue + (toBlue - fromBlue) * progress
//        let finalAlpha = fromAlpha + (toAlpha - fromAlpha) * progress
//
//        return UIColor(red: finalRed, green: finalGreen, blue: finalBlue, alpha: finalAlpha)
//    }
//
//    /**
//     *  获取当前UIColor对象里的红色色值
//     *
//     *  @return 红色通道的色值，值范围为0.0-1.0
//     */
//    var redValue: CGFloat {
//        var r: CGFloat = 0
//        if getRed(&r, green: nil, blue: nil, alpha: nil) {
//            return r
//        }
//        return 0
//    }
//
//    /**
//     *  获取当前UIColor对象里的绿色色值
//     *
//     *  @return 绿色通道的色值，值范围为0.0-1.0
//     */
//    var greenValue: CGFloat {
//        var g: CGFloat = 0
//        if getRed(nil, green: &g, blue: nil, alpha: nil) {
//            return g
//        }
//        return 0
//    }
//
//    /**
//     *  获取当前UIColor对象里的蓝色色值
//     *
//     *  @return 蓝色通道的色值，值范围为0.0-1.0
//     */
//    var blueValue: CGFloat {
//        var b: CGFloat = 0
//        if getRed(nil, green: nil, blue: &b, alpha: nil) {
//            return b
//        }
//        return 0
//    }
//
//    /**
//     *  获取当前UIColor对象里的透明色值
//     *
//     *  @return 透明通道的色值，值范围为0.0-1.0
//     */
//    var alphaValue: CGFloat {
//        var a: CGFloat = 0
//        if getRed(nil, green: nil, blue: nil, alpha: &a) {
//            return a
//        }
//        return 0
//    }
//
//    /// 原始颜色到目标颜色的平滑渐变过程
//    /// - Parameters:
//    ///   - progress: 渐变进度(0.0 ~ 1.0)
//    ///   - startColor: 开始的颜色
//    ///   - endColor: 结束的颜色
//    /// - Returns: 色值(如0xF5F5F5)
//    static func colorProgress(progress: CGFloat, startColor: Int, endColor: Int) -> Int {
//        var startR = CGFloat((startColor >> 16) & 0xff) / 255.0
//        var startG = CGFloat((startColor >> 8) & 0xff) / 255.0
//        var startB = CGFloat(startColor & 0xff) / 255.0
//
//        var endR = CGFloat((endColor >> 16) & 0xff) / 255.0
//        var endG = CGFloat((endColor >> 8) & 0xff) / 255.0
//        var endB = CGFloat(endColor & 0xff) / 255.0
//
//        startR = pow(startR, 2.2)
//        startG = pow(startG, 2.2)
//        startB = pow(startB, 2.2)
//
//        endR = pow(endR, 2.2)
//        endG = pow(endG, 2.2)
//        endB = pow(endB, 2.2)
//
//        var r = startR + progress * (endR - startR)
//        var g = startG + progress * (endG - startG)
//        var b = startB + progress * (endB - startB)
//
//        r = pow(r, 1.0/2.2) * 255.0
//        g = pow(g, 1.0/2.2) * 255.0
//        b = pow(b, 1.0/2.2) * 255.0
//
//        return Int(round(r)) << 16 | Int(round(g)) << 8 | Int(round(b))
//    }
//
//}
//
//
//public extension UIColor {
//
//    /// Create a ligher color
//    func lighter(byPercentage percentage: CGFloat = 10) -> UIColor {
//        return self.adjustBrightness(byPercentage: abs(percentage))
//    }
//
//    /// Create a darker color
//    func darker(byPercentage percentage: CGFloat = 10) -> UIColor {
//        return self.adjustBrightness(byPercentage: -abs(percentage))
//    }
//
//    /// Try to increase brightness or decrease beightness
//    func adjustBrightness(byPercentage percentage: CGFloat = 10) -> UIColor {
//        var h: CGFloat = 0, s: CGFloat = 0, b: CGFloat = 0, a: CGFloat = 0
//        if self.getHue(&h, saturation: &s, brightness: &b, alpha: &a) {
//            let newB: CGFloat = max(0, min(1.0, b + (percentage / 100.0) * b))
//            return UIColor(hue: h, saturation: s, brightness: newB, alpha: a)
//        }
//        return self
//    }
//
//    /// Create a brightness color
//    func colorWithBrightness(_ brightness: CGFloat) -> UIColor {
//        guard brightness >= 0, brightness <= 100 else { return self }
//        var h: CGFloat = 0, s: CGFloat = 0, b: CGFloat = 0, a: CGFloat = 0
//        if self.getHue(&h, saturation: &s, brightness: &b, alpha: &a) {
//            let newB = brightness / 100
//            return UIColor(hue: h, saturation: s, brightness: newB, alpha: a)
//        }
//        return self
//    }
//}
