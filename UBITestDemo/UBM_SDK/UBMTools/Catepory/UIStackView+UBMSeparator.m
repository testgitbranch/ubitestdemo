//
//  UIStackView+UBMSeparator.m
//  LoveCarChannel_iOS
//
//  Created by 金鑫 on 2021/3/9.
//  Copyright © 2021 魏振伟. All rights reserved.
//

#import "UIStackView+UBMSeparator.h"
#import <objc/runtime.h>

@interface UBMUIStackViewSeparatorHelper : NSObject
@property (nonatomic, strong) UIColor *separatorColor;
@property (nonatomic, assign) CGFloat separatorLength;
@property (nonatomic, assign) CGFloat separatorThickness;
@property (nonatomic, weak) UIStackView *stackView;
@property (strong, nonatomic) NSMutableArray<UIView *> *separatorViews;
- (void)makeSeparators;
@end

@implementation UBMUIStackViewSeparatorHelper
- (instancetype)init {
    if (self = [super init]) {
        self.separatorThickness = 1;
        self.separatorViews = [NSMutableArray array];
    }
    return self;
}

- (void)makeSeparators {
    UIStackView * __strong strongStackView = self.stackView;
    
    if (!strongStackView || !self.separatorColor || (self.separatorLength <= 0)) {
        return;
    }
    //从Superview当中移除
    [self.separatorViews enumerateObjectsUsingBlock:^(UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj removeFromSuperview];
    }];
    //移除所有的元素
    [self.separatorViews removeAllObjects];
    
    [strongStackView.arrangedSubviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (idx == 0) {
            return;
        }
        //idx == 1  取出第0个。(取上一个)
        UIView *previousView = strongStackView.arrangedSubviews[idx - 1];
        //创建了一个分割线
        UIView *separatorView = [[UIView alloc] init];
        //设置背景颜色
        separatorView.backgroundColor = self.separatorColor;
        
        CGFloat center;
        //如果排布的方式是水平的时候
        if (strongStackView.axis == UILayoutConstraintAxisHorizontal) {
            center = (CGRectGetMaxX(previousView.frame) + CGRectGetMinX(obj.frame)) / 2.0;
            
            separatorView.frame = CGRectMake(center, (CGRectGetHeight(strongStackView.frame) - self.separatorLength) / 2.0, self.separatorThickness, self.separatorLength);
        }
        else {
            center = (CGRectGetMaxY(previousView.frame) + CGRectGetMinY(obj.frame)) / 2.0;
            separatorView.frame = CGRectMake((CGRectGetWidth(strongStackView.frame) - self.separatorLength) / 2.0, center, self.separatorLength, self.separatorThickness);
        }
        //这里使用addSubview: 而不能使用arrangedSubviews，这样就能够避免系统的布局，而由自己来布局separater
        
        [strongStackView addSubview:separatorView];
        //将separatorView添加到容器数组当中
        [self.separatorViews addObject:separatorView];
    }];
}

@end

@implementation UIStackView (UBMSeparator)
static const char kHelperKey;
+ (void)load {
    //    Method m1 = class_getInstanceMethod([self class], @selector(layoutSubviews));
    //    Method m2 = class_getInstanceMethod([self class], @selector(bx_layoutSubviews));
    //    method_exchangeImplementations(m1, m2);
    
    Class cls = [self class];
    
    Method oriMethod = class_getInstanceMethod(cls, @selector(layoutSubviews));
    Method newMethod = class_getInstanceMethod(cls, @selector(bx_layoutSubviews));
    //v@: method_getTypeEncoding
    BOOL didAddMethod = class_addMethod([self class], @selector(layoutSubviews), method_getImplementation(newMethod), "v@:");
    if (didAddMethod) {
        class_replaceMethod([self class], @selector(bx_layoutSubviews), method_getImplementation(oriMethod), "v@:");
        
    }else{
        method_exchangeImplementations(oriMethod, newMethod);
    }
}

#pragma mark - setter方法 和 getter方法
/**
 * 这里的属性因为分类无法存储的缘故，所以采用了转移到其他类，我们只要拥有这个其他类，就能够获取到他的属性
 * 当触发set方法的时候，不仅要触发出传入值得存储，同时要创建和布局分割线
 */
- (void)setSeparatorColor:(UIColor *)separatorColor {
    [self stackViewSeparatorHelper].separatorColor = separatorColor;
    [[self stackViewSeparatorHelper] makeSeparators];
}

- (void)setSeparatorLength:(CGFloat)separatorLength {
    [self stackViewSeparatorHelper].separatorLength = separatorLength;
    [[self stackViewSeparatorHelper] makeSeparators];
}

- (void)setSeparatorThickness:(CGFloat)separatorThickness {
    [self stackViewSeparatorHelper].separatorThickness = separatorThickness;
    [[self stackViewSeparatorHelper] makeSeparators];
}

- (CGFloat)separatorThickness {
    return  [self stackViewSeparatorHelper].separatorThickness;
}

- (UIColor *)separatorColor {
    return  [self stackViewSeparatorHelper].separatorColor;
}

- (CGFloat)separatorLength {
    return  [self stackViewSeparatorHelper].separatorLength;
}

- (UBMUIStackViewSeparatorHelper *)stackViewSeparatorHelper {
    UBMUIStackViewSeparatorHelper *stackViewSeparatorHelper = objc_getAssociatedObject(self, &kHelperKey);
    //在这个get方法当中，如果没有get到，那么就触发set的操作
    if (!stackViewSeparatorHelper) {
        UBMUIStackViewSeparatorHelper * helper = [[UBMUIStackViewSeparatorHelper alloc] init];
        helper.stackView = self;
        objc_setAssociatedObject(self, &kHelperKey, helper, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        stackViewSeparatorHelper = helper;
    }
    return stackViewSeparatorHelper;
}

/**
 * 在布局stackView的时候，就需要同时布局分割线，所以，必须要监听到layoutSubViews方法
 * 比如说，UIStackView添加了子控件（通过arrangedSubviews添加），那么就会调用这个方法来布局，这个时候，就需要重新布局separator
 * UIStackView 只会负责 arrangedSubviews 的布局，也就是说我们利用这个特性来将分割线直接加到StackView上而不受其布局，只要我们不使用 addArrangedSubView即可。
 */
- (void)bx_layoutSubviews {
    //先让UIStackView的arrangedSubviews完成布局
    [self bx_layoutSubviews];
    //布局separater
    [[self stackViewSeparatorHelper] makeSeparators];
}

@end
