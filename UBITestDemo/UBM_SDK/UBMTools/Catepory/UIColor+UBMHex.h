//
//  UIColor+UBMHex.h
//  LoveCarChannel_iOS
//
//  Created by 金鑫 on 2020/12/14.
//  Copyright © 2020 魏振伟. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIColor (UBMHex)
+ (UIColor *)ubmColorWithHexString: (NSString *)color;
+ (UIColor*)ubmColorWithHexNum:(long)hexColor;
+ (UIColor *)ubmColorWithHexNum:(long)hexColor alpha:(float)opacity;

@end

NS_ASSUME_NONNULL_END
