//
//  NSString+BXUbmCatepory.h
//  LoveCarChannel_iOS
//
//  Created by 马翔 on 2020/12/10.
//  Copyright © 2020 魏振伟. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIkit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (UBMCatepory)
+ (NSString *)dateFormatStringWithTimestamp:(NSString *)FormatStr timestamp:(uint64_t)timestamp;
+ (NSString *)getFileSizeString:(CGFloat)size;
@end

NS_ASSUME_NONNULL_END
