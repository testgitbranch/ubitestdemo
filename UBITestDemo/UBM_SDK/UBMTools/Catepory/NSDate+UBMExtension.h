//
//  NSDate+UBMExtension.h
//  LoveCarChannel_iOS
//
//  Created by 金鑫 on 2021/1/29.
//  Copyright © 2021 魏振伟. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSDate (UBMExtension)
/// NSDate转化成本地日期时间
- (NSDate *)localDate;

/// 格式化的时间戳字符串转换NSDate
/// @param timestamp 格式化的时间字符串
/// @param format 当前时间字符串的格式
+ (NSDate *)dateFromTimestamp:(NSString *)timestamp withFormat:(NSString *)format;

/// NSDate转换格式化时间字符串
/// @param format 时间格式
- (NSString *)toTimestampWithFormat:(NSString *)format;

@end

NS_ASSUME_NONNULL_END
