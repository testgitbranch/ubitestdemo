//
//  NSDate+UBMExtension.m
//  LoveCarChannel_iOS
//
//  Created by 金鑫 on 2021/1/29.
//  Copyright © 2021 魏振伟. All rights reserved.
//

#import "NSDate+UBMExtension.h"

@implementation NSDate (UBMExtension)

/// NSDate转化成本地日期时间
- (NSDate *)localDate {
    //获得系统时区
    NSTimeZone *zone = [NSTimeZone systemTimeZone];
    //得到源日期与世界标准时间的偏移量
    NSInteger interval = [zone secondsFromGMTForDate: self];
    //返回以当前NSDate对象为基准，偏移多少秒后得到的新NSDate对象
    NSDate *localeDate = [self dateByAddingTimeInterval: interval];
    
    return localeDate;
}

/// 格式化的时间戳字符串转换NSDate
+ (NSDate *)dateFromTimestamp:(NSString *)timestamp withFormat:(NSString *)format {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = format;
    
    return [formatter dateFromString:timestamp];
}

/// NSDate转换格式化时间字符串
- (NSString *)toTimestampWithFormat:(NSString *)format {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = format;
    
    return [formatter stringFromDate:self];
}

@end
