//
//  UIStackView+UBMSeparator.h
//  LoveCarChannel_iOS
//
//  Created by 金鑫 on 2021/3/9.
//  Copyright © 2021 魏振伟. All rights reserved.
//
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIStackView (UBMSeparator)
@property (nonatomic, strong) UIColor *separatorColor;
@property (nonatomic, assign) CGFloat separatorLength;
@property (nonatomic, assign) CGFloat separatorThickness;
@end

NS_ASSUME_NONNULL_END
