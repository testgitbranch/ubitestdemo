//
//  UBMNotifications.m
//  LoveCarChannel_iOS
//
//  Created by 金鑫 on 2021/6/21.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

#import "UBMNotifications.h"


NSString * const UBMNetNotification = @"UBMNetNotification";

NSString * const UBMAlertNextNotification = @"UBMAlertNextNotification";

NSString * const UBMNewTripOverNotification = @"UBMNewTripOverNotification";

NSString * const UBMActivityDidApperNotification = @"UBMActivityDidApperNotification";

NSString * const UBMUpdateComplianceStateNotification = @"UBMUpdateComplianceStateNotification";

NSString * const UBMTaskCenterUpdateCarInfoNotification = @"UBMTaskCenterUpdateCarInfoNotification";

NSString * const UBMSwitchVehicleNotification = @"UBMSwitchVehicleNotification";

NSString * const UBMGoToNewURLNotification = @"UBMGoToNewURLNotification";

NSString * const UBMListenShouldLoadURLNameNotification = @"UBMListenShouldLoadURLNameNotification";

NSString * const UBMBaseDataReadyNotification = @"UBMBaseDataReadyNotification";

NSString * const UBMUpdateAlertShowNotification = @"UBMUpdateAlertShowNotification";

NSString * const UBMGetUserInforSucceedNotification = @"UBMGetUserInforSucceedNotification";

NSString * const UBMLoginOutSucceed = @"UBMLoginOutSucceed";

NSString * const UBMChildchangeScrollStatusNotification = @"UBMChildchangeScrollStatusNotification";

NSString * const UBMFatherchangeScrollStatusNotification = @"UBMFatherchangeScrollStatusNotification";

NSString * const UBMChildToTapNotification = @"UBMChildToTapNotification";




