//
//  UBMGetLoctionInfor.m
//  EngineeProject
//
//  Created by Edog on 2017/6/9.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "UBMGetLoctionInfor.h"

@interface UBMGetLoctionInfor()<CLLocationManagerDelegate>

@property(nonatomic,strong)CLLocationManager *locMgr;

@property(nonatomic,strong)CLGeocoder *geocoder;

//用户经纬度
@property(nonatomic,strong)CLLocation* userLocation;

//用户所在位置名称
@property(nonatomic,copy)NSString* userLocationName;

@end

@implementation UBMGetLoctionInfor


-(void)getMyLoctionNow
{
    if(self.locMgr == nil)
    {
        self.locMgr = [[CLLocationManager alloc] init];
        self.locMgr.delegate = self;
        
        //使用时授权
        [self.locMgr requestWhenInUseAuthorization];
    }
    
    if(self.geocoder == nil)
    {
        _geocoder = [[CLGeocoder alloc]init];
    }
    
    
    if ([CLLocationManager locationServicesEnabled] == YES) {
        
        //开始定位用户的位置
        [self.locMgr startUpdatingLocation];
        //每隔多少米定位一次（这里的设置为任何的移动）
        self.locMgr.distanceFilter=kCLDistanceFilterNone;
        //设置定位的精准度，一般精准度越高，越耗电（这里设置为精准度最高的，适用于导航应用）
        self.locMgr.desiredAccuracy=kCLLocationAccuracyBestForNavigation;
    }
    else
    {  //不能定位用户的位置
        //1.提醒用户检查当前的网络状况
        //2.提醒用户打开定位开关
        
//        Toast(@"无法获取位置信息，请检查定位是否开启或者当前网络状况");
        !self.callBackBlock ?: self.callBackBlock(@"",nil);
        
    }
    
}

//此代理方法会在用户选择权限或者开始定位时调用。可以用来判断用户是否允许获取位置
-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if ([CLLocationManager locationServicesEnabled] == YES)
    {
        //无法授权定位或者拒绝定位
        if(status == kCLAuthorizationStatusDenied || status == kCLAuthorizationStatusRestricted)
        {
//            Toast(@"无法获取位置信息，请检查定位是否开启或者当前网络状况");
            !self.callBackBlock ?: self.callBackBlock(@"",nil);
        }
    }
        
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    CLLocation* loc = [locations lastObject];
    self.userLocation = loc;
    
    [self.locMgr stopUpdatingLocation];
    
    //[self.delegate getLocName:self.userLocationName andCLLocation:self.userLocation];

    //逆地理编码
    [self.geocoder reverseGeocodeLocation:self.userLocation completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        
        if(error || placemarks.count == 0)
        {
//            Toast(@"无法获取当前位置信息，请稍后再试");
            !self.callBackBlock ?: self.callBackBlock(@"",nil);
        }
        else
        {
            CLPlacemark *firstPlacemark=[placemarks firstObject];
            self.userLocationName = firstPlacemark.name;
            
            NSDictionary* adressDicx = [firstPlacemark addressDictionary];
            NSString* shi = [adressDicx objectForKey:@"City"];
            
            !self.callBackBlock ?: self.callBackBlock(shi,self.userLocation);
        }
        
    }];
    
}


@end
