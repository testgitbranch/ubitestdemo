//
//  UBMVendorTool.h
//  TestIdfvKeyChain
//
//  Created by 金鑫 on 2021/11/12.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UBMVendorTool : NSObject
+ (NSString *)getIDFV;
@end

NS_ASSUME_NONNULL_END
