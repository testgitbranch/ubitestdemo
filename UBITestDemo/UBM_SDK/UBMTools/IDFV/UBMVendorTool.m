//
//  UBMVendorTool.m
//  TestIdfvKeyChain
//
//  Created by 金鑫 on 2021/11/12.
//

#import "UBMVendorTool.h"
#import "UBMKeyChainTool.h"
#import <UIKit/UIKit.h>

@implementation UBMVendorTool

+ (NSString *)getIDFV {
    NSString *IDFV = (NSString *)[UBMKeyChainTool load:@"IDFV"];
    
    if ([IDFV isEqualToString:@""] || !IDFV) {
        
        IDFV = [UIDevice currentDevice].identifierForVendor.UUIDString;
        [UBMKeyChainTool save:@"IDFV" data:IDFV];
    }
    
    return IDFV;
}

@end
