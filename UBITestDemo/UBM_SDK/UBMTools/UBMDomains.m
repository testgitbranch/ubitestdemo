//
//  UBMDomains.m
//  LoveCarChannel_iOS
//
//  Created by 金鑫 on 2021/8/13.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

#import "UBMDomains.h"

///MARK: 测试环境
NSString * const UBMTBaseURL =          @"https://media.test.haochezhu.club/";
NSString * const UBMTHiveBaseURL =      @"https://media.test.haochezhu.club/";
NSString * const UBMTRepairBaseURL =    @"https://media.test.haochezhu.club/";
NSString * const UBMTBaiXingKeFu =      @"https://media.test.haochezhu.club/www/";

NSString * const UBMTPayBaseURL =       @"https://pay.test.haochezhu.club/";
NSString * const UBMTActBaseURL =       @"https://act.test.haochezhu.club/";
//NSString * const UBMTUBMBaseURL =       @"https://ubm.test.haochezhu.club/";
NSString * const UBMTUBMBaseURL =       @"https://driver-behavior-api.test.haochezhu.club/";
NSString * const UBMTAthenaBaseURL =    @"https://athena.test.haochezhu.club/";
NSString * const UBMTUcBaseURL  =       @"https://uc.test.haochezhu.club/";

///MARK: QA环境
NSString * const UBMQABaseURL =          @"https://media.uat.haochezhu.club/";  //APP前端
NSString * const UBMQAHiveBaseURL =      @"https://media.uat.haochezhu.club/";  //大互助公众号前端
NSString * const UBMQARepairBaseURL =    @"https://media.uat.haochezhu.club/";  //小修宝公众号前端
NSString * const UBMQABaiXingKeFu =      @"https://media.test.haochezhu.club/www/";

NSString * const UBMQAPayBaseURL =       @"https://pay.uat.haochezhu.club/";    //接口 pay
NSString * const UBMQAActBaseURL =       @"https://act.uat.haochezhu.club/";    //接口 act
NSString * const UBMQAUBMBaseURL =       @"https://ubm.uat.haochezhu.club/";    //ubm
NSString * const UBMQAAthenaBaseURL =    @"https://athena.uat.haochezhu.club/"; //athena
NSString * const UBMQAUcBaseURL =        @"https://uc.uat.haochezhu.club/";     //uc

///MARK: 仿真环境
NSString * const UBMSBaseURL =          @"https://h5.sim.haochezhu.club/";
NSString * const UBMSHiveBaseURL =      @"https://h5.sim.haochezhu.club/";
NSString * const UBMSRepairBaseURL =    @"https://h5.sim.haochezhu.club/";
NSString * const UBMSBaiXingKeFu =      @"https://h5.sim.haochezhu.club/www/";

NSString * const UBMSPayBaseURL =       @"https://pay.sim.haochezhu.club/";
NSString * const UBMSActBaseURL =       @"https://act.sim.haochezhu.club/";
//NSString * const UBMSUBMBaseURL =       @"https://ubm.sim.haochezhu.club/";
NSString * const UBMSUBMBaseURL = @"https://driver-behavior-api.sim.haochezhu.club/";
NSString * const UBMSAthenaBaseURL =    @"https://athena.sim.haochezhu.club/";
NSString * const UBMSUcBaseURL =        @"https://uc.sim.haochezhu.club/";

///MARK: 生产环境
NSString * const UBMPBaseURL =          @"https://h5.haochezhu.club/";
NSString * const UBMPHiveBaseURL =      @"https://h5.haochezhu.club/";
NSString * const UBMPRepairBaseURL =    @"https://h5.haochezhu.club/";
NSString * const UBMPBaiXingKeFu =      @"https://www.baixingkefu.cn/";

NSString * const UBMPPayBaseURL =       @"https://pay.haochezhu.club/";
NSString * const UBMPActBaseURL =       @"https://act.haochezhu.club/";
NSString * const UBMPUBMBaseURL =       @"https://ubm.haochezhu.club/";
NSString * const UBMPAthenaBaseURL =    @"https://athena.haochezhu.club/";
NSString * const UBMPUcBaseURL =        @"https://uc.haochezhu.club/";
