//
//  UBMFile.m
//  LoveCarChannel_iOS
//
//  Created by 马翔 on 2020/12/9.
//  Copyright © 2020 魏振伟. All rights reserved.
//

#import "UBMFile.h"
#import "UBMPath.h"
#import <UIKit/UIkit.h>

@implementation UBMFile

+ (NSString *_Nullable)ensureDirectoryInLibrary:(NSString * _Nonnull)subPath skipBackup:(BOOL)skip
{
    if (!subPath || subPath.length == 0) return nil;
    
    NSArray  *libPaths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *hPath    = [libPaths objectAtIndex:0];
    NSString *fullPath = [hPath stringByAppendingPathComponent:subPath];
    
    int r = [self ensureDirectory:fullPath];
    if (r != 0)
    {
        if (skip && r == 2)
        {
            [self addSkipBackupAttributeToItemAtURL:[NSURL fileURLWithPath:fullPath]];
        }
        return fullPath;
    }
    else {
        return nil;
    }
}

+ (NSString * _Nullable)ensureDirectoryInDocument:(NSString * _Nonnull)subPath skipBackup:(BOOL)skip
{
    if (!subPath || subPath.length == 0) return nil;
    
    NSArray  *homePaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *hPath = [homePaths objectAtIndex:0];
    NSString *fullPath = [hPath stringByAppendingPathComponent:subPath];
    
    int r = [self ensureDirectory:fullPath];
    if (r != 0)
    {
        if (skip && r == 2)
        {
            [self addSkipBackupAttributeToItemAtURL:[NSURL fileURLWithPath:fullPath]];
        }
        return fullPath;
    }
    else {
        return nil;
    }
}

+ (NSString * _Nullable)ensureDirectoryInCache:(NSString * _Nonnull)subPath
{
    if (!subPath || subPath.length == 0) return nil;
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *path = [paths objectAtIndex:0];
    NSString *fullPath = [path stringByAppendingPathComponent:subPath];
    if ([self ensureDirectory:fullPath])
    {
        return fullPath;
    }
    else {
        return nil;
    }
}

+(NSString * _Nullable)ensureDirectoryInRoot:(NSString*)path
{
    if (!path || path.length == 0) return nil;
    
    NSString* fullPath = [[UBMPath fullPathStorageRoot] stringByAppendingPathComponent:path];
    
    if ([self ensureDirectory:fullPath])
    {
        return fullPath;
    }
    else {
        return nil;
    }
}

/**
 *  确保某一个目录存在，如果不存在则尝试创建这个目录。
 *
 *  @param path 目录的全路径
 *
 *  @return 如果目录存在 1，如果创建成功返回 2，如果创建失败则返回 0。
 */
+(int)ensureDirectory:(NSString*)path
{
    if (!path) {
        return 0;
    }
    
    NSFileManager *fm = [[NSFileManager alloc] init];
    [fm changeCurrentDirectoryPath:[path stringByExpandingTildeInPath]];
    
    BOOL isDir   = NO;
    BOOL isExist = [fm fileExistsAtPath:path isDirectory:&isDir];
    
    if (!isExist)
    {
        BOOL success = [fm createDirectoryAtPath:path
                     withIntermediateDirectories:YES
                                      attributes:nil
                                           error:nil];
        
        return success ? 2 : 0;
    }
    return 1;
}

+ (BOOL)isExist:(NSString *)fullPath
{
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    return [fileManager fileExistsAtPath:fullPath];
}

+ (BOOL)writeFile:(NSString * _Nonnull)path text:(NSString * _Nonnull)text
{
    return [self writeFile:path data:[text dataUsingEncoding:NSUTF8StringEncoding]];
}

+ (BOOL)writeFile:(NSString *_Nonnull)path data:(NSData * _Nonnull)data
{
    BOOL success = NO;
    NSError *error = nil;
    @try
    {
        [self removeFile:path];
        
        if(![self createFile:path])
        {
            return NO;
        }
        
        success = [data writeToFile:path options:NSDataWritingAtomic error:&error];
        if (!error)
        {
            success = YES;
        }
    }
    @catch (NSException *exception)
    {
        success = NO;
    }
    
    return success;
}

+ (BOOL)writeFileAppend:(NSString *_Nonnull)path data:(NSData * _Nonnull)data
{
    if (!path || path.length == 0) return NO;
    
    NSFileHandle  *outFile = [NSFileHandle fileHandleForWritingAtPath:path];
    if(outFile == nil)
    {
        //@throw [NSException exceptionWithName:@"openFile"   reason:@"open file fail" userInfo:nil];
        return NO;
    }
    /**找到并定位到outFile的末尾位置(在此后追加文件) */
    [outFile seekToEndOfFile];
    
    /** 写入文件*/
    [outFile writeData:data];
    
    /** 关闭读写文件 */
    [outFile closeFile];
    return YES;
}

+ (BOOL)writeFileWithName:(NSString *_Nonnull)path
                 fileName:(NSString *_Nonnull)fileName
                     data:(NSData * _Nonnull)data
{
    if (!path || path.length == 0 || !fileName || fileName.length == 0)
    {
        return NO;
    }
    NSString *Path = [path stringByAppendingPathComponent:fileName];
    return [self writeFile:Path data:data];
}

+ (BOOL)writeFileWithName:(NSString *_Nonnull)path
                 fileName:(NSString *_Nonnull)fileName
                 fileType:(NSString *_Nonnull)fileType
                     data:(NSData * _Nonnull)data
{
    if (!path || path.length == 0
        || !fileName || fileName.length == 0
        || !fileType || fileType.length == 0)
    {
        return NO;
    }
    NSString *fileFullName = [NSString stringWithFormat:@"%@.%@", fileName, fileType];
    NSString *Path         = [path stringByAppendingPathComponent:fileFullName];
    return [self writeFile:Path data:data];
}

+ (BOOL)writeFileWithNameByAppend:(NSString *_Nonnull)path
                 fileName:(NSString *_Nonnull)fileName
                     data:(NSData * _Nonnull)data
                 isAppend:(BOOL)isAppend
{
    if (!path || path.length == 0 || !fileName || fileName.length == 0)
    {
        return NO;
    }
    NSString *Path = [path stringByAppendingPathComponent:fileName];
    
    if (isAppend == YES)
    {
        return [self writeFileAppend:Path data:data];
    }
    else {
        return [self writeFile:Path data:data];
    }
}

+ (BOOL)writeFileWithNameByAppend:(NSString *_Nonnull)path
                 fileName:(NSString *_Nonnull)fileName
                 fileType:(NSString *_Nonnull)fileType
                     data:(NSData * _Nonnull)data
                         isAppend:(BOOL)isAppend
{
    if (!path || path.length == 0
        || !fileName || fileName.length == 0
        || !fileType || fileType.length == 0)
    {
        return NO;
    }
    NSString *fileFullName = [NSString stringWithFormat:@"%@.%@", fileName, fileType];
    NSString *Path         = [path stringByAppendingPathComponent:fileFullName];
    
    if (isAppend == YES)
    {
        return [self writeFileAppend:Path data:data];
    }
    else {
        return [self writeFile:Path data:data];
    }
}

+ (BOOL)writeFileByAppend:(NSString *_Nonnull)path
                     data:(NSData * _Nonnull)data
                 isAppend:(BOOL)isAppend
{
    if (!path || path.length == 0) return NO;
    
    if (isAppend == YES)
    {
        return [self writeFileAppend:path data:data];
    }
    else {
        return [self writeFile:path data:data];
    }
}


+ (BOOL)removeFile:(NSString * _Nonnull)path
{
    BOOL success = NO;
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    if ([fileManager fileExistsAtPath:path])
    {
        [fileManager removeItemAtPath:path error:nil];
        success = true;
    }
    
    return success;
}

+ (BOOL)createFile:(NSString *_Nonnull)path
{
    return [self createFile:path contents:nil];
}

+ (BOOL)copyFile:(NSString *_Nonnull)sourcePath toDesPath:(NSString *_Nonnull)desPath isOverWrite:(BOOL)flag
{
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    NSError *error;
    if ([self isExist:desPath])
    {
        if (![self removeFile:desPath])
        {
            return NO;
        }
    }
    
    [fileManager copyItemAtPath:sourcePath toPath:desPath error:&error];
    
    if (error)
    {
        return NO;
    }
    
    return YES;
}

#pragma mark - 私有方法
+ (BOOL)createFile:(NSString *_Nonnull)path contents:(NSData*)data
{
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    
    BOOL isDir;
    BOOL isExist = [fileManager fileExistsAtPath:path isDirectory:&isDir];
    if (isExist)
    {
        if (isDir)
        {
            return NO;
        }
        else
        {
            return YES;
        }
    }
    
    BOOL res = [fileManager createFileAtPath:path contents:data attributes:nil];
    return res;
}

#pragma mark - iCloud不备份方法
/**
 *  把目录设置为iCloud不自动备份(do not back up)
 *  https://developer.apple.com/library/ios/qa/qa1719/_index.html
 *
 *  支付iOS5.0.1及以上版本的SDK
 *
 *  @param URL 路径
 *
 *  @return 是否设置成功
 */
+ (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    if (!URL) return NO;
    
    NSString *systemVersion = [UIDevice currentDevice].systemVersion;
    
    if ([systemVersion floatValue]>=5.1)
    {
        NSError *error = nil;
        BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                      forKey: NSURLIsExcludedFromBackupKey error: &error];
        if(!success)
        {
            NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
        }
        return success;
    }
    else if ([systemVersion isEqualToString:@"5.0.1"])
    {
        
        const char* filePath = [[URL path] fileSystemRepresentation];
        
        const char* attrName = "com.apple.MobileBackup";
        u_int8_t attrValue = 1;
        
        int result = setxattr(filePath, attrName, &attrValue, sizeof(attrValue), 0, 0);
        return result == 0;
    }
    return YES;
}

#pragma mark 获取文件的创建时间
+(NSString * _Nullable)getFileCreateTimeWithPath:(NSString* _Nonnull)filePath
{
    NSDictionary* attr = [[NSFileManager defaultManager] attributesOfItemAtPath:filePath error:nil];
    NSDate *fileCreateDate = [attr objectForKey:NSFileCreationDate];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSString *documentCreateTimeString = [dateFormatter stringFromDate:fileCreateDate];
    return documentCreateTimeString;
}

#pragma mark 获取文件大小单位是KB
+ (long long)fileSizeAtPath:(NSString*)filePath
{
    NSFileManager* manager = [NSFileManager defaultManager];
    if ([manager fileExistsAtPath:filePath])
    {
        return [[manager attributesOfItemAtPath:filePath error:nil] fileSize] / 1024;
    }
    return 0;
}

#pragma mark 获取路径文件夹下所有文件及文件夹，包括子文件夹下的文件及文件夹
+ (NSArray *)getAllFileWithPath:(NSString *_Nonnull)filePath
{
    if (!filePath || filePath.length == 0) return nil;
    
    NSFileManager *mgr = [NSFileManager defaultManager];
    return [mgr subpathsAtPath:filePath];
}

#pragma mark 获取路径文件夹下所有文件，不包括文件夹及子文件夹下的文件(夹)
+ (NSArray *)getFilesWithPath:(NSString *_Nonnull)filePath
{
    if (!filePath || filePath.length == 0) return nil;
    
    NSArray *array            = [self getAllFileWithPath:filePath];
    NSMutableArray *pathArray = [NSMutableArray arrayWithCapacity:1];
    for (id path in array)
    {
        NSString *namePath = NSStringFromClass([path class]);
        if (![namePath isEqualToString:@"NSTaggedPointerString"] && ![namePath isEqualToString:@"NSPathStore2"] )
        {
            [pathArray addObject:path];
        }
    }
    return pathArray;
}

+ (NSData *)getDataWithPath:(NSString *_Nonnull)filePath
{
    if (!filePath || filePath.length == 0) return nil;
    NSFileManager *mgr = [[NSFileManager alloc] init];
    NSData *data = [mgr contentsAtPath:filePath];
    return data;
}

+ (void)createFileIfNonexisting:(NSString *)filePath attributes:(nullable NSDictionary *)attributes {
    NSFileManager *fm = [NSFileManager defaultManager];
    BOOL isExisted;
    isExisted = [fm fileExistsAtPath:filePath];
    if (isExisted) {
        return;
    }
    NSDictionary *attrs = attributes;
    if (!attrs) {
        attrs = @{ NSFilePosixPermissions : @(0666) };
    }
    [fm createFileAtPath:filePath contents:nil attributes:attrs];
}

+ (void)createAndTruncateExistingFile:(NSString *)filePath attributes:(nullable NSDictionary *)attributes {
    NSFileManager *fm = [NSFileManager defaultManager];
    BOOL isExisted;
    isExisted = [fm fileExistsAtPath:filePath];
    if (isExisted) {
        [fm removeItemAtPath:filePath error:nil];
    }
    NSDictionary *attrs = attributes;
    if (!attrs) {
        attrs = @{ NSFilePosixPermissions : @(0666) };
    }
    [fm createFileAtPath:filePath contents:nil attributes:attrs];
}

+ (void)appendToFileWithString:(NSString *)filePath text:(NSString *)text {
    NSFileHandle *fileHandle = [NSFileHandle fileHandleForUpdatingAtPath:filePath];
    [fileHandle seekToEndOfFile];
    [fileHandle writeData:[text dataUsingEncoding:NSUTF8StringEncoding]];
}

@end
