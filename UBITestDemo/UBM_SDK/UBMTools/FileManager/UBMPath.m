//
//  UBMPath.m
//  LoveCarChannel_iOS
//
//  Created by 马翔 on 2020/12/9.
//  Copyright © 2020 魏振伟. All rights reserved.
//

#import "UBMPath.h"

@implementation UBMPath

+ (NSString * _Nonnull)fullPathStorageRoot
{
    NSArray  *libPaths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *hPath = [libPaths objectAtIndex:0];
    return [hPath stringByDeletingLastPathComponent];
}

+ (NSString * _Nullable)getFullPathFromBundle:(NSString* _Nonnull)bundle
                                         file:(NSString* _Nonnull)name
                                       ofType:(NSString* _Nullable)type
{
    NSBundle *b = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:bundle ofType:@"bundle"]];
    if (b) {
        return [b pathForResource:name ofType:type];
    }
    
    return nil;
}

+ (NSString * _Nullable)getFullPathFromBundle:(NSString* _Nonnull)bundle
                                      subpath:(NSString* _Nonnull)subpath
{
    NSString *path = [[NSBundle mainBundle] pathForResource:bundle ofType:@"bundle"];
    if (path) {
        return [path stringByAppendingPathComponent:subpath];
    }
    
    return nil;
}

+ (NSString * _Nonnull)fullPathForCache:(NSString* _Nullable)subPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString* dir = [paths objectAtIndex:0];
    
    
    if (subPath){
        dir = [dir stringByAppendingPathComponent:subPath];
    }
    
    return dir;
}

+ (NSString *)getFullPathByTemporaryDirectoryComponent:(NSString *)fileName {
    NSString *dir = NSTemporaryDirectory();
    NSString *filePath = [dir stringByAppendingPathComponent:fileName];
    return filePath;
}

@end
