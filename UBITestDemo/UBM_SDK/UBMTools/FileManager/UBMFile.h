//
//  UBMFile.h
//  LoveCarChannel_iOS
//
//  Created by 马翔 on 2020/12/9.
//  Copyright © 2020 魏振伟. All rights reserved.
//

/*
  关于文件操作的基础封装
 */

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UBMFile : NSObject

/**
 *  确保子路径在 Library 目录中存在，如果不存则创建它。
 *
 *  @param subPath 子路径
 *  @param skip 当创建子路径时，指示其是否跳过 iCloud 自动备份
 *
 *  @return 如果路径存在或创建成功返回完成路径，否则返回 nil。
 */
+ (NSString * _Nullable)ensureDirectoryInLibrary:(NSString * _Nonnull)subPath skipBackup:(BOOL)skip;

/**
 *  确保子路径在 Document 目录中存在，如果不存则创建它。
 *
 *  @param subPath 子路径
 *  @param skip 当创建子路径时，指示其是否跳过 iCloud 自动备份
 *
 *  @return 如果路径存在或创建成功返回完成路径，否则返回 nil。
 */
+ (NSString * _Nullable)ensureDirectoryInDocument:(NSString * _Nonnull)subPath skipBackup:(BOOL)skip;

/**
 *  确保子路径在 Cache 目录中存在，如果不存则创建它。
 *
 *  @param subPath 子路径
 *
 *  @return 如果路径存在或创建成功返回完成路径，否则返回 nil。
 */
+ (NSString * _Nullable)ensureDirectoryInCache:(NSString * _Nonnull)subPath;

/**
 *  确保某一个目录存在，如果不存在则尝试创建这个目录。
 *
 *  @param path 相对于应用存储目录的根路径，这个根路径可以用 [BXUbmPath fullPathStorageRoot] 方法获取。
 *
 *  @return 如果路径存在或创建成功返回完成路径，否则返回 nil。
 */
+(NSString * _Nullable)ensureDirectoryInRoot:(NSString* _Nonnull)path;

/**
 *  确保某一个目录存在，如果不存在则尝试创建这个目录。
 *
 *  @param path 目录的全路径
 *
 *  @return 如果目录存在 1，如果创建成功返回 2，如果创建失败则返回 0。
 */
+(int)ensureDirectory:(NSString* _Nonnull)path;

/**
 *  判断一个文件或路径是否存在。
 *
 *  @param fullPath 等检测的路径
 *
 *  @return 是否存在
 */
+ (BOOL)isExist:(NSString * _Nonnull)fullPath;

/**
 *  写入文件
 *  如果文件不存在则创建并写入数据，如果已存在则覆盖；
 *  @param path 文件路径
 *  @param text 写入内容(UT8 编码保存)
 *
 *  @return 写入文件结果，写入文件失败或文件目录不存在则返回 NO。
 */
+ (BOOL)writeFile:(NSString * _Nonnull)path text:(NSString * _Nonnull)text;

/**
 *  写入文件
 *  如果文件不存在则创建并写入数据，如果已存在则覆盖；
 *
 *  @param path 文件路径
 *  @param data 数据(NSData)
 *
 *  @return 写入文件结果，写入文件失败或文件目录不存在则返回 NO
 */
+ (BOOL)writeFile:(NSString *_Nonnull)path data:(NSData * _Nonnull)data;

/**
 *  追加写入文件
 *  文件必须存在，并将数据追加到文件最后位置；
 *
 *  @param path 文件路径
 *  @param data 数据(NSData)
 *
 *  @return 写入文件结果，写入文件失败则返回 NO
 */
+ (BOOL)writeFileAppend:(NSString *_Nonnull)path data:(NSData * _Nonnull)data;

/**
 *  写入文件
 *  如果文件不存在则创建并写入数据，如果已存在则覆盖；
 *
 *  @param path 文件夹路径
 *  @param fileName 文件名称(可以是带后辍文件或无后辍文件, 例如：HomeFile或 Home.java)
 *  @param data 数据(NSData)
 *
 *  @return 写入文件结果，写入文件失败则返回 NO
 */
+ (BOOL)writeFileWithName:(NSString *_Nonnull)path
                 fileName:(NSString *_Nonnull)fileName
                     data:(NSData * _Nonnull)data;

/**
 *  写入文件
 *  如果文件不存在则创建并写入数据，如果已存在则覆盖；
 *
 *  @param path 文件夹路径
 *  例如：Home.java
 *  @param fileName 文件名称(Home)
 *  @param fileType 文件后辍(java)
 *  @param data 数据(NSData)
 *
 *  @return 写入文件结果，写入文件失败则返回 NO
 */
+ (BOOL)writeFileWithName:(NSString *_Nonnull)path
                 fileName:(NSString *_Nonnull)fileName
                 fileType:(NSString *_Nonnull)fileType
                     data:(NSData * _Nonnull)data;

/**
 *  写入文件
 *
 *  @param path 文件夹路径
 *  例如：Home.java
 *  @param fileName 文件名称(可以是带后辍文件或无后辍文件, 例如：HomeFile或 Home.java)
 *  @param data 数据(NSData)
 *  @param isAppend YES: 向文件末尾追加文件 NO: 如果文件不存在则创建并写入数据，如果已存在则覆盖；
 *  @return 写入文件结果，写入文件失败则返回 NO
 */
+ (BOOL)writeFileWithNameByAppend:(NSString *_Nonnull)path
                 fileName:(NSString *_Nonnull)fileName
                     data:(NSData * _Nonnull)data
                 isAppend:(BOOL)isAppend;

/**
 *  写入文件
 *
 *  @param path 文件夹路径
 *  例如：Home.java
 *  @param fileName 文件名称(Home)
 *  @param fileType 文件后辍(java)
 *  @param data 数据(NSData)
 *  @param isAppend YES: 向文件末尾追加文件 NO: 如果文件不存在则创建并写入数据，如果已存在则覆盖；
 *  @return 写入文件结果，写入文件失败则返回 NO
 */
+ (BOOL)writeFileWithNameByAppend:(NSString *_Nonnull)path
                 fileName:(NSString *_Nonnull)fileName
                 fileType:(NSString *_Nonnull)fileType
                     data:(NSData * _Nonnull)data
                 isAppend:(BOOL)isAppend;
/**
 *  写入文件
 *  @param path 文件路径
 *  @param data 数据(NSData)
 *  @param isAppend YES: 向文件末尾追加文件 NO: 如果文件不存在则创建并写入数据，如果已存在则覆盖；
 *  @return 写入文件结果，写入文件失败则返回 NO
 */

+ (BOOL)writeFileByAppend:(NSString *_Nonnull)path
                     data:(NSData * _Nonnull)data
                 isAppend:(BOOL)isAppend;

/**
 *  删除文件
 *
 *  @param path 文件路径
 *
 *  @return 删除结果
 */
+ (BOOL)removeFile:(NSString * _Nonnull)path;

/**
 *  创建文件
 *  如果文件不存在则创建，如果已存在则直接返回 YES；
 *  @param path 文件名称(含路径)
 *
 *  @return 创建结果，创建文件失败时返回 NO。
 */
+ (BOOL)createFile:(NSString *_Nonnull)path;

/**
 *  复制文件
 *
 *  @param sourcePath   源文件路径
 *  @param desPath      目标文件路径
 *
 *  @return 复制结果
 */
+ (BOOL)copyFile:(NSString *_Nonnull)sourcePath toDesPath:(NSString *_Nonnull)desPath isOverWrite:(BOOL)flag;

/**
 获取文件的创建时间
 
 @param filePath 文件路径
 @return 字符串
 */
+(NSString * _Nullable)getFileCreateTimeWithPath:(NSString* _Nonnull)filePath;

/**
 获取文件大小单位是KB
 
 @param filePath 文件路径
 @return float
 */
+ (long long)fileSizeAtPath:(NSString *_Nonnull)filePath;

/**
 获取路径文件夹下所有文件及文件夹，包括子文件夹下的文件及文件夹
 
 @param filePath 文件夹路径
 @return 文件路径组
 */
+ (NSArray *)getAllFileWithPath:(NSString *_Nonnull)filePath;

/**
 获取路径文件夹下所有文件，不包括文件夹及子文件夹下的文件(夹)
 
 @param filePath 文件夹路径
 @return 文件路径组
 */
+ (NSArray *)getFilesWithPath:(NSString *_Nonnull)filePath;

/**
 获取路径文件的数据
 
 @param filePath 文件路径
 @return 文件的 NSData
 */
+ (NSData *)getDataWithPath:(NSString *_Nonnull)filePath;

/// 如果文件不存在就创建
/// @param filePath 文件路径
/// @param attributes 文件属性(nil为默认属性, 权限0666) 如:attributes = @{ NSFilePosixPermissions : @(0644) };
+ (void)createFileIfNonexisting:(NSString *)filePath attributes:(nullable NSDictionary *)attributes;

/// 创建并清空文件
/// @param filePath 文件路径
/// @param attributes 文件属性(nil为默认属性, 权限0666) 如:attributes = @{ NSFilePosixPermissions : @(0644) };
+ (void)createAndTruncateExistingFile:(NSString *)filePath attributes:(nullable NSDictionary *)attributes;

/// 在文件中追加字符串
/// @param filePath 文件路径
/// @param text 字符串
+ (void)appendToFileWithString:(NSString *)filePath text:(NSString *)text;

@end

NS_ASSUME_NONNULL_END
