//
//  UBMPath.h
//  LoveCarChannel_iOS
//
//  Created by 马翔 on 2020/12/9.
//  Copyright © 2020 魏振伟. All rights reserved.
//

/*
  关于路径操作的基础封装
 */

#import <Foundation/Foundation.h>
#import <sys/xattr.h>

NS_ASSUME_NONNULL_BEGIN

@interface UBMPath : NSObject

/**
 *  获取应用存储目录的根目录，该目录下即是 Library、Documents 等目录。
 *
 *  @return 完整路径，结尾不含 "/"
 */
+ (NSString * _Nonnull)fullPathStorageRoot;

/**
 *  获取 Bundle 中的文件的全路径
 *
 *  @param bundle Bundle 名字（不含扩展名）
 *  @param name   文件名字（不含扩展名）
 *  @param type   文件扩展名
 *
 *  @return 如果文件不存在则返回 nil
 */
+ (NSString * _Nullable)getFullPathFromBundle:(NSString* _Nonnull)bundle
                                         file:(NSString* _Nonnull)name
                                       ofType:(NSString* _Nullable)type;

/**
 *  获取 Bundle 中的文件的全路径，此方法并不检测目标文件或路径是否存在
 *
 *  @param bundle  Bundle 名字（不含扩展名）
 *  @param subpath 文件子路径（含扩展名）
 *
 *  @return 如果文件不存在则返回 nil
 */
+ (NSString * _Nullable)getFullPathFromBundle:(NSString* _Nonnull)bundle
                                      subpath:(NSString* _Nonnull)subpath;

/**
 *  获取主 Cache 目录，此方法并不检测目标文件或路径是否存在
 *
 *  @param subPath 子目录，如果传 nil，则返回 Cache 根目录。
 *
 *  @return 完整路径
 */
+ (NSString * _Nonnull)fullPathForCache:(NSString* _Nullable)subPath;


/// 获取temp目录下的文件路径
/// @param fileName 文件名
+ (NSString *)getFullPathByTemporaryDirectoryComponent:(NSString *)fileName;

@end

NS_ASSUME_NONNULL_END
