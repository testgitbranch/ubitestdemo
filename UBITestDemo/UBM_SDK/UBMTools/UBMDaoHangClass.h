//
//  UBMDaoHangClass.h
//  YO
//
//  Created by SuperMan on 2018/12/14.
//  Copyright © 2018年 DeiDaiHuLianCompany. All rights reserved.
//

/*
 检测第三方地图app，并导航
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UBMDaoHangClass : NSObject

//endLocation:目的地。@“lat”经, @"long"纬, @"name"目的地名字； PresentVC：当前的viewcontroller
-(void)doNavigationWithEndLocation:(NSDictionary *)endLocation andVC:(UIViewController*)PresentVC;

@end
