//
//  UBMViewTools.m
//  LoveCarChannel_iOS
//
//  Created by wzw on 2020/7/15.
//  Copyright © 2020 魏振伟. All rights reserved.
//

#import "UBMViewTools.h"
#import "UBMPrefixHeader.h"

@implementation UBMViewTools

+ (void)addCornerToview:(CALayer *)layer andWidth:(float)width andPosition:(UBMBorderPosition)position{

    UIBezierPath *maskPath;

    if(position == ubmPTop){
        maskPath= [UIBezierPath bezierPathWithRoundedRect:layer.bounds byRoundingCorners:UIRectCornerTopLeft| UIRectCornerTopRight cornerRadii:CGSizeMake(width,width)];
    }else if (position == ubmPBottom){
        maskPath= [UIBezierPath bezierPathWithRoundedRect:layer.bounds byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii:CGSizeMake(width,width)];
    }else if (position == ubmPLeft){
        maskPath= [UIBezierPath bezierPathWithRoundedRect:layer.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerBottomLeft cornerRadii:CGSizeMake(width,width)];
    }else if (position == ubmPRight){
        maskPath= [UIBezierPath bezierPathWithRoundedRect:layer.bounds byRoundingCorners:UIRectCornerTopRight | UIRectCornerBottomRight cornerRadii:CGSizeMake(width,width)];
    }else if (position == ubmPTopLeft){
        maskPath= [UIBezierPath bezierPathWithRoundedRect:layer.bounds byRoundingCorners:UIRectCornerTopLeft cornerRadii:CGSizeMake(width,width)];
    }else if (position == ubmPTopRight){
        maskPath= [UIBezierPath bezierPathWithRoundedRect:layer.bounds byRoundingCorners: UIRectCornerTopRight cornerRadii:CGSizeMake(width,width)];
    }else if (position == ubmPBottomLeft){
        maskPath= [UIBezierPath bezierPathWithRoundedRect:layer.bounds byRoundingCorners:UIRectCornerBottomLeft cornerRadii:CGSizeMake(width,width)];
    }else if (position == ubmPBottomRight){
        maskPath= [UIBezierPath bezierPathWithRoundedRect:layer.bounds byRoundingCorners:UIRectCornerBottomRight cornerRadii:CGSizeMake(width,width)];
    }

    CAShapeLayer*maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = layer.bounds;
    maskLayer.path = maskPath.CGPath;
    layer.mask = maskLayer;
}

+(UILabel *)lableWithText:(id)tTitile font:(UIFont*)tFont textColor:(UIColor *)tTextColor{
    UILabel* lab = [[UILabel alloc] init];
    lab.text = [NSString stringWithFormat:@"%@", tTitile];
    lab.textColor = tTextColor;
    lab.font = tFont;
    return lab;
}

+(UIButton *)buttonRimg:(NSString *)title Color:(UIColor *)co font:(int)fo Img:(NSString *)iname ImgSize:(CGSize)dsize{

    UIButton* bton = [UIButton buttonWithType:UIButtonTypeCustom];
    bton.backgroundColor = ColorSet(255, 255, 255, 0);
    [bton setTitle:title forState:UIControlStateNormal];
    [bton setTitleColor:co forState:UIControlStateNormal];
    bton.titleLabel.font = [UIFont systemFontOfSize:fo];
    [bton setImage:[UIImage imageNamed:iname] forState:UIControlStateNormal];
    if(dsize.width>0){
        bton.imageView.width = dsize.width;
        bton.imageView.height = dsize.height;
    }
    //左图右文
    bton.semanticContentAttribute = UISemanticContentAttributeForceRightToLeft;

    return bton;
}

+(UIButton *)buttonBimgframe:(CGRect)rect title:(NSString *)title Color:(UIColor *)co font:(int)fo Img:(NSString *)iname ImgSize:(CGSize)dsize distance:(float)distance{
    UIButton* but = [UIButton buttonWithType:UIButtonTypeCustom];
    but.frame = rect;
    but.backgroundColor = ColorSet(255, 255, 255, 0);
    [but setTitle:title forState:UIControlStateNormal];
    [but setTitleColor:co forState:UIControlStateNormal];
    but.titleLabel.font = [UIFont systemFontOfSize:fo];
    [but setImage:[UIImage imageNamed:iname] forState:UIControlStateNormal];
    if(dsize.width>0){
        but.imageView.width = dsize.width;
        but.imageView.height = dsize.height;
    }
    //上图下文
    [but setTitleEdgeInsets:
           UIEdgeInsetsMake(but.frame.size.height/2,
                           (but.frame.size.width-but.titleLabel.intrinsicContentSize.width)/2-but.imageView.frame.size.width,
                            0,
                           (but.frame.size.width-but.titleLabel.intrinsicContentSize.width)/2)];
    [but setImageEdgeInsets:
               UIEdgeInsetsMake(
                           distance,
                           (but.frame.size.width-but.imageView.frame.size.width)/2,
                            but.titleLabel.intrinsicContentSize.height,
                           (but.frame.size.width-but.imageView.frame.size.width)/2)];

    return but;
}

///**
// *改变字符串中具体某字符串的颜色
// */
//+ (void)messageAction:(UILabel *)theLab changeString:(NSString *)change andAllColor:(UIColor *)allColor andMarkColor:(UIColor *)markColor andMarkFondSize:(float)fontSize {
//    NSString *tempStr = theLab.text;
//    NSMutableAttributedString *strAtt = [[NSMutableAttributedString alloc] initWithString:tempStr];
//    [strAtt addAttribute:NSForegroundColorAttributeName value:allColor range:NSMakeRange(0, [strAtt length])];
//    NSRange markRange = [tempStr rangeOfString:change];
//    [strAtt addAttribute:NSForegroundColorAttributeName value:markColor range:markRange];
//    [strAtt addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:fontSize] range:markRange];
//    theLab.attributedText = strAtt;
//}
//
+ (void)markerLable:(UILabel *)theLab changeString:(NSString *)change andAllColor:(UIColor *)allColor andMarkColor:(UIColor *)markColor andMarkFondSize:(UIFont*)font{
    NSString *tempStr = theLab.text;

    //防止因NSMutableAttributedString的叠加，导致之前的行间距等效果被去掉。
    NSMutableAttributedString *strAtt = [[NSMutableAttributedString alloc] initWithAttributedString:theLab.attributedText];
    if (strAtt ==nil || strAtt.length == 0) {
        strAtt = [[NSMutableAttributedString alloc] initWithString:tempStr];
    }

    [strAtt addAttribute:NSForegroundColorAttributeName value:allColor range:NSMakeRange(0, [strAtt length])];
    NSRange markRange = [tempStr rangeOfString:change];
    [strAtt addAttribute:NSForegroundColorAttributeName value:markColor range:markRange];
    [strAtt addAttribute:NSFontAttributeName value:font range:markRange];
    theLab.attributedText = strAtt;
}

+ (void)markerLableOverlay:(UILabel *)theLab ofString:(NSString *)change markColor:(UIColor *)markColor markFondSize:(UIFont*)font{
    NSString *tempStr = theLab.text;

    //防止因NSMutableAttributedString的叠加，导致之前的行间距等效果被去掉。
    NSMutableAttributedString *strAtt = [[NSMutableAttributedString alloc] initWithAttributedString:theLab.attributedText];
    if (strAtt ==nil || strAtt.length == 0) {
        strAtt = [[NSMutableAttributedString alloc] initWithString:tempStr];
    }

    NSRange markRange = [tempStr rangeOfString:change];
    [strAtt addAttribute:NSForegroundColorAttributeName value:markColor range:markRange];
    [strAtt addAttribute:NSFontAttributeName value:font range:markRange];
    theLab.attributedText = strAtt;
}

+ (void)markerLableOverlayCenter:(UILabel *)theLab ofString:(NSString *)change markColor:(UIColor *)markColor markFond:(UIFont*)font defultFontSize:(int)defultSize markFondSize:(int)markSize{
    NSString *tempStr = theLab.text;

    //防止因NSMutableAttributedString的叠加，导致之前的行间距等效果被去掉。
    NSMutableAttributedString *strAtt = [[NSMutableAttributedString alloc] initWithAttributedString:theLab.attributedText];
    if (strAtt ==nil || strAtt.length == 0) {
        strAtt = [[NSMutableAttributedString alloc] initWithString:tempStr];
    }

    NSRange markRange = [tempStr rangeOfString:change];
    [strAtt addAttribute:NSForegroundColorAttributeName value:markColor range:markRange];
    [strAtt addAttribute:NSFontAttributeName value:font range:markRange];
    [strAtt addAttribute:NSBaselineOffsetAttributeName value:@(0.36 * (defultSize-markSize)) range:markRange];

    theLab.attributedText = strAtt;
}

/**
 *判断字符串是否不全为空
 */
+ (BOOL)judgeStringIsNull:(NSString *)string {
    if ([[string class] isSubclassOfClass:[NSNumber class]]) {
        return YES;
    }
    BOOL result = NO;
    if (string != nil && string.length > 0) {
        for (int i = 0; i < string.length; i ++) {
            NSString *subStr = [string substringWithRange:NSMakeRange(i, 1)];
            if (![subStr isEqualToString:@" "] && ![subStr isEqualToString:@""]) {
                result = YES;
            }
        }
    }
    return result;
}


#pragma mark - 设置行间距
+(void)setLabelSpace:(UILabel*)label withSpace:(CGFloat)space withFont:(UIFont*)font  {
    
        NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
        paraStyle.lineBreakMode = NSLineBreakByCharWrapping;
        paraStyle.alignment = NSTextAlignmentLeft;
        paraStyle.lineSpacing = space; //设置行间距
        paraStyle.hyphenationFactor = 1.0;
        paraStyle.firstLineHeadIndent = 0.0;
        paraStyle.paragraphSpacingBefore = 0.0;
        paraStyle.headIndent = 0;
        paraStyle.tailIndent = 0;
        //设置字间距 NSKernAttributeName:@1.5f
        NSDictionary *dic = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:paraStyle, NSKernAttributeName:@0.0f
                              };
        NSAttributedString *attributeStr = [[NSAttributedString alloc] initWithString:label.text attributes:dic];
        label.attributedText = attributeStr;
}

//#pragma mark - lable高度自适应
//+(float)Line:(UILabel *)label andLineSpace:(CGFloat)line
//{
//    if(label.text != nil)
//    {
//        //可变属性字符串
//        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:label.text];
//        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
//        [paragraphStyle setLineSpacing:line];//调整行间距
//        
//        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [label.text length])];
//        label.attributedText = attributedString;
//        
//        CGSize tmpsize = [label sizeThatFits:CGSizeMake(label.width, MAXFLOAT)];
////        label.width = tmpsize.width;
////        label.height = tmpsize.height;
//        return tmpsize.height;
//    }
//    else
//        return 0.0;
//    
//}
//
+(float)getHeightLable:(UILabel *)wlab withWidth:(float)width{
    
    CGSize baseSize = CGSizeMake(width, CGFLOAT_MAX);
    CGSize labelsize = [wlab sizeThatFits:baseSize];
    return labelsize.height;
    
}

//根据高度度求宽度  text 计算的内容  Height 计算的高度 font字体大小 (by fontSize)
//+ (CGFloat)getWidthWithText:(NSString *)text height:(CGFloat)height fontSize:(CGFloat)size{
//
//    CGRect rect = [text boundingRectWithSize:CGSizeMake(MAXFLOAT, height)
//                                        options:NSStringDrawingUsesLineFragmentOrigin
//                                     attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:size]}
//                                        context:nil];
//    return rect.size.width;
//}

//根据高度度求宽度  text 计算的内容  Height 计算的高度 font字体大小 (by font)
//+ (CGFloat)getWidthWithText:(NSString *)text height:(CGFloat)height font:(UIFont *)font{
//
//    CGRect rect = [text boundingRectWithSize:CGSizeMake(MAXFLOAT, height)
//                                        options:NSStringDrawingUsesLineFragmentOrigin
//                                     attributes:@{NSFontAttributeName:font}
//                                        context:nil];
//    return rect.size.width;
//}
//
//+ (UIImage *)imageWithColor:(UIColor *)color
//{
//   CGRect rect=CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
//   UIGraphicsBeginImageContext(rect.size);
//   CGContextRef context=UIGraphicsGetCurrentContext();
//   CGContextSetFillColorWithColor(context, [color CGColor]);
//   CGContextFillRect(context, rect);
//
//   UIImage *image=UIGraphicsGetImageFromCurrentImageContext();
//   UIGraphicsEndImageContext();
//   return image;
//}



//+ (void)setAnchorPoint:(CGPoint)anchorPoint forView:(UIView *)view
//{
//    CGPoint oldOrigin = view.frame.origin;
//    view.layer.anchorPoint = anchorPoint;
//    CGPoint newOrigin = view.frame.origin;
//
//    CGPoint transition;
//    transition.x = newOrigin.x - oldOrigin.x;
//    transition.y = newOrigin.y - oldOrigin.y;
//
//    view.center = CGPointMake (view.center.x - transition.x, view.center.y - transition.y);
//}

//+ (void)loadHtmlTOLabel:(UILabel*)lab andHtmlStr:(NSString*)htmlStr {
//
//    if (htmlStr && htmlStr.length==0) {
//        lab.text = @"";
//        return;;
//    }
//
//    //富文本
//    NSDictionary *options = @{ NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute :@(NSUTF8StringEncoding) };
//    NSData *data = [htmlStr dataUsingEncoding:NSUTF8StringEncoding];
//
//    //设置富文本
//    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithData:data options:options documentAttributes:nil error:nil];
//    //设置段落格式
//    NSMutableParagraphStyle *para = [[NSMutableParagraphStyle alloc] init];
////    para.lineSpacing = 6;
//    para.paragraphSpacing = 0;
//    para.paragraphSpacingBefore = 0;
//    para.headIndent = 0;
//    [attStr addAttribute:NSParagraphStyleAttributeName value:para range:NSMakeRange(0, attStr.length)];
//
//    //设置文本的Font没有效果，默认12字号，这个只能服务器端控制吗？ 但是却能改变字重。是个问题
////    [attStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14 weight:UIFontWeightRegular] range:NSMakeRange(0, attStr.length)];
//
//    lab.attributedText = attStr;
//
//    //计算加载完成之后Label的frame
//    CGSize size = [lab sizeThatFits:CGSizeMake(lab.width, MAXFLOAT)];
//    NSLog(@"333, %f", size.height);
//    lab.frame = CGRectMake(lab.x, lab.y, lab.width, size.height);
//
//}
//
//
//#pragma mark - 添加内阴影
//+(void)addInShadow:(UIView*)viewx{
//    CAShapeLayer* shadowLayer = [CAShapeLayer layer];
//    [shadowLayer setFrame:viewx.bounds];
//
//    // Standard shadow stuff
//    [shadowLayer setShadowColor:[[UIColor colorWithWhite:0 alpha:1] CGColor]];
//    [shadowLayer setShadowOffset:CGSizeMake(0.0f, 0.0f)];
//    [shadowLayer setShadowOpacity:1.0f];
//    [shadowLayer setShadowRadius:4];
//
//    // Causes the inner region in this example to NOT be filled.
//    [shadowLayer setFillRule:kCAFillRuleEvenOdd];
//
//    // Create the larger rectangle path.
//    CGMutablePathRef path = CGPathCreateMutable();
//    CGPathAddRect(path, NULL, CGRectInset(viewx.bounds, -42, -42));
//
//    // Add the inner path so it's subtracted from the outer path.
//    // someInnerPath could be a simple bounds rect, or maybe
//    // a rounded one for some extra fanciness.
//    CGPathRef someInnerPath = [UIBezierPath bezierPathWithRoundedRect:viewx.bounds cornerRadius:viewx.layer.cornerRadius].CGPath;
//    CGPathAddPath(path, NULL, someInnerPath);
//    CGPathCloseSubpath(path);
//
//    [shadowLayer setPath:path];
//    CGPathRelease(path);
//
//    CAShapeLayer* maskLayer = [CAShapeLayer layer];
//    [maskLayer setPath:someInnerPath];
//    [shadowLayer setMask:maskLayer];
//
//    [viewx.layer addSublayer:shadowLayer];
//}
//
//
#pragma mark 生成image
+ (UIImage *)getmakeImageWithView:(UIView *)view andWithSize:(CGSize)size
{
    // 下面方法，第一个参数表示区域大小。第二个参数表示是否是非透明的。如果需要显示半透明效果，需要传NO，否则传YES。第三个参数就是屏幕密度了，关键就是第三个参数 [UIScreen mainScreen].scale。
    UIGraphicsBeginImageContextWithOptions(size, NO, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;

}

#pragma mark 添加渐变色背景。direction：1：上-下 2 左-右
+(void )addGradualChangeBckColor:(UIColor*)startColor :(UIColor*)endColor :(int)direction to:(UIView*)view{

    CAGradientLayer *gl = [CAGradientLayer layer];
    gl.frame = view.bounds;
    if (direction == 1) {
        gl.startPoint = CGPointMake(0.5, 0);
        gl.endPoint = CGPointMake(0.5, 1);
    }else{
        gl.startPoint = CGPointMake(0, 0.5);
        gl.endPoint = CGPointMake(1, 0.5);
    }

    gl.colors = @[(__bridge id)startColor.CGColor, (__bridge id)endColor.CGColor];

    gl.locations = @[@(0), @(1.0f)];

    [view.layer insertSublayer:gl atIndex:0];
}


+ (UIImage *)scaleToSize:(UIImage *)img size:(CGSize)newsize{

    //防止图片失真
    UIGraphicsBeginImageContextWithOptions(newsize, NO, [[UIScreen mainScreen] scale]);
    // 绘制改变大小的图片
    [img drawInRect:CGRectMake(0, 0, newsize.width, newsize.height)];
    // 从当前context中创建一个改变大小后的图片
    UIImage * scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    // 使当前的context出堆栈
    UIGraphicsEndImageContext();
    // 返回新的改变大小后的图片
    return scaledImage;
}

#pragma mark - 判断view是否全都显示在屏幕上
+ (BOOL)isDisplayedInSceenWithView:(UIView *)view{
    if (view == nil) {
        return NO;
    }
    CGRect screenRect = [UIScreen mainScreen].bounds;
    UIWindow * window = [UBMMTools currentWindow];
    CGRect rect = [view convertRect:view.bounds toView:window];
    if (view.hidden) {
        return NO;
    }
    if (CGRectIsEmpty(rect) || CGRectIsNull(rect)) {
        return NO;
    }
    CGRect intersectionRect = CGRectIntersection(rect, screenRect);
    if (CGRectIsEmpty(intersectionRect) || CGRectIsNull(intersectionRect)) {
        return NO;
    }
    if (CGSizeEqualToSize(rect.size, intersectionRect.size)) {
        return YES;
    }
    return NO;
}

@end
