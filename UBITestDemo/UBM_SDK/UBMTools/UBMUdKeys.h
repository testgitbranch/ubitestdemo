//
//  UBMUdKeys.h
//  LoveCarChannel_iOS
//
//  Created by 金鑫 on 2021/6/21.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

/*
 统一定义NSuserDefults 的 key
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

// UBM自动记录开关
UIKIT_EXTERN NSString * const UBMAutoRecordSwitchUDKey;

// 首页TopView中自动记录权限引导设置的时间戳
UIKIT_EXTERN NSString * const UBMHomeTopSettingTimestampUDKey;

// 判断是否打开手机权限
UIKIT_EXTERN NSString * const UBMJugeSavePhoneUDKey;

// Car ID
UIKIT_EXTERN NSString * const UBMCarIdUDKey;

// 客服联系方式
UIKIT_EXTERN NSString * const UBMContactsUDKey;

// 机车id
UIKIT_EXTERN NSString * const UBMCarVidUDKey;

// Vin码
UIKIT_EXTERN NSString * const UBMCarVinUDKey;

// 用户引导
UIKIT_EXTERN NSString * const UBMHadGuidedActivityUDKey;

// 签到
UIKIT_EXTERN NSString * const UBMShouldSiginUDKey;

// 主用户信息
UIKIT_EXTERN NSString * const UBMMainUserInforUDKey;

// 用户Uid
UIKIT_EXTERN NSString * const UBMUserUidUDKey;

// 用户重新设置Uid时，可以结束开始的行程
UIKIT_EXTERN NSString * const UBMUserIdForResettingUDKey;

// web判断是否打开手机权限
UIKIT_EXTERN NSString * const UBMWebJugeSavePhoneUDKey;

// web客服联系方式
UIKIT_EXTERN NSString * const UBMWebContactsUDKey;

// 隐私政策弹窗
UIKIT_EXTERN NSString * const UBMLCCPrivacyPolicyAlertViewShowFlagUDKey;

// 新用户代理
UIKIT_EXTERN NSString * const UBMNewUserAgentUDKey;

// 用户代理
UIKIT_EXTERN NSString * const UBMUserAgentUDKey;

// 权限显示时间戳
UIKIT_EXTERN NSString * const UBMPermissionTimeUDKey;
