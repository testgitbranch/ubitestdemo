//
//  UBMGetStateOfLimit.swift
//  LoveCarChannel_iOS
//
//  Created by wzw on 2021/4/22.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

import UIKit
import AVFoundation
import CoreMotion
import CoreLocation
//import WCustomAlert

@objcMembers class UBMGetStateOfLimit: NSObject{
    
    ///定位权限申请
    let manager = CLLocationManager()
    
}

//MARK: 提示并跳转应用的设置页
extension UBMGetStateOfLimit {
    @objc static func gotoAppSetAlert(alertTitle:String, alertDesc:String) -> Void{
        
        //主线程
        DispatchQueue.main.async() {
            
            let alrtVC = UBMWCustomAlert.init(frame: CGRect.init(origin: CGPoint.zero, size: UIScreen.main.bounds.size), andTitle: alertTitle, andContent: alertDesc, andleftBton: "取消", andRightBton: "去设置")
            alrtVC.leftActionBlock = {
                
            }
            alrtVC.rightActionBlock = {
                let settingUrl = URL(string: UIApplication.openSettingsURLString)
                if UIApplication.shared.canOpenURL(settingUrl!)
                 {
                    UIApplication.shared.openURL(settingUrl!)
                }
            }
            alrtVC.show()
        }

     }
}

//MARK: 获取麦克风权限状态
extension UBMGetStateOfLimit {
    @objc static func getStateOfAudio( stateBlock:@escaping (_ state:Bool, _ firstRequest:Bool)->Void ) -> Void{
         
         let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.audio)
         if authStatus == AVAuthorizationStatus.notDetermined {
             
             //请求权限
             AVCaptureDevice.requestAccess(for: AVMediaType.audio) { (state) in
                 if state == true{
                     stateBlock(true, true)
                 }else{
                     stateBlock(false, true)
                 }
             }
             
         }else if authStatus == AVAuthorizationStatus.denied || authStatus == AVAuthorizationStatus.restricted{
             stateBlock(false, false)
         }else if authStatus == AVAuthorizationStatus.authorized{
             stateBlock(true, false)
         }
     }
    
}

//MARK: 获取摄像头权限状态
extension UBMGetStateOfLimit{
    
   @objc static func getStateOfVideo( stateBlock:@escaping (_ state:Bool, _ firstRequest:Bool)->Void ) -> Void{
        
        let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        if authStatus == AVAuthorizationStatus.notDetermined {
            
            //请求权限
            AVCaptureDevice.requestAccess(for: AVMediaType.video) { (state) in
                if state == true{
                    stateBlock(true, true)
                }else{
                    stateBlock(false, true)
                }
            }
            
        }else if authStatus == AVAuthorizationStatus.denied || authStatus == AVAuthorizationStatus.restricted{
            stateBlock(false, false)
        }else if authStatus == AVAuthorizationStatus.authorized{
            stateBlock(true, false)
        }
    }
}

extension UBMGetStateOfLimit:CLLocationManagerDelegate{
    @objc static func needShowLocationPremission() -> Bool {
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined://未申请过
            return true
        
        default:
            return false
        }
    }
    
    //MARK:获取定位权限状态
    @objc func getLocationPermission()  {
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined://未申请过
            applyLocationPermission()
            break
        case .restricted,.denied://没有被赋予权限
            UBMLog("没有定位权限")
            break
        case .authorizedAlways,.authorizedWhenInUse://有权限
            getHealthServices()
            UBMLog("已经获取了定位权限")
            break
        @unknown default: break
            
        }
    }
    
    //MARK:申请定位权限
    @objc func applyLocationPermission()  {
        manager.requestAlwaysAuthorization()
        manager.delegate = self
    }
    
    //MARK:定位权限修改回调
    @objc func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        getHealthServices()
    }
    
    @objc static func needShowHealthServicesPremission() -> Bool {
        if #available(iOS 11.0, *) {
            switch CMMotionActivityManager.authorizationStatus() {
            case .notDetermined://未申请过
                return true

            default:
                return false
            }
        } else {
            return false
        }
    }
    
    
    //MARK:获取运动健康权限
    @objc func getHealthServices()  {
        if #available(iOS 11.0, *) {
            switch CMMotionActivityManager.authorizationStatus() {
            case .notDetermined://未申请过
                self.applyHealthPermission()
                UBMLog("可以申请运动健康权限")
                break
            case .restricted,.denied://没有被赋予权限
                
                UBMLog("没有运动健康权限")
                break
            case .authorized://有权限
                
                UBMLog("已经获取了运动健康权限")
                break
            default:
                break
            }
        } else {
            
            // Fallback on earlier versions
        }
        
    }
    
    //MARK:申请运动健康权限
    @objc func applyHealthPermission()  {
        let manager = CMMotionActivityManager()
        let operationQueue = OperationQueue.init()
        manager.startActivityUpdates(to: operationQueue) { (acitvity) in
            manager.stopActivityUpdates()
        }
    }
    
    //MARK:去设置
    @objc func gotoSettings()  {
        let settingUrl = URL(string: UIApplication.openSettingsURLString)
        if UIApplication.shared.canOpenURL(settingUrl!)
         {
            UIApplication.shared.openURL(settingUrl!)
        }
    }
    
}

