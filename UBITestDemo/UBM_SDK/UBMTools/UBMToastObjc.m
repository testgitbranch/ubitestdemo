//
//  UBMToastObjc.m
//  EngineeProject
//
//  Created by Edog on 2017/6/6.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "UBMToastObjc.h"
#import <UIKit/UIKit.h>
#import "UBMPrefixHeader.h"

@implementation UBMToastObjc

+(void)toastWithText:(NSString *)title {
    [self toastWithText:title andtime:3];
}

+(void)toastWithText:(NSString*)title andtime:(int)tim
{
    [self toastWithTitle:title andTime:tim andType:0];
}

+(void)toastFailWithText:(NSString *)title andtime:(int)tim
{
    [self toastWithTitle:title andTime:tim andType:0];

}

+(void)toastSuccessWithText:(NSString*)title andtime:(int)tim
{
    [self toastWithTitle:title andTime:tim andType:1];
}


+(void)toastWithTitle:(NSString*)title andTime:(int)tim andType:(int)type
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        UIWindow * window = [UIApplication sharedApplication].delegate.window;
        
        UIView *showview = [[UIView alloc]init];
        
        if(type == 0)
        showview.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.9];
        else
        showview.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.9];
        
        showview.frame = CGRectMake(1, 1, 1, 1);
        //    showview.alpha = 0.6f;
        showview.layer.cornerRadius = 15.0f;
        showview.layer.masksToBounds = YES;
        
        [window addSubview:showview];
        
        UILabel *label = [[UILabel alloc]init];
        //CGSize LabelSize = [title sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake(290, 9000)];
        CGRect LabelSize = [title boundingRectWithSize:CGSizeMake(290, MAXFLOAT) options:1 attributes:@{NSFontAttributeName: [ UIFont systemFontOfSize:15] }  context:nil];
        label.frame = CGRectMake(15, 20, LabelSize.size.width+10, LabelSize.size.height);
        label.text = title;
        label.textColor = [UIColor whiteColor];
        label.numberOfLines = 0;
        label.textAlignment = NSTextAlignmentCenter;
        label.backgroundColor = [UIColor clearColor];
        label.font = [UIFont systemFontOfSize:15];
        [showview addSubview:label];
        
        showview.frame = CGRectMake((ScreenWidth - LabelSize.size.width - 40)/2, ScreenHeight/2-LabelSize.size.height/2.0-40, LabelSize.size.width+40, LabelSize.size.height+40);
        
        [UIView animateWithDuration:tim animations:^{
            showview.alpha = 0;
        } completion:^(BOOL finished) {
            [showview removeFromSuperview];
        }];
        
    });
    
    
    
}

static UIView * loadingView = nil;

+ (void)toastLoadingWithText:(NSString *)text time:(int)time {
    dispatch_async(dispatch_get_main_queue(), ^{
        
        UIWindow * window = [UIApplication sharedApplication].delegate.window;
        
        UIView *showview = [[UIView alloc]init];
        showview.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.65];
        showview.frame = CGRectMake(1, 1, 1, 1);
        showview.layer.cornerRadius = 6.0f;
        showview.layer.masksToBounds = YES;
        [window addSubview:showview];
        loadingView = showview;
        
        CGRect LabelSize = [text boundingRectWithSize:CGSizeMake(290, MAXFLOAT) options:1 attributes:@{NSFontAttributeName: [ UIFont systemFontOfSize:15] }  context:nil];
        CGFloat _with = LabelSize.size.width + 20 > 150 ? LabelSize.size.width + 20 : 150;
        CGFloat _height = LabelSize.size.height + 40 + 50 > 110 ? LabelSize.size.height + 40 + 50 : 110;
        
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]init];
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
        [showview addSubview:activityIndicator];
        activityIndicator.frame= CGRectMake( _with/2-20, 20, 40, 40);
        activityIndicator.hidesWhenStopped = true;
        [activityIndicator startAnimating];
        
        UILabel *label = [[UILabel alloc]init];
        
        label.frame = CGRectMake(0, 20+50, _with, LabelSize.size.height);
        label.text = text;
        label.textColor = [UIColor whiteColor];
        label.numberOfLines = 0;
        label.textAlignment = NSTextAlignmentCenter;
        label.backgroundColor = [UIColor clearColor];
        label.font = [UIFont systemFontOfSize:15];
        [showview addSubview:label];
        
//        showview.frame = CGRectMake((ScreenWidth - LabelSize.size.width - 40)/2, ScreenHeight/2-LabelSize.size.height/2.0-20 - 20 -5, LabelSize.size.width+40, LabelSize.size.height+40 + 50);
        
        showview.frame = CGRectMake(ScreenWidth/2 - _with/2, ScreenHeight/2 -_height/2, _with, _height);
        
    });
}

+ (void)toastLoadingHiden {
    [loadingView removeFromSuperview];
    loadingView = nil;
}


@end
