//
//  UBMLog.swift
//  UBITestDemo
//
//  Created by 金鑫 on 2021/12/21.
//

import Foundation

import Foundation

#if DEBUG
public let UBMLog = { (items: Any...) in print(items) }
#else
public let UBMLog = { (items: Any...) in }
#endif
