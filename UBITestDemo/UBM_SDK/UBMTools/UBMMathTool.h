//
//  UBMMathTool.h
//  LoveCarChannel_iOS
//
//  Created by 金鑫 on 2021/3/12.
//  Copyright © 2021 魏振伟. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UBMMathTool : NSObject

/// 角度转弧度
+ (CGFloat)radianFromDegree:(CGFloat)degree;

/// 弧度转角度
+ (CGFloat)degreeFromRadian:(CGFloat)radian;

@end

NS_ASSUME_NONNULL_END
