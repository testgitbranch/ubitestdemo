//
//  UBMNotifications.h
//  LoveCarChannel_iOS
//
//  Created by 金鑫 on 2021/6/21.
//  Copyright © 2021 BaiXingKeFu. All rights reserved.
//

/*
 统一定义通知的name
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

///网络状态
UIKIT_EXTERN NSString * const UBMNetNotification;
///下一个弹框
UIKIT_EXTERN NSString * const UBMAlertNextNotification;
///接收行程解析结束后的通知
UIKIT_EXTERN NSString * const UBMNewTripOverNotification;
///活动页面已经显示
UIKIT_EXTERN NSString * const UBMActivityDidApperNotification;
///更新合规信息
UIKIT_EXTERN NSString * const UBMUpdateComplianceStateNotification;
///任务中心更新车辆新通知
UIKIT_EXTERN NSString * const UBMTaskCenterUpdateCarInfoNotification;
///切换车辆
UIKIT_EXTERN NSString * const UBMSwitchVehicleNotification;
///用户点击通知
UIKIT_EXTERN NSString * const UBMGoToNewURLNotification;
///注册监听通过scehem和通用链接进来的APP
UIKIT_EXTERN NSString * const UBMListenShouldLoadURLNameNotification;
///接收首页基础数据请求完成之后的通知
UIKIT_EXTERN NSString * const UBMBaseDataReadyNotification;
///升级提示通知
UIKIT_EXTERN NSString * const UBMUpdateAlertShowNotification;

///getUserInfor用户信息刷新成功时的通知（可以认为是登录成功，uid一定有值）
UIKIT_EXTERN NSString * const UBMGetUserInforSucceedNotification;
///用户退出登录APP时的通知（此时uid已被置成空字符串）
UIKIT_EXTERN NSString * const UBMLoginOutSucceed;

///首页通知子视图是否可以滑动
UIKIT_EXTERN NSString * const UBMChildchangeScrollStatusNotification;

///首页通知父视图是否可以滑动
UIKIT_EXTERN NSString * const UBMFatherchangeScrollStatusNotification;

///首页通知子视图触顶
UIKIT_EXTERN NSString * const UBMChildToTapNotification;








