//
//  AppDelegate.h
//  UBITestDemo
//
//  Created by 金鑫 on 2021/12/14.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) UIWindow *window;

@end

