//
//  AppDelegate.m
//  UBITestDemo
//
//  Created by 金鑫 on 2021/12/14.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    self.window = [[UIWindow alloc] initWithFrame:UIScreen. mainScreen.bounds];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:[[FirstViewController alloc] init]];
    self.window.rootViewController = nav;
    [self.window makeKeyAndVisible];
    return YES;
}

@end
