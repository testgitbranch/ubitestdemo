//
//  UBIDemoConst.m
//  UBITestDemo
//
//  Created by 金鑫 on 2021/12/14.
//

#import "UBIDemoConst.h"

const CGFloat kBtnHeight = 60;
const CGFloat kBtnWidth = 120;
const CGFloat kBtnMargin = 50;
const CGFloat kBtnLineSpace = 80;
