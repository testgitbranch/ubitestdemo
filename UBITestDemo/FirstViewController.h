//
//  FirstViewController.h
//  UBITestDemo
//
//  Created by 金鑫 on 2021/12/14.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

UIKIT_EXTERN NSString * const UBIUserIdUDKey;
UIKIT_EXTERN NSString * const UBIPassportTokenUDKey;

@interface FirstViewController : UIViewController

@end

NS_ASSUME_NONNULL_END
