//
//  FirstViewController.m
//  UBITestDemo
//
//  Created by 金鑫 on 2021/12/14.
//

#import "FirstViewController.h"
#import "SecondViewController.h"
#import "UBMNetWorkToolOfJson.h"
#import "UBIDemoConst.h"

// Domains
static NSString * const ProEnv = @"http://172.17.38.65:8009/"; // 生产

// ########## 修改环境 Domains ##############
static NSString * const CurrEnv = ProEnv;
// ########################################

// Apis
static NSString * const LoginApi = @"auth/ubiLogin";
static NSString * const RenewApi = @"auth/renewUbiToken";

// Users
//static NSString * const CurrUser = @"100001";
static NSString * const CurrUser = @"100002";

// UserDefaults
NSString * const UBIUserIdUDKey = @"UBIUserIdUDKey";
NSString * const UBIPassportTokenUDKey = @"UBIPassportTokenUDKey";

@interface FirstViewController () <UBICredentialProviderDelegate>
@property (nonatomic, assign) NSInteger ubiUserId;
@property (nonatomic, copy) NSString *ubiToken;

@property (nonatomic, strong) UIButton *loginBtn;
@property (nonatomic, strong) SecondViewController *secondVc;
@end

@implementation FirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setup];
}

- (void)setup {
    [self setupUI];
    [self setupLayout];
    [self setupAppearce];
}

- (void)setupUI {
    [self.view addSubview:self.loginBtn];
}

- (void)setupLayout {
    [self.loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.loginBtn.superview);
        make.width.mas_equalTo(kBtnWidth);
        make.height.mas_equalTo(kBtnHeight);
    }];
}

- (void)setupAppearce {
    self.view.backgroundColor = UIColor.whiteColor;
    [self.loginBtn setBackgroundColor:UIColor.orangeColor];
}

- (UIButton *)loginBtn {
    if (!_loginBtn) {
        _loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _loginBtn.frame = CGRectMake(0, 0, 0, 0);
        [_loginBtn setTitle:@"点击登录" forState:UIControlStateNormal];
        [_loginBtn setTitleColor:UIColor.blackColor forState:UIControlStateNormal];
        [_loginBtn setTitleColor:UIColor.grayColor forState:UIControlStateHighlighted];
        _loginBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [_loginBtn addTarget:self action:@selector(login) forControlEvents:UIControlEventTouchUpInside];
    }
    return _loginBtn;
}

- (void)login {
//    [UBMSLSLog addUBMLog:@"haha trip_stopIncorrectTrip" andTripID:@"123"];
    [UBMSLSLog addLog:@"STS_TEST: Demo 6666666666"];
    NSLog(@"login");
    UINavigationController *nav = self.navigationController;
    nav.navigationBar.hidden = YES;
    self.secondVc = [[SecondViewController alloc] init];
    [self requestUidAndToken];
//    [nav pushViewController:self.secondVc animated:YES];
}

- (void)requestUidAndToken {
    // 看userId和token是否有本地缓存
    NSNumber *userId = [NSUserDefaults.standardUserDefaults objectForKey:UBIUserIdUDKey];
    NSString *passportToken = [NSUserDefaults.standardUserDefaults objectForKey:UBIPassportTokenUDKey];
    
    if (userId && passportToken) {
        self.ubiUserId = [userId integerValue];
        self.ubiToken = passportToken;
        [UBITracking.shared enableSDK:[NSString stringWithFormat:@"%ld", self.ubiUserId] provider:[[UBICredentialProvider alloc] initWithPassportToken:self.ubiToken delegate:self]];
        [self.navigationController pushViewController:self.secondVc animated:YES];
        
    } else {
        @bx_weakify(self)
        [[UBMNetWorkToolOfJson shareNetWorkTool] requestWithBaseUrl:CurrEnv UrlString:LoginApi parameters:@{@"credential":CurrUser} method:@"POST" success:^(id response) {
            @bx_strongify(self)
            if (response && [response[@"code"] intValue]==1) {
                self.ubiUserId = [response[@"data"][@"ubiUserId"] integerValue];
                self.ubiToken = response[@"data"][@"ubiToken"];
                
                // save UserId and PassportToken UserDefaults
                [NSUserDefaults.standardUserDefaults setObject:@(self.ubiUserId) forKey:UBIUserIdUDKey];
                [NSUserDefaults.standardUserDefaults setObject:self.ubiToken forKey:UBIPassportTokenUDKey];
                [NSUserDefaults.standardUserDefaults synchronize];
                
                [UBITracking.shared enableSDK:[NSString stringWithFormat:@"%ld", self.ubiUserId] provider:[[UBICredentialProvider alloc] initWithPassportToken:self.ubiToken delegate:self]];
                
                [self.navigationController pushViewController:self.secondVc animated:YES];
            }
            
        } error:^(id response) {
            NSLog(@"response:%@", response);
        } isShowHUD:NO];
    }
}

- (NSString * _Nullable)UBICredentialRefreshPassportToken {
    NSLog(@"RefreshToken 刷新Token delegate");
    __block dispatch_semaphore_t signal = dispatch_semaphore_create(0);
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        // 弱引用
        @bx_weakify(self)
        [[UBMNetWorkToolOfJson shareNetWorkTool] requestWithBaseUrl:CurrEnv UrlString:RenewApi parameters:@{@"credential":CurrUser, @"ubiUserId":@(self.ubiUserId)} method:@"POST" success:^(id response) {
            // 强引用
            @bx_strongify(self)
            if (response && [response[@"code"] intValue]==1) {
                self.ubiUserId = [response[@"data"][@"ubiUserId"] integerValue];
                self.ubiToken = response[@"data"][@"ubiToken"];
                
                // save UserId and PassportToken UserDefaults
                [NSUserDefaults.standardUserDefaults setObject:@(self.ubiUserId) forKey:UBIUserIdUDKey];
                [NSUserDefaults.standardUserDefaults setObject:self.ubiToken forKey:UBIPassportTokenUDKey];
                [NSUserDefaults.standardUserDefaults synchronize];
            }
            dispatch_semaphore_signal(signal);
            NSLog(@"refreshToken delegate break signal");
            
        } error:^(id response) {
            NSLog(@"delegate response:%@", response);
            dispatch_semaphore_signal(signal);
            NSLog(@"refreshToken delegate signal error");
        } isShowHUD:NO];
    });
    
    intptr_t ret = dispatch_semaphore_wait(signal, DISPATCH_TIME_FOREVER);
    if (!ret) { // 没有超时
        NSLog(@"UBICredential 没有超时");
    } else { // 超时
        NSLog(@"UBICredential 超时");
    }
    
    return self.ubiToken;
}

@end
